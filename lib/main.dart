import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rxdart/rxdart.dart';
import 'package:technology/data/local_data_source/local_data_source.dart';
import 'package:technology/domain/entity/dashboard_entity.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';
import 'package:technology/presentation/bloc/addclient/add_client_bloc.dart';
import 'package:technology/presentation/bloc/home_screen/bloc.dart';
import 'package:technology/presentation/commonwidgets/button/custom_button.dart';
import 'package:technology/presentation/commonwidgets/langtile/lang_tile.dart';
import 'package:technology/presentation/screens/homescreen/home_screen.dart';

import 'data/models/getuser/get_user_response.dart';
import 'others/di/injection.dart';
import 'others/styles/app_theme.dart';
import 'others/styles/colors.dart';
import 'others/utils/images/images.dart';
import 'others/utils/routes/router.gr.dart';

UserModel userData;

final languageController = BehaviorSubject<Locale>();

Function(Locale) get changeLocale => languageController.sink.add;

Stream<Locale> get locale => languageController.stream;
UserModel userModel;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  configureDependencies();
  userModel = await getIt<LocalDataSource>().getUserData();
  HttpOverrides.global = MyHttpOverrides();
  runApp(Phoenix(
    child: EasyLocalization(
        supportedLocales: [
          const Locale('en', 'US'),
          const Locale.fromSubtags(languageCode: 'zh'),
//         generic Chinese 'zh'
          const Locale.fromSubtags(languageCode: 'zh', scriptCode: 'Hans'),
          // generic simplified Chinese 'zh_Hans'
          const Locale.fromSubtags(languageCode: 'zh', scriptCode: 'Hant'),
          // generic traditional Chinese 'zh_Hant'
          const Locale.fromSubtags(
              languageCode: 'zh', scriptCode: 'Hans', countryCode: 'CN'),
          // 'zh_Hans_CN'
          const Locale.fromSubtags(
              languageCode: 'zh', scriptCode: 'Hant', countryCode: 'TW'),
          // 'zh_Hant_TW'
          const Locale.fromSubtags(
              languageCode: 'zh', scriptCode: 'Hant', countryCode: 'HK'),
        ],
        path: 'translations',
        // <-- change patch to your
        fallbackLocale: Locale('en', 'US'),
        useOnlyLangCode: true,
        saveLocale: true,
        startLocale: Locale('en', 'US'),
        child: MyApp(isUserLoggedIn: userData != null)),
  ));
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  final bool isUserLoggedIn;

  const MyApp({Key key, this.isUserLoggedIn}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {


  @override
  void didChangeDependencies() {
    locale.listen((event) {
//      EasyLocalization.of(context).locale=event;
    });
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      title: 'Global Asset Technology',
      debugShowCheckedModeBanner: false,
      builder: ExtendedNavigator.builder<MyRouter>(
          router: MyRouter(),
          initialRoute: initialRoute(userModel),
          initialRouteArgs: initalArguments(userModel),
          guards: [AuthGuard()]),
      // ExtendedNavigator is just a widget so you can still wrap it with other widgets
      // if you need to
      theme: ThemeData(
        scaffoldBackgroundColor: Color(0XFFEFF3F6),
        textTheme: appTextTheme,
        primaryColor: AppColors.colorPrimary,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
//      home: Splash(title: 'Flutter Demo Home Page'),
    );
  }

  initialRoute(UserModel userModel) {
    if (userModel == null) return Routes.splash;
    return Routes.homeScreen;
  }

  dynamic initalArguments(UserModel userModel) {
    if (userModel != null)
      return HomeScreenArguments(
          dashBoardEntity: DashBoardEntity(
              firstName: userModel.payload.client.firstName,
              lastName: userModel.payload.client.lastName,
              id: userModel.payload.client.id.toString()));
    return null;
  }
}

class Splash extends StatefulWidget {
  Splash({Key key, this.title, this.isFromProfile = false}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;
  final bool isFromProfile;

  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  bool isEnglishSelected = true;

  @override
  void initState() {
    super.initState();
  }

//    if(userData!=null&&!widget.isFromProfile) {
//      var entity=DashBoardEntity(firstName: userData.payload.firstName,lastName: userData.payload.lastName,id: userData.payload.id.toString());
//      ExtendedNavigator.rootNavigator.pushReplacementNamed(Routes.homeScreen,arguments: HomeScreenArguments(dashBoardEntity: entity));
//    }
//    else{
//
//    }


  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Container(
              alignment: Alignment.bottomCenter,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SvgPicture.asset(Images.appIcon,
                      semanticsLabel: 'Acme Logo'),
                  sizedBox25,
                  sizedBox25,
                  Text(
                    "CHOOSE YOUR PREFERRED LANGUAGE",
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ],
              ),
            ),
            Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  LangTile(
                    icons: Images.engLang,
                    isSelected: isEnglishSelected,
                    langName: "English",
                    onTap: () {
                      setState(() {
                        isEnglishSelected = !isEnglishSelected;
                      });
                    },
                  ),
                  LangTile(
                    icons: Images.chineseLang,
                    isSelected: !isEnglishSelected,
                    langName: "Chinese",
                    onTap: () {
                      setState(() {
                        isEnglishSelected = !isEnglishSelected;
                      });
                    },
                  ),
                ],
              ),
            ),
            Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  CustomButton(
                    onTap: () async {
                      EasyLocalization.of(context)
                          .locale =
                      isEnglishSelected
                          ? const Locale('en', 'US')
                          : const Locale.fromSubtags(languageCode: 'zh');
                      ExtendedNavigator.root.replace(Routes.loginScreen);
                    },
                    child: Text(
                      "DONE",
                      style: Theme.of(context)
                          .textTheme
                          .button
                          .copyWith(color: Colors.white),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

var sizedBox25 = SizedBox(
  height: 10,
  width: 10,
);

extension TextStyleExtensions on BuildContext {
  TextStyle get headLine6 => Theme.of(this).textTheme.headline6;

  TextStyle get headLine5 => Theme.of(this).textTheme.headline5;

  TextStyle get headLine4 => Theme.of(this).textTheme.headline4;

  TextStyle get headLine3 => Theme.of(this).textTheme.headline3;

  TextStyle get headLine2 => Theme.of(this).textTheme.headline2;

  TextStyle get headLine1 => Theme.of(this).textTheme.headline1;

  TextStyle get body1 => Theme.of(this).textTheme.bodyText1;

  TextStyle get body2 => Theme.of(this).textTheme.bodyText2;

  TextStyle get subTitle1 => Theme.of(this).textTheme.subtitle1;

  TextStyle get subTitle2 => Theme.of(this).textTheme.subtitle2;

  TextStyle get button => Theme.of(this).textTheme.button;

  TextStyle get caption => Theme.of(this).textTheme.caption;
}
class MyHttpOverrides extends HttpOverrides{
  @override
  HttpClient createHttpClient(SecurityContext context){
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port)=> true;
  }
}