import 'package:easy_localization/easy_localization.dart';
import 'package:technology/data/models/commission/commission_response.dart';

class DashBoardEntity{
  final String firstName;
  final String lastName;
  final String id;
  final List<DashBoardCommissionEntity> referrals;
  DashBoardEntity(  {this.firstName, this.lastName,this.id,this.referrals,});
}

class DashBoardCommissionEntity{
  final String id;
  final String sdaProduct;
  final String purchasedOn;
  final String referral;
  final String clientId;
  final String amount;
  final String referralType;
  final String salesAmount;
  final String productAmount;
  DashBoardCommissionEntity( {this.productAmount,  this.id, this.sdaProduct, this.purchasedOn, this.referral, this.amount,this.referralType,this.clientId, this.salesAmount,});

  factory DashBoardCommissionEntity.fromResponse(ReferralsFiltered response){
    String computeReferralType(int degree){
    if(degree==1)return "Direct";
    else if(degree>1) return "Indirect";
    else return "Undefined";
    }
    return  DashBoardCommissionEntity(
        id: response.id.toString(),
        amount: (response.productRetailPrice??0.0).toString(),
        purchasedOn: DateFormat("dd-MMM-yyyy hh:mm a").format(response.recordCreated.toUtc()),
        referral:response.clientNickname??"--",
        clientId: response.clientId.toString(),
        salesAmount: response.productRetailPrice!=null?((response.productRetailPrice.toDouble()*response.commissionRate)/100).toString():null,
        sdaProduct: response.productNickname??"--",
        productAmount: response.productRetailPrice!=null?response.productRetailPrice?.toString():null,
        referralType:computeReferralType(response.degree)
    );
  }

}