import 'package:flutter/material.dart';
import 'package:technology/data/constants/AppConstatnt.dart';
import 'package:technology/data/models/commission/commission_response.dart';
import 'package:technology/domain/entity/dashboard_entity.dart';

class CommissionEntity {
  final Color badgeColor;
  final String productName;
  final String currentCommission;
  final String directCount;
  final String directSalesValue;
  final String directRate;
  final String directCommission;
  final String indirectCount;
  final String indirectSalesValue;
  final String indirectRate;
  final String indirectCommission;
  final String lifetimeSales;
  final String currentAgentLevel;
  final List<DashBoardCommissionEntity> referrals;

  CommissionEntity(
      {this.referrals,
      this.directCount,
      this.directSalesValue,
      this.directRate="0.15",
      this.directCommission,
      this.indirectCount,
      this.indirectSalesValue,
      this.indirectRate,
      this.indirectCommission,
      this.lifetimeSales,
        this.badgeColor,
        this.productName, this.currentCommission,
        this.currentAgentLevel
      }
      );

  factory CommissionEntity.fromResponse(CommissionResponse response){
    var data=response.payload;
    return CommissionEntity(
      lifetimeSales: "\$ ${data.newTotalCommission}",
      currentCommission: data.currentProductMaxCommission.toString(),
      productName: data.currentProduct[0].nickname,
      referrals: data.referralsFiltered.map((e) => DashBoardCommissionEntity.fromResponse(e)).toList(),
      currentAgentLevel: data.currentSalesLevel[0].nickname,
    indirectRate:data.currentSalesLevel[0].commissionRate.toString(),
      directSalesValue: "\$${data.newDirectTotalSale}",
      indirectSalesValue: "\$${data.newIndirectTotalSale}",
      directCount: data.newDirectTotalCount.toString(),
      indirectCount: data.totalCountIndirect.toString(),
      // directCommission: "\$ ${(data.totalSalesAmountDirect*AppConstants.directCommissionRate).toString()}",
      directCommission: "\$ ${(data.newDirectTotalCommission).toString()}",
//        indirectCommission: ((data.totalSalesAmountDirect*data.currentSalesLevel[0].commissionRate)-data.downstreamProcess.commissionTotal).toString()
        indirectCommission: data.newIndirectTotalCommission.toString()
    );
  }
}
