import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:technology/data/api_states/api_states.dart';
import 'package:technology/data/models/document_model.dart';
import 'package:technology/domain/repo/user_repo.dart';
import 'package:technology/domain/usecases/usecase.dart';
@injectable
class GetDocumentUseCase extends UseCase<DocumentModel,String>{
  final UserRepo userRepo;

  GetDocumentUseCase(this.userRepo);
  @override
  Future<Either<Failure, DocumentModel>> call(String params) {
    return userRepo.getDocumentList(params);
  }

}