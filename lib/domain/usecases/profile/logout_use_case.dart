import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:technology/data/api_states/api_states.dart';
import 'package:technology/domain/repo/user_repo.dart';
import 'package:technology/domain/usecases/usecase.dart';
@injectable
class LogOutUserCase extends UseCase<dynamic,Unit>{
  final UserRepo userRepo;

  LogOutUserCase(this.userRepo);
  @override
  Future<Either<Failure, dynamic>> call(Unit params) {
    return userRepo.logOutUser();
  }

}