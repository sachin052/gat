import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:technology/data/api_states/api_states.dart';
import 'package:technology/data/models/emp_status.dart';
import 'package:technology/domain/repo/user_repo.dart';
import 'package:technology/domain/usecases/usecase.dart';
@injectable
class GetEmpUseCase extends UseCase<EmpStatus,Unit>{
  final UserRepo userRepo;

  GetEmpUseCase(this.userRepo);
  @override
  Future<Either<Failure, EmpStatus>> call(params) {
    return userRepo.getEmpList();
  }

}