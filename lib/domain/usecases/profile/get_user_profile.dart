import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:technology/data/api_states/api_states.dart';
import 'package:technology/data/models/getuser/get_user_response.dart';
import 'package:technology/domain/repo/user_repo.dart';
import 'package:technology/domain/usecases/usecase.dart';
@injectable
class GetUserProfileUseCase extends UseCase<dynamic,Unit>{
  final UserRepo userRepo;
  GetUserProfileUseCase(this.userRepo);
  @override
  Future<Either<Failure, UserModel>> call(Unit params) {
   return userRepo.getUserProfile();
  }

}