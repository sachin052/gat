
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:technology/data/api_states/api_states.dart';
import 'package:technology/domain/repo/user_repo.dart';
import 'package:technology/domain/usecases/usecase.dart';

@injectable
class SendReferralUseCase extends UseCase<dynamic,Map<String,dynamic>>{
  final UserRepo _userRepo;

  SendReferralUseCase(this._userRepo);
  @override
  Future<Either<Failure, dynamic>> call(Map<String, dynamic> params) {
    return _userRepo.sendInviteByEmail(params);
  }

}
