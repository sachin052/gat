import 'dart:collection';

import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:technology/data/api_states/api_states.dart';
import 'package:technology/domain/repo/user_repo.dart';
import 'package:technology/domain/usecases/usecase.dart';

@injectable
class UploadVerification extends UseCase<String,HashMap<String,dynamic>>{
  final UserRepo userRepo;

  UploadVerification(this.userRepo);
  @override
  Future<Either<Failure, String>> call(HashMap<String, dynamic> params) {
    return userRepo.uploadVerification(params);
  }


}