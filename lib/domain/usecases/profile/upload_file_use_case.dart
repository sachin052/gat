import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:technology/data/api_states/api_states.dart';
import 'package:technology/domain/repo/user_repo.dart';
import 'package:technology/domain/usecases/usecase.dart';

@injectable
class UploadFileUseCase extends UseCase<String,String>{
  final UserRepo userRepo;
  UploadFileUseCase(this.userRepo);
  @override
  Future<Either<Failure, String>> call(String params) {
    return userRepo.uploadFile(params);
  }

}