import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:technology/data/api_states/api_states.dart';
import 'package:technology/data/models/signup/user_registration_model.dart';
import 'package:technology/domain/repo/user_repo.dart';
import 'package:technology/domain/usecases/usecase.dart';
@injectable
class UpdateProfileUseCase extends UseCase<dynamic,Map<String,dynamic>>{
  final UserRepo _userRepo;

  UpdateProfileUseCase(this._userRepo);
  @override
  Future<Either<Failure, dynamic>> call(Map<String,dynamic> params) {
    return _userRepo.updateUserProfile(params);
  }

}