import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:technology/data/api_states/api_states.dart';
import 'package:technology/domain/entity/product_entity.dart';
import 'package:technology/domain/repo/user_repo.dart';
import 'package:technology/domain/usecases/usecase.dart';
@injectable
class GetProductUseCase implements UseCase<List<ProductEntity>,Unit>{
  final UserRepo userRepo;

  GetProductUseCase(this.userRepo);
  @override
  Future<Either<Failure, List<ProductEntity>>> call(params) {
    return userRepo.getProductList();
  }


}