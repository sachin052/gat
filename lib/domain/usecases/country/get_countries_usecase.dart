import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:technology/data/api_states/api_states.dart';
import 'package:technology/data/models/country_model.dart';
import 'package:technology/domain/repo/user_repo.dart';
import 'package:technology/domain/usecases/usecase.dart';
@injectable
class GetCountriesUseCase extends UseCase<List<CountryEntity>,Unit>{
  final UserRepo userRepo;
  GetCountriesUseCase(this.userRepo);
  @override
  Future<Either<Failure, List<CountryEntity>>> call(Unit params) {
    return userRepo.getSupportedLanguages();
  }

}
