import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:technology/data/api_states/api_states.dart';
import 'package:technology/data/models/signup/user_registration_model.dart';
import 'package:technology/domain/repo/auth_repo.dart';
import 'package:technology/domain/usecases/usecase.dart';
@injectable
class CreateClientUseCase extends UseCase<dynamic,RegistrationModel>{
  final AuthRepo authRepo;
  CreateClientUseCase(this.authRepo);
  @override
  Future<Either<Failure, dynamic>> call(RegistrationModel params) async{
    return authRepo.createUser(params);
  }

}