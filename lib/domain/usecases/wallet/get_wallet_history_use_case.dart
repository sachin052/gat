import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:technology/data/api_states/api_states.dart';
import 'package:technology/data/models/wallet/wallet_history_response.dart';
import 'package:technology/domain/repo/wallet_repo.dart';

import '../usecase.dart';

@injectable
class GetWalletHistoryUseCase extends UseCase<WalletHistoryResponse,String>{
  final WalletRepo walletRepo;

  GetWalletHistoryUseCase(this.walletRepo);
  @override
  Future<Either<Failure, WalletHistoryResponse>> call(String params) {
    return walletRepo.getWalletHistory(params);
  }

}