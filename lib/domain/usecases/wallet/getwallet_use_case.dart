import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:technology/data/api_states/api_states.dart';
import 'package:technology/data/models/getuser/get_user_response.dart';
import 'package:technology/data/models/wallet/wallet_list_response.dart';
import 'package:technology/domain/repo/wallet_repo.dart';
import 'package:technology/domain/usecases/usecase.dart';

@injectable
class GetWalletUseCase extends UseCase<List<WalletModel>,Unit>{
  final WalletRepo walletRepo;
  GetWalletUseCase(this.walletRepo);

  @override
  Future<Either<Failure, List<WalletModel>>> call(Unit params) {
    return walletRepo.getWalletList();
  }


}