import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:technology/data/api_states/api_states.dart';
import 'package:technology/data/models/wallet/wallet_details_response.dart';
import 'package:technology/domain/repo/wallet_repo.dart';
import 'package:technology/domain/usecases/usecase.dart';

@injectable
class GetWalletDetailsUseCase extends UseCase<WalletDetailResponse,String>{
  final WalletRepo walletRepo;

  GetWalletDetailsUseCase(this.walletRepo);
  @override
  Future<Either<Failure, WalletDetailResponse>> call(String params) {
    return walletRepo.getWalletDetail(params);
  }

}