
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:technology/data/api_states/api_states.dart';
import 'package:technology/data/models/currency/CountryResponse.dart';
import 'package:technology/domain/repo/user_repo.dart';
import 'package:technology/domain/usecases/usecase.dart';
@injectable
class GetCurrencyUseCase extends UseCase<CurrencyResponse,Unit>{
  final UserRepo userRepo;

  GetCurrencyUseCase(this.userRepo);
  @override
  Future<Either<Failure, CurrencyResponse>> call(Unit params) {
    return userRepo.getCurrency();
  }

}