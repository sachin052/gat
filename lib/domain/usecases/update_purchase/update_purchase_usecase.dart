

import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:technology/data/api_states/api_states.dart';
import 'package:technology/domain/entity/product_entity.dart';
import 'package:technology/domain/repo/user_repo.dart';
import 'package:technology/domain/usecases/usecase.dart';
@injectable
class UpdatePurchaseUseCase extends UseCase<dynamic,ProductEntity>{
  final UserRepo _repo;

  UpdatePurchaseUseCase(this._repo);
  @override
  Future<Either<Failure, dynamic>> call(ProductEntity params) {
    return _repo.updateUserPurchase(params);
  }

}