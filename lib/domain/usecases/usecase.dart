import 'package:dartz/dartz.dart';
import 'package:technology/data/api_states/api_states.dart';

abstract class UseCase<Type,Params>{
  Future<Either<Failure, Type>> call(Params params);
}

class NoParam {}