import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:technology/data/api_states/api_states.dart';
import 'package:technology/data/models/login/login_model.dart';
import 'package:technology/domain/entity/dashboard_entity.dart';
import 'package:technology/domain/repo/auth_repo.dart';
import 'package:technology/domain/usecases/usecase.dart';
@injectable
class LoginUseCase extends UseCase<DashBoardEntity,LoginModel>{
  final AuthRepo _authRepo;
  LoginUseCase(this._authRepo);
  @override
  Future<Either<Failure, DashBoardEntity>> call(params) {
    return _authRepo.signIn(params);
  }

}