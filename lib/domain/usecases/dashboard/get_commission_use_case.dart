import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:technology/data/api_states/api_states.dart';
import 'package:technology/data/models/commission/commission_request_model.dart';
import 'package:technology/domain/entity/dashboard_entity.dart';
import 'package:technology/domain/repo/user_repo.dart';
import 'package:technology/domain/usecases/usecase.dart';
@injectable
class GetCommissionUseCase extends UseCase<dynamic
,CommissionRequestModel>{
  final UserRepo userRepo;

  GetCommissionUseCase(this.userRepo);
  @override
  Future<Either<Failure, dynamic>> call(param) {
    return userRepo.getUserCommission(param);
  }

}