import 'package:dartz/dartz.dart';
import 'package:technology/data/api_states/api_states.dart';
import 'package:technology/data/models/commission/commission_request_model.dart';
import 'package:technology/data/models/country_model.dart';
import 'package:technology/data/models/currency/CountryResponse.dart';
import 'package:technology/data/models/document_model.dart';
import 'package:technology/data/models/emp_status.dart';
import 'package:technology/data/models/getuser/get_user_response.dart';
import 'package:technology/data/models/signup/user_registration_model.dart';
import 'package:technology/domain/entity/product_entity.dart';

abstract class UserRepo{
  Future<Either<Failure,UserModel>> getUserProfile();

  Future<Either<Failure,List<CountryEntity>>> getSupportedLanguages();

  Future<Either<Failure,dynamic>> getUserCommission(CommissionRequestModel model);

  Future<Either<Failure,List<ProductEntity>>> getProductList();

  Future<Either<Failure,dynamic>> updateUserPurchase(ProductEntity productEntity);

  Future<Either<Failure,dynamic>> updateUserProfile(Map<String,dynamic> registrationModel);

  Future<Either<Failure,dynamic>> logOutUser();

  Future<Either<Failure,dynamic>> sendInviteByEmail(Map<String,dynamic> request);

  Future<Either<Failure,CurrencyResponse>> getCurrency();

  Future<Either<Failure,EmpStatus>> getEmpList();

  Future<Either<Failure,DocumentModel>> getDocumentList(String id);

  Future<Either<Failure,String>> uploadFile(String path);

  Future<Either<Failure,String>> uploadVerification(Map<String,dynamic> request);


}

enum CommissionEnum{
  FOR_DASHBOARD,
  FOR_COMMISSION
}