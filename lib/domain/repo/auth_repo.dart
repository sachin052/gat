import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:technology/data/api_states/api_states.dart';
import 'package:technology/data/models/login/login_model.dart';
import 'package:technology/data/models/signup/user_registration_model.dart';
import 'package:technology/domain/entity/dashboard_entity.dart';

abstract class AuthRepo{
  Future<Either<Failure,dynamic>> registerUser(RegistrationModel model);
  Future<Either<Failure,dynamic>> createUser(RegistrationModel model);
  Future<Either<Failure,DashBoardEntity>> signIn(LoginModel model);
}