import 'package:dartz/dartz.dart';
import 'package:technology/data/api_states/api_states.dart';
import 'package:technology/data/models/wallet/wallet_details_response.dart';
import 'package:technology/data/models/wallet/wallet_history_response.dart';
import 'package:technology/data/models/wallet/wallet_list_response.dart';

abstract class WalletRepo{
  Future<Either<Failure,List<WalletModel>>> getWalletList();
  Future<Either<Failure,WalletDetailResponse>> getWalletDetail(String walletId);
  Future<Either<Failure,WalletHistoryResponse>> getWalletHistory(String walletId);
}