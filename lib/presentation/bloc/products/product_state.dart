import 'package:meta/meta.dart';

@immutable
abstract class ProductState {}

class InitialProductState extends ProductState {}
