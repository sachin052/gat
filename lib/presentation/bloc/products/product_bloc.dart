import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/rxdart.dart';
import 'package:technology/data/local_data_source/local_data_source.dart';
import 'package:technology/domain/entity/product_entity.dart';
import 'package:technology/domain/usecases/products/get_product_usecase.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';
import './bloc.dart';
@injectable
class ProductBloc extends Bloc<ProductEvent, CommonUIState<List<ProductEntity>>> {
  final GetProductUseCase _getProductUseCase;
  final LocalDataSource localDataSource;
  final currentProductController=BehaviorSubject<String>.seeded("");
  Function(String) get changeProductName=>currentProductController.sink.add;
  Stream<String> get productName=>currentProductController.stream;
  ProductBloc(this._getProductUseCase, this.localDataSource);
  @override
  CommonUIState<List<ProductEntity>> get initialState => CommonUIState.initial();

  @override
  Stream<CommonUIState<List<ProductEntity>>> mapEventToState(
    ProductEvent event,
  ) async* {
    if(event is GetProductList){
      yield CommonUIState<List<ProductEntity>>.loading();

      updateCurrentProductName();
     var response=await _getProductUseCase(unit);
     yield response.fold
       ((l) => CommonUIState<List<ProductEntity>>.error(l.errorMessage),
             (r) {
               if (currentProductController.value == "Junior") {
                 return CommonUIState<List<ProductEntity>>.success(r
                     .where((element) => element.productTitle != "Junior").map((e) => e..productPrice=(int.tryParse(e.productPrice)-500).toString()..isDiscounted=true)
                     .toList());
               }
               else if (currentProductController.value == "Intermediate") {
                 return CommonUIState<List<ProductEntity>>.success(r
                     .where((element) => element.productTitle == "Senior").map((e) => e..productPrice=(int.tryParse(e.productPrice)-500).toString()..isDiscounted=true)
                     .toList());
               }
               else if (currentProductController.value == "Senior") {
                 return CommonUIState<List<ProductEntity>>.error(
                     "You are unable to purchase a further product at this time");
               }
               return CommonUIState<List<ProductEntity>>.success(r);
             }
     );
//      var items=[
//        ProductEntity(productTitle: "Senior",productPrice: "5000",maxCommission: "27",isActive: true,productColor: Color(0xFFFFE300)),
//        ProductEntity(productTitle: "Senior",productPrice: "5000",maxCommission: "27",isActive: true,productColor: Color(0xFF949494)),
//        ProductEntity(productTitle: "Senior",productPrice: "5000",maxCommission: "27",isActive: true,productColor: Color(0xFFC35F00))
//      ];
//      yield CommonUIState<List<ProductEntity>>.loading();
//       yield await Future.delayed(Duration(seconds: 2),(){
//        return CommonUIState<List<ProductEntity>>.success(items);
//      });
    }
  }

  void updateCurrentProductName() async{
    var userModel = await localDataSource.getUserData();
   var currentProductId = userModel.payload.client.currentProductId;
    // changeProductName("Senior");
    if(currentProductId==1){
      changeProductName("Senior");
    }
    else if(currentProductId==2){
      changeProductName("Intermediate");
    }
    else changeProductName("Junior");
  }
}
