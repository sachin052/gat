import 'dart:async';
import 'dart:collection';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';
import 'package:technology/data/models/document_model.dart';
import 'package:technology/domain/usecases/country/get_countries_usecase.dart';
import 'package:technology/domain/usecases/profile/get_doc_list.dart';
import 'package:technology/domain/usecases/profile/upload_file_use_case.dart';
import 'package:technology/domain/usecases/profile/upload_verification_usecase.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';

part 'verifications_event.dart';
part 'verifications_state.dart';
@injectable
class VerificationsBloc extends Bloc<VerificationsEvent, CommonUIState> {
  final GetDocumentUseCase getDocumentUseCase;
  final GetCountriesUseCase getCountriesUseCase;
  final UploadFileUseCase uploadFileUseCase;
  final UploadVerification uploadVerification;
  final documentListController=BehaviorSubject<DocumentModel>();
  Function(DocumentModel) get changeDocumentModel=>documentListController.sink.add;
  Stream<DocumentModel> get documentList=>documentListController.stream;

  final documentList2Controller=BehaviorSubject<DocumentModel>();
  Function(DocumentModel) get changeDocumentModel2=>documentListController.sink.add;
  Stream<DocumentModel> get documentList2=>documentListController.stream;

  String image1Location="";
  String image2Location="";
  VerificationsBloc(this.getDocumentUseCase, this.getCountriesUseCase, this.uploadFileUseCase, this.uploadVerification);



  @override
  Stream<CommonUIState> mapEventToState(
    VerificationsEvent event,
  ) async* {
   if(event is GetDocumentList){
     yield CommonUIState.loading();
     var response=await getDocumentUseCase(event.id);
     yield response.fold((l) => CommonUIState.error(l.errorMessage), (r) {
       if(event.docNo==1)
       changeDocumentModel(r);
       else changeDocumentModel2(r);
       return CommonUIState.success(r);
     });
   }
   else if(event is UploadFile){
     yield CommonUIState.loading();
     var response=await uploadFileUseCase(event.path);
     yield response.fold((l) => CommonUIState.error(l.errorMessage), (r) {
       if(event.docNo==1)
       image1Location=r.split(".com/")[1];
       else image2Location=r.split(".com/")[1];
       return CommonUIState.success({});
     });
   }
   else if(event is DoVerification){
     yield CommonUIState.loading();
     var response=await uploadVerification(event.map);
     yield response.fold((l) => CommonUIState.error(l.errorMessage), (r) => CommonUIState.success(r));
   }
  }

  @override
  CommonUIState get initialState => CommonUIState.initial();
  uploadFile(String path)async{
  return await uploadFileUseCase(path);
  }
}
