part of 'verifications_bloc.dart';

@immutable
abstract class VerificationsEvent {

}


class GetDocumentList extends VerificationsEvent{
  final String id;
  final int docNo;
  GetDocumentList(this.id, this.docNo);
}

class UploadFile extends VerificationsEvent{
  final String path;
  final int docNo;
  UploadFile(this.path, this.docNo);
}

class DoVerification extends VerificationsEvent{
 final HashMap<String,dynamic> map;
  DoVerification(this.map);
}
