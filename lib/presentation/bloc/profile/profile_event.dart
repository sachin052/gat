part of 'profile_bloc.dart';

@immutable
abstract class ProfileEvent {}
class GetUserProfile extends ProfileEvent{}
class UpdateUserSummary extends ProfileEvent{}
class UpdateUserContact extends ProfileEvent{}
class UpdateUserAddress extends ProfileEvent{}
class GetSupportedCountries extends ProfileEvent{}
class LogOutUser extends ProfileEvent{}
class SendReferralInvite extends ProfileEvent{}
class UpdatePersonal extends ProfileEvent{
  final HashMap<String,dynamic> map;
  UpdatePersonal(this.map);
}

class UpdateBankAccount extends ProfileEvent{
  final HashMap<String,String> map;
  UpdateBankAccount(this.map);
}
class UpdateFinancial extends ProfileEvent{
  final HashMap<String,String> map;
  UpdateFinancial(this.map);
}
