import 'dart:async';
import 'dart:collection';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';
import 'package:technology/data/models/country_model.dart';
import 'package:technology/data/models/currency/CountryResponse.dart';
import 'package:technology/data/models/currency/Payload.dart';
import 'package:technology/data/models/document_model.dart';
import 'package:technology/data/models/emp_status.dart';
import 'package:technology/data/models/getuser/get_user_response.dart';
import 'package:technology/data/models/signup/user_registration_model.dart';
import 'package:technology/domain/usecases/country/get_countries_usecase.dart';
import 'package:technology/domain/usecases/currency/get_currency_use_case.dart';
import 'package:technology/domain/usecases/profile/get_doc_list.dart';
import 'package:technology/domain/usecases/profile/get_emp_status.dart';
import 'package:technology/domain/usecases/profile/get_user_profile.dart';
import 'package:technology/domain/usecases/profile/logout_use_case.dart';
import 'package:technology/domain/usecases/profile/send_referral_email_usecase.dart';
import 'package:technology/domain/usecases/profile/update_profile_use_case.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';
import 'package:technology/others/utils/validators.dart';
import 'package:technology/presentation/bloc/profile/profile_interactions.dart';

part 'profile_event.dart';
part 'profile_state.dart';

@injectable
class ProfileBloc extends Bloc<ProfileEvent, CommonUIState<UserModel>> {
  final GetCountriesUseCase getCountriesUseCase;
  final GetUserProfileUseCase getUserProfileUseCase;
  final UpdateProfileUseCase updateProfileUseCase;
  final LogOutUserCase logOutUserCase;
  final SendReferralUseCase sendReferralUseCase;
  final GetCurrencyUseCase getCurrencyUseCase;
  final GetEmpUseCase getEmpUseCase;
  final GetDocumentUseCase getDocumentUseCase;
  // First Name
  BehaviorSubject<String> _firstNameController = BehaviorSubject<String>();


  Function(String) get changeFirstName => _firstNameController.sink.add;

  final countryListController = BehaviorSubject<List<CountryEntity>>();
  List<CountryEntity> allCountries=[];

  Function(List<CountryEntity>) get changeCountryList =>
      countryListController.sink.add;

  Stream<List<CountryEntity>> get countryList => countryListController.stream;

  Stream<String> get firstName =>
      _firstNameController.stream.transform(notEmptyValidator);
  TextEditingController firstNameController = TextEditingController();

  // Middle Name
  BehaviorSubject<String> _middleNameController = BehaviorSubject<String>();

  Function(String) get changeMiddleName => _middleNameController.sink.add;

  Stream<String> get middleName => _middleNameController.stream;
  TextEditingController middleNameController = TextEditingController();

  Stream<String> get firstLastName =>
      CombineLatestStream.combine2(firstName, lastName, (a, b) => a + " " + b);

  // Last Name Field
  BehaviorSubject<String> _lastNameController = BehaviorSubject<String>();

  Function(String) get changeLastName => _lastNameController.sink.add;

  Stream<String> get lastName =>
      _lastNameController.stream.transform(notEmptyValidator);
  TextEditingController lastNameTextController = TextEditingController();

  BehaviorSubject<bool> _postalSameController = BehaviorSubject<bool>();

  Function(bool) get changePostalSame {
    return _postalSameController.sink.add;
  }

  Stream<bool> get sameAsPostal => _postalSameController.stream;

  // Email Field
  final _emailController = BehaviorSubject<String>();

  Function(String) get changeEmail => _emailController.sink.add;

  Stream<String> get email => _emailController.stream.transform(validateEmail);
  TextEditingController emailTextController = TextEditingController();

  // Postal Address

  final _postalAddressController = BehaviorSubject<String>();

  Function(String) get changePostalAddress => _postalAddressController.sink.add;

  Stream<String> get postalAddress => _postalAddressController.stream;

  final _postalCityController = BehaviorSubject<String>();

  Function(String) get changePostalCity => _postalCityController.sink.add;

  Stream<String> get postalCity => _postalCityController.stream;

  final _postalStateController = BehaviorSubject<String>();

  Function(String) get changePostalState => _postalStateController.sink.add;

  Stream<String> get postalState => _postalStateController.stream;

  final _postalCountryController = BehaviorSubject<String>();

  Function(String) get changePostalCountry => _postalCountryController.sink.add;

  Stream<String> get postalCountry => _postalCountryController.stream;

  final _postalPinController = BehaviorSubject<String>();

  Function(String) get changePostalPin => _postalPinController.sink.add;

  Stream<String> get postalPin => _postalPinController.stream;

  final _teleHomeController = BehaviorSubject<String>.seeded("1");

  Function(String) get changeCodeHome => _teleHomeController.sink.add;

  Stream<String> get codeHome => _teleHomeController.stream;

  final _teleWorkController = BehaviorSubject<String>.seeded("1");

  Function(String) get changeCodeWork => _teleWorkController.sink.add;

  Stream<String> get codeWork => _teleWorkController.stream;

  final _mobileCodeController = BehaviorSubject<String>.seeded("1");

  Function(String) get changeMobileCode => _mobileCodeController.sink.add;

  Stream<String> get mobileCode => _mobileCodeController.stream;

  TextEditingController nickNameController = TextEditingController();
  TextEditingController workTelephone = TextEditingController();
  TextEditingController homeTelephone = TextEditingController();
  TextEditingController mobile = TextEditingController();
  TextEditingController secondaryEmail = TextEditingController();

  TextEditingController addressStreet = TextEditingController();
  TextEditingController cityStreet = TextEditingController();
  TextEditingController stateAddressStreet = TextEditingController();
  TextEditingController countryStreet = TextEditingController();
  TextEditingController pinCodeStreet = TextEditingController();

  TextEditingController addressPostal = TextEditingController();
  TextEditingController cityPostal = TextEditingController();
  TextEditingController stateAddressPostal = TextEditingController();
  TextEditingController countryPostal = TextEditingController();
  TextEditingController pinCodePostal = TextEditingController();

  PageController pageController = PageController(initialPage: 0);


  final _mobileNumberController = BehaviorSubject<String>();
  Function(String) get changeMobile => _mobileNumberController.sink.add;
  Stream<String> get mobileNumber=> _mobileNumberController.stream;


  final referrerController=BehaviorSubject<String>();
  Function(String) get changeReferrerId=>referrerController.sink.add;
  Stream<String> get referrerId=>referrerController.stream;


  final selectedLangController=BehaviorSubject<List<bool>>.seeded([true,false ]);
  Function(List<bool>) get changeSelectedLang=>selectedLangController.sink.add;
  Stream<List<bool>> get selectedLang =>selectedLangController.stream;


  final referrerEmailController=BehaviorSubject<String>();
  Function(String) get changeReferrerEmail=>referrerEmailController.sink.add;
  Stream<String> get referrerEmail=>referrerEmailController.stream.transform(validateEmail);



  final userModelController=BehaviorSubject<UserModel>();
  Function(UserModel) get changeUserModel=>userModelController.sink.add;
  Stream<UserModel> get userModel=>userModelController.stream;


  final currencyController=BehaviorSubject<List<CurrenyModel>>();
  Function(List<CurrenyModel>) get changeCurrencyList=>currencyController.sink.add;
  Stream<List<CurrenyModel>> get currencyList=>currencyController.stream;


  final empStatusController=BehaviorSubject<List<Status>>();
  Function(List<Status>) get changeEmpList=>empStatusController.sink.add;
  Stream<List<Status>> get empList =>empStatusController.stream;




  ProfileBloc(this.getUserProfileUseCase, this.updateProfileUseCase,
      this.getCountriesUseCase, this.logOutUserCase, this.sendReferralUseCase, this.getCurrencyUseCase, this.getEmpUseCase, this.getDocumentUseCase);

  @override
  Stream<CommonUIState<UserModel>> mapEventToState(ProfileEvent event,) async* {
    yield CommonUIState.loading();
    if (event is GetUserProfile) {
      var countries = await getCountriesUseCase(unit);
    var currencies=await getCurrencyUseCase(unit);
    currencies.fold((l) => null, (r) => changeCurrencyList(r.payload));
      countries.fold((l) => print(l.errorMessage), (r) async{
        allCountries = r;
        changeCountryList(r);
      });
    var empList=await getEmpUseCase(unit);
    empList.fold((l) => null, (r) => changeEmpList(r.payload));
      var response = await getUserProfileUseCase(unit);
      yield response.fold((l) =>
      CommonUIState<UserModel>.error(l.errorMessage), (r) {
        changeUserModel(r);
        changeReferrerId(r.payload.client.id.toString());
        firstNameController.text = r.payload.client.firstName;
        lastNameTextController.text = r.payload.client.lastName;
        nickNameController.text =
            r.payload.client.lastName + " " + r.payload.client.firstName;
        emailTextController.text = r.payload.client.email;
        workTelephone.text =
        r.payload.client.telWork != null && r.payload.client.telWork != "null" ? r.payload.client
            .telWork.toString() : "";
        homeTelephone.text =
        r.payload.client.telHome != null && r.payload.client.telHome != "null" ? r.payload.client
            .telHome.toString() : "";
        mobile.text =
        r.payload.client.telMobile != null && r.payload.client.telMobile != "null" ? r.payload.client
            .telMobile.toString() : "";
        addressStreet.text = r.payload.client.streetAddressLine1;
        cityStreet.text = r.payload.client.streetAddressSuburb;
        stateAddressStreet.text = r.payload.client.streetAddressState;
        countryStreet.text = r.payload.client.streetAddressCountry != null ? allCountries
            ?.firstWhere((element) =>
        element.id == r.payload.client.streetAddressCountry,orElse: (){
              return null;
        })
            ?.fullName : "";
        pinCodeStreet.text = r.payload.client.streetAddressPostcode.toString();
        addressPostal.text = r.payload.client.postalAddressLine1;
        cityPostal.text = r.payload.client.postalAddressSuburb;
        stateAddressPostal.text = r.payload.client.postalAddressState;
        countryPostal.text = r.payload.client.postalAddressCountry != null ? allCountries
            ?.firstWhere((element) =>
        element.id == r.payload.client.streetAddressCountry,orElse: ()=>null)
            ?.fullName : "";
        pinCodePostal.text = r.payload.client.postalAddressPostcode.toString();
        return CommonUIState<UserModel>.success(r);
      }
      );
    }
    else {
      if(event is LogOutUser){
        await logOutUserCase(unit);
        yield CommonUIState<UserModel>.success(UserModel());
      }
      if (event is UpdateUserSummary) {
        yield* updateUserSummary(this);
      }
      else if (event is UpdateUserContact) {
       yield* updateUserContact(this);
      }
      else if (event is UpdateUserAddress) {
        yield* updateUserAddress(this);
      }
      else if(event is SendReferralInvite){
      yield* sendReferralInvite(this);
      }
      else if(event is UpdatePersonal){
        yield* updatePersonal(this, event.map);
        add(GetUserProfile());
      }
    }
  }

  @override
  // TODO: implement initialState
  CommonUIState<UserModel> get initialState =>
      CommonUIState<UserModel>.initial();

  void sameAsPostalAddress() {
    addressPostal.text = addressStreet.text;
    cityPostal.text = cityStreet.text;
    stateAddressPostal.text = stateAddressStreet.text;
    countryPostal.text = countryStreet.text;
    pinCodePostal.text = pinCodeStreet.text;
    changePostalAddress(addressStreet.text);
    changePostalCity(cityStreet.text);
    changePostalState(stateAddressStreet.text);
    changePostalCountry(countryStreet.text);
    changePostalPin(pinCodeStreet.text);
  }

  clearPostalAddress() {
    addressPostal.clear();
    cityPostal.clear();
    stateAddressPostal.clear();
    countryPostal.clear();
    pinCodePostal.clear();
  }

}
