import 'dart:collection';
import 'dart:convert';
import 'package:technology/others/utils/validators.dart';
import 'package:technology/data/models/getuser/get_user_response.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';
import 'package:technology/presentation/bloc/profile/profile_bloc.dart';
import 'package:technology/main.dart';
Stream<CommonUIState<UserModel>> updateUserSummary(ProfileBloc profileBloc)async*{
  var response = await profileBloc.updateProfileUseCase(Map<String, dynamic>.from({
    "first_name": profileBloc.firstNameController.text,
    "last_name": profileBloc.lastNameTextController.text,
    "middle_names": profileBloc.middleNameController.text
  }));
  yield response.fold((l) =>
  CommonUIState<UserModel>.error(l.errorMessage),
          (r) {
        profileBloc.add(GetUserProfile());
        return CommonUIState<UserModel>.success(r);
      });
}

Stream<CommonUIState<UserModel>> updateUserContact(ProfileBloc profileBloc)async*{
  var response = await profileBloc.updateProfileUseCase(Map<String, dynamic>.from({
    "tel_home": profileBloc.homeTelephone.text,
    "tel_work": profileBloc.workTelephone.text,
    "tel_mobile": profileBloc.mobile.text
  }));
  yield response.fold((l) =>
  CommonUIState<UserModel>.error(l.errorMessage),
          (r) {
        profileBloc.add(GetUserProfile());
        return CommonUIState<UserModel>.success(r);
      });
}
Stream<CommonUIState<UserModel>> updateUserAddress(ProfileBloc profileBloc)async*{
  profileBloc.allCountries.forEach((element) {
    print(element);
  });
  var response = await profileBloc.updateProfileUseCase(Map<String, dynamic>.from({
    "street_address_line_1": profileBloc.addressStreet.text,
    "street_address_suburb": profileBloc.cityStreet.text,
    "street_address_state": profileBloc.stateAddressStreet.text,
    "street_address_postcode": profileBloc.pinCodeStreet.text,
    "street_address_country": profileBloc.allCountries
        .firstWhere((element) =>
    element.fullName.toLowerCase() == profileBloc.countryStreet.text.toLowerCase())
        .id
        .toString(),
    "postal_address_line_1": profileBloc.addressPostal.text,
    "postal_address_suburb": profileBloc.countryPostal.text,
    "postal_address_state": profileBloc.stateAddressPostal.text,
    "postal_address_postcode": profileBloc.pinCodePostal.text,
    "postal_address_country": profileBloc.allCountries
        ?.firstWhere((element) =>
    element.fullName.toLowerCase() == profileBloc.countryPostal.text.toLowerCase())
        ?.id
        ?.toString(),
  }));
  yield response.fold((l) =>
  CommonUIState<UserModel>.error(l.errorMessage),
          (r) {
            profileBloc.add(GetUserProfile());
        return CommonUIState<UserModel>.success(r);
      });
}

Stream<CommonUIState<UserModel>> sendReferralInvite(ProfileBloc profileBloc)async*{
  if(profileBloc.referrerEmailController.value.isValidEmail){
    yield CommonUIState.loading();
    var map={
      "client_id":int.tryParse(profileBloc.referrerController.value),
      "email":profileBloc.referrerEmailController.value,
      "language_id":profileBloc.selectedLangController.value[0]==true?"en":"zh"
    };
    var response=await profileBloc.sendReferralUseCase(map);
    yield response.fold((l) => CommonUIState.error(l.errorMessage), (r) => CommonUIState.success(UserModel(payload: UserInnerModel(defaultClientId: 0))));
  }
}
Stream<CommonUIState<UserModel>> updatePersonal(ProfileBloc profileBloc,HashMap<String,dynamic> map)async*{
    yield CommonUIState.loading();
    var response=await profileBloc.updateProfileUseCase(map);
    yield response.fold((l) => CommonUIState.error(l.errorMessage), (r) => CommonUIState.success(UserModel(payload: UserInnerModel(defaultClientId: 0))));
}

