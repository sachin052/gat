import 'package:meta/meta.dart';

@immutable
abstract class HomeScreenEvent {}

class DataFetched extends HomeScreenEvent{}
