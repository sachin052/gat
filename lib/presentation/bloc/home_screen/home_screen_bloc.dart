import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/rxdart.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';

import 'home_screen_event.dart';
import 'home_screen_state.dart';

@injectable
class HomeScreenBloc extends Bloc<HomeScreenEvent, CommonUIState> {

  final currentPageController=BehaviorSubject<int>.seeded(2);
  Function(int) get changeCurrentPage=>currentPageController.sink.add;
  Stream<int> get currentPage=>currentPageController.stream;
  @override
  CommonUIState get initialState => CommonUIState.loading();

  @override
  Stream<CommonUIState> mapEventToState(
    HomeScreenEvent event,
  ) async* {
   if(event is DataFetched) yield CommonUIState.success("");
  }
}
