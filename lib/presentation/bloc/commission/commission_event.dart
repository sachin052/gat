part of 'commission_bloc.dart';

@immutable
abstract class CommissionEvent {}

class GetDetailedCommission extends CommissionEvent{
  final CommissionRequestModel model;

  GetDetailedCommission(this.model);
}
