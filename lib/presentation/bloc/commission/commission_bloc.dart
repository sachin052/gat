import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';
import 'package:technology/data/models/commission/commission_request_model.dart';
import 'package:technology/domain/entity/commission_entity.dart';
import 'package:technology/domain/usecases/dashboard/get_commission_use_case.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';

part 'commission_event.dart';
part 'commission_state.dart';
@injectable
class CommissionBloc extends Bloc<CommissionEvent, CommonUIState<CommissionEntity>> {
  CommissionBloc(this.getCommissionUseCase) : super();
  final GetCommissionUseCase getCommissionUseCase;
  final _lastDateController=BehaviorSubject<DateTime>();
  Function(DateTime) get changeLastDate=>_lastDateController.sink.add;
  Stream<DateTime> get lastDate=>_lastDateController.stream;

  final _startDateController=BehaviorSubject<DateTime>();
  Function(DateTime) get changeStartDate=>_startDateController.sink.add;
  Stream<DateTime> get startDate=>_startDateController.stream;

  Stream<String> get startLastDate=>Rx.combineLatest2<DateTime,DateTime,String>(startDate, lastDate, (a, b) => "${DateFormat("dd MMM yyyy").format(a)} - ${DateFormat("dd MMM yyyy").format(b)}") ;
  Stream<List<DateTime>> get startLastList=>Rx.combineLatest2<DateTime,DateTime,List<DateTime>>(startDate, lastDate, (a, b) => [a,b]);


  @override
  Stream<CommonUIState<CommissionEntity>> mapEventToState(
    CommissionEvent event,
  ) async* {
    if(event is GetDetailedCommission)
  yield*  mapGetCommission(event.model);
    
  }


  CommonUIState<CommissionEntity> get initialState =>CommonUIState<CommissionEntity>.initial();

  Stream<CommonUIState<CommissionEntity>> mapGetCommission(CommissionRequestModel model)async*{
    yield CommonUIState<CommissionEntity>.loading();
    var response=await  getCommissionUseCase(model);
    yield    response.fold(
            (l) => CommonUIState<CommissionEntity>.error(l.errorMessage),
            (r) => CommonUIState<CommissionEntity>.success(r));
  }
}
