part of 'wallet_bloc.dart';

@immutable
abstract class WalletEvent {}

class GetWalletList extends WalletEvent{}

class GetWalletDetails extends WalletEvent{
 final String walletId;
  GetWalletDetails(this.walletId);
}
class GetWalletHistory extends WalletEvent{
  final String walletId;
  GetWalletHistory(this.walletId);
}
