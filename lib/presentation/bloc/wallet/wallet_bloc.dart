import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';
import 'package:technology/data/models/wallet/wallet_history_response.dart';
import 'package:technology/data/models/wallet/wallet_list_response.dart';
import 'package:technology/domain/usecases/wallet/get_wallet_details_use_cas.dart';
import 'package:technology/domain/usecases/wallet/get_wallet_history_use_case.dart';
import 'package:technology/domain/usecases/wallet/getwallet_use_case.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';

part 'wallet_event.dart';
part 'wallet_state.dart';

@injectable
class WalletBloc extends Bloc<WalletEvent, CommonUIState> {

  final GetWalletUseCase getWalletUseCase;
  final GetWalletDetailsUseCase getWalletDetailsUseCase;
  final GetWalletHistoryUseCase getWalletHistoryUseCase;

  final _walletController=BehaviorSubject<List<WalletModel>>();
  Function(List<WalletModel>) get changeWalletList=>_walletController.sink.add;
  Stream<List<WalletModel>> get walletList=>_walletController.stream;

  
  final _selectedWalletController=BehaviorSubject<String>();
  Function(String) get changeSelectedId=>_selectedWalletController.sink.add;
  Stream<String> get selectedId=>_selectedWalletController.stream;
    
  final _walletDetailsController=BehaviorSubject<String>.seeded("");
  Function(String) get changeWalletDetails=>_walletDetailsController.sink.add;
  Stream<String> get walletDetails=>_walletDetailsController.stream;

  final _walletHistoryController=BehaviorSubject<WalletHistoryResponse>();
  Function(WalletHistoryResponse) get changeWalletHistory=>_walletHistoryController.sink.add;
  Stream<WalletHistoryResponse> get walletHistory=>_walletHistoryController.stream;

  WalletBloc(this.getWalletUseCase, this.getWalletDetailsUseCase, this.getWalletHistoryUseCase);

  // WalletBloc() : super(WalletInitial());

  @override
  Stream<CommonUIState> mapEventToState(
    WalletEvent event,
  ) async* {
    if(event is GetWalletList){
      var response= await getWalletUseCase(unit);
      yield response.fold((l) => CommonUIState.error(l.errorMessage),
              (r)  {
        if(r.isEmpty){
          return CommonUIState.error("No wallet available");
        }

        else{
          changeWalletList(r);
                if(r.length==1){
                changeSelectedId(r[0].id.toString());
                add(GetWalletDetails(r[0].id.toString()));
                }
                return CommonUIState.success(r);
                }
              }
      );
    }
    else if (event is GetWalletDetails){
      yield CommonUIState.loading();
      var response=await getWalletDetailsUseCase(event.walletId);
      var walletHistory=await getWalletHistoryUseCase(event.walletId);
       response.fold((l) => CommonUIState.error(l.errorMessage), (r)  {
        changeWalletDetails("#${r.payload.id} "+r.payload.isoAlpha3);
      });

       yield walletHistory.fold((l) => CommonUIState.error(l.errorMessage), (r) {
         changeWalletHistory(r);
         return CommonUIState.success(r);
       });

    }
  }

  @override

  CommonUIState get initialState => CommonUIState.initial();

  @override
  Future<void> close() {
    _walletController.close();
    _selectedWalletController.close();
    return super.close();
  }
}
