import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/rxdart.dart';
import 'package:technology/data/aws_cognito/aws_cognito_helper.dart';
import 'package:technology/data/models/login/login_model.dart';
import 'package:technology/domain/usecases/login/login_usecase.dart';
import 'package:technology/others/styles/strings.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';
import 'package:technology/others/utils/validators.dart';
import 'package:technology/presentation/bloc/base_bloc.dart';

import 'bloc.dart';
import 'login_state.dart';
@injectable
class LoginBloc extends Bloc<LoginEvent, CommonUIState> implements BaseBloc {
  final LoginUseCase _loginUseCase;

  final _emailController = BehaviorSubject<String>();
  TextEditingController emailTextController = TextEditingController();

  final _passwordController = BehaviorSubject<String>();
  TextEditingController passController = TextEditingController();

  LoginBloc(this._loginUseCase);


  // change data
  Function(String) get changeEmail => _emailController.sink.add;

  Function(String) get changePassword => _passwordController.sink.add;

  Stream<String> get email => _emailController.stream.transform(validateEmail);

  Stream<String> get password =>
      _passwordController.stream.transform(validatePassword);

  @override
  CommonUIState get initialState => CommonUIState.initial();

  @override
  Stream<CommonUIState> mapEventToState(LoginEvent event,) async* {
    if (isValid())
      yield* emitCommonUIState();
  }

  @override
  dispose() {

  }

  @override
  Stream<CommonUIState> emitCommonUIState() async* {
    yield CommonUIState.loading();
    var response = await _loginUseCase(LoginModel(
        username: emailTextController.text.trim(), password: passController.text.trim()));
    yield response.fold(
            (error) => CommonUIState.error(error.errorMessage),
            (success) => CommonUIState.success(success)
    );
  }

  bool isValid() {
    if (emailTextController.text.isNotEmpty &&
        emailTextController.text.isValidEmail &&
        passController.text.isNotEmpty && passController.text.isValidPass) {
      return true;
    } else {
      if (emailTextController.text.isEmpty) {
        _emailController.addError(Strings.validEmail);
      }
      if (passController.text.isEmpty) {
        _passwordController.addError(Strings.notEmpty);
      }
      return false;
    }
  }
}