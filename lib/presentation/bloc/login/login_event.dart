import 'package:meta/meta.dart';

@immutable
abstract class LoginEvent {}

class LoginUser extends LoginEvent{}