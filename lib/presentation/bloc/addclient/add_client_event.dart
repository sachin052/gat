import 'package:meta/meta.dart';
import 'package:technology/data/models/country_model.dart';

@immutable
abstract class AddClientEvent {}
class CreateClient extends AddClientEvent{
  final List<CountryEntity> countries;

  CreateClient(this.countries);
}
class GetCountriesList extends AddClientEvent{}
