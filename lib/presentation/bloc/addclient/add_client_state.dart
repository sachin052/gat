import 'package:meta/meta.dart';

@immutable
abstract class AddClientState {}

class InitialAddClientState extends AddClientState {}
