import 'dart:async';
import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/rxdart.dart';
import 'package:technology/data/models/country_model.dart';
import 'package:technology/data/models/signup/user_registration_model.dart';
import 'package:technology/domain/usecases/country/get_countries_usecase.dart';
import 'package:technology/domain/usecases/createclient/create_client.dart';
import 'package:technology/others/styles/strings.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';
import 'package:technology/others/utils/validators.dart';

import './bloc.dart';

@injectable
class AddClientBloc extends Bloc<AddClientEvent, CommonUIState> {
  final CreateClientUseCase _createClientUseCase;
  final GetCountriesUseCase getCountriesUseCase;
  // First Name
  BehaviorSubject<String> _firstNameController = BehaviorSubject<String>();

  AddClientBloc(this._createClientUseCase, this.getCountriesUseCase);

  Function(String) get changeFirstName => _firstNameController.sink.add;

  Stream<String> get firstName =>
      _firstNameController.stream.transform(notEmptyValidator);
  TextEditingController firstNameController = TextEditingController();

  // Middle Name
  BehaviorSubject<String> _middleNameController = BehaviorSubject<String>();

  Function(String) get changeMiddleName => _middleNameController.sink.add;

  Stream<String> get middleName => _middleNameController.stream;
  TextEditingController middleNameController = TextEditingController();

  Stream<String> get firstLastName =>
      CombineLatestStream.combine2(firstName, lastName, (a, b) => a + " " + b);

  // Last Name Field
  BehaviorSubject<String> _lastNameController = BehaviorSubject<String>();

  Function(String) get changeLastName => _lastNameController.sink.add;

  Stream<String> get lastName =>
      _lastNameController.stream.transform(notEmptyValidator);
  TextEditingController lastNameTextController = TextEditingController();

  BehaviorSubject<bool> _postalSameController = BehaviorSubject<bool>();

  Function(bool) get changePostalSame {
    return _postalSameController.sink.add;
  }

  Stream<bool> get sameAsPostal => _postalSameController.stream;

  // Email Field
  var _emailController = BehaviorSubject<String>();
  Function(String) get changeEmail => _emailController.sink.add;
  Stream<String> get email => _emailController.stream.transform(validateEmail);
  TextEditingController emailTextController = TextEditingController();

  // Postal Address

  final _postalAddressController = BehaviorSubject<String>();
  Function(String) get changePostalAddress => _postalAddressController.sink.add;
  Stream<String> get postalAddress => _postalAddressController.stream;

  final _postalCityController = BehaviorSubject<String>();
  Function(String) get changePostalCity => _postalCityController.sink.add;
  Stream<String> get postalCity=> _postalCityController.stream;

  final _postalStateController = BehaviorSubject<String>();
  Function(String) get changePostalState => _postalStateController.sink.add;
  Stream<String> get postalState=> _postalStateController.stream;

  final _postalCountryController = BehaviorSubject<String>();
  Function(String) get changePostalCountry => _postalCountryController.sink.add;
  Stream<String> get postalCountry=> _postalCountryController.stream;

  final _postalPinController = BehaviorSubject<String>();
  Function(String) get changePostalPin => _postalPinController.sink.add;
  Stream<String> get postalPin=> _postalPinController.stream;

  final _teleHomeController = BehaviorSubject<String>.seeded("1");
  Function(String) get changeCodeHome => _teleHomeController.sink.add;
  Stream<String> get codeHome=> _teleHomeController.stream;

  final _teleWorkController = BehaviorSubject<String>.seeded("1");
  Function(String) get changeCodeWork => _teleWorkController.sink.add;
  Stream<String> get codeWork=> _teleWorkController.stream;

  var _mobileNumberController = BehaviorSubject<String>();
  Function(String) get changeMobile => _mobileNumberController.sink.add;
  Stream<String> get mobileNumber=> _mobileNumberController.stream;

  final _mobileCodeController = BehaviorSubject<String>.seeded("1");
  Function(String) get changeMobileCode => _mobileCodeController.sink.add;
  Stream<String> get mobileCode=> _mobileCodeController.stream;

  TextEditingController workTelephone = TextEditingController();
  TextEditingController homeTelephone = TextEditingController();
  TextEditingController mobile = TextEditingController();
  TextEditingController secondaryEmail = TextEditingController();

  TextEditingController addressStreet = TextEditingController();
  TextEditingController cityStreet = TextEditingController();
  TextEditingController stateAddressStreet = TextEditingController();
  TextEditingController countryStreet = TextEditingController();
  TextEditingController pinCodeStreet = TextEditingController();

  TextEditingController addressPostal = TextEditingController();
  TextEditingController cityPostal = TextEditingController();
  TextEditingController stateAddressPostal = TextEditingController();
  TextEditingController countryPostal = TextEditingController();
  TextEditingController pinCodePostal = TextEditingController();

  PageController pageController=PageController(initialPage: 0);

  final countryListController=BehaviorSubject<List<CountryEntity>>();
  List<CountryEntity> allCountries=[];
  Function(List<CountryEntity>) get changeCountryList=>countryListController.sink.add;
  Stream<List<CountryEntity>> get countryList=>countryListController.stream;
  
  
  final validFirstStepController=BehaviorSubject<bool>(); 
  Function(bool) get changeValidFirstStep=>validFirstStepController.sink.add; 
  Stream<bool> get validFirstStep=>Rx.combineLatest3(firstName, lastName, email, (a, b, c) => true).asBroadcastStream();

  Stream<bool> get allStepValid=>Rx.combineLatest2<bool,int,bool>(validFirstStep, currentPage, (a, b) {
    return a!=null&&b!=null&&a&&b==2;
  }).asBroadcastStream();


  final currentPageController=BehaviorSubject<int>.seeded(0);
  Function(int) get changeCurrentPage=>currentPageController.sink.add;
  Stream<int> get currentPage=>currentPageController.stream;

  @override
  CommonUIState get initialState => CommonUIState.initial();

  @override
  Stream<CommonUIState> mapEventToState(
    AddClientEvent event,
  ) async* {
    if (event is CreateClient) {
      if (validData()) yield* mapCreateClient(event.countries);
    }
    else{
      var response=await getCountriesUseCase(unit);
        yield response.fold((l) => CommonUIState.error(l.errorMessage), (r) {
          allCountries=[];
        allCountries.addAll(r);
       changeCountryList(r);
       return CommonUIState.success(r);
     });
    }
  }

  Stream<CommonUIState> mapCreateClient(List<CountryEntity> countries) async* {
    var list=countries;
    yield CommonUIState.loading();
    var randomPass = generatePassword(true, true, true, true, 8);
    var model=RegistrationModel(
      first_name: firstNameController.text,
      last_name: lastNameTextController.text,
      email: emailTextController.text,
      password: randomPass,
      middle_names: middleNameController.text,
      postal_address_line_1: addressPostal.text,
      postal_address_state: stateAddressPostal.text,
      postal_address_postcode: pinCodePostal.text,
      postal_address_country: countryPostal.text.isNotEmpty?list.firstWhere((element) => element.fullName.toLowerCase()==countryPostal.text.toLowerCase()).id.toString():"",
      street_address_country: countryStreet.text.isNotEmpty?list.firstWhere((element) => element.fullName.toLowerCase()==countryStreet.text.toLowerCase()).id.toString():"",
      street_address_postcode: pinCodeStreet.text,
      street_address_state: stateAddressStreet.text,
      tel_home: _teleHomeController.value+homeTelephone.text,
      tel_mobile: _mobileCodeController.value+mobile.text,
      tel_work: _teleWorkController.value+workTelephone.text,
    );
    var response = await _createClientUseCase(model);
    yield response.fold((l) => CommonUIState.error(l.errorMessage),
        (r) => CommonUIState.success(r));
  }

  String generatePassword(bool _isWithLetters, bool _isWithUppercase,
      bool _isWithNumbers, bool _isWithSpecial, double _numberCharPassword) {
    //Define the allowed chars to use in the password
    String _lowerCaseLetters = "abcdefghijklmnopqrstuvwxyz";
    String _upperCaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    String _numbers = "0123456789";
    String _special = "@#=+!£\$%&?[](){}";

    //Create the empty string that will contain the allowed chars
    String _allowedChars = "";

    //Put chars on the allowed ones based on the input values
    _allowedChars += (_isWithLetters ? _lowerCaseLetters : '');
    _allowedChars += (_isWithUppercase ? _upperCaseLetters : '');
    _allowedChars += (_isWithNumbers ? _numbers : '');
    _allowedChars += (_isWithSpecial ? _special : '');

    int i = 0;
    String _result = "";

    //Create password
    while (i < _numberCharPassword.round()) {
      //Get random int
      int randomInt = Random.secure().nextInt(_allowedChars.length);
      //Get random char and append it to the password
      _result += _allowedChars[randomInt];
      i++;
    }

    return _result;
  }

  bool validData() {
    var result=emailTextController.text.isValidEmail &&
        firstNameController.text.isNotEmpty &&
        lastNameTextController.text.isNotEmpty;
    if(!result){
      changePage(0);
      if(firstNameController.text.isEmpty)
      _firstNameController.sink.addError(Strings.notEmpty);
      if(lastNameTextController.text.isEmpty)
      _lastNameController.sink.addError(Strings.notEmpty);
      if(emailTextController.text.isEmpty)
      _emailController.sink.addError(Strings.validEmail);
      if(emailTextController.text.isValidPass)
        _emailController.sink.addError(Strings.validEmail);
      if(mobile.text.isEmpty)
        _mobileNumberController.sink.addError(Strings.notEmpty);
    }
    return result;
  }

  sameAsPostalAddress() {
    addressPostal.text = addressStreet.text;
    cityPostal.text = cityStreet.text;
    stateAddressPostal.text = stateAddressStreet.text;
    countryPostal.text = countryStreet.text;
    pinCodePostal.text = pinCodeStreet.text;
    changePostalAddress(addressStreet.text);
    changePostalCity(cityStreet.text);
    changePostalState(stateAddressStreet.text);
    changePostalCountry(countryStreet.text);
    changePostalPin(pinCodeStreet.text);
  }

  clearPostalAddress(){
    addressPostal.clear();
    cityPostal.clear();
    stateAddressPostal.clear();
    countryPostal.clear();
    pinCodePostal.clear();
  }
  clearAllData(){
    clearFirstStep();
    clearSecondStep();
    changePage(0);
  }
setFirstStepError(){
  if(firstNameController.text.isEmpty){
   changeFirstName("");
  }
  if(lastNameTextController.text.isEmpty){
    changeLastName("");
  }
  if(emailTextController.text.isEmpty||!emailTextController.text.isValidEmail){
    changeEmail("");
  }
}
  clearFirstStep(){
    firstNameController.clear();
    middleNameController.clear();
    lastNameTextController.clear();
    emailTextController.clear();
    _firstNameController=BehaviorSubject<String>();
    _lastNameController=BehaviorSubject<String>();
    _middleNameController=BehaviorSubject<String>();
    _emailController=BehaviorSubject<String>();
    _mobileNumberController=BehaviorSubject<String>();
  }

  clearSecondStep(){
    workTelephone.clear();
    homeTelephone.clear();
    mobile.clear();
    addressStreet.clear();
    cityStreet.clear();
    stateAddressStreet.clear();
    countryStreet.clear();
    pinCodeStreet.clear();
    clearPostalAddress();
  }

  changePage(int index){
    pageController.animateToPage(index, duration: Duration(milliseconds: 500), curve: Curves.easeInOut);
  }

}
