import 'dart:async';
import 'package:awesome_card/awesome_card.dart' as card;
import 'package:bloc/bloc.dart';
import 'package:credit_card/credit_card_model.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/rxdart.dart';
import 'package:stripe_payment/stripe_payment.dart';
import 'package:technology/domain/usecases/update_purchase/update_purchase_usecase.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';
import 'package:technology/others/utils/stripe/stripe_service.dart';
import './bloc.dart';

@injectable
class PaymentBloc extends Bloc<PaymentEvent, CommonUIState> {
  final testModeController = BehaviorSubject.seeded(true);
  final UpdatePurchaseUseCase updatePurchaseUseCase;

  PaymentBloc(this.updatePurchaseUseCase);
  Function (bool) get changeTestMode => testModeController.sink.add;

  Stream<bool> get testMode => testModeController.stream;
  static final model = CreditCardModel(
       "4000002760003184",
      "12/25",
//      amount: "100",
      "Card Holder",
     "666",
      false
  );

  final creditCardModelController = BehaviorSubject.seeded(model);
  String amount="";

  Function (CreditCardModel) get changeCreditCard =>
      creditCardModelController.sink.add;

  Stream<CreditCardModel> get creditCard => creditCardModelController.stream;

  @override
  CommonUIState get initialState => CommonUIState.initial();

  @override
  Stream<CommonUIState> mapEventToState(PaymentEvent event,) async* {
    if(event is CreatePurchase) {
      yield CommonUIState.loading();
      StripeService.init();
      var card = creditCardModelController.value;
      var response = await StripeService.payViaExistingCard(
          amount: 200.toString(), currency: "USD", card: CreditCard(
        number: card?.cardNumber,
        cvc: card?.cvvCode,
        expMonth: int.tryParse(card.expiryDate.split("/")[0]),
        expYear: int.tryParse(card.expiryDate.split("/")[1]),
      ));
      if (response.success) {
        print(response.message);
        var data=await updatePurchaseUseCase(event.product);
        yield data.fold((l) => CommonUIState.error(l.errorMessage), (r) => CommonUIState.success(r));
//      showSnackBar(context, response.message, false);
      }
      else {
     yield CommonUIState.error(response.message);
      }
    }
  }
}