import 'package:meta/meta.dart';
import 'package:technology/domain/entity/product_entity.dart';

@immutable
abstract class PaymentEvent {}
class CreatePurchase extends PaymentEvent{
  final ProductEntity product;
  CreatePurchase(this.product);
}