import 'package:meta/meta.dart';
import 'package:technology/data/models/commission/commission_request_model.dart';

@immutable
abstract class DashBoardEvent {}
class GetCommissionData extends DashBoardEvent{
  final CommissionRequestModel model;
  GetCommissionData(this.model);
}
