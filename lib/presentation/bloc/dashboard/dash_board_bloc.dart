import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/rxdart.dart';
import 'package:technology/data/models/commission/commission_request_model.dart';
import 'package:technology/domain/entity/dashboard_entity.dart';
import 'package:technology/domain/usecases/dashboard/get_commission_use_case.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';
import './bloc.dart';
@injectable
class DashBoardBloc extends Bloc<DashBoardEvent, CommonUIState<DashBoardEntity>> {
  final GetCommissionUseCase _getCommissionUseCase;

  
  final _lastDateController=BehaviorSubject<DateTime>();
  Function(DateTime) get changeLastDate=>_lastDateController.sink.add;
  Stream<DateTime> get lastDate=>_lastDateController.stream;

  final _startDateController=BehaviorSubject<DateTime>();
  Function(DateTime) get changeStartDate=>_startDateController.sink.add;
  Stream<DateTime> get startDate=>_startDateController.stream;

  Stream<String> get startLastDate=>Rx.combineLatest2<DateTime,DateTime,String>(startDate, lastDate, (a, b) => "${DateFormat("dd MMM yyyy").format(a)} - ${DateFormat("dd MMM yyyy").format(b)}") ;
  
  Stream<List<DateTime>> get startLastList=>Rx.combineLatest2<DateTime,DateTime,List<DateTime>>(startDate, lastDate, (a, b) => [a,b]);
  DashBoardBloc(this._getCommissionUseCase);
  @override
  CommonUIState<DashBoardEntity> get initialState => CommonUIState.initial();

  @override
  Stream<CommonUIState<DashBoardEntity>> mapEventToState(
    DashBoardEvent event,
  ) async* {

    if(event is GetCommissionData){
      yield* mapGetCommission(event.model);
    }
  }

  Stream<CommonUIState<DashBoardEntity>> mapGetCommission(CommissionRequestModel model)async*{
    yield CommonUIState<DashBoardEntity>.loading();
    var response=await  _getCommissionUseCase(model);
      yield    response.fold(
              (l) => CommonUIState<DashBoardEntity>.error(l.errorMessage),
              (r) => CommonUIState<DashBoardEntity>.success(r));
  }
}
