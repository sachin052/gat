import 'dart:async';
import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/rxdart.dart';
import 'package:technology/data/aws_cognito/aws_cognito_helper.dart';
import 'package:technology/data/models/signup/user_registration_model.dart';
import 'package:technology/domain/usecases/sign_up/sign_up_usecase.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';
import 'package:technology/others/utils/validators.dart';
import 'package:technology/presentation/bloc/base_bloc.dart';


import './bloc.dart';
@injectable
class SignUpBloc extends Bloc<SignupEvent, CommonUIState> implements BaseBloc{
  final SignUpUseCase _signUpUseCase;
//  final Api api;
  // First Name Field
  BehaviorSubject<String> _firstNameController = BehaviorSubject<String>();




  Function(String) get changeFirstName => _firstNameController.sink.add;
  Stream<String> get firstName => _firstNameController.stream.transform(notEmptyValidator);
  TextEditingController firstNameController = TextEditingController();

  // Last Name Field
  BehaviorSubject<String> _lastNameController = BehaviorSubject<String>();
  Function(String) get changeLastName => _lastNameController.sink.add;
  Stream<String> get lastName => _lastNameController.stream.transform(notEmptyValidator);
  TextEditingController lastNameTextController = TextEditingController();

  // Email Field
  final _emailController = BehaviorSubject<String>();
  Function(String) get changeEmail => _emailController.sink.add;
  Stream<String> get email => _emailController.stream.transform(validateEmail);
  TextEditingController emailTextController = TextEditingController();

  // Refer Field
  final _referIdController = BehaviorSubject<String>();
  Function(String) get changeReferId => _referIdController.sink.add;
  Stream<String> get referId => _referIdController.stream.transform(notEmptyValidator);
  TextEditingController referIdController = TextEditingController();

  // Password Field
  final _passwordController = BehaviorSubject<String>();
  Function(String) get changePassword => _passwordController.sink.add;
  Stream<String> get password => _passwordController.stream.transform(validatePassword);
  TextEditingController passwordTextController= TextEditingController();

  //Confirm Password
  final _cPasswordController = BehaviorSubject<String>();
  Function(String) get changeConfirmPassword => _cPasswordController.sink.add;
  // Used combineLatest2Stream for validations
  Stream<String> get confirmPassword =>
      CombineLatestStream.combine2(_passwordController, _cPasswordController,
              (password, cpassword) {
            if (password == cpassword) return cpassword;
            _cPasswordController.addError("Password Mismatched");
            return null;
          });

  Stream<bool> get isValid =>CombineLatestStream.combine5(firstName, lastName, email, password,
      confirmPassword, (a, b, c, d, e) => true);

  TextEditingController confirmPassTextController = TextEditingController();
  @override
  CommonUIState get initialState => CommonUIState.initial();
  SignUpBloc(this._signUpUseCase);
  @override
  Stream<CommonUIState> mapEventToState(
    SignupEvent event,
  ) async* {
    if(_isValidForm()){
      yield CommonUIState.loading();
      yield* mapSignUpdEvent();
    }

  }

  Stream<CommonUIState> mapSignUpdEvent()async*{
    var model=RegistrationModel(
      email: emailTextController.text.trim(),
      first_name: firstNameController.text.trim(),
      last_name: lastNameTextController.text.trim(),
      password: passwordTextController.text.trim(),
      referring_client_id: referIdController.text.trim().isEmpty?"1000":referIdController.text.trim(),
    );
    if(referIdController.text.isNotEmpty){
      var dio =  Dio();
      var request=await dio.getUri(Uri.parse("https://tzqehx0xp8.execute-api.ap-southeast-1.amazonaws.com/dev/clients/check-client-id/${referIdController.text}"));
      print(request.data);
      if(request.data["payload"]){
        var response=await _signUpUseCase(model);
        yield response.fold(
                (failure) => CommonUIState.error(failure.errorMessage),
                (poolData) => CommonUIState.success(poolData));
      }
      else yield CommonUIState.error('Invalid referral id');
    }

    else{
      var response=await _signUpUseCase(model);
      yield response.fold(
              (failure) => failure.errorMessage.contains("500")?CommonUIState.success("You will receive an email (please check spam) containing a link to verify your email address."):CommonUIState.error(failure.errorMessage),
              (poolData) => CommonUIState.success(poolData));
    }
  }
  bool _isValidForm(){
    if (firstNameController.text.isNotEmpty &&
        emailTextController.text.isValidEmail &&
        lastNameTextController.text.isNotEmpty &&
        passwordTextController.text.isNotEmpty&&
        confirmPassTextController.text.isNotEmpty&&
        passwordTextController.text == confirmPassTextController.text) {
      return true;
    } else {
      if (firstNameController.text.isEmpty) {
        _firstNameController.addError("Must not be empty");
      }
      if (lastNameTextController.text.isEmpty) {
        _emailController.addError("Enter a valid email");
      }
      if (lastNameTextController.text.isEmpty) {
        _lastNameController.addError("Must not be empty");
      }
      if (passwordTextController.text.isEmpty) {
        _passwordController.addError("Must not be empty");
      }
      if (confirmPassTextController.text.isEmpty) {
        _cPasswordController.addError("Must not be empty");
      }

      return false;
    }
  }

  @override
  dispose() {
//    confirmPassTextController
    _cPasswordController.close();
    _passwordController.close();
    _referIdController.close();
    _emailController.close();
    _lastNameController.close();
    _firstNameController.close();
    confirmPassTextController.dispose();
    passwordTextController.dispose();
    referIdController.dispose();
    emailTextController.dispose();
    lastNameTextController.dispose();
    firstNameController.dispose();
  }



}
