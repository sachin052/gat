import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:technology/data/models/country_model.dart';
import 'package:technology/data/models/signup/user_registration_model.dart';
import 'package:technology/others/styles/strings.dart';
import 'package:technology/main.dart';
import 'package:technology/others/utils/images/images.dart';
import 'package:technology/presentation/bloc/profile/profile_bloc.dart';
import 'package:technology/presentation/commonwidgets/CustomInputField.dart';
import 'package:technology/presentation/commonwidgets/button/custom_button.dart';
import 'package:technology/presentation/commonwidgets/country_picker.dart';
import 'package:easy_localization/easy_localization.dart';
class UserAddressScreen extends StatefulWidget {
  @override
  _UserAddressScreenState createState() => _UserAddressScreenState();
}

class _UserAddressScreenState extends State<UserAddressScreen> {
  ProfileBloc profileBloc;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    profileBloc=BlocProvider.of<ProfileBloc>(context);
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8.0),
      child: SingleChildScrollView(
      child: StreamBuilder<bool>(
        stream: profileBloc.sameAsPostal,
        builder: (context, snapshot) {
    if(snapshot.data!=null&&!snapshot.data){
    profileBloc.clearPostalAddress();}
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal:8.0,),
              child: Text(Strings.streetAddress,style: context.subTitle2.copyWith(fontWeight: FontWeight.bold),).tr(),
            ),
              CustomInputField(
                controller: profileBloc.addressStreet,
                labelText: Strings.address,
//          stream: _signUpBloc.email,
//          myNode: emailNode,
//          nextNode: passwordNode,
                onTextChange: (text){
                  if(snapshot.data!=null&&snapshot.data)
                    profileBloc.addressPostal.text=profileBloc.addressStreet.text;
                },
                icon: Images.location,
              ),

            Row(
              children: <Widget>[
                Expanded(child:    CustomInputField(
            controller: profileBloc.cityStreet,
                  labelText: Strings.city,
                  onTextChange: (text){
              if(snapshot.data!=null&&snapshot.data)
                profileBloc.cityPostal.text=profileBloc.cityStreet.text;
                  },
//          stream: _signUpBloc.email,
//          myNode: emailNode,
//          nextNode: passwordNode,
//          onTextChange: _signUpBloc.changeEmail,
                  icon: Images.city,
                ),),
                Expanded(child:    CustomInputField(
            controller: profileBloc.stateAddressStreet,
                  labelText: Strings.state,
//          stream: _signUpBloc.email,
                  onTextChange: (text){
              if(snapshot.data!=null&&snapshot.data)
                profileBloc.stateAddressPostal.text=profileBloc.stateAddressStreet.text;
                  },
//          myNode: emailNode,
//          nextNode: passwordNode,
//          onTextChange: _signUpBloc.changeEmail,
                  icon: Images.state,
                ),),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(child: Stack(
                  children: <Widget>[
                    Positioned(
                        top: 30,
                        right: 5,
                        child: Icon(Icons.arrow_drop_down)),
                    StreamBuilder<List<CountryEntity>>(
                      stream: profileBloc.countryList,
                      builder: (context, items) {
                        return CustomInputField(
                controller: profileBloc.countryStreet,
                          isReadOnly: true,
                          hasSuffix: true,
                          labelText: Strings.country,

//          stream: _signUpBloc.email,
                          onTextChange: (text){
                  if(snapshot.data!=null&&snapshot.data)
                        profileBloc.countryPostal.text=profileBloc.countryStreet.text;
                          },
                          onCountryTap: (){

                            showCountryPicker(context, (c){
                        profileBloc.countryStreet.text=c.name;
                            },allowCountry: false,items: items.data);
                          },
//          myNode: emailNode,
//          nextNode: passwordNode,
//          onTextChange: _signUpBloc.changeEmail,
                          icon: Images.country,
                        );
                      }
                    ),
                  ],
                ),),
                Expanded(child:    CustomInputField(
            controller: profileBloc.pinCodeStreet,
                  labelText: Strings.pinCode,
                  inputType: TextInputType.phone,
                  onTextChange: (text){
              if(snapshot.data!=null&&snapshot.data)
                profileBloc.pinCodePostal.text=profileBloc.pinCodeStreet.text;
                  },
//          stream: _signUpBloc.email,
//          myNode: emailNode,
//          nextNode: passwordNode,
//          onTextChange: _signUpBloc.changeEmail,
                  icon: Images.pin,
                ),),
              ],
            ),
            Row(

              children: <Widget>[
                StreamBuilder<bool>(
                  stream: profileBloc.sameAsPostal,
                    initialData: false,
                    builder: (context, snapshot) {
                      if(snapshot.data!=null&&snapshot.data){
                      profileBloc.sameAsPostalAddress();
                      }
                      return Checkbox(onChanged:(v){
                      profileBloc.changePostalSame(v);
                      },value: snapshot.data,);
                    }
                ),
                Text(Strings.samePostalAddress,style: context.subTitle2.copyWith(fontWeight: FontWeight.w600),).tr(),
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal:8.0,),
              child: Text(Strings.postalAddress,style: context.subTitle2.copyWith(fontWeight: FontWeight.bold),).tr(),
            ),
            CustomInputField(
        controller: profileBloc.addressPostal,
              labelText: Strings.address,
//        stream: profileBloc.postalAddress,
//          myNode: emailNode,
//          nextNode: passwordNode,
//          onTextChange: _signUpBloc.changeEmail,
              icon: Images.location,
            ),

            Row(
              children: <Widget>[
                Expanded(child:    CustomInputField(
            controller: profileBloc.cityPostal,
                  labelText: Strings.city,
//            stream: profileBloc.postalCity,
//          myNode: emailNode,
//          nextNode: passwordNode,
//          onTextChange: _signUpBloc.changeEmail,
                  icon: Images.city,
                ),),
                Expanded(child:    CustomInputField(
            controller: profileBloc.stateAddressPostal,
                  labelText: Strings.state,
//            stream: profileBloc.postalState,
//          myNode: emailNode,
//          nextNode: passwordNode,
//          onTextChange: _signUpBloc.changeEmail,
                  icon: Images.state,
                ),),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(child:    Stack(
                  children: <Widget>[
                    Positioned(
                        top: 30,
                        right: 5,
                        child: Icon(Icons.arrow_drop_down)),
                    StreamBuilder<List<CountryEntity>>(
                      stream: profileBloc.countryList,
                      builder: (context, items) {
                        return CustomInputField(
                controller: profileBloc.countryPostal,
                          isReadOnly: true,
                          hasSuffix: true,
                          labelText: Strings.country,

//          stream: _signUpBloc.email,
                          onTextChange: (text){
                  if(snapshot.data!=null&&snapshot.data)
                        profileBloc.countryPostal.text=profileBloc.countryStreet.text;
//                              else profileBloc.countryPostal.text=text;
                          },
//                stream: profileBloc.postalCountry,
//          myNode: emailNode,
//          nextNode: passwordNode,
//          onTextChange: _signUpBloc.changeEmail,
                          icon: Images.country,
                          onCountryTap: (){
                            showCountryPicker(context, (c){
                        profileBloc.countryPostal.text=c.name;
                            },allowCountry: false,items: items.data);
                          },
                        );
                      }
                    ),
                  ],
                ),),
                Expanded(child:    CustomInputField(
            controller: profileBloc.pinCodePostal,
                  labelText: Strings.pinCode,
                  inputType: TextInputType.phone,
//            stream: profileBloc.postalPin,
//          myNode: emailNode,
//          nextNode: passwordNode,
//          onTextChange: _signUpBloc.changeEmail,
                  icon: Images.pin,
                ),),
              ],
            ),
              CustomButton(onTap: (){
                profileBloc.add(UpdateUserAddress());
              },child: Text(Strings.saveCreate.tr(),style: context.button.copyWith(color: Colors.white),),)
          ],);
        }
      ),
    ),);
  }
}
