import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smart_select/smart_select.dart';
import 'package:technology/data/models/getuser/get_user_response.dart';
import 'package:technology/main.dart';
import 'package:technology/presentation/bloc/profile/profile_bloc.dart';
import 'package:technology/presentation/commonwidgets/button/custom_button.dart';
class KnowledgeScreen extends StatefulWidget {
  @override
  _KnowledgeScreenState createState() => _KnowledgeScreenState();
}

class _KnowledgeScreenState extends State<KnowledgeScreen> {
  ProfileBloc profileBloc;
  bool tradedCFX;
  bool tradedOptions;
  bool haveDiploma;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    profileBloc=BlocProvider.of<ProfileBloc>(context);
    profileBloc.userModel.listen((event) {
      var data=event.payload.client;
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        if(this.mounted)
        setState(() {
        tradedCFX=data.knowledgeTradedLeveraged==1?true:false;
        tradedOptions=data.knowledgeTradedOptions==1?true:false;
        haveDiploma=data.knowledgeQualification==1?true:false;
      });});
    });
  }
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<UserModel>(
      stream: profileBloc.userModel,
      builder: (context, snapshot) {
        if(snapshot.data==null||haveDiploma==null)return Container();
        return Container(child: Column(
          children: [
            SizedBox(height: 10,),

            SmartSelect<String>.single(
              title: "Have you traded CFDs, FX or other leveraged products before?",
              modalType: S2ModalType.bottomSheet,
              value: tradedCFX?"Yes":"No",
  tileBuilder: (context, state) {
              return S2Tile.fromState(state,
                title: Text("Have you traded CFDs, FX or other leveraged products before?",style: context.subTitle2,),
                );
            },
              choiceItems: ["Yes", "No"]
                  .map((e) => S2Choice(value: e, title: e))
                  .toList(), onChange: (S2SingleState<String> value) {
              setState(() {
                tradedCFX=value.value=="Yes"?true:false;
              });
            },
            ),
            SmartSelect<String>.single(
              title: "Have you traded Options before?",
              modalConfig: S2ModalConfig(title: "Select",),
              // choiceHeaderStyle: S2ChoiceHeaderStyle(textStyle: context.caption),
              // choiceStyle: S2ChoiceStyle(titleStyle: context.subTitle1,subtitleStyle: context.subTitle2,),
              modalHeaderStyle: S2ModalHeaderStyle(
                textStyle: context.subTitle1,
              ),
              tileBuilder: (context, state) {
                return S2Tile.fromState(state,
                  title: Text("Have you traded Options before?",style: context.subTitle2,),
                  value: tradedOptions?"Yes":"No",);
              },
              modalType: S2ModalType.bottomSheet,
              value: tradedOptions?"Yes":"No",
              onChange: (va) {
                setState(() {
                  tradedOptions=va.value=="Yes"?true:false;
                });
              },
              choiceItems: ["Yes", "No"]
                  .map((e) => S2Choice(value: e, title: e))
                  .toList(),
            ),
            SmartSelect<String>.single(
              title: "Do you have diploma or higher qualification in Commerce, Economics, Accounting, Finance or similar?",
              modalConfig: S2ModalConfig(title: "Select",),
              // choiceHeaderStyle: S2ChoiceHeaderStyle(textStyle: context.caption),
              // choiceStyle: S2ChoiceStyle(titleStyle: context.subTitle1,subtitleStyle: context.subTitle2,),
              modalHeaderStyle: S2ModalHeaderStyle(
                textStyle: context.subTitle1,
              ),
              tileBuilder: (context, state) {
                return S2Tile.fromState(state,
                  title: Text("Do you have diploma or higher qualification in Commerce, Economics, Accounting, Finance or similar?",
                    style: context.subTitle2,),
                  value: haveDiploma?"Yes":"No",);
              },
              modalType: S2ModalType.bottomSheet,
              value: haveDiploma?"Yes":"No",
              onChange: (va) {
                setState(() {
                  haveDiploma=va.value=="Yes"?true:false;
                });
              },
              choiceItems: ["Yes", "No"]
                  .map((e) => S2Choice(value: e, title: e))
                  .toList(),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: CustomButton(child: Text("Save",style: context.button.copyWith(color: Colors.white),),onTap: (){
                var map={
                  "knowledge_traded_leveraged":tradedCFX?1:0,
                  "knowledge_traded_options":tradedOptions?1:0,
                  "knowledge_qualification":haveDiploma?1:0};
                profileBloc.add(UpdatePersonal(HashMap.from(map)));
              }),
            )
          ],
        ),);
      }
    );
  }
}
