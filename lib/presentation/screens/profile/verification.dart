
import 'dart:collection';
import 'dart:io';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';
import 'package:technology/data/local_data_source/local_data_source.dart';
import 'package:technology/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:smart_select/smart_select.dart';
import 'package:technology/data/models/country_model.dart';
import 'package:technology/data/models/document_model.dart';
import 'package:technology/others/di/injection.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';
import 'package:technology/others/utils/loading_indicator.dart';
import 'package:technology/others/utils/routes/router.gr.dart';
import 'package:technology/presentation/bloc/profile/profile_bloc.dart';
import 'package:technology/presentation/bloc/profile/verification/verifications_bloc.dart';
import 'package:technology/presentation/commonwidgets/CustomInputField.dart';
import 'package:technology/presentation/commonwidgets/button/custom_button.dart';
import 'package:technology/presentation/commonwidgets/snackbar/snackbar.dart';

class VerificationScreen extends StatefulWidget {
  @override
  _VerificationScreenState createState() => _VerificationScreenState();
}

class _VerificationScreenState extends State<VerificationScreen> {
  ProfileBloc profileBloc;
  VerificationsBloc verificationsBloc;
  LocalDataSource localDataSource;
  int countryId;
  int countryId2;
  int selectedDocId;
  int selectedDocId2;
  String imagePath;
  String imagePath2;
  TextEditingController docNumberController;
  TextEditingController docNumber2Controller;

  String dob=DateFormat('yyyy-MMM-dd').format(DateTime.now());
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    docNumberController=TextEditingController();
    docNumber2Controller=TextEditingController();
    profileBloc = BlocProvider.of<ProfileBloc>(context);
    verificationsBloc = getIt<VerificationsBloc>();
    localDataSource=getIt<LocalDataSource>();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: BlocListener<VerificationsBloc, CommonUIState>(
        bloc: verificationsBloc,
        listener: (_,state){
          state.maybeWhen(orElse: (){},success: (s){
            if(s is String){
              showSnackBar(context, s, false);
            }
          });
        },
        child: BlocBuilder<VerificationsBloc, CommonUIState>(
            bloc: verificationsBloc,
            builder: (c, snapshot) {
              return snapshot.when(
                  initial: () => buildHomeWidget(),
                  success: (data) => buildHomeWidget(),
                  loading: () => LinearProgressIndicator(),
                  error: (e) => Container(
                        child: Text(e),
                      ));
            }),
      ),
    );
  }

  Widget buildHomeWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        StreamBuilder<List<CountryEntity>>(
            stream: profileBloc.countryList,
            initialData: [],
            builder: (c, snapshot) {
              if (snapshot.data != null && snapshot.data.isNotEmpty)
                return SmartSelect<String>.single(
                    title: "Issue Country",
                    placeholder: countryId==null?"Select":snapshot.data.firstWhere((element) => element.id==countryId).fullName,
                    choiceItems: snapshot.data
                        .map(
                          (e) => S2Choice(
                              value: e.id.toString(), title: e.fullName),
                        )
                        .toList(),
                    onChange: (S2SingleState<String> value) {
                      setState(() {
                        countryId=int.tryParse(value.value);
                      });
                      verificationsBloc.add(GetDocumentList(value.value,1));
                    },
                    value: null);
              return Container();
            }),
        StreamBuilder<DocumentModel>(
            stream: verificationsBloc.documentList,
            initialData: null,
            builder: (context, docs) {
              if (docs.data == null) return Container();
              return SmartSelect<String>.single(
                  title: "Document",
                  placeholder: selectedDocId==null?"Select":docs.data.payload.firstWhere((element) => element.id==selectedDocId,orElse: (){

                  }).nickname,
                  choiceItems: docs.data.payload
                      .map(
                        (e) =>
                            S2Choice(value: e.id.toString(), title: e.nickname),
                      )
                      .toList(),
                  onChange: (S2SingleState<String> value) {
                    setState(() {
                      selectedDocId=int.tryParse(value.value);
                    });
                    // verificationsBloc.add(GetDocumentList(value.value));
                  },
                  value: null);
            }),
        Visibility(
          visible: selectedDocId!=null,
          child: GestureDetector(
            onTap: ()async{
              DateTime newDateTime = await showRoundedDatePicker(
                context: context,

                initialDate: DateTime.now(),
                firstDate: DateTime(DateTime.now().year - 100),
                lastDate: DateTime(DateTime.now().year + 1),
                borderRadius: 16,
              );
              setState(() {
                dob=DateFormat('yyyy-MMM-dd').format(newDateTime);
              });
            },
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Row(children: [
                Text("Expiry",style: context.subTitle1,),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(dob,style: context.body2.copyWith(color: Colors.black54),),
                      SizedBox(width: 10,),
                      Icon(Icons.arrow_forward_ios,size: 14,color: Colors.grey,),
                      SizedBox(width: 5,),
                    ],),
                ),
              ],),
            ),
          ),
        ),
        CustomInputField(labelText: "Document Number",controller: docNumberController,),
        StreamBuilder<DocumentModel>(
          stream: verificationsBloc.documentList,
          builder: (context, snapshot) {
            if(snapshot?.data?.payload?.firstWhere((element) => element.id==selectedDocId,orElse: (){

            })?.detail1Label==null)return Container();
            return CustomInputField(labelText: snapshot.data.payload.firstWhere((element) => element.id==selectedDocId).detail1Label);
          }
        ),
        // CustomInputField(labelText: "Document Number"),
       Padding(
         padding: const EdgeInsets.symmetric(horizontal: 16.0,vertical: 8.0),
         child: Text("Document Front",style: context.subTitle1,),
       ),
        GestureDetector(
          onTap: ()async{
            showAlertDialog(context,true);
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: AspectRatio(
              aspectRatio: 16 / 7,
              child: Container(
                child: Visibility(
                  visible: imagePath==null,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [

                      Icon(Icons.upload_file),
                      Text("Upload Document")
                  ],),
                ),
                decoration:
                    BoxDecoration(
                      color: Colors.grey.withOpacity(.2),
                        image: imagePath!=null?DecorationImage(image: FileImage(File(imagePath)),fit: BoxFit.cover):null
                    )),
              ),
            ),
        ),
        StreamBuilder<List<CountryEntity>>(
            stream: profileBloc.countryList,
            initialData: [],
            builder: (context, snapshot) {
              if (snapshot.data != null && snapshot.data.isNotEmpty)
                return SmartSelect<String>.single(
                    title: "Issue Country",
                    placeholder: countryId2==null?"Select":snapshot.data.firstWhere((element) => element.id==countryId2).fullName,
                    choiceItems: snapshot.data
                        .map(
                          (e) => S2Choice(
                          value: e.id.toString(), title: e.fullName),
                    )
                        .toList(),
                    onChange: (S2SingleState<String> value) {
                      setState(() {
                        countryId2=int.tryParse(value.value);
                      });
                      verificationsBloc.add(GetDocumentList(value.value,2));
                    },
                    value: null);
              return Container();
            }),
        StreamBuilder<DocumentModel>(
            stream: verificationsBloc.documentList2,
            initialData: null,
            builder: (context, docs) {
              if (docs.data == null) return Container();
              return SmartSelect<String>.single(
                  title: "Document",
                  placeholder: selectedDocId2==null?"Select":docs.data.payload.firstWhere((element) => element.id==selectedDocId2,orElse: (){}).nickname,
                  choiceItems: docs.data.payload
                      .map(
                        (e) =>
                        S2Choice(value: e.id.toString(), title: e.nickname),
                  )
                      .toList(),
                  onChange: (S2SingleState<String> value) {
                    selectedDocId2=int.tryParse(value.value);
                    // verificationsBloc.add(GetDocumentList(value.value));
                  },
                  value: null);
            }),
        CustomInputField(labelText: "Document Number",controller: docNumber2Controller,),
        Visibility(
          visible: selectedDocId2!=null,
          child: GestureDetector(
            onTap: ()async{
              DateTime newDateTime = await showRoundedDatePicker(
                context: context,

                initialDate: DateTime.now(),
                firstDate: DateTime(DateTime.now().year - 100),
                lastDate: DateTime(DateTime.now().year + 1),
                borderRadius: 16,
              );
              setState(() {
                dob=DateFormat('yyyy-MMM-dd').format(newDateTime);
              });
            },
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Row(children: [
                Text("Expiry",style: context.subTitle1,),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(dob,style: context.body2.copyWith(color: Colors.black54),),
                      SizedBox(width: 10,),
                      Icon(Icons.arrow_forward_ios,size: 14,color: Colors.grey,),
                      SizedBox(width: 5,),
                    ],),
                ),
              ],),
            ),
          ),
        ),
        StreamBuilder<DocumentModel>(
            stream: verificationsBloc.documentList2,
            initialData: null,
            builder: (context, snapshot) {
              if(snapshot?.data?.payload?.firstWhere((element) => element.id==selectedDocId2,orElse: (){})?.detail1Label==null)return Container();
              return CustomInputField(labelText: snapshot.data.payload.firstWhere((element) => element.id==selectedDocId2).detail1Label);
            }
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0,vertical: 8.0),
          child: Text("Document Front",style: context.subTitle1,),
        ),
        GestureDetector(
          onTap: ()async{
            showAlertDialog(context,false);
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: AspectRatio(
              aspectRatio: 16 / 7,
              child: Container(
                  child: Visibility(
                    visible: imagePath2==null,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [

                        Icon(Icons.upload_file),
                        Text("Upload Document")
                      ],),
                  ),
                  decoration:
                  BoxDecoration(
                      color: Colors.grey.withOpacity(.2),
                      image: imagePath2!=null?DecorationImage(image: FileImage(File(imagePath2)),fit: BoxFit.cover):null
                  )),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: CustomButton(onTap: ()async{
            var data=await localDataSource.getUserData();
            var map={"client_id":data.payload.client.id,
              "document_issue_country_id":countryId,
              "document_issue_country_id2":countryId2,
              "document_category_id":selectedDocId,
              "document_category_id2":selectedDocId2,
              "document_number":docNumberController.text,
              "document_number2":docNumber2Controller.text,
              "file_id_front":verificationsBloc.image1Location,
              "file_id_front2":verificationsBloc.image2Location
            };
            verificationsBloc.add(DoVerification(HashMap.from(map)));
          },child: Text("Save",style: context.button.copyWith(color: Colors.white),),),
        )
      ],
    );
  }
  showAlertDialog(BuildContext context, bool isFirst) {

    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Camera"),
      onPressed:  () async{
        Navigator.of(context).pop();
        final pickedFile = await ImagePicker().getImage(source: ImageSource.camera);
        setState(() {
          if (isFirst){
            verificationsBloc.add(UploadFile(pickedFile.path, 1));
          imagePath=pickedFile.path;
          }
          else{
            verificationsBloc.add(UploadFile(pickedFile.path, 2));
            imagePath2=pickedFile.path;}
        }
        );
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Gallery"),
      onPressed:  () async{
        Navigator.of(context).pop();
        final pickedFile = await ImagePicker().getImage(source: ImageSource.gallery);
        setState(() {
          if (isFirst){
            verificationsBloc.add(UploadFile(pickedFile.path, 1));
            imagePath=pickedFile.path;
          }
          else{
            verificationsBloc.add(UploadFile(pickedFile.path, 2));
            imagePath2=pickedFile.path;}
        });
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Choose Media Type",style: context.subTitle1,),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  @override
  void dispose() {
    // TODO: implement dispose
    docNumber2Controller.dispose();
    docNumberController.dispose();
    super.dispose();

  }
}
