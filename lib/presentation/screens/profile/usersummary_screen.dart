import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:technology/others/styles/strings.dart';
import 'package:technology/others/utils/images/images.dart';
import 'package:technology/presentation/bloc/profile/profile_bloc.dart';
import 'package:technology/presentation/commonwidgets/CustomInputField.dart';
import 'package:technology/main.dart';
import 'package:technology/presentation/commonwidgets/button/custom_button.dart';

class UserSummaryScreen extends StatefulWidget {
  @override
  _UserSummaryScreenState createState() => _UserSummaryScreenState();
}

class _UserSummaryScreenState extends State<UserSummaryScreen> {
  ProfileBloc profileBloc;
  @override
  void initState() {
    profileBloc=BlocProvider.of<ProfileBloc>(context);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(child: Padding(
      padding: const EdgeInsets.symmetric(horizontal:24.0),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
//          Padding(
//            padding: const EdgeInsets.symmetric(horizontal:8.0,),
//            child: Text(Strings.contactDetails,style: context.subTitle2.copyWith(fontWeight: FontWeight.bold),),
//          ),
            Row(children: <Widget>[
              Expanded(
                child: CustomInputField(
              controller: profileBloc.nickNameController,
                  labelText: "Nick Name",
//            stream: profileBloc.firstName,
//            myNode: firstNameNode,
//            nextNode: middleNameNode,
//            onTextChange: _addClientBloc.changeFirstName,
                  icon: Images.personIcon,
                ),
              ),
              Expanded(
                child: CustomInputField(
              controller: profileBloc.firstNameController,
                  labelText: Strings.firstName,
              stream: profileBloc.firstName,
//            myNode: firstNameNode,
//            nextNode: middleNameNode,
//            onTextChange: _addClientBloc.changeFirstName,
                  icon: Images.personIcon,
                ),
              ),
           
            ],),
           Row(children: <Widget>[
             Expanded(
               child: CustomInputField(
//            controller: _addClientBloc.middleNameController,
                 labelText: Strings.middleName,
//            stream: _addClientBloc.middleName,
//            myNode: middleNameNode,
//            nextNode: lastNameNode,
//            onTextChange: _addClientBloc.changeMiddleName,
                 icon: Images.personIcon,
               ),
             ),
             Expanded(
               child: CustomInputField(
              controller: profileBloc.lastNameTextController,
                 labelText: Strings.lastName,
              stream: profileBloc.lastName,
//            myNode: lastNameNode,
//            nextNode: emailNode,
//            onTextChange: _addClientBloc.changeLastName,
                 icon: Images.personIcon,
               ),
             ),
           ],),
            CustomInputField(
              controller: profileBloc.emailTextController,
              labelText: Strings.email,
              stream: profileBloc.email,
isReadOnly: true,
//            myNode: emailNode,
              action: TextInputAction.done,
//          nextNode: passwordNode,
//            onTextChange: _addClientBloc.changeEmail,
              icon: Images.emailIcon,
            ),
CustomButton(
  onTap: (){
    profileBloc.add(UpdateUserSummary());
  },
  child: Text("Save",style: context.button.copyWith(color: Colors.white),),)
//          CustomButton(onTap: (){
//            BlocProvider.of<AddClientBloc>(context).changePage(1);
//          },child: Text(Strings.next,style: context.button.copyWith(color: Colors.white),),)
          ],),
      ),
    ),);
  }
}
