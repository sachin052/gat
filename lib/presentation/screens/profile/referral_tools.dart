import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:technology/main.dart';
import 'package:technology/others/styles/colors.dart';
import 'package:technology/others/styles/strings.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';
import 'package:technology/others/utils/images/images.dart';
import 'package:technology/presentation/bloc/profile/profile_bloc.dart';
import 'package:technology/presentation/commonwidgets/CustomInputField.dart';
import 'package:technology/presentation/commonwidgets/button/custom_button.dart';
import 'package:easy_localization/easy_localization.dart';

class ReferralTools extends StatefulWidget {

  @override
  _ReferralToolsState createState() => _ReferralToolsState();
}

class _ReferralToolsState extends State<ReferralTools> {
  ProfileBloc _profileBloc;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _profileBloc=BlocProvider.of<ProfileBloc>(context);
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8.0),
      child: SingleChildScrollView(
        child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(Strings.referrerDetails,style: context.subTitle2.copyWith(fontWeight: FontWeight.bold),).tr(),
          sizedBox25,
          StreamBuilder<String>(
            stream: _profileBloc.referrerId,
            initialData: "",
            builder: (context, snapshot) {
              return Text("Your referral number is ${snapshot.data}");
            }
          ),
          sizedBox25,
          Text(Strings.referrerLinks,style: context.subTitle2.copyWith(fontWeight: FontWeight.bold)).tr(),
          sizedBox25,
          StreamBuilder<String>(
            stream: _profileBloc.referrerId,
            builder: (context, snapshot) {
              return Text("https://referrer.gat.technology/pages/register?affid=${snapshot.data}");
            }
          ),

          StreamBuilder<String>(
            stream: _profileBloc.referrerId,
            builder: (context, snapshot) {
              return MaterialButton(onPressed: () {
                Clipboard.setData(new ClipboardData(text: "https://referrer.gat.technology/pages/register?affid=${snapshot.data}")).then((value) {
                  Scaffold.of(context).showSnackBar(
                      SnackBar(content:Text(Strings.referralLinkCopied).tr()));
                });
              },color: AppColors.darkBlue,child: Text(Strings.copyLink,style: context.button.copyWith(color: Colors.white),).tr(),);
            }
          ),
          sizedBox25,
          StreamBuilder<String>(
            stream: _profileBloc.referrerId,
            builder: (context, snapshot) {
              return Container(
                alignment: Alignment.center,
                child: QrImage(
                  data: "https://referrer.gat.technology/pages/register?affid=${snapshot.data}",
                  version: QrVersions.auto,
                  size: 200.0,
                ),
              );
            }
          ),
          Container(
              alignment: Alignment.center,
              child: Text(Strings.scanQRCode).tr()),
          sizedBox25,
          CustomButton(child: Text(Strings.sendNow,style: context.button.copyWith(color: Colors.white),).tr(),onTap: (){
         _showModal();
          },)
        ],
    ),
      ),);
  }
  void _showModal() {
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (BuildContext context) {
        return Container(
            padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).viewInsets.bottom,
        ),
        // You can wrap this Column with Padding of 8.0 for better design
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                alignment: Alignment.center,
                child: Text(Strings.sendReferralInvitation,style: context.headLine6,textAlign: TextAlign.center,).tr()),
            sizedBox25,
            sizedBox25,
            CustomInputField(labelText: Strings.email, icon: Images.emailIcon,stream: _profileBloc.referrerEmail,onTextChange: _profileBloc.changeReferrerEmail,),

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(Strings.chooseLanguage,style: context.subTitle2.copyWith(fontWeight: FontWeight.bold)).tr(),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: StreamBuilder<List<bool>>(
                  stream: _profileBloc.selectedLang,
                  initialData: [true,false],
                  builder: (context, snapshot) {
                    return ToggleButtons(
                      fillColor: AppColors.darkBlue,
                      selectedColor: Colors.white,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical:4.0,horizontal: 8.0),
                          child: Text(Strings.english).tr(),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical:4.0,horizontal: 8.0),
                          child: Text(Strings.chinese).tr(),
                        ),
                      ],
                      isSelected: snapshot.data,
                      onPressed: (int index) {
                        var items=snapshot.data;
                        items[0]=!items[0];
                        items[1]=!items[1];
                        _profileBloc.changeSelectedLang(items);
                      },
                    );
                  }
              ),
            ),
            BlocBuilder<ProfileBloc,CommonUIState>(
              bloc: _profileBloc,
              builder: (_,state)=>CustomButton(
                uiState: state,
                child: Text(Strings.sendCode,style: context.button.copyWith(color: Colors.white),).tr(),onTap: (){
//                        _modalBottomSheetMenu();
              ExtendedNavigator.root.pop();
              _profileBloc.add(SendReferralInvite());
              },),

            )
          ]
          ),
        ),
        );
      },
    );
  }
}
