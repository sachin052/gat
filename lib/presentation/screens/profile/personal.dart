import 'dart:collection';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';
import 'package:smart_select/smart_select.dart';
import 'package:technology/data/models/country_model.dart';
import 'package:technology/data/models/getuser/get_user_response.dart';
import 'package:technology/others/styles/colors.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';
import 'package:technology/presentation/bloc/profile/profile_bloc.dart';
import 'package:technology/main.dart';
import 'package:technology/presentation/commonwidgets/CustomInputField.dart';
import 'package:technology/presentation/commonwidgets/button/custom_button.dart';
class Personal extends StatefulWidget {
  @override
  _PersonalState createState() => _PersonalState();
}

class _PersonalState extends State<Personal> {

  String item="Male";
  int countryId=0;
  ProfileBloc profileBloc;
  TextEditingController controller;
  String dob="";
  int genderId=0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    profileBloc=BlocProvider.of<ProfileBloc>(context);
    controller=TextEditingController();
    profileBloc.userModel.listen((event) {
      countryId=event.payload.client.placeOfBirthCountryId;
      controller.text=event.payload.client.placeOfBirthCity;
      dob=event.payload.client.dateOfBirth!=null?DateFormat('yyyy-MM-dd').format(event.payload.client.dateOfBirth):null;
      genderId=event.payload.client.genderId;
    });
  }
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<UserModel>(
      stream: profileBloc.userModel,
      builder: (context, userData) {
        if(userData.data==null)return Container();
        else
        {
          return Container(child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // available configuration for single choice
                SmartSelect<String>.single(
                    title: "Gender",
                    modalType: S2ModalType.bottomSheet,
                    placeholder: genderId!=null?genderId==1?"Male":"Female":null,
                    choiceItems: [
                      S2Choice(value: "Male", title: 'Male'),
                      S2Choice(value: "Female", title: 'Female'),
                    ].toList(),

                    onChange: (S2SingleState<String> value) {
                      setState(() {
                        genderId=value.value=="Male"?1:2;
                      });
                    },
                    value: genderId!=null?genderId==1?"Male":"Female":null,),
                StreamBuilder<List<CountryEntity>>(
                    stream: profileBloc.countryList,
                    initialData: [],
                    builder: (context, snapshot) {
                      if(snapshot.data!=null&&snapshot.data.isNotEmpty)
                        return SmartSelect<String>.single(
                          title: "Select Country",
                          placeholder: countryId!=null?snapshot.data.firstWhere((element) => countryId==element.id).fullName:"Select",
                          choiceItems: snapshot.data.map((e) =>  S2Choice(value: e.id.toString(), title: e.fullName),).toList(),
                          onChange: (S2SingleState<String> value) {
                            setState(() {
                              countryId=int.tryParse(value.value);
                            });
                          },
                          value: null,);
                      return Container();
                    }
                ),
                GestureDetector(
                  onTap: ()async{
                    DateTime newDateTime = await showRoundedDatePicker(
                      context: context,

                      initialDate: DateTime.now(),
                      firstDate: DateTime(DateTime.now().year - 100),
                      lastDate: DateTime(DateTime.now().year + 1),
                      borderRadius: 16,
                    );
                   setState(() {
                     dob=DateFormat('yyyy-MM-dd').format(newDateTime);
                   });
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(16),
                    child: Row(children: [
                      Text("Date of birth",style: context.subTitle1,),
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(dob??"Select",style: context.body2.copyWith(color: Colors.black54),),
                            SizedBox(width: 10,),
                            Icon(Icons.arrow_forward_ios,size: 14,color: Colors.grey,),
                            SizedBox(width: 5,),
                          ],),
                      ),
                    ],),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Row(children: [
                    Text("Place of Birth City",style: context.subTitle1,),
                    SizedBox(width: 20,),
                    Expanded(child: TextField(
                        controller: controller,
                        decoration: InputDecoration(
                          contentPadding:  EdgeInsets.symmetric(horizontal: 10.0),
                          labelStyle: Theme.of(context).textTheme.bodyText2,
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: AppColors.colorPrimary, width: 0.8),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey, width: 0.8),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.red, width: 0.8),
                          ),
                          focusedErrorBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.red, width: 0.8),
                          ),
                    ))),

                  ],),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: BlocBuilder<ProfileBloc,CommonUIState>(
                    builder: (context, snapshot) {
                      return CustomButton(
                          // uiState: snapshot,
                          child: Text("Save",style: context.button.copyWith(color: Colors.white),),onTap: (){
                            var map={
                              "gender_id":genderId.toString(),
                              "date_of_birth":dob.toString(),
                              "place_of_birth_city":controller.text,
                              "place_of_birth_country_id":countryId.toString(),
                            };
                            
                            profileBloc.add(UpdatePersonal(HashMap.from(map)));
                      });
                    }
                  ),
                )
              ],
            ),
          ),);
        }
      }
    );
  }
}
