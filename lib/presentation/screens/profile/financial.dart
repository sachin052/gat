import 'dart:collection';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smart_select/smart_select.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';
import 'package:technology/data/models/country_model.dart';
import 'package:technology/data/models/emp_status.dart';
import 'package:technology/data/models/getuser/get_user_response.dart';
import 'package:technology/others/styles/colors.dart';
import 'package:technology/presentation/bloc/profile/profile_bloc.dart';
import 'package:technology/presentation/commonwidgets/CustomInputField.dart';
import 'package:technology/main.dart';
import 'package:technology/presentation/commonwidgets/button/custom_button.dart';
class Financial extends StatefulWidget {
  @override
  _FinancialState createState() => _FinancialState();
}

class _FinancialState extends State<Financial> {
  ProfileBloc profileBloc;
  int taxCountryId=0;
  int empStatusId=0;
  List<S2Choice<String>> empStatus;
  double _annualIncome = 0.0;
  double _netWorth = 0.0;
  double _deposit = 0.0;
  TextEditingController companyNameController= TextEditingController();
  bool tfnExp=false;
  bool usCitizen=false;
  bool usTaxResident=false;
  TextEditingController taxFileNumberController=TextEditingController();
  ScrollController scrollController;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    scrollController=ScrollController();
    profileBloc = BlocProvider.of<ProfileBloc>(context);
    profileBloc.userModel.listen((event) {
      var data=event.payload.client;
      companyNameController.text=data.employer;
      empStatusId= data.employmentStatusId;
      _annualIncome=data.annualIncome?.toDouble();
      _netWorth=data.netWorth?.toDouble();
      _deposit=data.intendedDeposit?.toDouble();
      taxCountryId=data.taxCountryId;
      tfnExp=data.tfnExemption==1?true:false;
      usCitizen=data.usCitizen==1?true:false;
      usTaxResident=data.usTaxResident==1?true:false;
      taxFileNumberController.text=data?.taxFileNumber??"";
      companyNameController.text=data.employer;

    });

  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: StreamBuilder<UserModel>(
          stream: profileBloc.userModel,
          builder: (context, userData) {
            if (userData.data == null)
              return Container();
            else {

              return ListView(

                controller: scrollController,
                children: [
                  StreamBuilder<List<Status>>(
                      stream: profileBloc.empList,
                      initialData: [],
                      builder: (context, snapshot) {
                        if (snapshot.data == null || snapshot.data.isEmpty)
                          return Container();
                        empStatus = snapshot.data
                            .map((e) => S2Choice(
                                value: e.id.toString(), title: e.fileBy))
                            .toList();
                        return SmartSelect<String>.single(
                          title: "Employment Status",
                          placeholder: userData
                              .data.payload.client?.employmentStatusId!=null?empStatus?.firstWhere((element) =>
                                  element.value ==
                                  userData
                                      .data.payload.client?.employmentStatusId
                                      .toString(),)
                              ?.title:"Select",
                          modalType: S2ModalType.bottomSheet,
                          choiceItems: empStatus,
                          onChange: (S2SingleState<String> value) {
                            empStatusId=int.tryParse(value.value);
                          },
                          value: userData
                              .data.payload.client?.employmentStatusId!=null?empStatus
                              .firstWhere((element) =>
                                  element.value ==
                                  userData
                                      .data.payload.client.employmentStatusId
                                      .toString())
                              .value:null,
                        );
                      }),
                  CustomInputField(
                    labelText: "Name of the company",
                    controller: companyNameController,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Row(
                      children: [
                        Text("Annual Income",style: context.subTitle1,),
                      ],
                    ),
                  ),
                  SfSlider(
                    min: 0.0,
                    max: 200000.0,
                    value: _annualIncome??0.0,
                    interval: 50000.0,
                    labelPlacement: LabelPlacement.onTicks,
                    // showTicks: true,
                    showLabels: true,
                    labelFormatterCallback: (s,v){
                      return NumberFormat.compact().format(int.tryParse(v));
                    },
                    tooltipTextFormatterCallback: (s,v)=>NumberFormat.compact().format(double.tryParse(v)),
                    showTooltip: true,
                    minorTicksPerInterval: 1,
                    onChanged: (dynamic value){
                        setState(() {
                          _annualIncome = value;
                        });
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 12),
                    child: Row(
                      children: [
                        Text("Net Worth",style: context.subTitle1,),
                      ],
                    ),
                  ),
                  SfSlider(
                    min: 0.0,
                    max: 2000000.0,
                    value: _netWorth??0.0,
                    interval: 500000,
                    labelPlacement: LabelPlacement.onTicks,
                    // showTicks: true,
                    showLabels: true,
                    labelFormatterCallback: (s,v){
                      return NumberFormat.compact().format(int.tryParse(v));
                    },
                    tooltipTextFormatterCallback: (s,v)=>NumberFormat.compact().format(double.tryParse(v)),
                    showTooltip: true,
                    minorTicksPerInterval: 1,
                    onChanged: (dynamic value){
                        setState(() {
                          _netWorth = value;
                        });
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 12),
                    child: Wrap(
                      children: [
                        Text("How much do you intend to deposit with SCM over the next 12 months (USD)",style: context.subTitle1,),
                      ],
                    ),
                  ),
                  SfSlider(
                    min: 0.0,
                    max: 100000.0,
                    value: _deposit??0.0,
                    interval: 20000,
                    labelPlacement: LabelPlacement.onTicks,
                    // showTicks: true,
                    showLabels: true,
                    labelFormatterCallback: (s,v){
                      return NumberFormat.compact().format(int.tryParse(v));
                    },
                    tooltipTextFormatterCallback: (s,v)=>NumberFormat.compact().format(double.tryParse(v)),
                    showTooltip: true,
                    minorTicksPerInterval: 1,
                    onChanged: (dynamic value){
                        setState(() {
                          _deposit = value;
                        });
                    },
                  ),
                  SizedBox(height: 20,),
                  StreamBuilder<List<CountryEntity>>(
                      stream: profileBloc.countryList,
                      initialData: [],
                      builder: (context, snapshot) {
                        if (snapshot.data != null &&
                            snapshot.data.isNotEmpty)
                          return SmartSelect<String>.single(
                            title: "Tax Country",
                            placeholder: taxCountryId!=null&&taxCountryId!=0?snapshot.data
                                .firstWhere(
                                    (element) => taxCountryId == element.id)
                                .fullName:"Select",
                            choiceItems: snapshot.data
                                .map(
                                  (e) => S2Choice(
                                      value: e.id.toString(), title: e.fullName),
                                )
                                .toList(),
                            onChange: (S2SingleState<String> value) {
                              taxCountryId=int.tryParse(value.value);
                            },
                            value: taxCountryId!=null&&taxCountryId!=0?snapshot.data
                                .firstWhere(
                                    (element) => taxCountryId == element.id)
                                .fullName:null,
                          );
                        return Container();
                      }),
                  SmartSelect<String>.single(
                    title: "TFN Exemption",
                    modalType: S2ModalType.bottomSheet,
                    value: tfnExp?"Yes":"No",

                    choiceItems: ["Yes", "No"]
                        .map((e) => S2Choice(value: e, title: e))
                        .toList(), onChange: (S2SingleState<String> value) {
                      setState(() {
                        tfnExp=value.value=="Yes"?true:false;
                      });
                  },
                  ),
                  CustomInputField(
                    labelText: "Tax File Number",controller: taxFileNumberController,
                  ),
                  SmartSelect<String>.single(
                    title: "US Citizen",
                    modalType: S2ModalType.bottomSheet,
                    value: usCitizen?"Yes":"No",
                    onChange: (v) {
                      setState(() {
                        usCitizen=v.value=="Yes"?true:false;
                      });
                    },
                    choiceItems: ["Yes", "No"]
                        .map((e) => S2Choice(value: e, title: e))
                        .toList(),
                  ),
                  SmartSelect<String>.single(
                    title: "US Tax Resident",
                    modalType: S2ModalType.bottomSheet,
                    value: usTaxResident?"Yes":"No",
                    onChange: (v) {
                      setState(() {
                        usTaxResident=v.value=="Yes"?true:false;
                      });
                    },
                    choiceItems: ["Yes", "No"]
                        .map((e) => S2Choice(value: e, title: e))
                        .toList(),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: CustomButton(child: Text("Save",style: context.button.copyWith(color: Colors.white),),onTap: (){
                      var map={"employment_status_id":empStatusId,
                        "employer":companyNameController.text,
                        "annual_income":_annualIncome,
                        "net_worth":_netWorth,
                        "intended_deposit":_deposit,
                        "tax_country_id":taxCountryId,
                        "tfn_exemption":tfnExp?1:0,
                        "tax_file_number":taxFileNumberController.text,
                        "us_citizen":usCitizen?1:0,
                        "us_tax_resident":usTaxResident?1:0
                      };
                      profileBloc.add(UpdatePersonal(HashMap.from(map)));
                    }),
                  )
                ],
              );
            }
          }),
    );
  }
}
