import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:technology/data/models/country_model.dart';
import 'package:technology/others/styles/strings.dart';
import 'package:technology/others/utils/images/images.dart';
import 'package:technology/presentation/bloc/profile/profile_bloc.dart';
import 'package:technology/presentation/commonwidgets/CustomInputField.dart';
import 'package:technology/presentation/commonwidgets/button/custom_button.dart';
import 'package:technology/main.dart';
import 'package:technology/presentation/commonwidgets/country_picker.dart';
class UserContact extends StatefulWidget {
  @override
  _UserContactState createState() => _UserContactState();
}

class _UserContactState extends State<UserContact> {
  ProfileBloc profileBloc;
  @override
  void initState() {
    profileBloc=BlocProvider.of<ProfileBloc>(context);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<CountryEntity>>(
      stream: profileBloc.countryList,
      builder: (context, snapshot) {
        return Container(
          padding: const EdgeInsets.all(8.0),
          child: SingleChildScrollView(
            child: Column(children: <Widget>[
            StreamBuilder<String>(
              stream: profileBloc.codeWork,
              builder: (context, code) {
                return CustomInputField(
                      controller: profileBloc.workTelephone,
                  labelText: Strings.teleWork,
//            stream: _addClientBloc.email,
//            myNode: emailNode,
                  action: TextInputAction.done,
                  countryCode: code.data,
                  onCountryTap: () {
                    showCountryPicker(context, (c) {
                      profileBloc
                          .changeCodeWork(c.phoneCode);
                    }, items: snapshot.data,allowCountry: true);

                  },
//          nextNode: passwordNode,
//            onTextChange: _addClientBloc.changeEmail,
                  inputType: TextInputType.number,
                  icon: Images.telephone,
                );
              }
            ),
            StreamBuilder<String>(
              stream: profileBloc.codeHome,
              builder: (context, code) {
                return CustomInputField(
                      controller: profileBloc.homeTelephone,
                  labelText: Strings.teleHome,
//            stream: _addClientBloc.email,
//            myNode: emailNode,
                countryCode: code.data,
                  action: TextInputAction.done,
//          nextNode: passwordNode,
//            onTextChange: _addClientBloc.changeEmail,
                  inputType: TextInputType.number,
                  icon: Images.telephone,
                  onCountryTap: () {
                    showCountryPicker(context, (c) {
                      profileBloc
                          .changeCodeHome(c.phoneCode);
                    }, items: snapshot.data,allowCountry: true);
                  },
                );
              }
            ),
            StreamBuilder<String>(
              stream: profileBloc.mobileCode,
              builder: (context, code) {
                return CustomInputField(
                      controller: profileBloc.mobile,
                  labelText: Strings.mobile,
//            stream: _addClientBloc.email,
//            myNode: emailNode,
                  action: TextInputAction.done,
//          nextNode: passwordNode,
                  inputType: TextInputType.number,
                  countryCode: code.data,
                  onCountryTap: () {
                    showCountryPicker(context, (c) {
                      profileBloc
                          .changeMobileCode(c.phoneCode);
                    }, items: snapshot.data,allowCountry: true);
                  },
//            onTextChange: _addClientBloc.changeEmail,
                  icon: Images.mobile,
                );
              }
            ),
              CustomButton(onTap: (){
                profileBloc.add(UpdateUserContact());
              },child: Text("Save",style: context.button.copyWith(color: Colors.white),),)
        ],),
          ),);
      }
    );
  }
}
