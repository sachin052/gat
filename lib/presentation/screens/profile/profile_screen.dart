import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:technology/data/models/getuser/get_user_response.dart';
import 'package:technology/main.dart';
import 'package:technology/others/styles/colors.dart';
import 'package:technology/others/styles/strings.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';
import 'package:technology/others/utils/loading_indicator.dart';
import 'package:technology/others/utils/routes/router.gr.dart';
import 'package:technology/presentation/bloc/home_screen/bloc.dart';
import 'package:technology/presentation/bloc/profile/profile_bloc.dart';
import 'package:technology/presentation/commonwidgets/snackbar/snackbar.dart';
import 'package:technology/presentation/screens/profile/bank_account.dart';
import 'package:technology/presentation/screens/profile/financial.dart';
import 'package:technology/presentation/screens/profile/knowledge.dart';
import 'package:technology/presentation/screens/profile/personal.dart';
import 'package:technology/presentation/screens/profile/referral_tools.dart';
import 'package:technology/presentation/screens/profile/user_address_screen.dart';
import 'package:technology/presentation/screens/profile/user_contact.dart';
import 'package:technology/presentation/screens/profile/usersummary_screen.dart';
import 'package:technology/presentation/screens/profile/verification.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> with SingleTickerProviderStateMixin{
  TabController _tabController;
  ProfileBloc profileBloc;
  @override
  void initState() {
    _tabController =  TabController(length: 9, vsync: this);
    profileBloc=BlocProvider.of<ProfileBloc>(context);
//    profileBloc.add(GetSupportedCountries());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      backgroundColor: AppColors.darkBlue,
      body: BlocListener<ProfileBloc,CommonUIState<UserModel>>(
      listener: (_,state){
        state.maybeWhen(orElse: (){},success: (data){
          if(data.payload?.defaultClientId==0){
            showSnackBar(context, "Profile updated successfully", false);
          }
          else if(data.payload==null)
            ExtendedNavigator.root.pushAndRemoveUntil(Routes.loginScreen, (router)=>false);
        }
        );
      },
        child: BlocBuilder<ProfileBloc,CommonUIState<UserModel>>(
            builder: (context, state) {
              print(state);
              return state?.when(initial: ()=>LoadingBar(), success: (data)=>DefaultTabController(
                length: 3,
                child: NestedScrollView(
                  headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
                    return <Widget>[
                      SliverAppBar(
                        expandedHeight: 150.0,
                        floating: false,
                        pinned: false,
                        flexibleSpace: FlexibleSpaceBar(
                          centerTitle: true,
//                    title: Text("Collapsing Toolbar",
//                        style: TextStyle(
//                          color: Colors.white,
//                          fontSize: 16.0,
//                        )),
                          background: Container(
                            padding: const EdgeInsets.all(16.0),
                            height: 400,color:AppColors.lightBlue,child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(children: <Widget>[
                                Text(Strings.currentSda, style: context.subTitle2.copyWith(
                                    fontWeight: FontWeight.w400,
                                    color: Colors.white),).tr(),
                                sizedBox25,
                                Text(data?.payload?.client?.productsNickname??"", style: context.subTitle2.copyWith(
                                    fontWeight: FontWeight.w800,
                                    color: Colors.white),),
                                Expanded(
                                    child: Container(

                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          children: <Widget>[
                                            GestureDetector(
                                                onTap: (){
                                                  if(EasyLocalization.of(context).locale.languageCode=="en"){
                                                    EasyLocalization.of(context).locale=const Locale.fromSubtags(
                                                        languageCode: 'zh');
                                                  }
                                                  else{
                                                    EasyLocalization.of(context).locale=Locale('en', 'US');
                                                  }
//                                              BlocProvider.of<HomeScreenBloc>(context).add(DataFetched());
//                                              EasyLocalization.of(context).locale=Locale('en', 'US');
                                                  EasyLocalization.of(context).locale=const Locale.fromSubtags(
                                                      languageCode: 'zh');

                                                  changeLocale(Locale('en', 'US'));

                                                  changeLocale(const Locale.fromSubtags(
                                                      languageCode: 'zh'));
                                                  Phoenix.rebirth(context);
                                                  userData=null;
                                                  ExtendedNavigator.root.push(Routes.splash,arguments: SplashArguments(isFromProfile: true));
                                                },
                                                child: Icon(Icons.translate,color: Colors.white,)),
                                            GestureDetector(
                                                onTap: (){
                                                  profileBloc.add(LogOutUser());
                                                },
                                                child: Icon(Icons.power_settings_new,color: Colors.white,)),
                                          ],
                                        )))
                              ],),
                              Text("(Max 2.15%)", style: context.caption.copyWith(
                                  fontWeight: FontWeight.w800,
                                  color: Colors.white),),
                              Row(children: <Widget>[
                                Text("Referring Client", style: context.subTitle2.copyWith(
                                    fontWeight: FontWeight.w400,
                                    color: Colors.white),),
                                sizedBox25,
                                Text("John Dave", style: context.subTitle2.copyWith(
                                    fontWeight: FontWeight.w800,
                                    color: Colors.white),),
                              ],),
                              Row(children: <Widget>[
                                Text("Client Status", style: context.subTitle2.copyWith(
                                    fontWeight: FontWeight.w400,
                                    color: Colors.white),),
                                sizedBox25,
                                Text("Active", style: context.subTitle2.copyWith(
                                    fontWeight: FontWeight.w800,
                                    color: Colors.white),),
                              ],),
                              Row(children: <Widget>[
                                Text("Price", style: context.subTitle2.copyWith(
                                    fontWeight: FontWeight.w400,
                                    color: Colors.white),),
                                sizedBox25,
                                Text("US \$3000", style: context.subTitle2.copyWith(
                                    fontWeight: FontWeight.w800,
                                    color: Colors.white),),
                              ],),
                            ],),),
                        ),
                      ),
                      SliverPersistentHeader(
                        delegate: _SliverAppBarDelegate(
                          TabBar(
                            controller: _tabController,
                            labelColor: Colors.white,
                            indicatorColor: Colors.white,
                            isScrollable: true,

                            unselectedLabelColor: Colors.white70,
                            tabs: [
                              Tab( text: Strings.userSummary.tr()),
                              Tab(text: Strings.contact.tr()),

                              Tab( text: Strings.address.tr()),
                              Tab( text: Strings.referralTools.tr()),
                              Tab( text: Strings.personal.tr()),
                              Tab( text: "Bank Account"),
                              Tab( text: "Financial"),
                              Tab( text: "Verification"),
                              Tab( text: "Knowledge"),
                            ],
                          ),
                        ),
                        pinned: true,
                      ),
                    ];
                  },
                  body: Padding(
                    padding: const EdgeInsets.all(0.0),
                    child: Container(

                      decoration: BoxDecoration( color: Colors.white,borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0),topRight: Radius.circular(10.0))),
                      child: TabBarView(
                        children: [
                          UserSummaryScreen(),
                          UserContact(),
                          UserAddressScreen(),
                          ReferralTools(),
                          Personal(),
                          BankAccount(),
                          Financial(),
                          VerificationScreen(),
                          KnowledgeScreen()
                        ],
                        controller: _tabController,),
                    ),
                  ),
                ),
              ), loading: ()=>LoadingBar(), error: (e)=>Container(child: Center(child: Text(e,textAlign: TextAlign.center,),),));
            }
        ),
      ),
    );
  }
}
class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate(this._tabBar);

  final TabBar _tabBar;

  @override
  double get minExtent => _tabBar.preferredSize.height;
  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Stack(
      children: <Widget>[
//        Column(
//          children: <Widget>[
//            Container(color: AppColors.lightBlue,),
//            sizedBox25,
//            sizedBox25,
//          ],
//        ),
      Container(color: AppColors.lightBlue,width: MediaQuery.of(context).size.width,),
        new Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0),topRight: Radius.circular(10.0)),  color: AppColors.darkBlue,),
          child: _tabBar,
        ),
      ],
    );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return false;
  }
}