import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:smart_select/smart_select.dart';
import 'package:technology/data/models/country_model.dart';
import 'package:technology/data/models/currency/Payload.dart';
import 'package:technology/data/models/getuser/get_user_response.dart';
import 'package:technology/others/utils/images/images.dart';
import 'package:technology/presentation/bloc/profile/profile_bloc.dart';
import 'package:technology/presentation/commonwidgets/CustomInputField.dart';
import 'package:technology/presentation/commonwidgets/button/custom_button.dart';
import 'package:technology/main.dart';
class BankAccount extends StatefulWidget {
  @override
  _BankAccountState createState() => _BankAccountState();
}

class _BankAccountState extends State<BankAccount> {
  ProfileBloc profileBloc;
  int countryId;
  int currencyId;
  TextEditingController nameController=TextEditingController();
  TextEditingController bankNameController=TextEditingController();
  TextEditingController bankAddressController=TextEditingController();
  TextEditingController swiftController=TextEditingController();
  TextEditingController bsbController=TextEditingController();
  TextEditingController acNumberController=TextEditingController();
  TextEditingController usdtWallterController=TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    profileBloc=BlocProvider.of<ProfileBloc>(context);
    profileBloc.userModel.listen((event) {
      var data=event.payload.client;
      countryId=data.accountCountry;
      currencyId=data.accountCurrency;
      nameController.text=data.accountBeneficiaryName;
      bankNameController.text=data.accountBankName;
      bankAddressController.text=data.accountBankAddress;
      swiftController.text=data.accountSwift;
      bsbController.text=data.accountBsb;
      acNumberController.text=data.accountNumber;
      usdtWallterController.text=data.usdtWalletAddress;
    });
  }
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<UserModel>(
      stream: profileBloc.userModel,
      builder: (context, userModel) {
       if(userModel.data==null)return Container();
      else{
         return SingleChildScrollView(
           child: Column(children: [
             // StreamBuilder<List<CountryEntity>>(
             //     stream: profileBloc.countryList,
             //     initialData: [],
             //     builder: (context, snapshot) {
             //       if(snapshot.data!=null&&snapshot.data.isNotEmpty)
             //         return SmartSelect<String>.single(
             //           title: "Select Country",
             //           placeholder: countryId!=null?snapshot.data.firstWhere((element) => countryId==element.id).fullName:"Select",
             //           choiceItems: snapshot.data.map((e) =>  S2Choice(value: e.id.toString(), title: e.fullName),).toList(),
             //           onChange: (S2SingleState<String> value) {
             //             countryId=int.tryParse(value.value);
             //           },
             //           value: null);
             //       return Container();
             //     }
             // ),
             StreamBuilder<List<CurrenyModel>>(
                 stream: profileBloc.currencyList,
                 initialData: [],
                 builder: (context, currency) {
                   if(currency.data!=null&&currency.data.isNotEmpty)
                     return SmartSelect<String>.single(
                       title: "Select Currency",
                       placeholder: currencyId!=0&&currencyId!=null?currency.data.firstWhere((element) => currencyId==element.id).short_name:"Select",
                       choiceItems: currency.data.map((e) =>  S2Choice(value: e.id.toString(), title: e.full_name),).toList(),
                       onChange: (S2SingleState<String> value) {
                         // setState(() {
                         //   currencyId=int.tryParse(value.value);
                         // });
                       },
                       value: null,);
                   return Container();
                 }
             ),
             CustomInputField(labelText: "Account Beneficiary Name",controller: nameController,),
             CustomInputField(labelText: "Bank Name", controller: bankNameController,),
             CustomInputField(labelText: "Bank Address", controller: bankAddressController,),
             CustomInputField(labelText: "SWIFT Code", controller: swiftController,),
             CustomInputField(labelText: "BSB", controller: bsbController,),
             CustomInputField(labelText: "Account Number", controller: acNumberController,),
             CustomInputField(labelText: "USDT Wallet Address",controller: usdtWallterController, ),
             Padding(
               padding: const EdgeInsets.all(8.0),
               child: CustomButton(child: Text("Save",style: context.button.copyWith(color: Colors.white),),onTap: (){
                 var map={
                   "account_aba":nameController.text,
                   "account_bank_address":bankAddressController.text,
                   "account_bank_name":bankNameController.text,
                   "account_bsb":bsbController.text,
                   "account_country":countryId,
                   "account_currency":currencyId,
                   "account_number":acNumberController.text,
                   "usdt_wallet_address":usdtWallterController.text
                 };
                 profileBloc.add(UpdatePersonal(HashMap.from(map)));
               }),
             )
           ],),
         );
       }
      }
    );
  }
}
