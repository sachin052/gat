import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smart_select/smart_select.dart';
import 'package:technology/data/models/wallet/wallet_history_response.dart';
import 'package:technology/data/models/wallet/wallet_list_response.dart';
import 'package:technology/others/di/injection.dart';
import 'package:technology/others/styles/colors.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';
import 'package:technology/others/utils/loading_indicator.dart';
import 'package:technology/presentation/bloc/wallet/wallet_bloc.dart';
import 'package:technology/main.dart';
class WalletScreen extends StatefulWidget {
  @override
  _WalletScreenState createState() => _WalletScreenState();
}

class _WalletScreenState extends State<WalletScreen> {
  GlobalKey<S2State> collectionKey = GlobalKey<S2State>();
  GlobalKey<S2State> collectionKey2 = GlobalKey<S2State>();
  WalletBloc walletBloc;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    walletBloc=BlocProvider.of<WalletBloc>(context);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.darkBlue,
      appBar: AppBar(title: Text("Wallet",style: Theme.of(context).textTheme.headline6.copyWith(color: Colors.white),),
        centerTitle: true,backgroundColor: AppColors.darkBlue,elevation: 0,),
      body: Container(
        child: BlocBuilder<WalletBloc,CommonUIState>(
          bloc: walletBloc,
          builder: (c,state){
            return state.when(initial: ()=>LoadingBar(),
                success: (s)=>StreamBuilder<List<WalletModel>>(builder: (c,s){
                  if(s.data!=null&&s.data.isNotEmpty)
                    return Stack(
                      // fit: StackFit.expand,
                      children: [
                        Container(
                          height: 150,
                          padding: const EdgeInsets.all(16.0),
                          child:   Card(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                            color: AppColors.lightBlue,
                            child: ListView(children: [
                              Column(children: List.generate(s.data.length, (index) => SmartSelect<WalletModel>.single(

                                key: UniqueKey(),
                                choiceItems: s.data.map((e) => S2Choice<WalletModel>(
                                    value: e, title: e.isoAlpha3)).toList(),
                                title: "Change Wallet",
                                placeholder: s.data[index].isoAlpha3,
                                // choiceBuilder: (c,s,d)=>Text("Test"),
                                // choiceTitleBuilder: (a,b,c)=>Text("h"),

                                tileBuilder: (context, state) {
                                  return S2Tile.fromState(state,
                                    title: Text("Change Wallet",style: context.subTitle2.copyWith(color: Colors.white),),
                                    trailing: Icon(Icons.arrow_forward_ios_outlined,size: 15,color: Colors.white,),
                                    value: null,);
                                },
                                onChange: (e){
                                  walletBloc.add(GetWalletDetails(e.value.id.toString()));
                                }, value: s.data[index],)),),
                              StreamBuilder<String>(
                                  stream: walletBloc.walletDetails,
                                  initialData: "",
                                  builder: (context, snapshot) {
                                    return Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [

                                          Padding(
                                            padding: const EdgeInsets.symmetric(horizontal: 10),
                                            child: Text("Wallet Details ",style: context.subTitle2.copyWith(color: Colors.white),),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.symmetric(horizontal: 20),
                                            child: Text(snapshot.data,style: context.subTitle2.copyWith(color: Colors.white),),
                                          )
                                        ],),
                                    );
                                  }
                              ),

                            ],),
                          ),
                        ),
                        DraggableScrollableSheet(

                          initialChildSize: 0.7,
                          minChildSize: 0.7,
                          maxChildSize: 1.0,
                          builder: (BuildContext context, ScrollController scrollController) {
                          return   StreamBuilder<WalletHistoryResponse>(
                              stream: walletBloc.walletHistory,
                              builder: (context, snapshot) {
                                if(snapshot.data!=null&&snapshot.data.payload.isNotEmpty)
                                {
                                  var data=snapshot.data.payload;
                                  return Container(
                                    decoration: BoxDecoration(borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0),topRight: Radius.circular(10.0)),   color: Colors.white,),
                                    child: ListView(
                                      children: [
                                        // SizedBox(height: 50,),
                                        Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            children: List.generate(data.length, (index) => Container(
                                              padding: EdgeInsets.all(4),
                                              decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Colors.black12,width: 1.0))),
                                              child: Card(
                                                  elevation: 0.0,
                                                  child: Padding(
                                                    padding: const EdgeInsets.all(8.0),
                                                    child: Column(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      children: [
                                                        Row(
                                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,

                                                          children: [
                                                            Row(children: [
                                                              Text("Id:",style: Theme.of(context).textTheme.subtitle2.copyWith(color: AppColors.colorPrimary),),
                                                              SizedBox(width: 5,),
                                                              Text("${data[index].id.toString()}",style: Theme.of(context).textTheme.subtitle2.copyWith(fontWeight: FontWeight.bold),),
                                                            ],),
                                                            Text(DateFormat("dd-MMM-yyyy hh:mm a").format(data[index].createdDatetime.toUtc()),style: Theme.of(context).textTheme.subtitle2.copyWith(fontWeight: FontWeight.bold)),
                                                          ],),
                                                        SizedBox(height: 10,),
                                                        Row(
                                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,

                                                          children: [
                                                            Text("Credit",style: Theme.of(context).textTheme.subtitle2.copyWith(color: AppColors.colorPrimary),),
                                                            Text("Debit",style: Theme.of(context).textTheme.subtitle2.copyWith(color: AppColors.colorPrimary),)
                                                          ],),
                                                        Row(
                                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                          children: [
                                                            Text(data[index].credit!=null?data[index].credit.toString():" -- ",style: Theme.of(context).textTheme.subtitle2.copyWith(fontWeight: FontWeight.bold)),
                                                            Text(data[index].debit!=null?data[index].debit.toString():" -- ",style: Theme.of(context).textTheme.subtitle2.copyWith(fontWeight: FontWeight.bold)),
                                                          ],
                                                        ),
                                                        // SizedBox(height: 10,),
                                                        Text("Memo",style: Theme.of(context).textTheme.subtitle2.copyWith(color: AppColors.colorPrimary),),
                                                        Text(data[index].memo,style: Theme.of(context).textTheme.subtitle2.copyWith(fontWeight: FontWeight.bold),),

                                                      ],
                                                    ),
                                                  )),
                                            ))
                                        ),
                                        SizedBox(height: 50,),
                                      ],
                                    ),
                                  );
                                }
                                return Container();
                              }
                          );
                        },)
                      ],
                    );
                  return Container();
                },stream: walletBloc.walletList,),
                loading: ()=>LoadingBar(),
                error: (e)=>Center(child: Container(child: Text(e,style: context.subTitle2.copyWith(color: Colors.white),),)));
          },
        ),
      ),
    );
  }
}
