import 'package:animations/animations.dart';
import 'package:auto_route/auto_route.dart';
import 'package:awesome_card/awesome_card.dart';
import 'package:credit_card/credit_card_form.dart';
import 'package:credit_card/credit_card_model.dart';
import 'package:credit_card_type_detector/credit_card_type_detector.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:technology/domain/entity/product_entity.dart';
import 'package:technology/main.dart';
import 'package:technology/others/di/injection.dart';
import 'package:technology/others/styles/colors.dart';
import 'package:technology/others/styles/strings.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';
import 'package:technology/presentation/bloc/payment/bloc.dart';
import 'package:technology/presentation/commonwidgets/button/custom_button.dart';
import 'package:technology/presentation/commonwidgets/snackbar/snackbar.dart';
import 'package:easy_localization/easy_localization.dart';
const double _fabDimension = 56.0;

/// The demo page for [OpenContainerTransform].
class OpenContainerTransformDemo extends StatefulWidget {
  final ProductEntity productEntity;

  const OpenContainerTransformDemo({Key key, this.productEntity})
      : super(key: key);

  @override
  _OpenContainerTransformDemoState createState() {
    return _OpenContainerTransformDemoState();
  }
}

class _OpenContainerTransformDemoState
    extends State<OpenContainerTransformDemo> {
  ContainerTransitionType _transitionType = ContainerTransitionType.fade;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
//
      body: OpenContainerWrapper(
        transitionType: _transitionType,
        closedBuilder: (BuildContext _, VoidCallback openContainer) {
          return ProductCardItem(
            productEntity: widget.productEntity,
          );
        },
      ),
    );
  }
}

class OpenContainerWrapper extends StatelessWidget {
  const OpenContainerWrapper({
    this.closedBuilder,
    this.transitionType,
    this.onClosed,
    this.entity,
  });

  final OpenContainerBuilder closedBuilder;
  final ContainerTransitionType transitionType;
  final ClosedCallback<bool> onClosed;
  final ProductEntity entity;

  @override
  Widget build(BuildContext context) {
    return OpenContainer<bool>(
      transitionType: transitionType,
      openBuilder: (BuildContext context, VoidCallback _) {
        return _DetailsPage(
          entity: entity,
        );
      },
      onClosed: onClosed,
      tappable: false,
      closedElevation: 0.0,
      openElevation: 0.0,
      closedColor: Colors.transparent,
      closedBuilder: closedBuilder,
    );
  }
}

createProductItem(
    ProductEntity entity, BuildContext context, VoidCallback callback) {
  return Card(
      margin: const EdgeInsets.all(8.0),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      elevation: 3.0,
      child: Container(
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10.0),
                        bottomLeft: Radius.circular(10.0)),
                    color: entity.productColor),
              ),
            ),
            Expanded(
                flex: 20,
                child: Container(
                  color: Colors.white,
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
//                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Spacer(
                        flex: 2,
                      ),
                      Text(
                        entity.productTitle,
                        style: context.headLine6
                            .copyWith(fontWeight: FontWeight.bold),
                      ),
                      sizedBox25,
                      Text(
                        "US \$${entity.productPrice}",
                        style: context.headLine6
                            .copyWith(fontWeight: FontWeight.bold),
                      ),
                      Text(
                        "US \$${int.tryParse(entity.productPrice)+500}-\$500",
                        style: context.caption
                            .copyWith(fontWeight: FontWeight.bold),
                      ),
                      sizedBox25,
                      FittedBox(
                          child: Text(
                              "Maximum Commission : ${entity.maxCommission}%")),
                      Spacer(
                        flex: 5,
                      ),
                    ],
                  ),
                )),
            Expanded(
                flex: 8,
                child: Container(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.white,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Visibility(
                          visible: entity.isActive,
                          child: Text(
                            "Active",
                            style: context.subTitle2.copyWith(
                                color: Colors.blue,
                                fontWeight: FontWeight.w600),
                          )),
                      Padding(
                        padding: const EdgeInsets.only(right:8.0),
                        child: MaterialButton(
                          padding: const EdgeInsets.all(0.0),
                          color: Colors.blue[900],
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                          child: Text(
                            "Buy Now",
                            style: context.button.copyWith(color: Colors.white),
                          ),
                          onPressed: () {
                          callback.call();

                              // // set up the buttons
                              // Widget cancelButton = FlatButton(
                              //   child: Text(Strings.cancel).tr(),
                              //   onPressed:  () {
                              //     ExtendedNavigator.root.pop();
                              //   },
                              // );
                              // Widget continueButton = FlatButton(
                              //   child: Text(Strings.continueDialog).tr(),
                              //   onPressed:  () {
                              //     ExtendedNavigator.root.pop();
                              //     callback.call();
                              //     },
                              // );
                              //
                              // // set up the AlertDialog
                              // AlertDialog alert = AlertDialog(
                              //   title: Text(Strings.dialogTitle).tr(),
                              //   content: Text(Strings.dialogSuTitle).tr(),
                              //   actions: [
                              //     cancelButton,
                              //     continueButton,
                              //   ],
                              // );
                              //
                              // // show the dialog
                              // showDialog(
                              //   context: context,
                              //   builder: (BuildContext context) {
                              //     return alert;
                              //   },
                              // );


                          },
                        ),
                      )
                    ],
                  ),
                )),
          ],
        ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0), color: Colors.white),
        height: MediaQuery.of(context).size.height / 4,
      ));
}

class ProductCardItem extends StatelessWidget {
  final ProductEntity productEntity;
  final ContainerTransitionType _transitionType = ContainerTransitionType.fade;

  const ProductCardItem({this.productEntity});

//  final VoidCallback openContainer;

  @override
  Widget build(BuildContext context) {
//    const double height = 100.0;

    return OpenContainerWrapper(
        transitionType: _transitionType,
        entity: productEntity,
        closedBuilder: (BuildContext _, VoidCallback openContainer) {
          return createProductItem(productEntity, context, openContainer);
        });
  }
}

class _DetailsPage extends StatefulWidget {
  final ProductEntity entity;

  _DetailsPage({this.includeMarkAsDoneButton = true, this.entity});

  final bool includeMarkAsDoneButton;

  @override
  __DetailsPageState createState() => __DetailsPageState();
}

class __DetailsPageState extends State<_DetailsPage> {
  CreditCardType cardType;
  PaymentBloc paymentBloc;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PaymentBloc>(
      create: (c) => paymentBloc,
      child: Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: AppColors.colorPrimary),
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          title: Text(
            widget.entity.productTitle,
            style: context.headLine6.copyWith(color: AppColors.colorPrimary),
          ),
          actions: <Widget>[],
        ),
        body: BlocListener<PaymentBloc, CommonUIState>(
          listener: (_, state) {
            state.maybeWhen(
                success: (s) {
                  ExtendedNavigator.root.pop();
                  showSnackBar(context, s, false);
                },
                error: (e) {
                  showSnackBar(context, e, true);
                },
                orElse: () {});
          },
          child: Container(
            child: BlocBuilder<PaymentBloc, CommonUIState>(
              builder: (_, state) => SingleChildScrollView(
                child: StreamBuilder<bool>(
                    stream: paymentBloc.testMode,
                    initialData: true,
                    builder: (context, mode) {
                      return Column(
                        children: <Widget>[
                          sizedBox25,
                          // SwitchListTile(
                          //   value: mode.data,
                          //   title: Text("Test Card"),
                          //   onChanged: (value) {
                          //     paymentBloc.changeTestMode(value);
                          //   },
                          // ),
                          sizedBox25,
                          StreamBuilder<CreditCardModel>(
                              stream: paymentBloc.creditCard,
                              builder: (context, snapshot) {
                                if (snapshot.data == null) return Container();
                                var model = snapshot.data;
                                return CreditCard(
                                  cardNumber: model.cardNumber,
                                  cardExpiry: model.expiryDate,
                                  cardHolderName: model.cardHolderName,
                                  cvv: model.cvvCode,

//                                  cardType: cardType, // Optional if you want to override Card Type
                                  showBackSide: model.isCvvFocused,
                                  frontBackground: CardBackgrounds.black,
                                  backBackground: CardBackgrounds.white,
                                  showShadow: true,
                                );
                              }),
                          CreditCardForm(
//            themeColor: Colors.red,
                            onCreditCardModelChange: (CreditCardModel data) {
//                              data.amount=widget.entity.productPrice;
                              print(data.cardHolderName);
                              paymentBloc.changeCreditCard(data);
//                      setState(() {
//
//                        cardType=detectCCType(data.cardNumber);
//                      });
                            },
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: StreamBuilder<CreditCardModel>(
                                stream: paymentBloc.creditCardModelController,
                                builder: (context, card) {
                                  if (card.data == null) return Container();
                                  var cardData = card.data;
                                  return CustomButton(
                                    uiState: state,
                                    onTap: () async {
//                                      StripeService.init();
//                                      var response=await StripeService.payViaExistingCard(amount: cardData.amount,currency: "USD",card: stripe.CreditCard(
//                                        number: cardData.cardNumber.replaceAll(" ", ""),
//                                        cvc: cardData.cvvCode,
//                                        expMonth: int.tryParse(cardData.expiryDate.split("/")[0]),
//                                        expYear: int.tryParse(cardData.expiryDate.split("/")[1]),
//                                      ));
//                                      if(response.success)
//                                        showSnackBar(context, response.message, false);
//                                      else
//                                        showSnackBar(context, response.message, true);
                                      paymentBloc
                                          .add(CreatePurchase(widget.entity));
                                    },
                                    child: Text(
                                      "Pay Now ",
                                      style: context.button
                                          .copyWith(color: Colors.white),
                                    ),
                                  );
                                }),
                          )
                        ],
                      );
                    }),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    paymentBloc = getIt<PaymentBloc>();
    paymentBloc.amount = widget.entity.productPrice;
//    stripe.StripePayment.setOptions(
//        stripe.StripeOptions(publishableKey: "pk_test_aSaULNS8cJU6Tvo20VAXy6rp", merchantId: "Test", androidPayMode: 'test'));
  }
}
