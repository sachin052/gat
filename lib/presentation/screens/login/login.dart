import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:technology/others/di/injection.dart';
import 'package:technology/others/styles/colors.dart';
import 'package:technology/others/styles/strings.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';
import 'package:technology/others/utils/images/images.dart';
import 'package:technology/others/utils/routes/router.gr.dart';
import 'package:technology/presentation/bloc/login/bloc.dart';
import 'package:technology/presentation/commonwidgets/CustomInputField.dart';
import 'package:technology/presentation/commonwidgets/button/custom_button.dart';
import 'package:technology/presentation/commonwidgets/snackbar/snackbar.dart';


import '../../../main.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  LoginBloc _loginBloc;
  FocusNode emailNode;
  FocusNode passwordNode;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    emailNode=FocusNode();
    passwordNode=FocusNode();
    _loginBloc = getIt<LoginBloc>();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context,allowFontScaling: true);
    return Scaffold(
//      appBar: AppBar(
//        backgroundColor: Colors.transparent,
//        elevation: 0.0,
//        automaticallyImplyLeading: false,
//      ),
      body: BlocProvider<LoginBloc>(
        create: (c)=>_loginBloc,
        child: BlocListener<LoginBloc,CommonUIState>(
          listener: (_,state){
            state.maybeWhen(
                success: (data){
                  ExtendedNavigator.root.replace(Routes.homeScreen,arguments: HomeScreenArguments(dashBoardEntity: data));
                },
                error: (e)=>showSnackBar(context, e, true),
                orElse: (){});
          },
          child: BlocBuilder<LoginBloc,CommonUIState>(
            builder: (c,state)=>Padding(
              padding: const EdgeInsets.all(24.0),
              child: SingleChildScrollView(
                child: Column(
                   crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 20.0,
                    ),
                    SvgPicture.asset(Images.appIcon, semanticsLabel: 'Acme Logo'),
                    SizedBox(
                      height: 20.0,
                    ),
                    Text(
                      Strings.loginYourAccount,
                      style: Theme.of(context).textTheme.headline6,
                    ),
                    SizedBox(
                      height: 40.0,
                    ),
                    CustomInputField(
                      stream: _loginBloc.email,
                      controller: _loginBloc.emailTextController,
                      onTextChange: _loginBloc.changeEmail,
                      myNode: emailNode,
                      nextNode: passwordNode,
                      labelText: Strings.email,
                      icon: Images.emailIcon,
                    ),
                    sizedBox25,
                    CustomInputField(
                      myNode: passwordNode,
                      controller: _loginBloc.passController,
                      action: TextInputAction.done,
                      stream: _loginBloc.password,
                      icon: Images.lockIcon,
                      labelText: Strings.password,
                      isPassword: true,
                      onTextChange: _loginBloc.changePassword,
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Container(
                        alignment: Alignment.centerRight,
                        child: Text(
                          Strings.forgotPassword,
                          style: Theme.of(context).textTheme.caption.copyWith(
                              decoration: TextDecoration.underline,
                              fontWeight: FontWeight.w600),
                        )),
                    SizedBox(
                      height: 80.0,
                    ),
                    CustomButton(
                      onTap: (){
//                         ExtendedNavigator.rootNavigator.pushNamed(Routes.homeScreen);
                        _loginBloc.add(LoginUser());
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            Strings.continueButton,
                            style: Theme.of(context)
                                .textTheme
                                .button
                                .copyWith(color: Colors.white),
                          ),
                          SvgPicture.asset(Images.arrowRight,
                              semanticsLabel: 'Arrow Logo'),
                        ],
                      ),
                      uiState: state,
                    ),
                    SizedBox(
                      height: 40.0,
                    ),
                    GestureDetector(
                      onTap: (){
                        ExtendedNavigator.root.push(Routes.signUpScreen);
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            Strings.dontHaveAccount,
                            style: Theme.of(context)
                                .textTheme
                                .caption
                                .copyWith(fontWeight: FontWeight.w600),
                          ),
                          Text(
                            Strings.createAccount,
                            style: Theme.of(context).textTheme.caption.copyWith(
                                color: AppColors.colorPrimary,
                                fontWeight: FontWeight.w600,
                                fontSize: 11.0),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );

  }

}
