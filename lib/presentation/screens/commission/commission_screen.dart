import 'package:date_calc/date_calc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:technology/data/constants/AppConstatnt.dart';
import 'package:technology/data/models/commission/commission_request_model.dart';
import 'package:technology/domain/entity/commission_entity.dart';
import 'package:technology/domain/entity/dashboard_entity.dart';
import 'package:technology/domain/repo/user_repo.dart';
import 'package:technology/main.dart';
import 'package:technology/others/styles/colors.dart';
import 'package:technology/others/styles/strings.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';
import 'package:technology/others/utils/images/images.dart';
import 'package:technology/others/utils/loading_indicator.dart';
import 'package:technology/presentation/bloc/commission/commission_bloc.dart';
import 'package:technology/presentation/screens/dashboard/dashboard.dart';
import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;
import 'package:easy_localization/easy_localization.dart';
class CommissionScreen extends StatefulWidget {
  final DashBoardEntity dashBoardEntity;

  const CommissionScreen({Key key, this.dashBoardEntity}) : super(key: key);
  @override
  _CommissionScreenState createState() => _CommissionScreenState();
}

class _CommissionScreenState extends State<CommissionScreen> {
  CommissionBloc commissionBloc;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    commissionBloc=BlocProvider.of<CommissionBloc>(context);
    var date = DateCalc.now().subtractDay(14).toUtc();
//    twoWeeksLater = DateFormat("dd MMM yyyy").format(date);
//    currentDate = DateFormat("dd MMM yyyy").format(DateCalc.now().toUtc());
    commissionBloc.changeLastDate(DateCalc.now());
    commissionBloc.changeStartDate(date);
  }
  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
//    EasyLocalization.of(context).locale= Locale('en', 'US');
  }
  @override
  Widget build(BuildContext context) {

    return BlocBuilder<CommissionBloc,CommonUIState<CommissionEntity>>(
      builder: (_,state){
        return state.when(initial: ()=>LoadingBar(),
            success: (data)=>Stack(
              // fit: StackFit.expand,
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.all(16.0),
                  color: AppColors.darkBlue,
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
//              mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 8,top: 8,bottom: 8),
                        child: Row(
                          children: <Widget>[
                            Text(
                              Strings.currentSda,
                              style: context.body2.copyWith(color: Colors.white),
                            ).tr(),
                            SizedBox(
                              width: 10.0,
                            ),
                            Text(
                              "${data.productName}(Max ${data.currentCommission}%)",
                              style: context.subTitle2.copyWith(
                                  fontWeight: FontWeight.w600, color: Colors.white),
                            ),
                            Expanded(
                              child: Container(
                                alignment: Alignment.centerRight,
                                child: StreamBuilder<List<DateTime>>(
                                    stream: commissionBloc.startLastList,
                                    builder: (context, snapshot) {
                                      return GestureDetector(
                                        onTap: ()async{
                                          final List<DateTime> picked = await DateRagePicker.showDatePicker(
                                              context: context,
                                              initialFirstDate: snapshot.data[0],
                                              initialLastDate: snapshot.data[1],
                                              firstDate: new DateTime(2015),
                                              lastDate: new DateTime(2021)
                                          );
                                          if (picked != null && picked.length == 2) {
                                            var startDate=DateFormat("ddMMyyyy").format(picked[0]);
                                            var endDate=DateFormat("ddMMyyyy").format(picked[1]);
                                            print(DateFormat("dd MMM yyyy").format(picked[0]));
                                            var model=CommissionRequestModel(userId: widget.dashBoardEntity.id,startDate: startDate,endDate:endDate,commissionEnum: CommissionEnum.FOR_COMMISSION );
                                            commissionBloc.changeStartDate(picked[0]);
                                            commissionBloc.changeLastDate(picked[1]);
                                            commissionBloc.add(GetDetailedCommission(model));
                                            print(picked);
                                          }
                                        },
                                        child: Padding(
                                          padding:
                                          const EdgeInsets.symmetric(horizontal: 8.0),
                                          child: SvgPicture.asset(Images.calendar),
                                        ),
                                      );
                                    }
                                ),
                              ),
                            ),
//                       Text(
//                         Strings.addNewClient,
//                         style: context.subTitle1
//                             .copyWith(fontWeight: FontWeight.w800),
//                       ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: <Widget>[
                            Text(
                              Strings.totalCommission,
                              style: context.subTitle2.copyWith(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 12.0,
                                  color: Colors.white),

                            ).tr(),
                            Text(
                              " US "+data.lifetimeSales,
                              style: context.subTitle2.copyWith(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 20.0,
                                  color: Colors.white),

                            ),
                          ],
                        ),
                      ),
                      Card(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                        color: AppColors.lightBlue,
                        child: Column(
                          children: <Widget>[
                            sizedBox25,
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                    child: Text(
                                      Strings.direct,
                                      style: context.caption.copyWith(
                                          fontWeight: FontWeight.w800,
                                          color: Colors.white),
                                    ).tr(),
                                    alignment: Alignment.centerRight,
                                  ),
                                ),
                                Expanded(
                                  child: Container(),
                                ),
                                Expanded(
                                  child: Container(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      Strings.indirect,
                                      style: context.caption.copyWith(
                                          fontWeight: FontWeight.w800,
                                          color: Colors.white),
                                    ).tr(),
                                  ),
                                ),
                              ],
                            ),
                            Divider(
                              color: Colors.black45,
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Text(
                                          data.directCount,
                                          style: context.caption.copyWith(
                                              fontWeight: FontWeight.w800,
                                              color: Colors.white),
                                        ),
                                        sizedBox25,
                                        Text(
                                          data.directSalesValue,
                                          style: context.caption.copyWith(
                                              fontWeight: FontWeight.w800,
                                              color: Colors.white),
                                        ),
                                        sizedBox25,
                                        Text(
                                          AppConstants.directCommissionRate.toString(),
                                          style: context.caption.copyWith(
                                              fontWeight: FontWeight.w800,
                                              color: Colors.white),
                                        ),
                                        sizedBox25,
                                        Text(
                                          data.directCommission,
                                          style: context.caption.copyWith(
                                              fontWeight: FontWeight.w800,
                                              color: Colors.white),
                                        ),
                                      ],
                                    ),
                                    alignment: Alignment.centerRight,
                                  ),
                                ),
                                Expanded(
                                  child:  Container(
                                    child: Column(
//                                    crossAxisAlignment: CrossAxisAlignment.center,
//                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Text(
                                          Strings.salesCount,
                                          style: context.caption.copyWith(
                                              fontWeight: FontWeight.w400,fontSize: 11.0,
                                              color: Colors.white),
                                        ).tr(),
                                        sizedBox25,
                                        Text(
                                          Strings.value,
                                          style: context.caption.copyWith(
                                              fontWeight: FontWeight.w400,fontSize: 11.0,
                                              color: Colors.white),
                                        ).tr(),
                                        sizedBox25,
                                        Text(
                                          Strings.rate,
                                          style: context.caption.copyWith(
                                              fontWeight: FontWeight.w400,fontSize: 11.0,
                                              color: Colors.white),
                                        ).tr(),
                                        sizedBox25,
                                        Text(
                                          Strings.commission,
                                          style: context.caption.copyWith(
                                              fontWeight: FontWeight.w400,fontSize: 11.0,
                                              color: Colors.white),

                                        ).tr(),

                                      ],
                                    ),
                                    alignment: Alignment.center,
                                  ),
                                ),
                                Expanded(
                                  child:  Container(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Text(
                                          data.indirectCount,
                                          style: context.caption.copyWith(
                                              fontWeight: FontWeight.w800,
                                              color: Colors.white),
                                        ),
                                        sizedBox25,
                                        Text(
                                          data.indirectSalesValue,
                                          style: context.caption.copyWith(
                                              fontWeight: FontWeight.w800,
                                              color: Colors.white),
                                        ),
                                        sizedBox25,
                                        Text(
                                          data.indirectRate,
                                          style: context.caption.copyWith(
                                              fontWeight: FontWeight.w800,
                                              color: Colors.white),
                                        ),
                                        sizedBox25,
                                        Text(
                                          data.indirectCommission,
                                          style: context.caption.copyWith(
                                              fontWeight: FontWeight.w800,
                                              color: Colors.white),
                                        ),
                                      ],
                                    ),
                                    alignment: Alignment.centerLeft,
                                  ),
                                ),
                              ],
                            ),
                            sizedBox25,
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[

                                Expanded(child: Container(
                                  alignment: Alignment.center,
                                ),),
                                Expanded(child: Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    Strings.lifetimeSales,
                                    style: context.caption.copyWith(
                                        fontWeight: FontWeight.w400,fontSize: 11.0,
                                        color: Colors.white),

                                  ).tr(),
                                ),),
                                Expanded(child: Container(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    data.lifetimeSales,
                                    style: context.caption.copyWith(
                                        fontWeight: FontWeight.w800,
                                        color: Colors.white),

                                  ),
                                ),),


                              ],),
                            sizedBox25,
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox.expand(
                  child: DraggableScrollableSheet(
                    initialChildSize: 0.5,
                    minChildSize: 0.5,
                    maxChildSize: 1.0,
                    builder: (BuildContext context, myscrollController) {

                      return Container(
                        alignment: Alignment.center,
                        decoration: BoxDecoration(borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0),topRight: Radius.circular(10.0)),   color: Colors.white,),

                        child: data.referrals.isEmpty? Container(child: Text("No data found"),):ListView.separated(
                          controller: myscrollController,
                          itemCount: data.referrals.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                              color: Colors.white,
                              height: 130,
                              child: dashboardCardItem(context,entity:data.referrals[index]),);
                          }, separatorBuilder: (BuildContext context, int index) =>Divider(thickness: 1,),
                        ),
                      );
                    },
                  ),
                )
              ],
            ),
            loading: ()=>LoadingBar(),
            error: (e)=>Container(child: Center(child: Text(e,textAlign: TextAlign.center,),),));
      },

    );
  }
}
