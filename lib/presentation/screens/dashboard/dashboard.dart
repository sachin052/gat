import 'package:date_calc/date_calc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:technology/data/models/commission/commission_request_model.dart';
import 'package:technology/domain/entity/dashboard_entity.dart';
import 'package:technology/main.dart';
import 'package:technology/others/di/injection.dart';
import 'package:technology/others/styles/colors.dart';
import 'package:technology/others/styles/strings.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';
import 'package:technology/others/utils/images/images.dart';
import 'package:technology/others/utils/loading_indicator.dart';
import 'package:technology/presentation/bloc/dashboard/bloc.dart';
import 'package:technology/presentation/bloc/home_screen/bloc.dart';
import 'package:technology/presentation/bloc/wallet/wallet_bloc.dart';
import 'package:technology/presentation/commonwidgets/button/custom_button.dart';
import 'package:technology/presentation/commonwidgets/customshapes/dashboard_curve.dart';
import 'package:technology/presentation/commonwidgets/snackbar/snackbar.dart';
import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;

class DashBoardScreen extends StatefulWidget {
  final DashBoardEntity dashBoardEntity;

  const DashBoardScreen({Key key, this.dashBoardEntity}) : super(key: key);

  @override
  _DashBoardScreenState createState() => _DashBoardScreenState();
}

class _DashBoardScreenState extends State<DashBoardScreen> {
  String twoWeeksLater;

  // DashBoardBloc dashBoardBloc;
  WalletBloc walletBloc;

  String currentDate;

  @override
  void initState() {
    // TODO: implement initState
    walletBloc = getIt<WalletBloc>();
    walletBloc.add(GetWalletList());
    super.initState();
    var date = DateCalc.now().subtractDay(14).toUtc();
    twoWeeksLater = DateFormat("dd MMM yyyy").format(date);
    currentDate = DateFormat("dd MMM yyyy").format(DateCalc.now().toUtc());
    // dashBoardBloc.changeLastDate(DateCalc.now());
    // dashBoardBloc.changeStartDate(date);
//    var model = CommissionRequestModel(
//        userId: widget.dashBoardEntity.id,
//        startDate: DateFormat("ddMMyyyy").format(date),
//        endDate: DateFormat("ddMMyyyy").format(DateCalc.now().toUtc()));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: BlocListener<DashBoardBloc, CommonUIState<DashBoardEntity>>(
        listener: (_, state) {
          state.maybeWhen(
              success: (data) {
                if (data.referrals.isEmpty)
                  showSnackBar(context, "No Referrals", false);
//                BlocProvider.of<HomeScreenBloc>(context).add(DataFetched());
              },
              orElse: () {});
        },
        child: BlocBuilder<DashBoardBloc, CommonUIState<DashBoardEntity>>(
          builder: (_, state) {
            print(state.toString());
            return state.when(
                initial: () => Stack(
                      children: [
                        ClipPath(
                          clipper: BottomWaveClipper(),
                          child: Container(
                            color: AppColors.colorPrimary,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 25.0, vertical: 20.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                  "Hello ${widget.dashBoardEntity.firstName + " " + widget.dashBoardEntity.lastName}",
                                  style: context.headLine5.copyWith(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w800)),
                              // StreamBuilder<List<DateTime>>(
                              //     stream: dashBoardBloc.startLastList,
                              //     initialData: [DateTime.now(),DateTime.now().add(Duration(days: 7))],
                              //     builder: (context, snapshot) {
                              //       return GestureDetector(
                              //         onTap: ()async{
                              //           final List<DateTime> picked = await DateRagePicker.showDatePicker(
                              //               context: context,
                              //               initialFirstDate: snapshot.data[0],
                              //               initialLastDate: snapshot.data[1],
                              //               firstDate: new DateTime(2015),
                              //               lastDate: new DateTime(2021)
                              //           );
                              //           // if (picked != null && picked.length == 2) {
                              //           //   var startDate=DateFormat("ddMMyyyy").format(picked[0]);
                              //           //   var endDate=DateFormat("ddMMyyyy").format(picked[1]);
                              //           //   print(DateFormat("dd MMM yyyy").format(picked[0]));
                              //           //   var model=CommissionRequestModel(userId: widget.dashBoardEntity.id,startDate: startDate,endDate:endDate );
                              //           //   dashBoardBloc.changeStartDate(picked[0]);
                              //           //   dashBoardBloc.changeLastDate(picked[1]);
                              //           //   dashBoardBloc.add(GetCommissionData(model));
                              //           //   print(picked);
                              //           // }
                              //         },
                              //         child: Padding(
                              //           padding: const EdgeInsets.symmetric(
                              //               horizontal: 16.0),
                              //           child: SvgPicture.asset(Images.calendar),
                              //         ),
                              //       );
                              //     }
                              // ),
                            ],
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          child: SvgPicture.asset(Images.appIcon,
                              semanticsLabel: 'Acme Logo'),
                        )
                      ],
                    ),
                success: (s) => AnimatedSwitcher(
                      duration: Duration(milliseconds: 500),
                      child: state.when(
                          initial: () => Container(),
                          success: (data) {
                            print(
                                "state is" + data.referrals.length.toString());
                            data.referrals.clear();
                            return Stack(
                              fit: StackFit.expand,
                              children: <Widget>[
                                ClipPath(
                                  clipper: BottomWaveClipper(),
                                  child: Container(
                                    color: AppColors.colorPrimary,
                                  ),
                                ),
                                SingleChildScrollView(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      sizedBox25,
                                      sizedBox25,
//                        AppBar(
//                          titleSpacing:25,
//                          centerTitle: false,
//                          automaticallyImplyLeading: false,
//                          actions: <Widget>[
//                            Padding(
//                              padding: const EdgeInsets.symmetric(horizontal:16.0),
//                              child: SvgPicture.asset(Images.calendar),
//                            )
//                          ],
//                          elevation: 0.0,title: ,),),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 25.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Text(
                                                "Hello ${widget.dashBoardEntity.firstName + " " + widget.dashBoardEntity.lastName}",
                                                style: context.headLine5
                                                    .copyWith(
                                                        color: Colors.white,
                                                        fontWeight:
                                                            FontWeight.w800)),
                                            // StreamBuilder<List<DateTime>>(
                                            //     stream: dashBoardBloc.startLastList,
                                            //     initialData: [DateTime.now(),DateTime.now().add(Duration(days: 7))],
                                            //     builder: (context, snapshot) {
                                            //       return GestureDetector(
                                            //         onTap: ()async{
                                            //           final List<DateTime> picked = await DateRagePicker.showDatePicker(
                                            //               context: context,
                                            //               initialFirstDate: snapshot.data[0],
                                            //               initialLastDate: snapshot.data[1],
                                            //               firstDate: new DateTime(2015),
                                            //               lastDate: new DateTime(2021)
                                            //           );
                                            //           if (picked != null && picked.length == 2) {
                                            //             var startDate=DateFormat("ddMMyyyy").format(picked[0]);
                                            //             var endDate=DateFormat("ddMMyyyy").format(picked[1]);
                                            //             print(DateFormat("dd MMM yyyy").format(picked[0]));
                                            //             var model=CommissionRequestModel(userId: widget.dashBoardEntity.id,startDate: startDate,endDate:endDate );
                                            //             dashBoardBloc.changeStartDate(picked[0]);
                                            //             dashBoardBloc.changeLastDate(picked[1]);
                                            //             dashBoardBloc.add(GetCommissionData(model));
                                            //             print(picked);
                                            //           }
                                            //         },
                                            //         child: Padding(
                                            //           padding: const EdgeInsets.symmetric(
                                            //               horizontal: 16.0),
                                            //           child: SvgPicture.asset(Images.calendar),
                                            //         ),
                                            //       );
                                            //     }
                                            // ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 25.0),
                                        child: Text(
                                          Strings.referralHistory,
                                          style: context.body2
                                              .copyWith(color: Colors.white),
                                        ).tr(),
                                      ),
                                      // Padding(
                                      //   padding:
                                      //   const EdgeInsets.symmetric(horizontal: 25.0),
                                      //   child: StreamBuilder<String>(
                                      //       stream: dashBoardBloc.startLastDate,
                                      //       initialData: "",
                                      //       builder: (context, snapshot) {
                                      //         return Text(
                                      //           snapshot.data,
                                      //           style: context.caption
                                      //               .copyWith(color: Colors.white),
                                      //         );
                                      //       }
                                      //   ),
                                      // ),
                                      sizedBox25,
                                      sizedBox25,
                                      Visibility(
                                        visible: data.referrals.isEmpty,
                                        child: Container(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              .60,
                                          alignment: Alignment.center,
//                                  color: Colors.red,
                                          child: Center(
                                            child: Column(
                                              mainAxisSize: MainAxisSize.max,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                SvgPicture.asset(Images.appIcon,
                                                    semanticsLabel:
                                                        'Acme Logo'),
                                                sizedBox25,
                                                Text("No Referrals Founds")
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Column(
                                        children: List<Widget>.generate(
                                            data.referrals.length,
                                            (index) => Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      vertical: 12.0,
                                                      horizontal: 16.0),
                                                  child: CustomButton(
                                                    child: dashboardCardItem(
                                                        context,
                                                        entity: data
                                                            .referrals[index]),
                                                    color: Colors.white,
                                                    height: 163,
                                                    cornerRadius: 14.0,
                                                  ),
                                                )),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            );
                          },
                          loading: () => LoadingBar(),
                          error: (e) => Container()),
                    ),
                loading: () => LoadingBar(),
                error: (e) => Center(
                      child: Text(
                        e,
                        textAlign: TextAlign.center,
                      ),
                    ));
          },
        ),
      ),
    );
  }
}

Widget dashboardCardItem(BuildContext context,
    {DashBoardCommissionEntity entity}) {
  return Column(
    // mainAxisAlignment: MainAxisAlignment.center,
    // crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
      Flexible(
        child: Container(
//                color: Colors.amber,
          child: Column(
//                mainAxisAlignment: MainAxisAlignment.center,
//                crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                color: Colors.grey.shade300,
                padding: EdgeInsets.all(8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Text(
                          "Id: ",
                          style: Theme.of(context)
                              .textTheme
                              .subtitle2
                              .copyWith(color: AppColors.colorPrimary),
                        ),
                        Text(
                          entity?.id ?? "2323",
                          style: Theme.of(context)
                              .textTheme
                              .subtitle2
                              .copyWith(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    Text(
                      entity?.purchasedOn ?? "11 may 2018",
                      style: context.subTitle1
                          .copyWith(fontSize: 14, fontWeight: FontWeight.w800),
                    )
                  ],
                ),
              ),
              Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left:8.0),
                    child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                    Text(
                      "Client Info:",
                      style: Theme.of(context)
                          .textTheme
                          .subtitle2
                          .copyWith(color: Colors.black54),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 5),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(5.0),
                            bottomLeft: Radius.circular(5.0)),
                        color: entity?.referralType != null &&
                                entity?.referralType == "Direct"
                            ? Colors.amber
                            : Colors.lightBlue,
                      ),
                      height: 23,
                      width: 53,
                      child: Text(
                        entity?.referralType ?? "Direct",
                        style: context.caption.copyWith(color: Colors.white),
                      ),
                    ),
                ],
              ),
                  )),
              SizedBox(
                height: 10,
              ),
              Container(
                // color: Colors.red,
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Wrap(
                    alignment: WrapAlignment.spaceBetween,
                    runAlignment: WrapAlignment.spaceEvenly,
                    crossAxisAlignment: WrapCrossAlignment.start,
                    verticalDirection: VerticalDirection.down,
                    // runSpacing: 0,
                    // runSpacing: 50,
                    spacing: 10,
                    children: [
                      dashBoardTitle(
                        context,
                        "Id",
                        entity?.id ?? "2323",
                      ),
                      dashBoardTitle(
                        context,
                        "Name",
                        entity?.referral ?? "--",
                      ),
                      dashBoardTitle(
                        context,
                        "Sale Price",
                        entity.productAmount != null
                            ? "\$ " + entity.productAmount
                            : "--",
                      ),
                      dashBoardTitle(
                        context,
                        "Commission",
                        entity.salesAmount != null
                            ? "\$ " + entity.salesAmount
                            : "--",
                      ),
                    ],
                  ),
                ),
              ),

//                     Expanded(
//                       child: Row(
// //                      mainAxisAlignment: MainAxisAlignment.center,
// //                      crossAxisAlignment: CrossAxisAlignment.start,
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: <Widget>[
//                           Row(children: [
//                             Text("Id: ",style: Theme.of(context).textTheme.subtitle2.copyWith(color: AppColors.colorPrimary),),
//                             Text(entity?.id??"2323",style: Theme.of(context).textTheme.subtitle2.copyWith(fontWeight: FontWeight.bold),),
//                           ],),
//                           Row(children: [
//                             Text("Client ID: ",style: Theme.of(context).textTheme.subtitle2.copyWith(color: AppColors.colorPrimary),),
//                             Text(entity?.clientId??"2323",style: Theme.of(context).textTheme.subtitle2.copyWith(fontWeight: FontWeight.bold),),
//                           ],),
//                           Row(children: [
//                             Text("Price: ",style: Theme.of(context).textTheme.subtitle2.copyWith(color: AppColors.colorPrimary),),
//                             Text(entity.productAmount!=null?"US \$"+entity.productAmount:"--",style: Theme.of(context).textTheme.subtitle2.copyWith(fontWeight: FontWeight.bold),),
//                           ],),
//
//                         ],
//                       ),
//                     ),
//
//                     Expanded(
//                       child: Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: [
//                           dashBoardTitle(context, "Client Name", entity.referral??"--",),
//                           dashBoardTitle(context, "Commission", entity.salesAmount!=null?"US \$"+entity.salesAmount:"--",isEndAlignment: true)
//                         ],),
//                     )
            ],
          ),
        ),
      ),
    ],
  );
}

Widget dashBoardTitle(BuildContext context, String s1, String s2,
    {bool isEndAlignment = false}) {
  return Container(
//      alignment: Alignment.center,

    child: Wrap(
      direction: Axis.vertical,
      // crossAxisAlignment: isEndAlignment?CrossAxisAlignment.end:CrossAxisAlignment.center,
      // mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          s1,
          style: context.subTitle1.copyWith(
              fontSize: 12,
              color: AppColors.colorPrimary,
              fontWeight: FontWeight.w600),
        ),
        Text(
          s2,
          style: context.subTitle1
              .copyWith(fontSize: 14, fontWeight: FontWeight.w800),
        )
      ],
    ),
  );
}
