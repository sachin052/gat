

import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:technology/main.dart';
import 'package:technology/others/di/injection.dart';
import 'package:technology/others/styles/colors.dart';
import 'package:technology/others/styles/strings.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';
import 'package:technology/others/utils/images/images.dart';
import 'package:technology/presentation/bloc/signUp/bloc.dart';
import 'package:technology/presentation/commonwidgets/CustomInputField.dart';
import 'package:technology/presentation/commonwidgets/button/custom_button.dart';
import 'package:technology/presentation/commonwidgets/snackbar/snackbar.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  SignUpBloc _signUpBloc;
  FocusNode firstNameNode;
  FocusNode lastNameNode;
  FocusNode emailNode;
  FocusNode passwordNode;
  FocusNode confirmPasswordNode;
  FocusNode referIdNode;
  @override
  void initState() {
    super.initState();
    _signUpBloc = getIt<SignUpBloc>();
    firstNameNode=FocusNode();
    lastNameNode=FocusNode();
    emailNode=FocusNode();
    passwordNode=FocusNode();
    confirmPasswordNode=FocusNode();
    referIdNode=FocusNode();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(backgroundColor: Colors.transparent,iconTheme: IconThemeData(color: AppColors.colorPrimary),elevation: 0.0,),
      body: SafeArea(
        child: BlocProvider<SignUpBloc>(
          create: (c)=>_signUpBloc,
          child: BlocListener<SignUpBloc,CommonUIState>(
            listener: (context,state){
              state.maybeWhen(
                  error: (error)=>showSnackBar(context,error,true),
                  success: (data){
                    ExtendedNavigator.root.pop();
                    showSnackBar(context, data, false);
                  },
                  orElse: (){});
            },
            child: BlocBuilder<SignUpBloc,CommonUIState>(
              builder: (context,state){
                print(state.toString());
                return SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(24.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          height: 20.0,
                        ),
                        SvgPicture.asset(Images.appIcon, semanticsLabel: 'Acme Logo'),
                        SizedBox(
                          height: 20.0,
                        ),
                        Text(
                          Strings.createAccountSignUp,
                          style: Theme.of(context).textTheme.headline6,
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        CustomInputField(
                          controller: _signUpBloc.firstNameController,
                          onTextChange: _signUpBloc.changeFirstName,
                          labelText: Strings.firstName,
                          stream: _signUpBloc.firstName,
                          myNode: firstNameNode,
                          nextNode: lastNameNode,
                          icon: Images.personIcon,
                        ),
                        SizedBox(width: 20.0,),
                        CustomInputField(
                          labelText: Strings.lastName,
                          controller: _signUpBloc.lastNameTextController,
                          onTextChange: _signUpBloc.changeLastName,
                          stream: _signUpBloc.lastName,
                          nextNode: emailNode,
                          myNode: lastNameNode,
                          icon: Images.personIcon,
                        ),

                        CustomInputField(
                          controller: _signUpBloc.emailTextController,
                          labelText: Strings.email,
                          stream: _signUpBloc.email,
                          myNode: emailNode,
                          nextNode: passwordNode,
                          onTextChange: _signUpBloc.changeEmail,
                          icon: Images.emailIcon,
                        ),

                        CustomInputField(
                          labelText: Strings.password,
                          controller: _signUpBloc.passwordTextController,
                          stream: _signUpBloc.password,
                          onTextChange: _signUpBloc.changePassword,
                          icon: Images.lockIcon,
                          myNode: passwordNode,
                          nextNode: confirmPasswordNode,
                          isPassword: true,
                        ),

                        CustomInputField(
                          labelText: Strings.confirmPassword,
                          controller: _signUpBloc.confirmPassTextController,
                          onTextChange: _signUpBloc.changeConfirmPassword,
                          stream: _signUpBloc.confirmPassword,
                          myNode: confirmPasswordNode,
                          nextNode: referIdNode,
                          icon: Images.lockIcon,
                          isPassword: true,
                        ),

                        CustomInputField(
                            onTextChange: _signUpBloc.changeReferId,
                            controller: _signUpBloc.referIdController,
                            myNode: referIdNode,
                            action: TextInputAction.done,
                            stream: _signUpBloc.referId,
                            labelText:Strings.referID, icon: Images.refer),
                        SizedBox(
                          height: 20.0,
                        ),
                        CustomButton(
                          uiState: state,
                          onTap:(){
                            _signUpBloc.add(SignUpAws());
                          },
                          child:  Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                Strings.registerNow,
                                style: Theme.of(context)
                                    .textTheme
                                    .button
                                    .copyWith(color: Colors.white),
                              ),
                            ],
                          ),),


                        SizedBox(
                          height: 20.0,
                        ),
                        GestureDetector(
                          onTap: (){
                            ExtendedNavigator.root.pop();
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                Strings.alreadyHaveAnAccount,
                                style: Theme.of(context)
                                    .textTheme
                                    .caption
                                    .copyWith(fontWeight: FontWeight.w600),
                              ),
                              Text(
                                Strings.login,
                                style: Theme.of(context).textTheme.caption.copyWith(
                                    color: AppColors.colorPrimary,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 11.0),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
