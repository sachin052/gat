import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
import 'package:technology/data/models/commission/commission_request_model.dart';
import 'package:technology/main.dart';
import 'package:technology/others/di/injection.dart';
import 'package:technology/others/styles/colors.dart';
import 'package:technology/others/styles/strings.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';
import 'package:technology/presentation/bloc/addclient/add_client_bloc.dart';
import 'package:technology/presentation/bloc/addclient/add_client_event.dart';
import 'package:technology/presentation/bloc/home_screen/bloc.dart';
import 'package:technology/presentation/commonwidgets/snackbar/snackbar.dart';
import 'package:technology/presentation/screens/addclient/add_client_step_one.dart';
import 'package:technology/presentation/screens/addclient/add_client_step_three.dart';
import 'add_client_step_two.dart';
import 'package:easy_localization/easy_localization.dart';
class AddClientScreen extends StatefulWidget {
  @override
  _AddClientScreenState createState() => _AddClientScreenState();
}

class _AddClientScreenState extends State<AddClientScreen> with TickerProviderStateMixin {

  int _currentPage = 0;
  int previousPage;
  bool step2 = false;
  bool step3 = false;
  AnimationController _animationController;
  AnimationController _animationController2;
  double progressValue = 0.0;
  AddClientBloc _addClientBloc;
  List<Widget> pages=[ AddClientScreenStepOne(),
    AddClientScreenStepTwo(),
    AddClientStepThree()];

  @override
  void initState() {
    _addClientBloc=getIt<AddClientBloc>();
// _addClientBloc.add(GetCountriesList());

    super.initState();
    setUpAnimationControllers();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AddClientBloc>(
      create: (_)=>_addClientBloc,
      child: Scaffold(
        backgroundColor: Colors.white,
        body: NestedScrollView(
          body: BlocListener<AddClientBloc,CommonUIState>(
            listener: (c,state){
              state.maybeWhen(
                  orElse: (){},error: (e){
                    showSnackBar(context, e, true);
              },success: (s){
                    if(s is String){
                      BlocProvider.of<HomeScreenBloc>(context).changeCurrentPage(2);
                      _addClientBloc.clearAllData();
                      showSnackBar(context, s, false);
                    }
              });
            },
            child: BlocBuilder<AddClientBloc,CommonUIState>(
              builder: (c,state)=>IgnorePointer(
                ignoring: state==CommonUIState.loading(),
                child: PageView(
                  physics: NeverScrollableScrollPhysics(),
                  controller: _addClientBloc.pageController,
                  onPageChanged: (index) {
                    onPageChanged(index);
                  },
                  children: List<Widget>.generate(pages.length, (index) => pages[index]),
                ),
              ),
            ),
          ),
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return [
              SliverAppBar(
                backgroundColor: Colors.white,
                expandedHeight: 150,
                elevation: 5.0,
                titleSpacing: 0.0,
                automaticallyImplyLeading: false,
                flexibleSpace: FlexibleSpaceBar(
                  background: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      AppBar(
                        centerTitle: false,
                        automaticallyImplyLeading: false,
                        backgroundColor: Colors.transparent,
                        elevation: 0.0,
                        title: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              Strings.createNewClient,
                              style: context.headLine6
                                  .copyWith(fontWeight: FontWeight.w800),
                            ).tr(),
                            Text(
                             Strings.addNewClient,
                              style: context.subTitle1
                                  .copyWith(fontWeight: FontWeight.w800),
                            ).tr(),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: 100,
                        ),
                        child: Stack(
                          alignment: Alignment.center,
                          children: <Widget>[
                            StepProgressIndicator(
                              totalSteps: 100,
                              currentStep: progressValue.toInt(),
                              size: 4,
                              padding: 0,
                              selectedColor: AppColors.colorPrimary,
                              unselectedColor:
                                  AppColors.colorPrimary.withOpacity(.4),
                              roundedEdges: Radius.circular(10),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                StreamBuilder<bool>(
                                  stream: _addClientBloc.validFirstStep,
                                  initialData: false,
                                  builder: (context, snapshot) {
                                    return buildTickBox(snapshot?.data??false, "1");
                                  }
                                ),
                                StreamBuilder<int>(
                                  stream: _addClientBloc.currentPage,
                                  initialData: 0,
                                  builder: (context, snapshot) {
                                    return buildTickBox(snapshot.data!=null&&!snapshot.hasError&&snapshot.data>0, "2");
                                  }
                                ),
                                StreamBuilder<bool>(
                                  stream: _addClientBloc.allStepValid,
                                  initialData: false,
                                  builder: (context, snapshot) {
                                    return buildTickBox(snapshot?.data??false, "3");
                                  }
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 20.0,
                        ),
                        alignment: Alignment.centerRight,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            StreamBuilder<int>(
                              stream:_addClientBloc.currentPage,
                              builder: (context, snapshot) {
                                return GestureDetector(
                                  onTap: (){
                                    if(_currentPage==2)_addClientBloc.changePage(1);
                                    else if(_currentPage==1)_addClientBloc.changePage(0);
                                  },
                                  child: Row(
                                    children: <Widget>[
                                      Visibility(
                                        visible: snapshot.data!=0,
                                        child: Icon(
                                          Icons.arrow_back,
                                          color: AppColors.colorPrimary,
                                          size: 15.0,
                                        ),
                                      ),
                                      Text(
                                        snapshot.data==0?"":Strings.previous,
                                        style: context.subTitle2.copyWith(
                                            decoration: TextDecoration.underline,
                                            color: AppColors.colorPrimary,
                                            fontWeight: FontWeight.w800),
                                      ).tr(),

                                    ],
                                  ),
                                );
                              }
                            ),
                            StreamBuilder<bool>(
                                stream: _addClientBloc.validFirstStep,
                                initialData: false,
                                builder: (context, snapshot) {
                                  return StreamBuilder<String>(
                                    stream: _addClientBloc.mobileNumber,
                                    builder: (context, number) {
                                      return GestureDetector(
                                        onTap: (){
                                          if(!snapshot.data){
                                            _addClientBloc.setFirstStepError();
                                          }
                                          if(snapshot.data!=null&&snapshot.data&&_currentPage==0){
                                            if(_currentPage==0)_addClientBloc.changePage(1);
                                          }
                                          else if(_currentPage==1)_addClientBloc.changePage(2);
                                        },
                                        child: Row(
                                          children: <Widget>[

                                            Text(
                                              _currentPage!=2?Strings.next:"",
                                              style: context.subTitle2.copyWith(
                                                  decoration: TextDecoration.underline,
                                                  color: AppColors.colorPrimary,
                                                  fontWeight: FontWeight.w800),
                                            ).tr(),
                                            Visibility(
                                              visible: _currentPage!=2,
                                              child: Icon(
                                                Icons.arrow_forward,
                                                color: AppColors.colorPrimary,
                                                size: 15.0,
                                              ),
                                            ),
                                          ],
                                        ),
                                      );
                                    }
                                  );
                                }
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              )
            ];
          },
        ),
      ),
    );
  }

  Widget buildTickBox(bool active, String number) {
    return Stack(
      overflow: Overflow.visible,
      children: <Widget>[
        AnimatedContainer(
          duration: Duration(milliseconds: 500),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6.0),
              color: active ? AppColors.colorPrimary : Colors.white,
              border: active
                  ? null
                  : Border.all(color: AppColors.colorPrimary, width: 2.0)),
          height: 30.0,
          width: 30.0,
          child: Text(
            number,
            style: context.subTitle2.copyWith(
                color: active ? Colors.white : AppColors.colorPrimary),
          ),
        ),
        Visibility(
          visible: active,
          child: Positioned(
            right: -5,
            top: -5,
            child: Container(
              height: 15.0,
              width: 15.0,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                  border:
                      Border.all(color: AppColors.colorPrimary, width: 2.0)),
              child: Icon(
                Icons.done,
                color: AppColors.colorPrimary,
                size: 10.0,
              ),
            ),
          ),
        ),
      ],
    );
  }

  void setUpAnimationControllers() {

    _animationController = AnimationController(
        vsync: this,
        duration: Duration(milliseconds: 500),
        lowerBound: 0.0,
        upperBound: 50.0,);
    _animationController2 = AnimationController(
        vsync: this,
        duration: Duration(milliseconds: 500),
        lowerBound: 50.0,
        upperBound: 100.0);

    _animationController.addListener(() {
      setState(() {
        progressValue = _animationController.value;
      });
    });
    _animationController.addStatusListener((status) {
      if(status==AnimationStatus.completed)step2=true;
      else if(status==AnimationStatus.reverse)step2=false;
    });
    _animationController2.addStatusListener((status) {
      if(status==AnimationStatus.completed)step3=true;
      else if(status==AnimationStatus.reverse)step3=false;
    });
    _animationController2.addListener(() {
      setState(() {
        if(_animationController2.isCompleted){
          if(_currentPage==1){
            step3=true;
          }
          else{
            step3=false;
          }
        }
        progressValue = _animationController2.value;
      });
    });

    _addClientBloc.pageController.addListener(() {});
  }

  void onPageChanged(int index) {
    _addClientBloc.changeCurrentPage(index);
    previousPage = _currentPage;
    _currentPage = index;
    if (index == 0)
      _animationController.reverse();
    else if (index == 1) {
      if (previousPage == 0)
        _animationController.forward();
      else if (previousPage == 2) _animationController2.reverse();
    } else if (index == 2 && previousPage == 1) {
      _animationController2.forward();
    }
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

}
