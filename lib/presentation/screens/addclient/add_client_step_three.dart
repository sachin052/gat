import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:technology/others/styles/strings.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';
import 'package:technology/presentation/bloc/addclient/add_client_bloc.dart';
import 'package:technology/presentation/bloc/addclient/add_client_event.dart';
import 'package:technology/presentation/commonwidgets/button/custom_button.dart';
import 'package:technology/main.dart';
import 'package:easy_localization/easy_localization.dart';
class AddClientStepThree extends StatefulWidget {
  @override
  _AddClientStepThreeState createState() => _AddClientStepThreeState();
}

class _AddClientStepThreeState extends State<AddClientStepThree> {
  AddClientBloc _addClientBloc;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _addClientBloc=BlocProvider.of<AddClientBloc>(context);
  }
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(24.0),
      child: Container(
//        color: Colors.red,
//        height: 600,
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
           Column(
             crossAxisAlignment: CrossAxisAlignment.start,
             children: <Widget>[
             Text(Strings.kindlyConfirm).tr(),
             sizedBox25,
             sizedBox25,
             sizedBox25,
             Column(
               mainAxisAlignment: MainAxisAlignment.start,
               crossAxisAlignment: CrossAxisAlignment.start,
               children: <Widget>[
                 StreamBuilder<String>(
                     stream: _addClientBloc.firstLastName,
                     initialData: "No Name",
                     builder: (context, snapshot) {
                       return Text(snapshot.data!=null?snapshot.data:"No Name",style: context.subTitle2.copyWith(fontWeight: FontWeight.bold),);
                     }
                 ),
                 StreamBuilder<String>(
                     stream: _addClientBloc.email,
                     initialData: "No Email",
                     builder: (context, snapshot) {
                       return Text(snapshot.data!=null?snapshot.data:"No Email",style: context.subTitle2);
                     }
                 ),
                 sizedBox25,
                 Text(_addClientBloc.pinCodeStreet.text,style: context.subTitle2),
               ],
             ),
           ],),

              BlocBuilder<AddClientBloc,CommonUIState>(
                bloc: _addClientBloc,
                builder: (c,state)=>CustomButton(
                  uiState: state,
                  onTap: (){
                    _addClientBloc.add(CreateClient(_addClientBloc.allCountries));
                  },child: Text(Strings.saveCreate,style: context.button.copyWith(color: Colors.white),).tr(),),
              )
            ],
          ),
        ),
      ),
    );
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}

