import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:technology/main.dart';
import 'package:technology/others/styles/strings.dart';
import 'package:technology/others/utils/images/images.dart';
import 'package:technology/presentation/bloc/addclient/add_client_bloc.dart';
import 'package:technology/presentation/bloc/addclient/add_client_event.dart';
import 'package:technology/presentation/commonwidgets/CustomInputField.dart';
import 'package:technology/presentation/commonwidgets/button/custom_button.dart';
import 'package:easy_localization/easy_localization.dart';
class AddClientScreenStepOne extends StatefulWidget {
  @override
  _AddClientScreenStepOneState createState() => _AddClientScreenStepOneState();
}

class _AddClientScreenStepOneState extends State<AddClientScreenStepOne> {
  FocusNode firstNameNode;
  FocusNode lastNameNode;
  FocusNode emailNode;
  FocusNode middleNameNode;
  AddClientBloc _addClientBloc;

  @override
  void initState() {
    // TODO: implement initState
    _addClientBloc=BlocProvider.of<AddClientBloc>(context);
    firstNameNode=FocusNode();
    lastNameNode=FocusNode();
    emailNode=FocusNode();
    middleNameNode=FocusNode();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(

        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal:24.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal:8.0,),
              child: Text(Strings.contactDetails,style: context.subTitle2.copyWith(fontWeight: FontWeight.bold),).tr(),
            ),

            CustomInputField(
          controller: _addClientBloc.firstNameController,
              labelText: Strings.firstName,
          stream: _addClientBloc.firstName,
          myNode: firstNameNode,
          nextNode: middleNameNode,
          onTextChange: _addClientBloc.changeFirstName,
              icon: Images.personIcon,
            ),
            CustomInputField(
          controller: _addClientBloc.middleNameController,
              labelText: Strings.middleName,
          stream: _addClientBloc.middleName,
          myNode: middleNameNode,
          nextNode: lastNameNode,
          onTextChange: _addClientBloc.changeMiddleName,
              icon: Images.personIcon,
            ),
            CustomInputField(
          controller: _addClientBloc.lastNameTextController,
              labelText: Strings.lastName,
          stream: _addClientBloc.lastName,
          myNode: lastNameNode,
          nextNode: emailNode,
          onTextChange: _addClientBloc.changeLastName,
              icon: Images.personIcon,
            ),
              CustomInputField(
          controller: _addClientBloc.emailTextController,
                labelText: Strings.email,
          stream: _addClientBloc.email,
          myNode: emailNode,
action: TextInputAction.done,
//          nextNode: passwordNode,
          onTextChange: _addClientBloc.changeEmail,
                icon: Images.emailIcon,
              ),
            StreamBuilder<bool>(
              stream: _addClientBloc.validFirstStep,
              initialData: false,
              builder: (context, snapshot) {
                return CustomButton(onTap: (){
                  if(snapshot.data!=null&&snapshot.data)
                  _addClientBloc.changePage(1);
                  else{
                    _addClientBloc.setFirstStepError();
                  }
                },child: Text(Strings.next,style: context.button.copyWith(color: Colors.white),).tr(),);
              }
            )
          ],),
        ),
      ),
    );
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
