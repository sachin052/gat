import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:technology/data/models/country_model.dart';
import 'package:technology/main.dart';
import 'package:technology/others/styles/strings.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';
import 'package:technology/others/utils/images/images.dart';
import 'package:technology/others/utils/loading_indicator.dart';
import 'package:technology/presentation/bloc/addclient/add_client_bloc.dart';
import 'package:technology/presentation/bloc/addclient/add_client_event.dart';
import 'package:technology/presentation/commonwidgets/CustomInputField.dart';
import 'package:technology/presentation/commonwidgets/button/custom_button.dart';
import 'package:technology/presentation/commonwidgets/country_picker.dart';
import 'package:easy_localization/easy_localization.dart';
class AddClientScreenStepTwo extends StatefulWidget {
  @override
  _AddClientScreenStepTwoState createState() => _AddClientScreenStepTwoState();
}

class _AddClientScreenStepTwoState extends State<AddClientScreenStepTwo> {
  AddClientBloc _addClientBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _addClientBloc = BlocProvider.of<AddClientBloc>(context);
    _addClientBloc.add(GetCountriesList());
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BlocBuilder<AddClientBloc,CommonUIState>(
        builder: (_,state){
          return state.when(initial: ()=>LoadingBar(), success: (s)=>SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24.0),
              child: StreamBuilder<bool>(
                  stream: _addClientBloc.sameAsPostal,
                  builder: (context, snapshot) {
                    if (snapshot.data != null && !snapshot.data) {
                      _addClientBloc.clearPostalAddress();
                    }
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 8.0,
                          ),
                          child: Text(
                            Strings.contactDetails,
                            style: context.subTitle2
                                .copyWith(fontWeight: FontWeight.bold),
                          ).tr(),
                        ),
                        StreamBuilder<String>(
                            stream: _addClientBloc.codeHome,
                            builder: (context, snapshot) {
                              return CustomInputField(
                                countryCode: snapshot.data,
                                hasSuffix: true,
                                onCountryTap: () {
                                  showCountryPicker(context, (c) {
                                    _addClientBloc.changeCodeHome(c.phoneCode);
                                  },items:(s as List<CountryEntity>),allowCountry: true);
                                },
                                controller: _addClientBloc.homeTelephone,
                                labelText: Strings.teleHome,
                                inputType: TextInputType.number,
//          stream: _signUpBloc.email,
//          myNode: emailNode,
//          nextNode: passwordNode,
//          onTextChange: _signUpBloc.changeEmail,
                                icon: Images.telephone,
                              );
                            }),
                        StreamBuilder<String>(
                            stream: _addClientBloc.codeWork,
                            builder: (context, snapshot) {
                              return CustomInputField(
                                controller: _addClientBloc.workTelephone,
                                labelText: Strings.teleWork,
                                hasSuffix: true,
                                onCountryTap: () {
                                  showCountryPicker(context, (c) {
                                    _addClientBloc
                                        .changeCodeWork(c.phoneCode);
                                  }, items: (s as List<CountryEntity>),allowCountry: true);
                                },
                                countryCode: snapshot.data,
                                inputType: TextInputType.number,
//          stream: _signUpBloc.email,
//          myNode: emailNode,
//          nextNode: passwordNode,
//          onTextChange: _signUpBloc.changeEmail,
                                icon: Images.telephone,
                              );
                            }),
                        StreamBuilder<String>(
                            stream: _addClientBloc.mobileCode,
                            builder: (context, snapshot) {
                              return CustomInputField(
                                controller: _addClientBloc.mobile,
                                labelText: Strings.mobile,
                                stream: _addClientBloc.mobileNumber,
                                onTextChange: _addClientBloc.changeMobile,
                                hasSuffix: true,
                                onCountryTap: () {
                                  showCountryPicker(context, (c) {
                                    _addClientBloc
                                        .changeMobileCode(c.phoneCode);
                                  }, items: (s as List<CountryEntity>),allowCountry: true);
                                },
                                countryCode: snapshot.data,
                                inputType: TextInputType.number,
//          stream: _signUpBloc.email,
//          myNode: emailNode,
//          nextNode: passwordNode,
//          onTextChange: _signUpBloc.changeEmail,
                                icon: Images.mobile,
                              );
                            }),
                        CustomInputField(
                          controller: _addClientBloc.secondaryEmail,
                          labelText: Strings.secondaryEmail,
//          stream: _signUpBloc.email,
//          myNode: emailNode,
//          nextNode: passwordNode,
//          onTextChange: _signUpBloc.changeEmail,
                          icon: Images.emailIcon,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 8.0,
                          ),
                          child: Text(
                            Strings.streetAddress,
                            style: context.subTitle2
                                .copyWith(fontWeight: FontWeight.bold),
                          ).tr(),
                        ),
                        CustomInputField(
                          controller: _addClientBloc.addressStreet,
                          labelText: Strings.address,
//          stream: _signUpBloc.email,
//          myNode: emailNode,
//          nextNode: passwordNode,
                          onTextChange: (text) {
                            if (snapshot.data != null && snapshot.data)
                              _addClientBloc.addressPostal.text =
                                  _addClientBloc.addressStreet.text;
                          },
                          icon: Images.location,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: CustomInputField(
                                controller: _addClientBloc.cityStreet,
                                labelText: Strings.city,
                                onTextChange: (text) {
                                  if (snapshot.data != null && snapshot.data)
                                    _addClientBloc.cityPostal.text =
                                        _addClientBloc.cityStreet.text;
                                },
//          stream: _signUpBloc.email,
//          myNode: emailNode,
//          nextNode: passwordNode,
//          onTextChange: _signUpBloc.changeEmail,
                                icon: Images.city,
                              ),
                            ),
                            Expanded(
                              child: CustomInputField(
                                controller: _addClientBloc.stateAddressStreet,
                                labelText: Strings.state,
//          stream: _signUpBloc.email,
                                onTextChange: (text) {
                                  if (snapshot.data != null && snapshot.data)
                                    _addClientBloc.stateAddressPostal.text =
                                        _addClientBloc.stateAddressStreet.text;
                                },
//          myNode: emailNode,
//          nextNode: passwordNode,
//          onTextChange: _signUpBloc.changeEmail,
                                icon: Images.state,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Stack(
                                children: <Widget>[
                                  Positioned(
                                      top: 30,
                                      right: 5,
                                      child: Icon(Icons.arrow_drop_down)),
                                  StreamBuilder<List<CountryEntity>>(
                                      stream: _addClientBloc.countryList,
                                      builder: (context, items) {
                                        return CustomInputField(
                                          controller: _addClientBloc.countryStreet,
                                          isReadOnly: true,
                                          hasSuffix: true,
                                          labelText: Strings.country,

//          stream: _signUpBloc.email,
                                          onTextChange: (text) {
                                            if (snapshot.data != null &&
                                                snapshot.data)
                                              _addClientBloc.countryPostal.text =
                                                  _addClientBloc.countryStreet.text;
                                          },
                                          onCountryTap: () {
                                            showCountryPicker(context, (c) {
                                              _addClientBloc.countryStreet.text =
                                                  c.name;
                                            },
                                                allowCountry: false,
                                                items: items.data);
                                          },
//          myNode: emailNode,
//          nextNode: passwordNode,
//          onTextChange: _signUpBloc.changeEmail,
                                          icon: Images.country,
                                        );
                                      }),
                                ],
                              ),
                            ),
                            Expanded(
                              child: CustomInputField(
                                controller: _addClientBloc.pinCodeStreet,
                                labelText: Strings.pinCode,
                                inputType: TextInputType.phone,
                                onTextChange: (text) {
                                  if (snapshot.data != null && snapshot.data)
                                    _addClientBloc.pinCodePostal.text =
                                        _addClientBloc.pinCodeStreet.text;
                                },
//          stream: _signUpBloc.email,
//          myNode: emailNode,
//          nextNode: passwordNode,
//          onTextChange: _signUpBloc.changeEmail,
                                icon: Images.pin,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            StreamBuilder<bool>(
                                stream: _addClientBloc.sameAsPostal,
                                initialData: false,
                                builder: (context, snapshot) {
                                  if (snapshot.data != null && snapshot.data) {
                                    _addClientBloc.sameAsPostalAddress();
                                  }
                                  return Checkbox(
                                    onChanged: _addClientBloc.changePostalSame,
                                    value: snapshot.data,
                                  );
                                }),
                            Text(
                              Strings.samePostalAddress,
                              style: context.subTitle2
                                  .copyWith(fontWeight: FontWeight.w600),
                            ).tr(),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 8.0,
                          ),
                          child: Text(
                            Strings.postalAddress,
                            style: context.subTitle2
                                .copyWith(fontWeight: FontWeight.bold),
                          ).tr(),
                        ),
                        CustomInputField(
                          controller: _addClientBloc.addressPostal,
                          labelText: Strings.address,
                          stream: _addClientBloc.postalAddress,
//          myNode: emailNode,
//          nextNode: passwordNode,
//          onTextChange: _signUpBloc.changeEmail,
                          icon: Images.location,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: CustomInputField(
                                controller: _addClientBloc.cityPostal,
                                labelText: Strings.city,
                                stream: _addClientBloc.postalCity,
//          myNode: emailNode,
//          nextNode: passwordNode,
//          onTextChange: _signUpBloc.changeEmail,
                                icon: Images.city,
                              ),
                            ),
                            Expanded(
                              child: CustomInputField(
                                controller: _addClientBloc.stateAddressPostal,
                                labelText: Strings.state,
                                stream: _addClientBloc.postalState,
//          myNode: emailNode,
//          nextNode: passwordNode,
//          onTextChange: _signUpBloc.changeEmail,
                                icon: Images.state,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Stack(
                                children: <Widget>[
                                  Positioned(
                                      top: 30,
                                      right: 5,
                                      child: Icon(Icons.arrow_drop_down)),
                                  StreamBuilder<List<CountryEntity>>(
                                      stream: _addClientBloc.countryList,
                                      builder: (context, items) {
                                        return CustomInputField(
                                          controller: _addClientBloc.countryPostal,
                                          isReadOnly: true,
                                          labelText: Strings.country,

//          stream: _signUpBloc.email,
                                          onTextChange: (text) {
                                            if (snapshot.data != null &&
                                                snapshot.data)
                                              _addClientBloc.countryPostal.text =
                                                  _addClientBloc.countryStreet.text;
//                              else _addClientBloc.countryPostal.text=text;
                                          },
                                          stream: _addClientBloc.postalCountry,
//          myNode: emailNode,
//          nextNode: passwordNode,
//          onTextChange: _signUpBloc.changeEmail,
                                          icon: Images.country,
                                          hasSuffix: true,
                                          onCountryTap: () {
                                            showCountryPicker(context, (c) {
                                              _addClientBloc.countryPostal.text =
                                                  c.name;
                                            }, allowCountry: false,items: items.data);
                                          },
                                        );
                                      }),
                                ],
                              ),
                            ),
                            Expanded(
                              child: CustomInputField(
                                controller: _addClientBloc.pinCodePostal,
                                labelText: Strings.pinCode,
                                inputType: TextInputType.phone,
                                stream: _addClientBloc.postalPin,
//          myNode: emailNode,
//          nextNode: passwordNode,
//          onTextChange: _signUpBloc.changeEmail,
                                icon: Images.pin,
                              ),
                            ),
                          ],
                        ),
                        StreamBuilder<String>(
                          stream: _addClientBloc.mobileNumber,
                          builder: (context, snapshot) {
                            return CustomButton(
                              onTap: () {
                                // if(snapshot.data!=null&&snapshot.data.isNotEmpty)
                                _addClientBloc.changePage(2);
                              },
                              child: Text(
                                "Next",
                                style: context.button.copyWith(color: Colors.white),
                              ),
                            );
                          }
                        ),
                      ],
                    );
                  }),
            ),
          ), loading: ()=>LoadingBar(), error: (e)=>Container(child: Center(child: Text(e,textAlign: TextAlign.center,),),));
        },
      ),
    );
  }
  @override
  void dispose() {
    super.dispose();
  }
}
