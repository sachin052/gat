import 'package:flutter/material.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';

class BaseScreen extends StatefulWidget {
  final Widget child;
  final CommonUIState state;
  const BaseScreen({Key key, this.child, this.state}) : super(key: key);
  @override
  _BaseScreenState createState() => _BaseScreenState();
}

class _BaseScreenState extends State<BaseScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IgnorePointer(
          ignoring: widget.state==CommonUIState.initial(),
          child: Padding(
            padding: const EdgeInsets.all(24.0),
            child: widget.child,
          )),
    );
  }
}
