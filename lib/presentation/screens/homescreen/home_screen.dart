import 'package:auto_route/auto_route.dart';
import 'package:date_calc/date_calc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:technology/data/models/commission/commission_request_model.dart';
import 'package:technology/domain/entity/dashboard_entity.dart';
import 'package:technology/domain/repo/user_repo.dart';
import 'package:technology/main.dart';
import 'package:technology/others/di/injection.dart';
import 'package:technology/others/styles/strings.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';
import 'package:technology/others/utils/images/images.dart';
import 'package:technology/presentation/bloc/addclient/add_client_bloc.dart';
import 'package:technology/presentation/bloc/addclient/bloc.dart';
import 'package:technology/presentation/bloc/commission/commission_bloc.dart';
import 'package:technology/presentation/bloc/dashboard/bloc.dart';
import 'package:technology/presentation/bloc/home_screen/bloc.dart';
import 'package:technology/presentation/bloc/products/bloc.dart';
import 'package:technology/presentation/bloc/products/product_bloc.dart';
import 'package:technology/presentation/bloc/profile/profile_bloc.dart';
import 'package:technology/presentation/bloc/wallet/wallet_bloc.dart';
import 'package:technology/presentation/screens/addclient/add_client.dart';
import 'package:technology/presentation/screens/commission/commission_screen.dart';
import 'package:technology/presentation/screens/dashboard/dashboard.dart';
import 'package:technology/presentation/screens/login/login.dart';
import 'package:technology/presentation/screens/products_screen.dart';
import 'package:technology/presentation/screens/profile/profile_screen.dart';
import 'package:technology/presentation/screens/signup/signupscreen.dart';
import 'package:technology/presentation/screens/wallet/wallet_screen.dart';

class HomeScreen extends StatefulWidget {
  final DashBoardEntity dashBoardEntity;

  const HomeScreen({Key key, this.dashBoardEntity}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // int _currentIndex = 2;
  HomeScreenBloc _homeScreenBloc;
  ProductBloc productBloc;
  DashBoardBloc dashBoardBloc;
  CommissionBloc commissionBloc;
  ProfileBloc profileBloc;
  AddClientBloc addClientBloc;
  WalletBloc walletBloc;
  PageController pageController=PageController(initialPage: 0, keepPage: true, viewportFraction: 1);
  var model;

  @override
  Widget build(BuildContext context) {
//    _homeScreenBloc = BlocProvider.of<HomeScreenBloc>(context);
    // bezier.dart supports both quadratic curves...

    return MultiBlocProvider(
      providers: [
        BlocProvider<WalletBloc>(
          create: (c) => walletBloc,
        ),
       BlocProvider<HomeScreenBloc>(
         create: (c) => _homeScreenBloc,
       ),
        BlocProvider<DashBoardBloc>(
          create: (c) => dashBoardBloc,
        ),
        BlocProvider<ProductBloc>(
          create: (c) => productBloc,
        ),
        BlocProvider<CommissionBloc>(
          create: (c) => commissionBloc,
        ),
        BlocProvider<ProfileBloc>(
          create: (c) => profileBloc,
        ),
        BlocProvider<AddClientBloc>(
          create: (context) => addClientBloc,
        )
      ],
      child: SafeArea(
        child: StreamBuilder<int>(
          initialData: 2,
          stream: _homeScreenBloc.currentPage,
          builder: (context, snapshot) {
            return Scaffold(
              bottomNavigationBar: BottomNavigationBar(
                onTap: onTabTapped,

                // new
                elevation: 5.0,
                currentIndex: snapshot.data,
                // new
//        showSelectedLabels: true,
//        showUnselectedLabels: true,
                type: BottomNavigationBarType.fixed,
                items: [
                  BottomNavigationBarItem(
                    activeIcon: Image.asset(
                      Images.conversionactive,
                      height: 20.0,
                      width: 20.0,
                    ),
                    icon: Image.asset(
                        Images.conversion,
                        height: 20.0,
                        width: 20.0
                    ),
                    title: FittedBox(
                        child: Text(Strings.conversion,
                            style: context.caption.copyWith(
                                fontSize: 10.0, fontWeight: FontWeight.w600)).tr(context: context)),
                  ),
                  BottomNavigationBarItem(
                    activeIcon: Image.asset(
                      Images.addClientActive,
                      height: 20.0,
                      width: 20.0,
                    ),
                    icon: Image.asset(
                      Images.addClient,
                      height: 20.0,
                      width: 20.0,
                    ),
                    title: FittedBox(
                        child: Text(Strings.addClient,
                            style: context.caption.copyWith(
                                fontSize: 10.0, fontWeight: FontWeight.w600)).tr()),
                  ),
                  BottomNavigationBarItem(
                    activeIcon: Image.asset(
                      Images.dashboardActive,
                      height: 20.0,
                      width: 20.0,
                    ),
                    icon: Image.asset(
                        Images.dashboard,
                        height: 20.0,
                        width: 20.0
                    ),
                    title: FittedBox(
                        child: Text(Strings.dashboard,
                            style: context.caption.copyWith(
                                fontSize: 10.0, fontWeight: FontWeight.w600)).tr(context: context)),
                  ),
                  // BottomNavigationBarItem(
                  //     activeIcon: Image.asset(
                  //       Images.dashboardActive,
                  //       height: 20.0,
                  //       width: 20.0,
                  //     ),
                  //     icon: Image.asset(
                  //       Images.dashboard,
                  //       height: 20.0,
                  //       width: 20.0,
                  //     ),
                  //     title: FittedBox(
                  //         child: Text(Strings.dashboard,
                  //             style: context.caption.copyWith(
                  //                 fontSize: 10.0, fontWeight: FontWeight.w600)).tr())),
                  BottomNavigationBarItem(
                      activeIcon: Image.asset(
                        Images.productsActive,
                        height: 20.0,
                        width: 20.0,
                      ),
                      icon: Image.asset(
                        Images.products,
                        height: 20.0,
                        width: 20.0,
                      ),
                      title: FittedBox(
                          child: Text(Strings.products,
                              style: context.caption.copyWith(
                                  fontSize: 10.0, fontWeight: FontWeight.w600)).tr())),
                  BottomNavigationBarItem(
                      activeIcon: Image.asset(
                        Images.profileActive,
                        height: 20.0,
                        width: 20.0,
                      ),
                      icon: Image.asset(
                        Images.profile,
                        height: 20.0,
                        width: 20.0,
                      ),
                      title: FittedBox(
                          child: Text("bottomProfile",
                              style: context.caption.copyWith(
                                  fontSize: 10.0, fontWeight: FontWeight.w600)).tr()))
                ],
              ),
              body: AnimatedSwitcher(
                  key: ValueKey(snapshot.data),
                  duration: Duration(milliseconds: 500),
                  child: IndexedStack(children: _buildScreens(),index: snapshot.data,)),
            );
          }
        ),
      ),
    ); _homeScreenBloc = BlocProvider.of<HomeScreenBloc>(context);
  }

  @override
  void initState() {

    // TODO: implement initState
    _homeScreenBloc=getIt<HomeScreenBloc>();
    productBloc = getIt<ProductBloc>();
    dashBoardBloc = getIt<DashBoardBloc>();
    commissionBloc = getIt<CommissionBloc>();
    profileBloc = getIt<ProfileBloc>();
    addClientBloc = getIt<AddClientBloc>();
    walletBloc = getIt<WalletBloc>();
    super.initState();
    loadCommissionData();
  }

  void onTabTapped(int index) {
    print("current state is " + index.toString());
//    pageController.animateToPage(index, duration: Duration(milliseconds: 500), curve: Curves.ease);
//     if (index == 2) loadDashBoardData();
     if (index == 3) {
      productBloc.add(GetProductList());
    }
    else if(index==1) loadCountries();
    else if(index==4)loadProfileData();
    else if(index==2)loadCommissionData();
    else if(index==0) walletBloc.add(GetWalletList());
    _homeScreenBloc.changeCurrentPage(index);
    // setState(() {
    //   _currentIndex = index;
    // });
  }

//  Widget configureBottomNavBar(){
//    return TitledBottomNavigationBar(
//        currentIndex: 2, // Use this to update the Bar giving a position
//        onTap: (index){
//          print("Selected Index: $index");
//        },
//        items: [
//          TitledNavigationBarItem(title: Text(Strings.conversion,style: context.caption.copyWith(fontWeight: FontWeight.w600),), icon: Images.conversion),
//          TitledNavigationBarItem(title: Text(Strings.addClient,style: context.caption.copyWith(),), icon: Images.addClient),
//          TitledNavigationBarItem(title: Text(Strings.dashboard,style: context.caption,), icon: Images.dashboard),
//          TitledNavigationBarItem(title: Text(Strings.products,style: context.caption,), icon: Images.products),
//          TitledNavigationBarItem(title: Text(Strings.profile,style: context.caption,), icon: Images.profile),
//        ]
//    );
//  }
  List<Widget> _buildScreens() {
    return [
      // DashBoardScreen(dashBoardEntity: widget.dashBoardEntity,),
      WalletScreen(),
      AddClientScreen(),
      CommissionScreen(dashBoardEntity: widget.dashBoardEntity,),
      ProductsScreen(),
      ProfileScreen()
    ];
  }

  void loadDashBoardData() {
    var date = DateCalc.now().subtractDay(14).toUtc();
    model = CommissionRequestModel(
        userId: widget.dashBoardEntity.id,
        startDate: DateFormat("ddMMyyyy").format(date),
        endDate: DateFormat("ddMMyyyy").format(DateCalc.now().toUtc()));
    dashBoardBloc.add(GetCommissionData(model));
  }
  void loadCommissionData() {
    var date = DateCalc.now().subtractDay(14).toUtc();
    model = CommissionRequestModel(
        userId: widget.dashBoardEntity.id,
        startDate: DateFormat("ddMMyyyy").format(date),
        endDate: DateFormat("ddMMyyyy").format(DateCalc.now().toUtc()),commissionEnum: CommissionEnum.FOR_COMMISSION);
    commissionBloc.add(GetDetailedCommission(model));
  }

  void loadProfileData() {
    profileBloc.add(GetUserProfile());
  }
  void loadCountries() {
    addClientBloc.add(GetCountriesList());
//    addClientBloc.add(GetCountriesList());
  }
//  _onTap(int index){
//    if(index==)
//  }
}
class AuthGuard extends RouteGuard {
  @override
  Future<bool> canNavigate(
      ExtendedNavigatorState navigator, String routeName, Object arguments) async {
    return userModel != null;
  }
}
