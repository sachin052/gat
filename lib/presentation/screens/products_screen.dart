
import 'package:awesome_card/awesome_card.dart' as card;
import 'package:credit_card/credit_card_form.dart';
import 'package:credit_card/credit_card_model.dart';
import 'package:credit_card/credit_card_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:stripe_payment/stripe_payment.dart';
import 'package:technology/domain/entity/product_entity.dart';
import 'package:technology/main.dart';
import 'package:technology/others/di/injection.dart';
import 'package:technology/others/styles/strings.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';
import 'package:technology/others/utils/loading_indicator.dart';
import 'package:technology/presentation/bloc/products/bloc.dart';
import 'package:technology/presentation/commonwidgets/snackbar/snackbar.dart';
import 'package:technology/presentation/screens/payment/payment.dart';
//import 'package:stripe_payment/stripe_payment.dart';
import 'package:easy_localization/easy_localization.dart';
class ProductsScreen extends StatefulWidget {
  @override
  _ProductsScreenState createState() => _ProductsScreenState();
}

class _ProductsScreenState extends State<ProductsScreen> {
  ProductBloc productBloc;
  String cardNumber = '';
  String expiryDate = '';
  String cardHolderName = '';
  String cvvCode = '';
  bool isCvvFocused = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    productBloc=BlocProvider.of<ProductBloc>(context);
    StripePayment.setOptions(
        StripeOptions(publishableKey: "pk_test_aSaULNS8cJU6Tvo20VAXy6rp", merchantId: "Test", androidPayMode: 'test'));
//    productBloc.add(GetProductList());
  }
  @override
  Widget build(BuildContext context) {
    return BlocListener<ProductBloc,CommonUIState<List<ProductEntity>>>(
      listener: (_,state){
        state.maybeWhen(
            error: (error)=>showSnackBar(context, error, true),
            orElse: (){});
      },
      child: BlocBuilder<ProductBloc,CommonUIState<List<ProductEntity>>>(
        builder: (context, snapshot) {
          return AnimatedSwitcher(
            duration: Duration(milliseconds: 500),
            child: snapshot.when(
                initial: ()=>Container(),
                success: (data)=>SafeArea(
                  child: Container(
                    padding: const EdgeInsets.only(top: 16.0,left: 16.0,right: 16.0),
                    child: ListView(
//          mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(Strings.productHeadLine,style: context.headLine6.copyWith(fontWeight: FontWeight.bold),).tr(),
                        Row(
                          children: <Widget>[
                            Text(Strings.currentProduct,style: context.body1,).tr(),
//              Flexible(child: FractionallySizedBox(widthFactor: .1,),),
                            SizedBox(width: 20.0,),
                            Expanded(child: Container(
                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(4.0),color: Colors.grey),
                              height: 20.0,child: StreamBuilder<String>(
                                stream: productBloc.productName,
                                initialData: "",
                                builder: (context, snapshot) {
                                  return Text(snapshot.data,style: context.body2.copyWith(color:Colors.white),);
                                }
                              ),alignment: Alignment.center,))
                          ],
                        ),
                        SizedBox(height: 30.0,),
                        Column(children: List.generate(data.length, (index) =>ProductCardItem(productEntity: data[index],)),),
                      ],
                    ),),
                ),
                loading: ()=>LoadingBar(),
                error: (e)=>Center(child: Text(e,textAlign: TextAlign.center,),)),
          );
        }
      ),
    );
  }


  void onCreditCardModelChange(CreditCardModel creditCardModel) {
    setState(() {
      cardNumber = creditCardModel.cardNumber;
      expiryDate = creditCardModel.expiryDate;
      cardHolderName = creditCardModel.cardHolderName;
      cvvCode = creditCardModel.cvvCode;
      isCvvFocused = creditCardModel.isCvvFocused;
    });
  }
}
