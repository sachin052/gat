import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class TitledNavigationBarItem {
  final Widget title;
  final String icon;
  final Color backgroundColor;

  TitledNavigationBarItem({
    @required this.icon,
    @required this.title,
    this.backgroundColor = Colors.white,
  });
}