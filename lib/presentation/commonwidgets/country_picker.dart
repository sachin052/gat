import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_picker_dialog.dart';
import 'package:country_pickers/country_pickers.dart';
import 'package:flutter/material.dart';
import 'package:technology/data/models/country_model.dart';
import 'package:technology/others/styles/colors.dart';

void showCountryPicker(BuildContext context,Function(Country) onCountryChange,{bool allowCountry,List<CountryEntity> items}){
  showDialog(
    context: context,
    builder: (context) => Theme(
      data: Theme.of(context).copyWith(primaryColor: Colors.pink),
      child: CountryPickerDialog(
        titlePadding: EdgeInsets.all(8.0),
        searchCursorColor: AppColors.colorPrimary,

        searchInputDecoration: InputDecoration(
            prefixIcon: Icon(Icons.search,color: AppColors.colorPrimary,),
            focusColor: AppColors.colorPrimary,
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: AppColors.colorPrimary),
            ),
            hintText: 'Search...',border: UnderlineInputBorder(

            borderSide: BorderSide(color: AppColors.colorPrimary,width: 1.0)
        )),
        isSearchable: true,
        itemFilter: (c){
          if(items!=null)
          return items.map((e) => e.countryName).toList().contains(c.isoCode);
          return true;
        },
        title: Text('Select your country',style: TextStyle(fontSize: 20.0),),
        onValuePicked: onCountryChange,
        itemBuilder: (c){
          return _buildDialogItem(c,allowCountry: allowCountry);
        },
      ),
    ),
  );
}
Widget _buildDialogItem(Country country,{bool allowCountry}) => Row(
  children: <Widget>[
    CountryPickerUtils.getDefaultFlagImage(country),
    Visibility(
        visible: allowCountry,
        child: SizedBox(width: 8.0)),
    Visibility(
        visible: allowCountry??false,
        child: Text("+${country.phoneCode}",style: TextStyle(fontSize: 16.0),)),
    SizedBox(width: 8.0),
    Flexible(child: Text(country.name,style: TextStyle(fontSize: 16.0),))
  ],
);