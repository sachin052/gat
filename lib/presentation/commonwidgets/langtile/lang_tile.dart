import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:styled_widget/styled_widget.dart';
import 'package:technology/others/utils/images/images.dart';
import 'package:technology/presentation/commonwidgets/button/custom_button.dart';

import '../../../main.dart';

class LangTile extends StatefulWidget {
  final String icons;
  final bool isSelected;
  final String langName;
  final VoidCallback onTap;
  const LangTile({Key key, this.icons, this.isSelected, this.langName, this.onTap }) : super(key: key);
  @override
  _LangTileState createState() => _LangTileState();
}

class _LangTileState extends State<LangTile> {
  bool pressed=false;
  @override
  Widget build(BuildContext context) {

    return CustomButton(
      color: Color(0XFFEFF3F6),
      onTap: (){
        widget.onTap();
      },
      height: 150,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              sizedBox25,
              Image.asset(widget.icons,height: 40,width: 40,),
              sizedBox25,
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(widget.langName,style: Theme.of(context).textTheme.headline5,),

                ],
              ),
            ],
          ),
          Visibility(
              visible: widget.isSelected,
              child: CircleAvatar(maxRadius: 12,child: Icon(Icons.done),))
        ],
      ),
    );
  }
}

Color mC = Colors.grey.shade100;
Color mCL = Colors.white;
Color mCD = Colors.black.withOpacity(0.075);
Color mCC = Colors.green.withOpacity(0.65);
Color fCD = Colors.grey.shade700;
Color fCL = Colors.grey;

BoxDecoration nMbox = BoxDecoration(
    borderRadius: BorderRadius.circular(15),
    color: Color(0XFFEFF3F6),
    boxShadow: [
      BoxShadow(
          color: Color.fromRGBO(0, 0, 0, 0.1),
          offset: Offset(6, 2),
          blurRadius: 3.0,
          spreadRadius: 3.0
      ),
      BoxShadow(
          color: Color.fromRGBO(255, 255, 255, 0.9),
          offset: Offset(-6, -2),
          blurRadius: 3.0,
          spreadRadius: 3.0
      )
    ]
);

BoxDecoration nMboxInvert = BoxDecoration(
    borderRadius: BorderRadius.circular(15),
    color: mCD,
    boxShadow: [
      BoxShadow(
          color: mCL,
          offset: Offset(3, 3),
          blurRadius: 3,
          spreadRadius: -3
      ),
    ]
);

BoxDecoration nMboxInvertActive = nMboxInvert.copyWith(color: mCC);

BoxDecoration nMbtn = BoxDecoration(
    borderRadius: BorderRadius.circular(10),
    color: mC,
    boxShadow: [
      BoxShadow(
        color: mCD,
        offset: Offset(2, 2),
        blurRadius: 2,
      )
    ]
);