import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:styled_widget/styled_widget.dart';
import 'package:technology/others/styles/colors.dart';
import 'package:easy_localization/easy_localization.dart';
//class CustomInputField extends StatefulWidget {
//  final Function(String) onTextChange;
//  final String onError;
//  final String icon;
//  final String labelText;
//  final TextInputAction action;
//  final FocusNode myNode;
//  final FocusNode nextNode;
//  final TextInputType inputType;
//  final TextEditingController controller;
//  final bool isPassword;
//  final Stream<String> stream;
//  final bool isReadOnly;
//  const CustomInputField(
//      {Key key,
//      this.onTextChange,
//      this.onError,
//      this.icon,
//      this.labelText,
//      this.action=TextInputAction.next,
//      this.myNode,
//      this.inputType=TextInputType.text,
//      this.controller, this.isPassword=false, this.nextNode, this.stream, this.isReadOnly=false})
//      : super(key: key);
//
//  @override
//  _CustomInputFieldState createState() => _CustomInputFieldState();
//}
//
//class _CustomInputFieldState extends State<CustomInputField> {
//  bool hasFocused = false;
//  @override
//  void initState() {
//    // TODO: implement initState
//    super.initState();
//    if(widget.myNode!=null)
//    widget.myNode.addListener(() {
//      setState(() {
//        hasFocused=widget.myNode.hasFocus;
//      });
//    });
//  }
//  @override
//  Widget build(BuildContext context) {
//    final settingsItem = (Widget child) => Styled.widget(child:child)
////        .borderRadius(all: 4)
////        .ripple()
//        .backgroundColor(AppColors.colorPrimary.withOpacity(.01), animate: true)
////        .clipRRect(all: 4) // clip ripple
////        .borderRadius(all: 4, animate: true)
////
////        .elevation(
////      hasFocused ? 0 : 0,
////      shadowColor: Colors.transparent,
////      borderRadius: BorderRadius.circular(25),
////    ) // shadow borderRadius
//        .constrained(height: 80)
////        .gestures(
//////      onTapDown: (details) => widget.onTap,
//////      onTap: () {
//////        widget.onTap();
//////      },
////    )
//        .scale(hasFocused ? 1.0 : 0.95, animate: true)
//        .animate(Duration(milliseconds: 150), Curves.easeOut);
//    return settingsItem(
//       Container(
//        height: 80.0,
//        color: Colors.transparent,
//        margin: EdgeInsets.only(top: 10.0),
//        child: StreamBuilder<String>(
//            stream: widget.stream,
//            builder: (context, snapshot) {
//              return TextField(
//                controller: widget.controller,
//                autofocus: false,
//                readOnly: widget.isReadOnly,
//                onSubmitted: (value){
//                  FocusScope.of(context).requestFocus(widget.nextNode);
//                },
//                onChanged: widget.onTextChange,
//                textInputAction: widget.action,
//                obscureText: widget.isPassword,
//                focusNode: widget.myNode,
//
//                keyboardType: widget.inputType == TextInputType.phone
//                    ? TextInputType.numberWithOptions(signed: true, decimal: true)
//                    : widget.inputType,
//                decoration: new InputDecoration(
//
//                  contentPadding: const EdgeInsets.symmetric(vertical: 0.0),
//                  errorText: snapshot.error??null,
//                  prefixIcon: Padding(
//                    padding: const EdgeInsets.all(12.0),
//                    child: SvgPicture.asset(
//                      widget.icon,
//                      color: widget.onError != null ? Colors.red : AppColors.colorPrimary,
//                      height: 10.0,
//                      width: 10.0,
//                    ),
//                  ),
//                  labelStyle: Theme.of(context).textTheme.bodyText2,
//                  focusedBorder: OutlineInputBorder(
//                    borderSide: BorderSide(color: AppColors.colorPrimary, width: 0.8),
//                  ),
//                  enabledBorder: OutlineInputBorder(
//                    borderSide: BorderSide(color: Colors.grey, width: 0.8),
//                  ),
//                  errorBorder: OutlineInputBorder(
//                    borderSide: BorderSide(color: Colors.red, width: 0.8),
//                  ),
//                  focusedErrorBorder: OutlineInputBorder(
//                    borderSide: BorderSide(color: Colors.red, width: 0.8),
//                  ),
//                  labelText: widget.labelText,
//                ),
//              );
//            }
//        ),
//      ),
//    );
//  }
//}

class CustomInputField extends StatefulWidget {
  final Function(String) onTextChange;
  final String errorText;
  final String labelText;
  final String icon;
  final TextInputAction action;
  final Function(String) onSubmit;
    final FocusNode myNode;
  final FocusNode nextNode;
  final TextInputType inputType;
  final TextEditingController controller;
  final bool isReadOnly;
  final bool hasSuffix;
  final VoidCallback onCountryTap;
  final String countryCode;
    final bool isPassword;
    final Stream<String> stream;
  const CustomInputField(
      {Key key,
        this.onTextChange,
        this.errorText,
        @required this.labelText,
         this.icon,
        this.inputType = TextInputType.text,
        this.action = TextInputAction.next,
        this.onSubmit,
        this.controller,
        this.isReadOnly = false,
        this.hasSuffix = false,
        this.onCountryTap,
        this.countryCode, this.myNode, this.nextNode, this.stream, this.isPassword=false})
      : super(key: key);

  @override
  _CustomInputFieldState createState() => _CustomInputFieldState();

}

class _CustomInputFieldState extends State<CustomInputField> {
  bool pressed = false;
  OverlayEntry overlayEntry;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.inputType);
    widget?.myNode?.addListener(() {
      setState(() {
        hasFocused=widget.myNode.hasFocus;
      });
    });
    // if (widget.inputType == TextInputType.number && Platform.isIOS) {
    //   if(widget?.myNode?.hasFocus==true){
    //
    //     showOverLay(context);
    //   }
    //   else{
    //     removeOverLay();
    //   }
    // }
//      KeyboardVisibilityNotification().addNewListener(
//        onChange: (bool visible) {
//          if (widget?.node?.hasFocus == true) {
//            showOverLay(context);
//          } else {
//            removeOverLay();
//          }
//        },
//      );
  }
  @override
  void dispose() {
    // TODO: implement dispose
    removeOverLay();
    super.dispose();
  }
  bool hasFocused = false;
  @override
  Widget build(BuildContext context) {
        final settingsItem = (Widget child) => Styled.widget(child:child)
//        .borderRadius(all: 4)
//        .ripple()
        .backgroundColor(AppColors.colorPrimary.withOpacity(.01), animate: true)
//        .clipRRect(all: 4) // clip ripple
//        .borderRadius(all: 4, animate: true)
//
//        .elevation(
//      hasFocused ? 0 : 0,
//      shadowColor: Colors.transparent,
//      borderRadius: BorderRadius.circular(25),
//    ) // shadow borderRadius
        .constrained(height: 80)
//        .gestures(
////      onTapDown: (details) => widget.onTap,
////      onTap: () {
////        widget.onTap();
////      },
//    )
        .scale( animate: true,all: hasFocused ? 1.0 : 0.95,)
        .animate(Duration(milliseconds: 150), Curves.easeOut);
     return settingsItem(Row(
      children: <Widget>[

        GestureDetector(
          onTap: widget.onCountryTap,
          child: AbsorbPointer(
            child: Visibility(
              visible: widget.inputType == TextInputType.number,
              child: Container(
                height: 40,
//                width: 90,
                margin: EdgeInsets.only(right: 8.0),
                padding: const EdgeInsets.symmetric(
                    horizontal: 8.0, vertical: 0.0),
                decoration: BoxDecoration(
                    border: Border(
                        right: BorderSide(
                            color: Colors.black.withOpacity(.5),
                            width: 1.0))),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(widget.countryCode != null
                        ? "+" + widget.countryCode
                        : ""),
                    Icon(Icons.arrow_drop_down)
                  ],
                ),
              ),
            ),
          ),
        ),
        Flexible(
          child: StreamBuilder<String>(
            stream: widget.stream,
            builder: (context, snapshot) {
              return TextField(
                onTap: widget.isReadOnly?widget.onCountryTap:null,
                controller: widget.controller,
                keyboardType: widget.inputType == TextInputType.phone
                    ? TextInputType.numberWithOptions(signed: true, decimal: true)
                    : widget.inputType,
                focusNode: widget.myNode,
                enabled: true,
                obscureText: widget.isPassword,
                readOnly: widget.isReadOnly,
                inputFormatters: <TextInputFormatter>[
                  widget.inputType == TextInputType.number
                      ? WhitelistingTextInputFormatter.digitsOnly
                      : BlacklistingTextInputFormatter.singleLineFormatter,
                ],
                textInputAction: widget.action,
                onSubmitted: (s){
                  FocusScope.of(context).requestFocus(widget.nextNode);
                },
                style: Theme.of(context).textTheme.body2,
                autofocus: false,
                onChanged: widget.onTextChange,
                decoration: InputDecoration(
                  contentPadding:  EdgeInsets.symmetric(vertical: 0.0,horizontal: widget.hasSuffix||widget.icon==null?10.0:0.0),
                  labelStyle: Theme.of(context).textTheme.bodyText2,
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: AppColors.colorPrimary, width: 0.8),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey, width: 0.8),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.red, width: 0.8),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.red, width: 0.8),
                  ),
//

                    labelText: widget.labelText.tr(),
                    errorText: snapshot.error??null,
                                prefixIcon: widget.icon!=null?Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: SvgPicture.asset(
                    widget.icon,
                    color: snapshot.hasError? Colors.red : AppColors.colorPrimary,
                    height: 10.0,
                    width: 10.0,
                  ),
                ):null,)

              );
            }
          ),
        ),
      ],
    ));
  }


}
OverlayEntry overlayEntry;
showOverLay(BuildContext context) {
  if (overlayEntry != null&&!Platform.isIOS) return;
  OverlayState overlayState = Overlay.of(context);
  overlayEntry = OverlayEntry(
      builder: (context) => Positioned(
        bottom: MediaQuery.of(context).viewInsets.bottom,
        right: 0.0,
        left: 0.0,
        child: InputDoneView(),
      ));
  overlayState.insert(overlayEntry);
}

removeOverLay() {
  if (overlayEntry != null) {
    overlayEntry.remove();
    overlayEntry = null;
  }
}

class InputDoneView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: Colors.white,
      padding: const EdgeInsets.only(right: 10.0),
      child: Align(
        alignment: Alignment.topRight,
        child: OutlineButton(
          highlightedBorderColor: AppColors.colorPrimary,
          onPressed: () {
            removeOverLay();
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: Text(
            "Done",style: Theme.of(context).textTheme.body2.copyWith(color: AppColors.colorPrimary),
          ),
        ),
      ),
    );
  }
}