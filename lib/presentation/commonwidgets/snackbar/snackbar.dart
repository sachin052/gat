import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:technology/others/styles/colors.dart';

showSnackBar(BuildContext context, String text, bool isError) {
  Flushbar(
    margin: EdgeInsets.all(8),
    borderRadius: 8,
    backgroundColor: isError ? Colors.red : AppColors.colorPrimary,
    flushbarStyle: FlushbarStyle.FLOATING,
    icon: Icon(
      isError ? Icons.error : Icons.done,
      color: Colors.white,
    ),
    message: text,
    duration: Duration(seconds: 3),
  )..show(context);
}