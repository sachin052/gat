
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:styled_widget/styled_widget.dart';
import 'package:technology/others/styles/colors.dart';
import 'package:technology/others/utils/common_ui_state/common_ui_state.dart';
import 'package:technology/presentation/commonwidgets/langtile/lang_tile.dart';


class CustomButton extends StatefulWidget {
  final Widget child;
  final VoidCallback onTap;
  final CommonUIState uiState;
  final Color color;
  final double height;
  final double cornerRadius;
  const CustomButton({Key key, this.child, this.onTap, this.uiState, this.color, this.height, this.cornerRadius}) : super(key: key);
  @override
  _CustomButtonState createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  bool pressed = false;
  @override
  Widget build(BuildContext context) {
    final settingsItem = (Widget child) => Styled.widget(child:child)
        .alignment(Alignment.center)
        .decorated(
      borderRadius: BorderRadius.circular(widget.cornerRadius??4),
      boxShadow: [
      BoxShadow(
          color: widget.color!=null?Color.fromRGBO(0, 0, 0, 0.1):Colors.transparent,
          offset: widget.color!=null?Offset(4, 2):Offset(0, 0),
          blurRadius: 6.0,
          spreadRadius: 1.0
      ),
      BoxShadow(
          color: computeColor(widget.color),
          offset: widget.color!=null?Offset(-4, -2):Offset(0.0,0.0),
          blurRadius: 6.0,
          spreadRadius: 2.0
      )
    ],color: widget.color??AppColors.colorPrimary, animate: true,)
//        .borderRadius(all: 4)
        .ripple()
//        .clipRRect(all: 4) // clip ripple
        .borderRadius(all: widget.cornerRadius??4, animate: true)
//        .elevation(
//      pressed ? 0 : 4,
//      shadowColor: widget.color!=null?widget.color:Color(0x30000000),
//      borderRadius: BorderRadius.circular(4),
//    ) // shadow borderRadius
        .constrained(height: widget.height??40)
        .padding(vertical: widget.color==Colors.white?0.0:12) // margin
        .gestures(
      onTapChange: (tapStatus) => setState(() => pressed = tapStatus),
      onTapDown: (details) => widget.onTap,
      onTap: () {
        widget.onTap();
      },
    )
        .scale( animate: true,all: pressed ? 0.95 : 1.0,)
        .animate(Duration(milliseconds: 150), Curves.easeOut);
    return settingsItem(<Widget>[
      Visibility(
          visible:widget.uiState==CommonUIState.loading(),
          child: LinearProgressIndicator()),
      Container(
        width: double.infinity,
        alignment: Alignment.center,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal:16.0),
          child: widget.child,
        ),
      )
    ].toStack(alignment: Alignment(-1, -1.1),));
  }
  Color computeColor(Color color){
        if(widget.color==null||widget.color==Colors.white)return Colors.transparent;
        return Color.fromRGBO(255, 255, 255, 0.9);
  }
}
