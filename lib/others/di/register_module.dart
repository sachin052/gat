import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:technology/data/constants/AppConstatnt.dart';

@module
abstract class RegisterModule {

  // You can register named preemptive types like follows
  @Named("BaseUrl")
  String get baseUrl => AppConstants.baseURLRegistration;

  // url here will be injected
  @lazySingleton
  Dio dio(@Named('BaseUrl') String url) => Dio(BaseOptions(baseUrl: url));

  @lazySingleton
  FlutterSecureStorage get storage=>FlutterSecureStorage();

  @preResolve // if you need to  pre resolve the value
  Future<SharedPreferences> get prefs async => await SharedPreferences.getInstance();
      // same thing works for instances that's gotten asynchronous.
      // all you need to do is wrap your instance with a future and tell injectable how
      // to initialize it
//      @preResolve // if you need to  pre resolve the value
//      Future<SharedPreferences> get prefs => SharedPreferences.getInstance();
// Also make sure you await for your configure function before running the App.

}