// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:amazon_cognito_identity_dart_2/cognito.dart';
import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../presentation/bloc/addclient/add_client_bloc.dart';
import '../../data/api_helper/api_helper.dart';
import '../../domain/repo/auth_repo.dart';
import '../../data/remote_data_source/repo_impl/auth_repo_impl.dart';
import '../../data/aws_cognito/aws_cognito_helper.dart';
import '../../data/aws_cognito/aws_cognito_user_pool.dart';
import '../../data/aws_cognito/cognito_storage_manager.dart';
import '../../presentation/bloc/commission/commission_bloc.dart';
import '../../domain/usecases/createclient/create_client.dart';
import '../../presentation/bloc/dashboard/dash_board_bloc.dart';
import '../../domain/usecases/dashboard/get_commission_use_case.dart';
import '../../domain/usecases/country/get_countries_usecase.dart';
import '../../domain/usecases/currency/get_currency_use_case.dart';
import '../../domain/usecases/profile/get_doc_list.dart';
import '../../domain/usecases/profile/get_emp_status.dart';
import '../../domain/usecases/products/get_product_usecase.dart';
import '../../domain/usecases/profile/get_user_profile.dart';
import '../../domain/usecases/wallet/get_wallet_details_use_cas.dart';
import '../../domain/usecases/wallet/get_wallet_history_use_case.dart';
import '../../domain/usecases/wallet/getwallet_use_case.dart';
import '../../presentation/bloc/home_screen/home_screen_bloc.dart';
import '../../data/local_data_source/local_data_source.dart';
import '../../domain/usecases/profile/logout_use_case.dart';
import '../../presentation/bloc/login/login_bloc.dart';
import '../../domain/usecases/login/login_usecase.dart';
import '../../presentation/bloc/payment/payment_bloc.dart';
import '../../presentation/bloc/products/product_bloc.dart';
import '../../presentation/bloc/profile/profile_bloc.dart';
import 'register_module.dart';
import '../../domain/usecases/profile/send_referral_email_usecase.dart';
import '../../presentation/bloc/signUp/signup_bloc.dart';
import '../../domain/usecases/sign_up/sign_up_usecase.dart';
import '../../domain/usecases/profile/update_profile_use_case.dart';
import '../../domain/usecases/update_purchase/update_purchase_usecase.dart';
import '../../domain/usecases/profile/upload_file_use_case.dart';
import '../../domain/usecases/profile/upload_verification_usecase.dart';
import '../../domain/repo/user_repo.dart';
import '../../data/remote_data_source/repo_impl/user_repo_impl.dart';
import '../../presentation/bloc/profile/verification/verifications_bloc.dart';
import '../../presentation/bloc/wallet/wallet_bloc.dart';
import '../../domain/repo/wallet_repo.dart';
import '../../data/remote_data_source/repo_impl/wallet_repo_impl.dart';

/// adds generated dependencies
/// to the provided [GetIt] instance

Future<GetIt> $initGetIt(
  GetIt get, {
  String environment,
  EnvironmentFilter environmentFilter,
}) async {
  final gh = GetItHelper(get, environment, environmentFilter);
  final registerModule = _$RegisterModule();
  gh.factory<ApiHelper>(() => ApiHelper());
  gh.lazySingleton<FlutterSecureStorage>(() => registerModule.storage);
  gh.factory<HomeScreenBloc>(() => HomeScreenBloc());
  gh.factory<LocalDataSource>(() => LocalDataSourceImpl());
  final sharedPreferences = await registerModule.prefs;
  gh.factory<SharedPreferences>(() => sharedPreferences);
  gh.factory<String>(() => registerModule.baseUrl, instanceName: 'BaseUrl');
  gh.factory<CognitoStorage>(
      () => CognitoStorageManager(get<SharedPreferences>()));
  gh.lazySingleton<Dio>(
      () => registerModule.dio(get<String>(instanceName: 'BaseUrl')));
  gh.factory<UserRepo>(() => UserRepoImpl(
        get<AwsCognitoUserPool>(),
        get<LocalDataSource>(),
        get<ApiHelper>(),
        get<AwsCognitoHelper>(),
      ));
  gh.factory<WalletRepo>(() => WalletRepoImpl(
        get<AwsCognitoUserPool>(),
        get<LocalDataSource>(),
        get<ApiHelper>(),
        get<AwsCognitoHelper>(),
      ));
  gh.factory<AuthRepo>(() => AuthRepoImpl(get<AwsCognitoHelper>()));
  gh.factory<CreateClientUseCase>(() => CreateClientUseCase(get<AuthRepo>()));
  gh.factory<GetCommissionUseCase>(() => GetCommissionUseCase(get<UserRepo>()));
  gh.factory<GetCountriesUseCase>(() => GetCountriesUseCase(get<UserRepo>()));
  gh.factory<GetCurrencyUseCase>(() => GetCurrencyUseCase(get<UserRepo>()));
  gh.factory<GetDocumentUseCase>(() => GetDocumentUseCase(get<UserRepo>()));
  gh.factory<GetEmpUseCase>(() => GetEmpUseCase(get<UserRepo>()));
  gh.factory<GetProductUseCase>(() => GetProductUseCase(get<UserRepo>()));
  gh.factory<GetUserProfileUseCase>(
      () => GetUserProfileUseCase(get<UserRepo>()));
  gh.factory<GetWalletDetailsUseCase>(
      () => GetWalletDetailsUseCase(get<WalletRepo>()));
  gh.factory<GetWalletHistoryUseCase>(
      () => GetWalletHistoryUseCase(get<WalletRepo>()));
  gh.factory<GetWalletUseCase>(() => GetWalletUseCase(get<WalletRepo>()));
  gh.factory<LogOutUserCase>(() => LogOutUserCase(get<UserRepo>()));
  gh.factory<LoginUseCase>(() => LoginUseCase(get<AuthRepo>()));
  gh.factory<ProductBloc>(
      () => ProductBloc(get<GetProductUseCase>(), get<LocalDataSource>()));
  gh.factory<SendReferralUseCase>(() => SendReferralUseCase(get<UserRepo>()));
  gh.factory<SignUpUseCase>(() => SignUpUseCase(get<AuthRepo>()));
  gh.factory<UpdateProfileUseCase>(() => UpdateProfileUseCase(get<UserRepo>()));
  gh.factory<UpdatePurchaseUseCase>(
      () => UpdatePurchaseUseCase(get<UserRepo>()));
  gh.factory<UploadFileUseCase>(() => UploadFileUseCase(get<UserRepo>()));
  gh.factory<UploadVerification>(() => UploadVerification(get<UserRepo>()));
  gh.factory<VerificationsBloc>(() => VerificationsBloc(
        get<GetDocumentUseCase>(),
        get<GetCountriesUseCase>(),
        get<UploadFileUseCase>(),
        get<UploadVerification>(),
      ));
  gh.factory<WalletBloc>(() => WalletBloc(
        get<GetWalletUseCase>(),
        get<GetWalletDetailsUseCase>(),
        get<GetWalletHistoryUseCase>(),
      ));
  gh.factory<AddClientBloc>(() =>
      AddClientBloc(get<CreateClientUseCase>(), get<GetCountriesUseCase>()));
  gh.factory<CommissionBloc>(() => CommissionBloc(get<GetCommissionUseCase>()));
  gh.factory<DashBoardBloc>(() => DashBoardBloc(get<GetCommissionUseCase>()));
  gh.factory<LoginBloc>(() => LoginBloc(get<LoginUseCase>()));
  gh.factory<PaymentBloc>(() => PaymentBloc(get<UpdatePurchaseUseCase>()));
  gh.factory<ProfileBloc>(() => ProfileBloc(
        get<GetUserProfileUseCase>(),
        get<UpdateProfileUseCase>(),
        get<GetCountriesUseCase>(),
        get<LogOutUserCase>(),
        get<SendReferralUseCase>(),
        get<GetCurrencyUseCase>(),
        get<GetEmpUseCase>(),
        get<GetDocumentUseCase>(),
      ));
  gh.factory<SignUpBloc>(() => SignUpBloc(get<SignUpUseCase>()));

  // Eager singletons must be registered in the right order
  gh.singleton<AwsCognitoUserPool>(AwsCognitoUserPool(get<CognitoStorage>()));
  gh.singleton<AwsCognitoHelper>(AwsCognitoHelper(
    get<ApiHelper>(),
    get<AwsCognitoUserPool>(),
    get<LocalDataSource>(),
  ));
  return get;
}

class _$RegisterModule extends RegisterModule {}
