import 'package:easy_localization/easy_localization.dart';

class Strings {
  // Login
  static String loginYourAccount = "login_your_account".tr();
  static String email = "email".tr();
  static String password = "password".tr();
  static String validEmail = "validEmail".tr();
  static String notEmpty = "notEmpty".tr();
  static String continueButton = "continue".tr();
  static String forgotPassword = "forgotPassword".tr();
  static String dontHaveAccount = "dontHaveAccount".tr();
  static String createAccount = "createAccount".tr();
  static String reqMinPass = "reqMinPass".tr();

  //Sign Up
  static String createAccountSignUp = "createAccountSignUp";
  static String firstName = "firstName";
  static String lastName = "lastName";
  static String confirmPassword = "confirmPassword";
  static String referID = "referID";
  static String registerNow = "registerNow";
  static String alreadyHaveAnAccount = "alreadyHaveAnAccount".tr();
  static String login = "login".tr();

  // Bottom Nav Bar
  static String conversion = "conversion";
  static String addClient = "addClient";
  static String dashboard = "dashboard";
  static String products = "products";
  static String profile = "profileTab";

  // Products
  static String productHeadLine = "productHeadLine";
  static String currentProduct = "currentProduct";

  // Add Client
  static String next = "next";
  static String middleName = "middleName";
  static String teleHome = "teleHome";
  static String teleWork = "teleWork";
  static String mobile = "mobile";
  static String secondaryEmail = "secondaryEmail";
  static String contactDetails = "contactDetails";
  static String streetAddress = "streetAddress";
  static String address = "address";
  static String city = "city";
  static String state = "state";
  static String country = "country";
  static String pinCode = "pinCode";
  static String postalAddress = "postalAddress";
  static String samePostalAddress = "samePostalAddress";
  static String createNewClient = "createNewClient";
  static String addNewClient = "addNewClient";
  static String kindlyConfirm = "kindlyConfirm";
  static String saveCreate = "saveCreate";



  static String currentSda = "currentSDA";
  static String totalCommission = "totalCommission";

  static String direct="direct";

  static String indirect="indirect";

  static String salesCount="salesCount";

  static String value="value";

  static String rate="rate";

  static String commission="commission";

  static String lifetimeSales='lifetimeSales';

  static String userSummary="userSummary";

  static String contact="contact";

  static String referralHistory="referralHistory";

  static String previous="previous";

  static String dialogSuTitle="dialogSubtitle";
  static String dialogTitle="dialogTitle";
  static String cancel="cancel";
  static String continueDialog="continue";

  static String referralTools="referralTools";
  static String personal="personal";
  static String referrerDetails="referrerDetails";
  static String referrerLinks="referrerLinks";
  static String sendCode="sendCode";
  static String sendReferralInvitation="sendReferralInvitation";
  static String chooseLanguage="chooseLanguage";
  static String english="english";
  static String chinese="chinese";
  static String sendNow="sendNow";
  static String copyLink="copyLink";
  static String referralLinkCopied="referralLinkCopied";
  static String scanQRCode="scanQRCode";

}
