import 'package:flutter/material.dart';

class AppColors{
  static const Color colorPrimary=Color(0xFF115FD2);
  static const Color textColor=Color(0xFF05264D);
  static const Color sfBgColor=Color(0XFFEFF3F6);
  static const Color darkBlue=Color(0xFF1E5FD2);
  static const Color lightBlue=Color(0xFF286ED6);
}