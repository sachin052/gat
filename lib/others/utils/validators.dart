import 'dart:async';

import 'package:technology/others/styles/strings.dart';



  final validateEmail =
      StreamTransformer<String, String>.fromHandlers(handleData: (email, sink) {
    if (email.isValidEmail) {
      sink.add(email);
    } else {
      sink.addError(Strings.validEmail);
    }
  });
  final validatePassword = StreamTransformer<String, String>.fromHandlers(
      handleData: (string, sink) {
   try{
     if (string.length > 5) {
       sink.add(string);
     } else {
       sink.addError(Strings.reqMinPass);
     }
   }catch(e){
     print("Exceptio:"+e.toString());
   }
  });
  final notEmptyValidator = StreamTransformer<String, String>.fromHandlers(
      handleData: (string, sink) {
    if (string.isNotEmpty) {
      sink.add(string);
    } else {
      sink.addError("Must not be empty");
    }
  });

final phoneValidator = StreamTransformer<String, String>.fromHandlers(
    handleData: (string, sink) {
      if (string.length>=10) {
        sink.add(string);
      } else {
        sink.addError("Enter a valid phone");
      }
    });

   confirmPassValidator(String password) => StreamTransformer<String, String>.fromHandlers(
      handleData: (string, sink) {
        print("actual pass"+ password);
        print("c pass"+ string);
        if (string.isNotEmpty&&string==password) {
          sink.add(string);
        } else {
          sink.addError("Password Mismatched");
        }
      });
extension StringExtensions on String {
  bool get isValidEmail {
    final emailRegExp = RegExp(r"^[a-zA-Z0-9.-_]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
    return emailRegExp.hasMatch(this);
  }
}

extension StringExtension on String {
  bool get isValidPass {
    return this.length > 5;
  }
}