class Images{
  static const String basePath="images/";
  static const String appIcon='$basePath'+'appicon.svg';
  static const String arrowRight='$basePath'+'arrowright.svg';
  static const String emailIcon='$basePath'+'email.svg';
  static const String lockIcon='$basePath'+'lock.svg';
  static const String personIcon='$basePath'+'person.svg';
  static const String refer='$basePath'+'reffer.svg';
  static const String engLang='$basePath'+'englang.png';
  static const String chineseLang='$basePath'+'chineselang.png';
  static const String calendar='$basePath'+'calendar.svg';
  static const String conversion='$basePath'+'wallet.png';
  static const String conversionactive='$basePath'+'wallet_active.png';
  static const String profile='$basePath'+'profile.png';
  static const String profileActive='$basePath'+'profileactive.png';
  static const String dashboard='$basePath'+'dashboard.png';
  static const String dashboardActive='$basePath'+'dashboardactive.png';
  static const String products='$basePath'+'products.png';
  static const String productsActive='$basePath'+'productactive.png';
  static const String addClient='$basePath'+'addclient.png';
  static const String addClientActive='$basePath'+'addclientactive.png';

  // Add Client
  static const String telephone='$basePath'+'telephone.svg';
  static const String mobile='$basePath'+'mobile.svg';
  static const String location='$basePath'+'location.svg';
  static const String city='$basePath'+'city.svg';
  static const String state='$basePath'+'state.svg';
  static const String country='$basePath'+'country.svg';
  static const String pin='$basePath'+'pin.svg';

}