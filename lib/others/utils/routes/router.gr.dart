// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';

import '../../../domain/entity/dashboard_entity.dart';
import '../../../main.dart';
import '../../../presentation/screens/homescreen/home_screen.dart';
import '../../../presentation/screens/login/login.dart';
import '../../../presentation/screens/signup/signupscreen.dart';

class Routes {
  static const String homeScreen = '/home-screen';
  static const String splash = '/Splash';
  static const String loginScreen = '/login-screen';
  static const String signUpScreen = '/sign-up-screen';
  static const all = <String>{
    homeScreen,
    splash,
    loginScreen,
    signUpScreen,
  };
}

class MyRouter extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.homeScreen, page: HomeScreen),
    RouteDef(Routes.splash, page: Splash),
    RouteDef(Routes.loginScreen, page: LoginScreen),
    RouteDef(Routes.signUpScreen, page: SignUpScreen),
  ];
  @override
  Map<Type, AutoRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, AutoRouteFactory>{
    HomeScreen: (data) {
      final args = data.getArgs<HomeScreenArguments>(
        orElse: () => HomeScreenArguments(),
      );
      return CupertinoPageRoute<dynamic>(
        builder: (context) => HomeScreen(
          key: args.key,
          dashBoardEntity: args.dashBoardEntity,
        ),
        settings: data,
      );
    },
    Splash: (data) {
      final args = data.getArgs<SplashArguments>(
        orElse: () => SplashArguments(),
      );
      return CupertinoPageRoute<dynamic>(
        builder: (context) => Splash(
          key: args.key,
          title: args.title,
          isFromProfile: args.isFromProfile,
        ),
        settings: data,
      );
    },
    LoginScreen: (data) {
      return CupertinoPageRoute<dynamic>(
        builder: (context) => LoginScreen(),
        settings: data,
      );
    },
    SignUpScreen: (data) {
      return CupertinoPageRoute<dynamic>(
        builder: (context) => SignUpScreen(),
        settings: data,
      );
    },
  };
}

/// ************************************************************************
/// Arguments holder classes
/// *************************************************************************

/// HomeScreen arguments holder class
class HomeScreenArguments {
  final Key key;
  final DashBoardEntity dashBoardEntity;
  HomeScreenArguments({this.key, this.dashBoardEntity});
}

/// Splash arguments holder class
class SplashArguments {
  final Key key;
  final String title;
  final bool isFromProfile;
  SplashArguments({this.key, this.title, this.isFromProfile = false});
}
