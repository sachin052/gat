import 'package:auto_route/auto_route_annotations.dart';
import 'package:technology/presentation/screens/homescreen/home_screen.dart';
import 'package:technology/presentation/screens/login/login.dart';
import 'package:technology/presentation/screens/signup/signupscreen.dart';

import '../../../main.dart';

@CupertinoAutoRouter(
  routes: <AutoRoute>[
    // initial route is named "/"
    CupertinoRoute(page: HomeScreen,),
    CupertinoRoute(page: Splash,),
    CupertinoRoute(page: LoginScreen, ),
    CupertinoRoute(page: SignUpScreen, ),
  ],
)  //CustomAutoRoute(..config)
class $MyRouter {
}