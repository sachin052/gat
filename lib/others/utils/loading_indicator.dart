import 'package:flutter/material.dart';
import 'package:technology/main.dart';
class LoadingBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          alignment: Alignment.bottomCenter,
          child: LinearProgressIndicator(),),
        Container(
          color: Colors.transparent,
          child: Center(child: Text("Loading. . .",style: context.headLine6,),),)
      ],
    );
  }
}
