import 'dart:convert';

import 'package:amazon_cognito_identity_dart_2/cognito.dart';
import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:technology/data/api_states/api_states.dart';
import 'package:technology/data/constants/AppConstatnt.dart';
import 'package:technology/others/di/injection.dart';
@injectable
class ApiHelper {
  Dio dio = getIt<Dio>();
  ApiHelper(){
    var interceptor=InterceptorsWrapper(
      onError: (e){
        print("error in response ${e.response.data["message"]}");
      },
      onRequest: (req){
//        req.headers.addAll({
//          'Content-Type': 'application/json; charset=utf-8'
//        });
        print(req);
        req.headers.toString()
        ;
      },
      onResponse: (res){
        print(res);
      }
    );
    dio.interceptors.add(interceptor);
  }
  changeBaseUrl(String baseUrl)=>dio.options.baseUrl=baseUrl;
  Future<Either<Failure,dynamic>> safeApiHelperRequest(Future<dynamic> Function() function)async{
  try{
    var response=await function.call();
    return Right(response);
  } on DioError catch(e){
      return Left(Failure.serverError(_handleError(e)));
  } catch(e){
    if (e is CognitoClientException) {
      if (e.code == "NetworkError")
        return Left(Failure.networkError("Connection to API server failed due to internet connection"));
      return Left(Failure.serverError(e.message));
    }
    return Left(Failure.serverError("Something went wrong"));
  }
  }
  Future<Either<Failure,dynamic>> post(String path,dynamic body,{dynamic headers})async => await safeApiHelperRequest(() => dio.post(path,data: body,options: Options(headers: headers)));

  Future<Either<Failure,T>> get<T>(String path,{Map<String,dynamic> headers})async =>
      await safeApiHelperRequest(() => dio.get(path,options: Options(headers:headers)));

  Future<Either<Failure,T>> put<T>(String path,{Map<String,dynamic> headers,dynamic body})async =>
      await safeApiHelperRequest(() => dio.put(path,options: Options(headers:headers),data: body));

  String _handleError(DioError error) {
    String errorDescription = "";
    if (error is DioError) {
      DioError dioError = error;
      switch (dioError.type) {
        case DioErrorType.CANCEL:
          errorDescription = "Request to API server was cancelled";
          break;
        case DioErrorType.CONNECT_TIMEOUT:
          errorDescription = "Connection timeout with API server";
          break;
        case DioErrorType.DEFAULT:
          errorDescription =
          "Connection to API server failed due to internet connection";
          break;
        case DioErrorType.RECEIVE_TIMEOUT:
          errorDescription = "Receive timeout in connection with API server";
          break;
        case DioErrorType.RESPONSE:
          errorDescription =
          "Received invalid status code: ${dioError.response.statusCode}";
          break;
        case DioErrorType.SEND_TIMEOUT:
          errorDescription = "Send timeout in connection with API server";
          break;
      }
    } else {
      errorDescription = "Unexpected error occured";
    }
    return errorDescription;
  }
}
