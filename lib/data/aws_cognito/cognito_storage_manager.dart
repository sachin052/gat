import 'dart:convert';

import 'package:amazon_cognito_identity_dart_2/cognito.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:technology/others/di/injection.dart';

@Injectable(as:CognitoStorage)
@singleton
class CognitoStorageManager extends CognitoStorage {
  SharedPreferences _prefs;
  CognitoStorageManager(this._prefs);

  @override
  Future getItem(String key) async {
    String item;
    try {
      item = json.decode(_prefs.getString(key));
    } catch (e) {
      return null;
    }
    return Future.value(item);
  }

  @override
  Future<String> setItem(String key, value) async {
    await _prefs.setString(key, json.encode(value));
    return getItem(key).then((value) => value);
  }

  @override
  Future removeItem(String key) async {
    final item = getItem(key);
    if (item != null) {
      await _prefs.remove(key);
      return item;
    }
    return null;
  }

  @override
  Future<void> clear() async {
    await _prefs.clear();
  }
}