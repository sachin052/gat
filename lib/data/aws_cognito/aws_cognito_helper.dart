import 'dart:convert';

import 'package:amazon_cognito_identity_dart_2/cognito.dart';
import 'package:amazon_cognito_identity_dart_2/sig_v4.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:technology/data/api_helper/api_helper.dart';
import 'package:technology/data/api_states/api_states.dart';
import 'package:technology/data/aws_cognito/aws_api_helper.dart';
import 'package:technology/data/aws_cognito/aws_cognito_user_pool.dart';
import 'package:technology/data/constants/AppConstatnt.dart';
import 'package:http/http.dart' as http;
import 'package:technology/data/local_data_source/local_data_source.dart';
import 'package:technology/data/models/getuser/get_user_response.dart';
import 'package:technology/data/models/login/login_model.dart';
import 'package:technology/data/models/signup/user_registration_model.dart';
import 'package:technology/others/di/injection.dart';

@singleton
class AwsCognitoHelper {
  final ApiHelper apiRequest;
  final AwsCognitoUserPool cognitoUserPoolHelper;
  final LocalDataSource localDataSource;
  RegistrationModel registrationModel;
  CognitoUserSession _session;
  AwsCognitoHelper(this.apiRequest, this.cognitoUserPoolHelper, this.localDataSource);
  Future<Either<Failure, T>> safeRequest<T>(
      Future<dynamic> Function() function) async {
    try {
      var response = await function.call();
      return Right(response);
    } catch (e) {
      if (e is CognitoClientException) {
        if (e.code == "NetworkError")
          return Left(Failure.networkError("Connection to API server failed due to internet connection"));
        return Left(Failure.serverError(e.message));
      }
      return Left(Failure.serverError("Something Went Wrong"));
    }
  }
  // User registration is done using 4 steps
  // 1. sign up user using cognito
  // 2. send api post request for registering user in database
  // 3. send two api request for email ( Step 3 and Step 4 )
  Future<Either<Failure, dynamic>> registerUser(RegistrationModel model,{bool createAccountForReferral=false}) async {
    registrationModel=model;
    apiRequest.changeBaseUrl(AppConstants.baseURLRegistration);
    var response = await safeRequest<CognitoUserPoolData>(() => cognitoUserPoolHelper.cognitoUserPool.signUp(
          model.email,
          model.password,
        ));
    return response.fold((failure) async{
      // return error
      return Left(failure);
    }, (poolData) async{

      // creating user model for saving user in DB

      var requestModel=model.copyWith.call(password: null,aws_cognito_id:poolData.userSub);
      // saving user
      var response=await saveUserInDB(requestModel,createAccountForReferral: createAccountForReferral);
      return response;

    });
  }
  Future<Either<Failure, dynamic>> saveUserInDB(RegistrationModel model,{bool createAccountForReferral=false}) async{
    if(createAccountForReferral){
    var userData=await localDataSource.getUserData();
    model=model.copyWith.call(referring_client_id: userData.payload.defaultClientId.toString());
      apiRequest.changeBaseUrl(AppConstants.createUser);
      var user= await cognitoUserPoolHelper.cognitoUserPool.getCurrentUser();
      var _session=await safeRequest(() => user.getSession());
     return _session.fold((l) => Left(l), (session) async{
        var awsApiHelper = AwsApiHelper(AppConstants.createUser,cognitoUserPoolHelper.cognitoUserPool,session);
        await awsApiHelper.createAwsSignV4Client();
        awsApiHelper.apiHelper.changeBaseUrl(AppConstants.baseURLRegistration);
        var response=await awsApiHelper.post("", model.toJson());
        return response.fold(
                (failure)  {
              return Left(failure);
            },
                (data) async{
              // required for next api call
              var insertId=data.value.data["payload"]["client_creation"]["insertId"].toString();
              var emailInteractionModel=EmailInteractionModel(
                  data: model.toJson(),
                  api_key: AppConstants.emailApiKey,
                  interaction: AppConstants.welcomeMessage
              );
              var response=await sendWelcomeEmail(emailInteractionModel,insertId);
              return response;
            });
      });
    }
    else{
      var response=await apiRequest.post(createAccountForReferral?"/"+AppConstants.createUser:AppConstants.registerUser,model.toJson());
      return response.fold(
              (failure)  {
            return Left(failure);
          },
              (data) async{
            // required for next api call
            var insertId=data.data["payload"]["client_creation"]["insertId"].toString();
            var emailInteractionModel=EmailInteractionModel(
                data: model.toJson(),
                api_key: AppConstants.emailApiKey,
                interaction: AppConstants.welcomeMessage
            );
            var response=await sendWelcomeEmail(emailInteractionModel,insertId);
            return response;
          });
    }
  }
  Future<Either<Failure, dynamic>> sendWelcomeEmail(EmailInteractionModel model,String insertId) async{
    apiRequest.dio.options.baseUrl=AppConstants.baseURLEmail;
    var response=await apiRequest.post(AppConstants.emailInteractionOpen,model.toJson());
    return  response.fold(
            (failure) => Left(Failure.serverError("Something went wrong")),
            (data) async{
            var model=NewPortalCreatedModel(api_key: AppConstants.emailApiKey,id: insertId,username: registrationModel.email,password: registrationModel.password);
            var response=await sendEmailPortalCreated(model);
            return response;
            });
  }
  Future<Either<Failure, dynamic>> sendEmailPortalCreated(NewPortalCreatedModel model) async{
    var response=await apiRequest.post("/"+AppConstants.emailNewPortalCreated,model.toJson());
    return response.fold(
            (failure) => Left(Failure.serverError("Something went wrong")),
            (data) => Right("User registered Successfully"));
  }

  Future<Either<Failure,UserModel>>authenticateUser(LoginModel model)async{
    final cognitoUser = new CognitoUser(model.username,cognitoUserPoolHelper.cognitoUserPool,storage: CognitoStorageHelper(getIt<CognitoStorage>()).storage);
    final authDetails = new AuthenticationDetails(
      username: model.username,
      password: model.password,
    );
      var sessionRequest = await safeRequest<CognitoUserSession>(() => cognitoUser.authenticateUser(authDetails));
      return sessionRequest.fold(
              (failure) => Left(failure),
              (currentUserSession) async{
                _session=currentUserSession;
                var userName=_session.getIdToken().payload["cognito:username"];
                apiRequest.changeBaseUrl(AppConstants.baseGetUser);
                var awsApiHelper=AwsApiHelper(AppConstants.getUserDetails,cognitoUserPoolHelper.cognitoUserPool,_session);
                var response=await awsApiHelper.get("/"+userName);
                return response.fold(
                        (error) => Left(error),
                        (r) async{
                          var model=UserModel.fromJson(r.data);
                          await localDataSource.saveUserData(model);
//                          await localDataSource.getCognitoUser();
//                          var data=await localDataSource.getCognitoUser();
//                          await localDataSource.saveRefreshToken(_session.getRefreshToken());
//                          await localDataSource.getRefreshToken();
                          return Right(model);
                        });
              });
  }

}

