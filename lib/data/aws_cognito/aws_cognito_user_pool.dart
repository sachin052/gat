import 'package:amazon_cognito_identity_dart_2/cognito.dart';
import 'package:injectable/injectable.dart';
import 'package:technology/data/aws_cognito/cognito_storage_manager.dart';
import 'package:technology/data/constants/AppConstatnt.dart';
import 'package:technology/others/di/injection.dart';

@singleton
class AwsCognitoUserPool{
  CognitoUserPool cognitoUserPool;
  final CognitoStorage _cognitoStorageManager;
  AwsCognitoUserPool(this._cognitoStorageManager){
    cognitoUserPool = new CognitoUserPool(
      AppConstants.userPoolId,
      AppConstants.clientId,
      storage: CognitoStorageHelper<CognitoStorageManager>(_cognitoStorageManager).storage
    );
  }
}