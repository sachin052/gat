/// payload : {"user_client":{"id":1,"aws_cognito_id":"f92521db-a30d-4574-80d7-e1c8b03f2b40","email":"test.001@gat.technology"},"default_client_id":113,"client":{"id":113,"user_id":"","aws_cognito_id":"","email":"test.001@gat.technology","nickname":"001, DevTest","client_status":2,"client_category_id":null,"referring_client_id":null,"current_product_id":3,"impact_id":null,"delios_crm_id":58,"delios_crm_status_id":3,"te_id":"677116","te_id_demo":"","te_login_demo":"","te_id_live":"","te_login_live":"","first_name":"FirstUser1","middle_names":"FirstUser1","last_name":"FirstUser2","gender_id":null,"tel_work":null,"tel_home":null,"tel_mobile":null,"date_of_birth":null,"place_of_birth_city":null,"place_of_birth_country_id":null,"email_secondary":null,"wechat_id":"we23456","wechat_deposit_enabled":0,"street_address_line_1":null,"street_address_line_2":null,"street_address_suburb":null,"street_address_state":null,"street_address_postcode":null,"street_address_country":null,"postal_address_line_1":null,"postal_address_line_2":null,"postal_address_suburb":null,"postal_address_state":null,"postal_address_postcode":null,"postal_address_country":null,"product_max_commission_override":0,"commission_override":0.25,"commission_override_base":0.15,"commission_override_extra":0.1,"preferred_language":"en","wechat_deposit":null,"usdt_wallet_address":"123123123","usdt_wallet_file_id":"","account_bsb":"060 123","account_country":31,"account_currency":0,"account_number":"1234 1234","account_sort_code":null,"account_iban":null,"account_swift":null,"account_aba":"","active_data_id":2,"account_beneficiary_name":null,"account_bank_card_file_id":"document-20200902-213625-9b4fbf96-58ab-42bf-bf53-865b96c94cc3","account_bank_name":"","account_bank_address":"new sachin","street_address_city":null,"postal_address_city":null,"street_address_region":null,"postal_address_region":null,"employment_status_id":null,"employer":"Pimwa Technologies","annual_income":149988,"net_worth":171038,"intended_deposit":55701,"tax_country_id":null,"tfn_exemption":0,"tax_file_number":null,"us_citizen":0,"us_tax_resident":0,"knowledge_traded_leveraged":0,"knowledge_traded_options":0,"knowledge_qualification":0,"sales_director_override":2,"record_updated":"2020-12-03T22:15:45.000Z","record_created":null,"products_nickname":"Junior","products_badge_color":"#a85200"},"client_access":[{"client_id":113},{"client_id":114}]}

class Test {
  Payload _payload;

  Payload get payload => _payload;

  Test({
      Payload payload}){
    _payload = payload;
}

  Test.fromJson(dynamic json) {
    _payload = json["payload"] != null ? Payload.fromJson(json["payload"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_payload != null) {
      map["payload"] = _payload.toJson();
    }
    return map;
  }

}

/// user_client : {"id":1,"aws_cognito_id":"f92521db-a30d-4574-80d7-e1c8b03f2b40","email":"test.001@gat.technology"}
/// default_client_id : 113
/// client : {"id":113,"user_id":"","aws_cognito_id":"","email":"test.001@gat.technology","nickname":"001, DevTest","client_status":2,"client_category_id":null,"referring_client_id":null,"current_product_id":3,"impact_id":null,"delios_crm_id":58,"delios_crm_status_id":3,"te_id":"677116","te_id_demo":"","te_login_demo":"","te_id_live":"","te_login_live":"","first_name":"FirstUser1","middle_names":"FirstUser1","last_name":"FirstUser2","gender_id":null,"tel_work":null,"tel_home":null,"tel_mobile":null,"date_of_birth":null,"place_of_birth_city":null,"place_of_birth_country_id":null,"email_secondary":null,"wechat_id":"we23456","wechat_deposit_enabled":0,"street_address_line_1":null,"street_address_line_2":null,"street_address_suburb":null,"street_address_state":null,"street_address_postcode":null,"street_address_country":null,"postal_address_line_1":null,"postal_address_line_2":null,"postal_address_suburb":null,"postal_address_state":null,"postal_address_postcode":null,"postal_address_country":null,"product_max_commission_override":0,"commission_override":0.25,"commission_override_base":0.15,"commission_override_extra":0.1,"preferred_language":"en","wechat_deposit":null,"usdt_wallet_address":"123123123","usdt_wallet_file_id":"","account_bsb":"060 123","account_country":31,"account_currency":0,"account_number":"1234 1234","account_sort_code":null,"account_iban":null,"account_swift":null,"account_aba":"","active_data_id":2,"account_beneficiary_name":null,"account_bank_card_file_id":"document-20200902-213625-9b4fbf96-58ab-42bf-bf53-865b96c94cc3","account_bank_name":"","account_bank_address":"new sachin","street_address_city":null,"postal_address_city":null,"street_address_region":null,"postal_address_region":null,"employment_status_id":null,"employer":"Pimwa Technologies","annual_income":149988,"net_worth":171038,"intended_deposit":55701,"tax_country_id":null,"tfn_exemption":0,"tax_file_number":null,"us_citizen":0,"us_tax_resident":0,"knowledge_traded_leveraged":0,"knowledge_traded_options":0,"knowledge_qualification":0,"sales_director_override":2,"record_updated":"2020-12-03T22:15:45.000Z","record_created":null,"products_nickname":"Junior","products_badge_color":"#a85200"}
/// client_access : [{"client_id":113},{"client_id":114}]

class Payload {
  User_client _userClient;
  int _defaultClientId;
  Client _client;
  List<Client_access> _clientAccess;

  User_client get userClient => _userClient;
  int get defaultClientId => _defaultClientId;
  Client get client => _client;
  List<Client_access> get clientAccess => _clientAccess;

  Payload({
      User_client userClient, 
      int defaultClientId, 
      Client client, 
      List<Client_access> clientAccess}){
    _userClient = userClient;
    _defaultClientId = defaultClientId;
    _client = client;
    _clientAccess = clientAccess;
}

  Payload.fromJson(dynamic json) {
    _userClient = json["user_client"] != null ? User_client.fromJson(json["userClient"]) : null;
    _defaultClientId = json["default_client_id"];
    _client = json["client"] != null ? Client.fromJson(json["client"]) : null;
    if (json["client_access"] != null) {
      _clientAccess = [];
      json["client_access"].forEach((v) {
        _clientAccess.add(Client_access.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_userClient != null) {
      map["user_client"] = _userClient.toJson();
    }
    map["default_client_id"] = _defaultClientId;
    if (_client != null) {
      map["client"] = _client.toJson();
    }
    if (_clientAccess != null) {
      map["client_access"] = _clientAccess.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// client_id : 113

class Client_access {
  int _clientId;

  int get clientId => _clientId;

  Client_access({
      int clientId}){
    _clientId = clientId;
}

  Client_access.fromJson(dynamic json) {
    _clientId = json["client_id"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["client_id"] = _clientId;
    return map;
  }

}

/// id : 113
/// user_id : ""
/// aws_cognito_id : ""
/// email : "test.001@gat.technology"
/// nickname : "001, DevTest"
/// client_status : 2
/// client_category_id : null
/// referring_client_id : null
/// current_product_id : 3
/// impact_id : null
/// delios_crm_id : 58
/// delios_crm_status_id : 3
/// te_id : "677116"
/// te_id_demo : ""
/// te_login_demo : ""
/// te_id_live : ""
/// te_login_live : ""
/// first_name : "FirstUser1"
/// middle_names : "FirstUser1"
/// last_name : "FirstUser2"
/// gender_id : null
/// tel_work : null
/// tel_home : null
/// tel_mobile : null
/// date_of_birth : null
/// place_of_birth_city : null
/// place_of_birth_country_id : null
/// email_secondary : null
/// wechat_id : "we23456"
/// wechat_deposit_enabled : 0
/// street_address_line_1 : null
/// street_address_line_2 : null
/// street_address_suburb : null
/// street_address_state : null
/// street_address_postcode : null
/// street_address_country : null
/// postal_address_line_1 : null
/// postal_address_line_2 : null
/// postal_address_suburb : null
/// postal_address_state : null
/// postal_address_postcode : null
/// postal_address_country : null
/// product_max_commission_override : 0
/// commission_override : 0.25
/// commission_override_base : 0.15
/// commission_override_extra : 0.1
/// preferred_language : "en"
/// wechat_deposit : null
/// usdt_wallet_address : "123123123"
/// usdt_wallet_file_id : ""
/// account_bsb : "060 123"
/// account_country : 31
/// account_currency : 0
/// account_number : "1234 1234"
/// account_sort_code : null
/// account_iban : null
/// account_swift : null
/// account_aba : ""
/// active_data_id : 2
/// account_beneficiary_name : null
/// account_bank_card_file_id : "document-20200902-213625-9b4fbf96-58ab-42bf-bf53-865b96c94cc3"
/// account_bank_name : ""
/// account_bank_address : "new sachin"
/// street_address_city : null
/// postal_address_city : null
/// street_address_region : null
/// postal_address_region : null
/// employment_status_id : null
/// employer : "Pimwa Technologies"
/// annual_income : 149988
/// net_worth : 171038
/// intended_deposit : 55701
/// tax_country_id : null
/// tfn_exemption : 0
/// tax_file_number : null
/// us_citizen : 0
/// us_tax_resident : 0
/// knowledge_traded_leveraged : 0
/// knowledge_traded_options : 0
/// knowledge_qualification : 0
/// sales_director_override : 2
/// record_updated : "2020-12-03T22:15:45.000Z"
/// record_created : null
/// products_nickname : "Junior"
/// products_badge_color : "#a85200"

class Client {
  int _id;
  String _userId;
  String _awsCognitoId;
  String _email;
  String _nickname;
  int _clientStatus;
  dynamic _clientCategoryId;
  dynamic _referringClientId;
  int _currentProductId;
  dynamic _impactId;
  int _deliosCrmId;
  int _deliosCrmStatusId;
  String _teId;
  String _teIdDemo;
  String _teLoginDemo;
  String _teIdLive;
  String _teLoginLive;
  String _firstName;
  String _middleNames;
  String _lastName;
  dynamic _genderId;
  dynamic _telWork;
  dynamic _telHome;
  dynamic _telMobile;
  dynamic _dateOfBirth;
  dynamic _placeOfBirthCity;
  dynamic _placeOfBirthCountryId;
  dynamic _emailSecondary;
  String _wechatId;
  int _wechatDepositEnabled;
  dynamic _streetAddressLine1;
  dynamic _streetAddressLine2;
  dynamic _streetAddressSuburb;
  dynamic _streetAddressState;
  dynamic _streetAddressPostcode;
  dynamic _streetAddressCountry;
  dynamic _postalAddressLine1;
  dynamic _postalAddressLine2;
  dynamic _postalAddressSuburb;
  dynamic _postalAddressState;
  dynamic _postalAddressPostcode;
  dynamic _postalAddressCountry;
  int _productMaxCommissionOverride;
  double _commissionOverride;
  double _commissionOverrideBase;
  double _commissionOverrideExtra;
  String _preferredLanguage;
  dynamic _wechatDeposit;
  String _usdtWalletAddress;
  String _usdtWalletFileId;
  String _accountBsb;
  int _accountCountry;
  int _accountCurrency;
  String _accountNumber;
  dynamic _accountSortCode;
  dynamic _accountIban;
  dynamic _accountSwift;
  String _accountAba;
  int _activeDataId;
  dynamic _accountBeneficiaryName;
  String _accountBankCardFileId;
  String _accountBankName;
  String _accountBankAddress;
  dynamic _streetAddressCity;
  dynamic _postalAddressCity;
  dynamic _streetAddressRegion;
  dynamic _postalAddressRegion;
  dynamic _employmentStatusId;
  String _employer;
  int _annualIncome;
  int _netWorth;
  int _intendedDeposit;
  dynamic _taxCountryId;
  int _tfnExemption;
  dynamic _taxFileNumber;
  int _usCitizen;
  int _usTaxResident;
  int _knowledgeTradedLeveraged;
  int _knowledgeTradedOptions;
  int _knowledgeQualification;
  int _salesDirectorOverride;
  String _recordUpdated;
  dynamic _recordCreated;
  String _productsNickname;
  String _productsBadgeColor;

  int get id => _id;
  String get userId => _userId;
  String get awsCognitoId => _awsCognitoId;
  String get email => _email;
  String get nickname => _nickname;
  int get clientStatus => _clientStatus;
  dynamic get clientCategoryId => _clientCategoryId;
  dynamic get referringClientId => _referringClientId;
  int get currentProductId => _currentProductId;
  dynamic get impactId => _impactId;
  int get deliosCrmId => _deliosCrmId;
  int get deliosCrmStatusId => _deliosCrmStatusId;
  String get teId => _teId;
  String get teIdDemo => _teIdDemo;
  String get teLoginDemo => _teLoginDemo;
  String get teIdLive => _teIdLive;
  String get teLoginLive => _teLoginLive;
  String get firstName => _firstName;
  String get middleNames => _middleNames;
  String get lastName => _lastName;
  dynamic get genderId => _genderId;
  dynamic get telWork => _telWork;
  dynamic get telHome => _telHome;
  dynamic get telMobile => _telMobile;
  dynamic get dateOfBirth => _dateOfBirth;
  dynamic get placeOfBirthCity => _placeOfBirthCity;
  dynamic get placeOfBirthCountryId => _placeOfBirthCountryId;
  dynamic get emailSecondary => _emailSecondary;
  String get wechatId => _wechatId;
  int get wechatDepositEnabled => _wechatDepositEnabled;
  dynamic get streetAddressLine1 => _streetAddressLine1;
  dynamic get streetAddressLine2 => _streetAddressLine2;
  dynamic get streetAddressSuburb => _streetAddressSuburb;
  dynamic get streetAddressState => _streetAddressState;
  dynamic get streetAddressPostcode => _streetAddressPostcode;
  dynamic get streetAddressCountry => _streetAddressCountry;
  dynamic get postalAddressLine1 => _postalAddressLine1;
  dynamic get postalAddressLine2 => _postalAddressLine2;
  dynamic get postalAddressSuburb => _postalAddressSuburb;
  dynamic get postalAddressState => _postalAddressState;
  dynamic get postalAddressPostcode => _postalAddressPostcode;
  dynamic get postalAddressCountry => _postalAddressCountry;
  int get productMaxCommissionOverride => _productMaxCommissionOverride;
  double get commissionOverride => _commissionOverride;
  double get commissionOverrideBase => _commissionOverrideBase;
  double get commissionOverrideExtra => _commissionOverrideExtra;
  String get preferredLanguage => _preferredLanguage;
  dynamic get wechatDeposit => _wechatDeposit;
  String get usdtWalletAddress => _usdtWalletAddress;
  String get usdtWalletFileId => _usdtWalletFileId;
  String get accountBsb => _accountBsb;
  int get accountCountry => _accountCountry;
  int get accountCurrency => _accountCurrency;
  String get accountNumber => _accountNumber;
  dynamic get accountSortCode => _accountSortCode;
  dynamic get accountIban => _accountIban;
  dynamic get accountSwift => _accountSwift;
  String get accountAba => _accountAba;
  int get activeDataId => _activeDataId;
  dynamic get accountBeneficiaryName => _accountBeneficiaryName;
  String get accountBankCardFileId => _accountBankCardFileId;
  String get accountBankName => _accountBankName;
  String get accountBankAddress => _accountBankAddress;
  dynamic get streetAddressCity => _streetAddressCity;
  dynamic get postalAddressCity => _postalAddressCity;
  dynamic get streetAddressRegion => _streetAddressRegion;
  dynamic get postalAddressRegion => _postalAddressRegion;
  dynamic get employmentStatusId => _employmentStatusId;
  String get employer => _employer;
  int get annualIncome => _annualIncome;
  int get netWorth => _netWorth;
  int get intendedDeposit => _intendedDeposit;
  dynamic get taxCountryId => _taxCountryId;
  int get tfnExemption => _tfnExemption;
  dynamic get taxFileNumber => _taxFileNumber;
  int get usCitizen => _usCitizen;
  int get usTaxResident => _usTaxResident;
  int get knowledgeTradedLeveraged => _knowledgeTradedLeveraged;
  int get knowledgeTradedOptions => _knowledgeTradedOptions;
  int get knowledgeQualification => _knowledgeQualification;
  int get salesDirectorOverride => _salesDirectorOverride;
  String get recordUpdated => _recordUpdated;
  dynamic get recordCreated => _recordCreated;
  String get productsNickname => _productsNickname;
  String get productsBadgeColor => _productsBadgeColor;

  Client({
      int id, 
      String userId, 
      String awsCognitoId, 
      String email, 
      String nickname, 
      int clientStatus, 
      dynamic clientCategoryId, 
      dynamic referringClientId, 
      int currentProductId, 
      dynamic impactId, 
      int deliosCrmId, 
      int deliosCrmStatusId, 
      String teId, 
      String teIdDemo, 
      String teLoginDemo, 
      String teIdLive, 
      String teLoginLive, 
      String firstName, 
      String middleNames, 
      String lastName, 
      dynamic genderId, 
      dynamic telWork, 
      dynamic telHome, 
      dynamic telMobile, 
      dynamic dateOfBirth, 
      dynamic placeOfBirthCity, 
      dynamic placeOfBirthCountryId, 
      dynamic emailSecondary, 
      String wechatId, 
      int wechatDepositEnabled, 
      dynamic streetAddressLine1, 
      dynamic streetAddressLine2, 
      dynamic streetAddressSuburb, 
      dynamic streetAddressState, 
      dynamic streetAddressPostcode, 
      dynamic streetAddressCountry, 
      dynamic postalAddressLine1, 
      dynamic postalAddressLine2, 
      dynamic postalAddressSuburb, 
      dynamic postalAddressState, 
      dynamic postalAddressPostcode, 
      dynamic postalAddressCountry, 
      int productMaxCommissionOverride, 
      double commissionOverride, 
      double commissionOverrideBase, 
      double commissionOverrideExtra, 
      String preferredLanguage, 
      dynamic wechatDeposit, 
      String usdtWalletAddress, 
      String usdtWalletFileId, 
      String accountBsb, 
      int accountCountry, 
      int accountCurrency, 
      String accountNumber, 
      dynamic accountSortCode, 
      dynamic accountIban, 
      dynamic accountSwift, 
      String accountAba, 
      int activeDataId, 
      dynamic accountBeneficiaryName, 
      String accountBankCardFileId, 
      String accountBankName, 
      String accountBankAddress, 
      dynamic streetAddressCity, 
      dynamic postalAddressCity, 
      dynamic streetAddressRegion, 
      dynamic postalAddressRegion, 
      dynamic employmentStatusId, 
      String employer, 
      int annualIncome, 
      int netWorth, 
      int intendedDeposit, 
      dynamic taxCountryId, 
      int tfnExemption, 
      dynamic taxFileNumber, 
      int usCitizen, 
      int usTaxResident, 
      int knowledgeTradedLeveraged, 
      int knowledgeTradedOptions, 
      int knowledgeQualification, 
      int salesDirectorOverride, 
      String recordUpdated, 
      dynamic recordCreated, 
      String productsNickname, 
      String productsBadgeColor}){
    _id = id;
    _userId = userId;
    _awsCognitoId = awsCognitoId;
    _email = email;
    _nickname = nickname;
    _clientStatus = clientStatus;
    _clientCategoryId = clientCategoryId;
    _referringClientId = referringClientId;
    _currentProductId = currentProductId;
    _impactId = impactId;
    _deliosCrmId = deliosCrmId;
    _deliosCrmStatusId = deliosCrmStatusId;
    _teId = teId;
    _teIdDemo = teIdDemo;
    _teLoginDemo = teLoginDemo;
    _teIdLive = teIdLive;
    _teLoginLive = teLoginLive;
    _firstName = firstName;
    _middleNames = middleNames;
    _lastName = lastName;
    _genderId = genderId;
    _telWork = telWork;
    _telHome = telHome;
    _telMobile = telMobile;
    _dateOfBirth = dateOfBirth;
    _placeOfBirthCity = placeOfBirthCity;
    _placeOfBirthCountryId = placeOfBirthCountryId;
    _emailSecondary = emailSecondary;
    _wechatId = wechatId;
    _wechatDepositEnabled = wechatDepositEnabled;
    _streetAddressLine1 = streetAddressLine1;
    _streetAddressLine2 = streetAddressLine2;
    _streetAddressSuburb = streetAddressSuburb;
    _streetAddressState = streetAddressState;
    _streetAddressPostcode = streetAddressPostcode;
    _streetAddressCountry = streetAddressCountry;
    _postalAddressLine1 = postalAddressLine1;
    _postalAddressLine2 = postalAddressLine2;
    _postalAddressSuburb = postalAddressSuburb;
    _postalAddressState = postalAddressState;
    _postalAddressPostcode = postalAddressPostcode;
    _postalAddressCountry = postalAddressCountry;
    _productMaxCommissionOverride = productMaxCommissionOverride;
    _commissionOverride = commissionOverride;
    _commissionOverrideBase = commissionOverrideBase;
    _commissionOverrideExtra = commissionOverrideExtra;
    _preferredLanguage = preferredLanguage;
    _wechatDeposit = wechatDeposit;
    _usdtWalletAddress = usdtWalletAddress;
    _usdtWalletFileId = usdtWalletFileId;
    _accountBsb = accountBsb;
    _accountCountry = accountCountry;
    _accountCurrency = accountCurrency;
    _accountNumber = accountNumber;
    _accountSortCode = accountSortCode;
    _accountIban = accountIban;
    _accountSwift = accountSwift;
    _accountAba = accountAba;
    _activeDataId = activeDataId;
    _accountBeneficiaryName = accountBeneficiaryName;
    _accountBankCardFileId = accountBankCardFileId;
    _accountBankName = accountBankName;
    _accountBankAddress = accountBankAddress;
    _streetAddressCity = streetAddressCity;
    _postalAddressCity = postalAddressCity;
    _streetAddressRegion = streetAddressRegion;
    _postalAddressRegion = postalAddressRegion;
    _employmentStatusId = employmentStatusId;
    _employer = employer;
    _annualIncome = annualIncome;
    _netWorth = netWorth;
    _intendedDeposit = intendedDeposit;
    _taxCountryId = taxCountryId;
    _tfnExemption = tfnExemption;
    _taxFileNumber = taxFileNumber;
    _usCitizen = usCitizen;
    _usTaxResident = usTaxResident;
    _knowledgeTradedLeveraged = knowledgeTradedLeveraged;
    _knowledgeTradedOptions = knowledgeTradedOptions;
    _knowledgeQualification = knowledgeQualification;
    _salesDirectorOverride = salesDirectorOverride;
    _recordUpdated = recordUpdated;
    _recordCreated = recordCreated;
    _productsNickname = productsNickname;
    _productsBadgeColor = productsBadgeColor;
}

  Client.fromJson(dynamic json) {
    _id = json["id"];
    _userId = json["user_id"];
    _awsCognitoId = json["aws_cognito_id"];
    _email = json["email"];
    _nickname = json["nickname"];
    _clientStatus = json["client_status"];
    _clientCategoryId = json["client_category_id"];
    _referringClientId = json["referring_client_id"];
    _currentProductId = json["current_product_id"];
    _impactId = json["impact_id"];
    _deliosCrmId = json["delios_crm_id"];
    _deliosCrmStatusId = json["delios_crm_status_id"];
    _teId = json["te_id"];
    _teIdDemo = json["te_id_demo"];
    _teLoginDemo = json["te_login_demo"];
    _teIdLive = json["te_id_live"];
    _teLoginLive = json["te_login_live"];
    _firstName = json["first_name"];
    _middleNames = json["middle_names"];
    _lastName = json["last_name"];
    _genderId = json["gender_id"];
    _telWork = json["tel_work"];
    _telHome = json["tel_home"];
    _telMobile = json["tel_mobile"];
    _dateOfBirth = json["date_of_birth"];
    _placeOfBirthCity = json["place_of_birth_city"];
    _placeOfBirthCountryId = json["place_of_birth_country_id"];
    _emailSecondary = json["email_secondary"];
    _wechatId = json["wechat_id"];
    _wechatDepositEnabled = json["wechat_deposit_enabled"];
    _streetAddressLine1 = json["street_address_line_1"];
    _streetAddressLine2 = json["street_address_line_2"];
    _streetAddressSuburb = json["street_address_suburb"];
    _streetAddressState = json["street_address_state"];
    _streetAddressPostcode = json["street_address_postcode"];
    _streetAddressCountry = json["street_address_country"];
    _postalAddressLine1 = json["postal_address_line_1"];
    _postalAddressLine2 = json["postal_address_line_2"];
    _postalAddressSuburb = json["postal_address_suburb"];
    _postalAddressState = json["postal_address_state"];
    _postalAddressPostcode = json["postal_address_postcode"];
    _postalAddressCountry = json["postal_address_country"];
    _productMaxCommissionOverride = json["product_max_commission_override"];
    _commissionOverride = json["commission_override"];
    _commissionOverrideBase = json["commission_override_base"];
    _commissionOverrideExtra = json["commission_override_extra"];
    _preferredLanguage = json["preferred_language"];
    _wechatDeposit = json["wechat_deposit"];
    _usdtWalletAddress = json["usdt_wallet_address"];
    _usdtWalletFileId = json["usdt_wallet_file_id"];
    _accountBsb = json["account_bsb"];
    _accountCountry = json["account_country"];
    _accountCurrency = json["account_currency"];
    _accountNumber = json["account_number"];
    _accountSortCode = json["account_sort_code"];
    _accountIban = json["account_iban"];
    _accountSwift = json["account_swift"];
    _accountAba = json["account_aba"];
    _activeDataId = json["active_data_id"];
    _accountBeneficiaryName = json["account_beneficiary_name"];
    _accountBankCardFileId = json["account_bank_card_file_id"];
    _accountBankName = json["account_bank_name"];
    _accountBankAddress = json["account_bank_address"];
    _streetAddressCity = json["street_address_city"];
    _postalAddressCity = json["postal_address_city"];
    _streetAddressRegion = json["street_address_region"];
    _postalAddressRegion = json["postal_address_region"];
    _employmentStatusId = json["employment_status_id"];
    _employer = json["employer"];
    _annualIncome = json["annual_income"];
    _netWorth = json["net_worth"];
    _intendedDeposit = json["intended_deposit"];
    _taxCountryId = json["tax_country_id"];
    _tfnExemption = json["tfn_exemption"];
    _taxFileNumber = json["tax_file_number"];
    _usCitizen = json["us_citizen"];
    _usTaxResident = json["us_tax_resident"];
    _knowledgeTradedLeveraged = json["knowledge_traded_leveraged"];
    _knowledgeTradedOptions = json["knowledge_traded_options"];
    _knowledgeQualification = json["knowledge_qualification"];
    _salesDirectorOverride = json["sales_director_override"];
    _recordUpdated = json["record_updated"];
    _recordCreated = json["record_created"];
    _productsNickname = json["products_nickname"];
    _productsBadgeColor = json["products_badge_color"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["user_id"] = _userId;
    map["aws_cognito_id"] = _awsCognitoId;
    map["email"] = _email;
    map["nickname"] = _nickname;
    map["client_status"] = _clientStatus;
    map["client_category_id"] = _clientCategoryId;
    map["referring_client_id"] = _referringClientId;
    map["current_product_id"] = _currentProductId;
    map["impact_id"] = _impactId;
    map["delios_crm_id"] = _deliosCrmId;
    map["delios_crm_status_id"] = _deliosCrmStatusId;
    map["te_id"] = _teId;
    map["te_id_demo"] = _teIdDemo;
    map["te_login_demo"] = _teLoginDemo;
    map["te_id_live"] = _teIdLive;
    map["te_login_live"] = _teLoginLive;
    map["first_name"] = _firstName;
    map["middle_names"] = _middleNames;
    map["last_name"] = _lastName;
    map["gender_id"] = _genderId;
    map["tel_work"] = _telWork;
    map["tel_home"] = _telHome;
    map["tel_mobile"] = _telMobile;
    map["date_of_birth"] = _dateOfBirth;
    map["place_of_birth_city"] = _placeOfBirthCity;
    map["place_of_birth_country_id"] = _placeOfBirthCountryId;
    map["email_secondary"] = _emailSecondary;
    map["wechat_id"] = _wechatId;
    map["wechat_deposit_enabled"] = _wechatDepositEnabled;
    map["street_address_line_1"] = _streetAddressLine1;
    map["street_address_line_2"] = _streetAddressLine2;
    map["street_address_suburb"] = _streetAddressSuburb;
    map["street_address_state"] = _streetAddressState;
    map["street_address_postcode"] = _streetAddressPostcode;
    map["street_address_country"] = _streetAddressCountry;
    map["postal_address_line_1"] = _postalAddressLine1;
    map["postal_address_line_2"] = _postalAddressLine2;
    map["postal_address_suburb"] = _postalAddressSuburb;
    map["postal_address_state"] = _postalAddressState;
    map["postal_address_postcode"] = _postalAddressPostcode;
    map["postal_address_country"] = _postalAddressCountry;
    map["product_max_commission_override"] = _productMaxCommissionOverride;
    map["commission_override"] = _commissionOverride;
    map["commission_override_base"] = _commissionOverrideBase;
    map["commission_override_extra"] = _commissionOverrideExtra;
    map["preferred_language"] = _preferredLanguage;
    map["wechat_deposit"] = _wechatDeposit;
    map["usdt_wallet_address"] = _usdtWalletAddress;
    map["usdt_wallet_file_id"] = _usdtWalletFileId;
    map["account_bsb"] = _accountBsb;
    map["account_country"] = _accountCountry;
    map["account_currency"] = _accountCurrency;
    map["account_number"] = _accountNumber;
    map["account_sort_code"] = _accountSortCode;
    map["account_iban"] = _accountIban;
    map["account_swift"] = _accountSwift;
    map["account_aba"] = _accountAba;
    map["active_data_id"] = _activeDataId;
    map["account_beneficiary_name"] = _accountBeneficiaryName;
    map["account_bank_card_file_id"] = _accountBankCardFileId;
    map["account_bank_name"] = _accountBankName;
    map["account_bank_address"] = _accountBankAddress;
    map["street_address_city"] = _streetAddressCity;
    map["postal_address_city"] = _postalAddressCity;
    map["street_address_region"] = _streetAddressRegion;
    map["postal_address_region"] = _postalAddressRegion;
    map["employment_status_id"] = _employmentStatusId;
    map["employer"] = _employer;
    map["annual_income"] = _annualIncome;
    map["net_worth"] = _netWorth;
    map["intended_deposit"] = _intendedDeposit;
    map["tax_country_id"] = _taxCountryId;
    map["tfn_exemption"] = _tfnExemption;
    map["tax_file_number"] = _taxFileNumber;
    map["us_citizen"] = _usCitizen;
    map["us_tax_resident"] = _usTaxResident;
    map["knowledge_traded_leveraged"] = _knowledgeTradedLeveraged;
    map["knowledge_traded_options"] = _knowledgeTradedOptions;
    map["knowledge_qualification"] = _knowledgeQualification;
    map["sales_director_override"] = _salesDirectorOverride;
    map["record_updated"] = _recordUpdated;
    map["record_created"] = _recordCreated;
    map["products_nickname"] = _productsNickname;
    map["products_badge_color"] = _productsBadgeColor;
    return map;
  }

}

/// id : 1
/// aws_cognito_id : "f92521db-a30d-4574-80d7-e1c8b03f2b40"
/// email : "test.001@gat.technology"

class User_client {
  int _id;
  String _awsCognitoId;
  String _email;

  int get id => _id;
  String get awsCognitoId => _awsCognitoId;
  String get email => _email;

  User_client({
      int id, 
      String awsCognitoId, 
      String email}){
    _id = id;
    _awsCognitoId = awsCognitoId;
    _email = email;
}

  User_client.fromJson(dynamic json) {
    _id = json["id"];
    _awsCognitoId = json["aws_cognito_id"];
    _email = json["email"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["aws_cognito_id"] = _awsCognitoId;
    map["email"] = _email;
    return map;
  }

}