import 'dart:convert';
import 'dart:io';
import 'package:amazon_cognito_identity_dart_2/sig_v4.dart';
import 'package:async/async.dart';
import 'package:http/http.dart' as http;
import 'package:amazon_cognito_identity_dart_2/cognito.dart';
import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

import 'package:technology/data/api_helper/api_helper.dart';
import 'package:technology/data/api_states/api_states.dart';
import 'package:technology/data/aws_cognito/aws_api_helper.dart';
import 'package:technology/data/aws_cognito/aws_cognito_helper.dart';
import 'package:technology/data/aws_cognito/aws_cognito_user_pool.dart';
import 'package:technology/data/aws_cognito/cognito_storage_manager.dart';
import 'package:technology/data/constants/AppConstatnt.dart';
import 'package:technology/data/local_data_source/local_data_source.dart';
import 'package:technology/data/models/commission/commission_request_model.dart';
import 'package:technology/data/models/commission/commission_response.dart';
import 'package:technology/data/models/country_model.dart';
import 'package:technology/data/models/currency/CountryResponse.dart';
import 'package:technology/data/models/document_model.dart';
import 'package:technology/data/models/emp_status.dart';
import 'package:technology/data/models/getuser/get_user_response.dart';
import 'package:technology/data/models/products/product_response.dart';
import 'package:technology/data/models/referral/referral_model.dart';
import 'package:technology/data/models/signup/user_registration_model.dart';
import 'package:technology/domain/entity/commission_entity.dart';
import 'package:technology/domain/entity/dashboard_entity.dart';
import 'package:technology/domain/entity/product_entity.dart';
import 'package:technology/domain/repo/user_repo.dart';
// import 'package:upload_to_s3bucket/upload_to_s3bucket.dart';

@Injectable(as: UserRepo)
class UserRepoImpl implements UserRepo{
   AwsApiHelper _awsApiHelper;
   final AwsCognitoUserPool cognitoUserPoolHelper;
   final LocalDataSource localDataSource;
   final ApiHelper _apiHelper;
   final AwsCognitoHelper helper;
  UserRepoImpl(this.cognitoUserPoolHelper, this.localDataSource, this._apiHelper, this.helper);


  @override
  Future<Either<Failure, List<ProductEntity>>> getProductList() async{
    _apiHelper.changeBaseUrl(AppConstants.baseGetProductList);
    var user= await cognitoUserPoolHelper.cognitoUserPool.getCurrentUser();
    var _session=await helper.safeRequest(() => user.getSession());
    return _session.fold((l) => Left(l), (session)async{
      _awsApiHelper=AwsApiHelper(AppConstants.getProductList,cognitoUserPoolHelper.cognitoUserPool,session);
      await _awsApiHelper.createAwsSignV4Client();
      var response=await _awsApiHelper.get("");
      return response
          .fold((l) {
        return Left(l);
      }
          , (data)  {
            var model=ProductResponse.fromJson(data.data);
            return Right(List<ProductEntity>.generate(model.payload.length, (index) => ProductEntity.fromApiResponse(model.payload[index])));
          });
    } );

  }

  @override
  Future<Either<Failure, dynamic>> updateUserPurchase(ProductEntity productEntity) async{
    _apiHelper.changeBaseUrl(AppConstants.baseCreatePurchaseProduct);
    var savedUserData=await localDataSource.getUserData();
    var user= await cognitoUserPoolHelper.cognitoUserPool.getCurrentUser();
    var _session=await helper.safeRequest(() => user.getSession());
    return _session.fold((l) => Left(l), (session) async{
      _awsApiHelper=AwsApiHelper(AppConstants.createPurchase,cognitoUserPoolHelper.cognitoUserPool,session);
      // creating purchase record in table
      var response=await _awsApiHelper.post("", {
        "client_id":savedUserData.payload.client.id.toString(),
        "product_id":productEntity.id.toString(),
        "payment_method_id":"2"
      });
      return response.fold
        ((l) => Left(l),
              (r) async{
                var purchaseId=r.value.data["payload"]["purchase_creation"]["insertId"];
                _apiHelper.changeBaseUrl(AppConstants.baseURLEmail);
                _awsApiHelper=AwsApiHelper(AppConstants.emailInteraction,cognitoUserPoolHelper.cognitoUserPool,session);
                Map<String,dynamic> map={
                  "interaction": "Purchase CREATE",
                  "data":{
                    "client_id":savedUserData.payload.client.id,
                    "client_nickname":"${savedUserData.payload.client.firstName}, ${savedUserData.payload.client.lastName}",
                    "product_id":productEntity.id,
                    "product_nickname":productEntity.productTitle,
                    "product_retail_price":productEntity.productPrice,
                    "payment_method_id":"2"
                  },
                  "result":"Purchase ID: $purchaseId"};
                var emailResponse=await _awsApiHelper.post("", map);
                return emailResponse.fold((l) => Left(l),
                        (r)async => await  makePurchase(purchaseId.toString(),productEntity,session,savedUserData));
            return makePurchase(r,productEntity,session,savedUserData);
          });
    });
  }

  @override
  Future<Either<Failure, dynamic>> getUserCommission(CommissionRequestModel model,) async{
    _apiHelper.changeBaseUrl(AppConstants.baseGetCommission);
    var user= await cognitoUserPoolHelper.cognitoUserPool.getCurrentUser();
    var session=await helper.safeRequest(() => user.getSession());
   return session.fold((l) => Left(l), (r) async{
     _awsApiHelper=AwsApiHelper(AppConstants.getUserCommission,cognitoUserPoolHelper.cognitoUserPool,r);
     var result=await  _awsApiHelper.get("/${model.userId}/${model.startDate}/${model.endDate}");
     return result.fold(
             (l) => Left(l),
             (r) {
           var response=CommissionResponse.fromJson(r.data);
           if(model.commissionEnum==CommissionEnum.FOR_DASHBOARD) return Right(DashBoardEntity(referrals: response.payload.referralsFiltered.map((e) => DashBoardCommissionEntity.fromResponse(e)).toList()));
           return Right(CommissionEntity.fromResponse(response));
         });

   });
  }

  @override
  Future<Either<Failure, UserModel>> getUserProfile() async{
    var user= await cognitoUserPoolHelper.cognitoUserPool.getCurrentUser();
    var _session=await helper.safeRequest(() => user.getSession());
    return _session.fold((l) => Left(l), (session) async{
      var userName=session.getIdToken().payload["cognito:username"];
      _apiHelper.changeBaseUrl(AppConstants.baseGetUser);
      var awsApiHelper=AwsApiHelper(AppConstants.getUserDetails,cognitoUserPoolHelper.cognitoUserPool,session);
      var response=await awsApiHelper.get("/"+userName);
      return response.fold(
              (error) => Left(error),
              (r) async{
            var model=UserModel.fromJson(r.data);
            await localDataSource.saveUserData(model);
//                          await localDataSource.getCognitoUser();
//                          var data=await localDataSource.getCognitoUser();
//                          await localDataSource.saveRefreshToken(_session.getRefreshToken());
//                          await localDataSource.getRefreshToken();
            return Right(model);
          });
    });

  }

  @override
  Future<Either<Failure, dynamic>> updateUserProfile(Map<String,dynamic> registrationModel) async{
    var user= await cognitoUserPoolHelper.cognitoUserPool.getCurrentUser();
    var userData=await localDataSource.getUserData();
    var _session=await helper.safeRequest(() => user.getSession());
    return _session.fold((l) => Left(l), (session) async{
      // var userName=session.getIdToken().payload["cognito:username"];
      var userName=userData.payload.client.id;
      _apiHelper.changeBaseUrl(AppConstants.baseURLRegistration);
      var awsApiHelper=AwsApiHelper(AppConstants.updateUserDetails,cognitoUserPoolHelper.cognitoUserPool,session);
      var response=await awsApiHelper.put("/$userName",registrationModel);
      return response;
    });
  }

  @override
  Future<Either<Failure, List<CountryEntity>>> getSupportedLanguages() async{
    var user= await cognitoUserPoolHelper.cognitoUserPool.getCurrentUser();
    var _session=await helper.safeRequest(() => user.getSession());
    return _session.fold((l) => Left(l), (session) async{
      _apiHelper.changeBaseUrl(AppConstants.baseCountry);
      var awsApiHelper=AwsApiHelper(AppConstants.getCountry,cognitoUserPoolHelper.cognitoUserPool,session);
      var response=await awsApiHelper.get("");
      return response.fold((l) =>
          Left(l),
              (r) {
        var data=CustomCountry.fromJson(r.data);
        return Right(data.payload.map((e) => CountryEntity(countryName: e.isoAlpha2,id:e.id,fullName: e.fullName)).toList());
      });
    });

  }

  @override
  Future<Either<Failure, dynamic>> logOutUser() async{
    var user= await cognitoUserPoolHelper.cognitoUserPool.getCurrentUser();
    await user.signOut();
    await localDataSource.clearData();
    return Right("");
  }

  @override
  Future<Either<Failure, dynamic>> sendInviteByEmail(Map<String, dynamic> request) async{
    var user= await cognitoUserPoolHelper.cognitoUserPool.getCurrentUser();
    var _session=await helper.safeRequest(() => user.getSession());
    return _session.fold((l) => Left(l), (session) async{
      var model=ReferralModel.fromJson(request);
      _apiHelper.changeBaseUrl(AppConstants.baseURLEmail);
      Map<String,dynamic> map={
        "data":{"client_id":model.client_id,"email":model.email,"language_id":model.language_id}
      };
       _awsApiHelper=AwsApiHelper(AppConstants.sendReferralEmail,cognitoUserPoolHelper.cognitoUserPool,session);
       _awsApiHelper.apiHelper.changeBaseUrl(AppConstants.baseURLEmail);
      var response=await _awsApiHelper.post("",map);
      return response.fold((l) => Left(l), (r) {
        return Right("Email sent successfully");
      });
    });
  }

  Future<Either<Failure, dynamic>> makePurchase(String purchaseId, ProductEntity productEntity, session, UserModel savedUserData) async{
     _apiHelper.changeBaseUrl(AppConstants.baseCreatePurchaseProduct);
     // var purchaseId=r.value.data["payload"]["purchase_creation"]["insertId"];
     _awsApiHelper=AwsApiHelper(AppConstants.updatePurchase,cognitoUserPoolHelper.cognitoUserPool,session);
     Map<String,dynamic> requesBody= {
       "status": "3"
     };
     // updating purchase record
     var updatePurchase=await _awsApiHelper.put("/"+purchaseId.toString(),requesBody);
     return updatePurchase.fold((l) => Left(l), (r) async{
       _apiHelper.changeBaseUrl(AppConstants.baseUpdateProduct);
       _awsApiHelper=AwsApiHelper(AppConstants.updateClient,cognitoUserPoolHelper.cognitoUserPool,session);
       await _awsApiHelper.createAwsSignV4Client();
       Map<String,dynamic> map={
         "client_status":"3",
         "current_product_id":productEntity.id
       };
       // updating client table with purchased product
       var response=await _awsApiHelper.put("/${savedUserData.payload.client.id.toString()}",map);
       return response
           .fold((l) {
         return Left(l);
       }
           , (data) async{
             _awsApiHelper=AwsApiHelper(AppConstants.purchaseSuccess,cognitoUserPoolHelper.cognitoUserPool,session);
             Map<String,dynamic> map={
               "client_id":savedUserData.payload.client.id,
               "purchase_id":purchaseId
             };
             var response=await _awsApiHelper.post("", map);
             return response.fold((l) => null, (r) async{
               // sending confirmation email
               _apiHelper.changeBaseUrl(AppConstants.baseURLEmail);
               _awsApiHelper=AwsApiHelper(AppConstants.emailInteraction,cognitoUserPoolHelper.cognitoUserPool,session);
               Map<String,dynamic> map={
                 "interaction": "Purchase SUCCESS",
                 "data":{
                   "client_id":savedUserData.payload.client.id,
                   "client_nickname":"${savedUserData.payload.client.firstName}, ${savedUserData.payload.client.lastName}",
                   "product_id":productEntity.id,
                   "product_nickname":productEntity.productTitle,
                   "product_retail_price":productEntity.productPrice,
                   "payment_method_id":"2"
                 },
                 "result":"stripe reports SUCCESS"};
               var emailResponse=await _awsApiHelper.post("", map);
               return emailResponse.fold((l) => Left(l), (r) => Right("Purchased Successfully"));
             });

           });
     });
   }

  @override
  Future<Either<Failure, CurrencyResponse>> getCurrency()async {
    var user= await cognitoUserPoolHelper.cognitoUserPool.getCurrentUser();
    var _session=await helper.safeRequest(() => user.getSession());
    return _session.fold((l) => Left(Failure.serverError(l.errorMessage)), (r) async{
      _apiHelper.changeBaseUrl(AppConstants.baseCurrency);
      _awsApiHelper=AwsApiHelper(AppConstants.getCurrency,cognitoUserPoolHelper.cognitoUserPool,r);

    var currency=await _awsApiHelper.get("");
    return currency.fold(
            (l) => Left(l),
            (r) => Right(CurrencyResponse.fromJson(r.data)));
    });
  }

  @override
  Future<Either<Failure, EmpStatus>> getEmpList() async{
    var user= await cognitoUserPoolHelper.cognitoUserPool.getCurrentUser();
    var _session=await helper.safeRequest(() => user.getSession());
    return _session.fold((l) => Left(Failure.serverError(l.errorMessage)), (r) async{
      _apiHelper.changeBaseUrl(AppConstants.baseURLRegistration);
      _awsApiHelper=AwsApiHelper(AppConstants.getEmpList,cognitoUserPoolHelper.cognitoUserPool,r);

      var currency=await _awsApiHelper.get("");
      return currency.fold(
              (l) => Left(l),
              (r) => Right(EmpStatus.fromJson(r.data)));
    });
  }

  @override
  Future<Either<Failure, DocumentModel>> getDocumentList(String id) async{
    var user= await cognitoUserPoolHelper.cognitoUserPool.getCurrentUser();
    var _session=await helper.safeRequest(() => user.getSession());

    return _session.fold((l) => Left(Failure.serverError(l.errorMessage)), (r) async{
      var userName=r.getIdToken().payload["cognito:username"];
      _apiHelper.changeBaseUrl(AppConstants.baseDocument);
      _awsApiHelper=AwsApiHelper(AppConstants.getDocumentList,cognitoUserPoolHelper.cognitoUserPool,r);

      var currency=await _awsApiHelper.get("/"+id);
      return currency.fold(
              (l) => Left(l),
              (r) => Right(DocumentModel.fromJson(r.data)));
    });
  }

  @override
  Future<Either<Failure, String>> uploadFile(String path) async{
    var user= await cognitoUserPoolHelper.cognitoUserPool.getCurrentUser();
    var _session=await helper.safeRequest<CognitoUserSession>(() => user.getSession());

    return _session.fold((l) => null, (r) async{
      var cognitoCredentials = CognitoCredentials(AppConstants.identityPool, cognitoUserPoolHelper.cognitoUserPool);
      await cognitoCredentials.getAwsCredentials(r.getIdToken().getJwtToken());
      _awsApiHelper=AwsApiHelper(AppConstants.getDocumentList,cognitoUserPoolHelper.cognitoUserPool,r);
      var userName=r.getIdToken().payload["cognito:username"];
      var _s3Endpoint = 'https://${AppConstants.bucketName}.s3-ap-southeast-1.amazonaws.com';
      final file = File(path);
      final String usrIdentityId = cognitoCredentials.userIdentityId;
      final String bucketKey = 'document-$userName';
      final stream = http.ByteStream(DelegatingStream.typed(file.openRead()));
      final length = await file.length();
      final uri = Uri.parse(_s3Endpoint);
      final req = http.MultipartRequest("POST", uri);
      final multipartFile = http.MultipartFile('file', stream, length,
          filename: file.path);

      final String fileName = 'square-cinnamon.jpg';

      final policy = Policy.fromS3PresignedPost(
          bucketKey,
          AppConstants.bucketName,
          15,
          cognitoCredentials.accessKeyId,
          length,
          cognitoCredentials.sessionToken,
          region: AppConstants.region);
      final key = SigV4.calculateSigningKey(
          cognitoCredentials.secretAccessKey, policy.datetime, AppConstants.region, 's3');
      final signature = SigV4.calculateSignature(key, policy.encode());
      req.files.add(multipartFile);
      req.fields['key'] = policy.key;
      req.fields['Content-type'] = "image/jpeg";
      // req.fields['ACL'] = 'public-read'; // Safe to remove this if your bucket has no ACL permissions
      req.fields['X-Amz-Credential'] = policy.credential;
      req.fields['X-Amz-Algorithm'] = 'AWS4-HMAC-SHA256';
      req.fields['X-Amz-Date'] = policy.datetime;
      req.fields['Policy'] = policy.encode();
      req.fields['X-Amz-Signature'] = signature;
      req.fields['x-amz-security-token'] = cognitoCredentials.sessionToken;
      try {
        final res = await req.send();
        if(res.statusCode==204)
          return Right(res.headers["location"]);
        else return Left(Failure.serverError("Something went wrong"));
        // await for (var value in res.stream.transform(utf8.decoder)) {
        //   print(value);
        // }
      } catch (e) {
        print(e.toString());
        return Left(Failure.serverError("Something went wrong"));
      }
    }
    );

  }

  @override
  Future<Either<Failure, String>> uploadVerification(Map<String, dynamic> request) async{
    var user= await cognitoUserPoolHelper.cognitoUserPool.getCurrentUser();
    var _session=await helper.safeRequest(() => user.getSession());

    return _session.fold((l) => null, (r) async{
      _apiHelper.changeBaseUrl(AppConstants.baseDocument);
      _awsApiHelper=AwsApiHelper(AppConstants.uploadVerification,cognitoUserPoolHelper.cognitoUserPool,r);
      var currency=await _awsApiHelper.post("",request);
      return currency.fold(
              (l) => Left(l),
              (r) => Right("Identification Document Submitted"));
    });

  }



}

