
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:technology/data/api_helper/api_helper.dart';
import 'package:technology/data/api_states/api_states.dart';
import 'package:technology/data/aws_cognito/aws_api_helper.dart';
import 'package:technology/data/aws_cognito/aws_cognito_helper.dart';
import 'package:technology/data/aws_cognito/aws_cognito_user_pool.dart';
import 'package:technology/data/constants/AppConstatnt.dart';
import 'package:technology/data/local_data_source/local_data_source.dart';
import 'package:technology/data/models/wallet/wallet_details_response.dart';
import 'package:technology/data/models/wallet/wallet_history_response.dart';
import 'package:technology/data/models/wallet/wallet_list_response.dart';
import 'package:technology/domain/repo/wallet_repo.dart';

@Injectable(as: WalletRepo)
class WalletRepoImpl extends WalletRepo{

  AwsApiHelper _awsApiHelper;
  final AwsCognitoUserPool cognitoUserPoolHelper;
  final LocalDataSource localDataSource;
  final ApiHelper _apiHelper;
  final AwsCognitoHelper helper;

  WalletRepoImpl(this.cognitoUserPoolHelper, this.localDataSource, this._apiHelper, this.helper);

  @override
  Future<Either<Failure, List<WalletModel>>> getWalletList() async{
    _apiHelper.changeBaseUrl(AppConstants.walletBase);
    var user= await cognitoUserPoolHelper.cognitoUserPool.getCurrentUser();
    var _session=await helper.safeRequest(() => user.getSession());
    return _session.fold((l) => null, (r) async{
      var data=await localDataSource.getUserData();
      _awsApiHelper=AwsApiHelper(AppConstants.getWallet,cognitoUserPoolHelper.cognitoUserPool,r);
      await _awsApiHelper.createAwsSignV4Client();
      var response=await _awsApiHelper.get("/${data.payload.client.id}");
      return response.fold((l) => null, (r){
        var walletList=WalletListResponse.fromJson(r.data);
        return right(walletList.payload);
      });
    });
  }

  @override
  Future<Either<Failure, WalletDetailResponse>> getWalletDetail(String walletId) async{

      _apiHelper.changeBaseUrl(AppConstants.walletBase);
      var user= await cognitoUserPoolHelper.cognitoUserPool.getCurrentUser();
      var _session=await helper.safeRequest(() => user.getSession());
      return _session.fold((l) => null, (r) async{
        var data=await localDataSource.getUserData();
        _awsApiHelper=AwsApiHelper(AppConstants.getWalletDetails,cognitoUserPoolHelper.cognitoUserPool,r);
        await _awsApiHelper.createAwsSignV4Client();
        var response=await _awsApiHelper.get("/$walletId");
        return response.fold((l) => null, (r){
           var walletList=WalletDetailResponse.fromJson(r.data);
          return right(walletList);
        });
      });
  }

  @override
  Future<Either<Failure, WalletHistoryResponse>> getWalletHistory(String walletId) async{
    _apiHelper.changeBaseUrl(AppConstants.walletBase);
    var user= await cognitoUserPoolHelper.cognitoUserPool.getCurrentUser();
    var _session=await helper.safeRequest(() => user.getSession());
    return _session.fold((l) => null, (r) async{
      var data=await localDataSource.getUserData();
      _awsApiHelper=AwsApiHelper(AppConstants.getWalletHistory,cognitoUserPoolHelper.cognitoUserPool,r);
      await _awsApiHelper.createAwsSignV4Client();
      var response=await _awsApiHelper.get("/$walletId");
      return response.fold((l) => null, (r){
        var walletList=WalletHistoryResponse.fromJson(r.data);
        return right(walletList);
      });
    });
  }

}