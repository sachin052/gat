import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:technology/data/api_states/api_states.dart';
import 'package:technology/data/aws_cognito/aws_cognito_helper.dart';
import 'package:technology/data/local_data_source/local_data_source.dart';
import 'package:technology/data/models/getuser/get_user_response.dart';
import 'package:technology/data/models/signup/user_registration_model.dart';
import 'package:technology/domain/entity/dashboard_entity.dart';
import 'package:technology/domain/repo/auth_repo.dart';

@Injectable(as: AuthRepo)
class AuthRepoImpl implements AuthRepo{
  final AwsCognitoHelper apiHelper;
  AuthRepoImpl(this.apiHelper);
  @override
  Future<Either<Failure, dynamic>> registerUser(RegistrationModel model) async{
    var response=await apiHelper.registerUser(model,createAccountForReferral: false);
    return response;
  }

  @override
  Future<Either<Failure, DashBoardEntity>> signIn(model) async{
    var response= await apiHelper.authenticateUser(model);
    return response.fold(
            (l) => Left(l),
            (model) async{
              return Right(DashBoardEntity(firstName: model.payload.client.firstName,lastName: model.payload.client.lastName,id: model.payload.client.id.toString()));
            });
  }

  @override
  Future<Either<Failure, dynamic>> createUser(RegistrationModel model)async {
    var response=await apiHelper.registerUser(model,createAccountForReferral: true);
    return response;
  }
}