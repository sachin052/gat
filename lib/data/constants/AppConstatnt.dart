class AppConstants{
  // Aws Cognito
  static const String userPoolId="ap-southeast-1_GvVYcAS6I";
  static const String clientId="107ihau7c35pl5mrrfer10nt7t";
  static const String identityPool='ap-southeast-1:7b3892ef-21d2-4df5-825d-11809974c2a8';
  static const String region="ap-southeast-1";
  static const String bucketName="gat.uploads.001";

  // Api Base Url
  static const baseEndPoint=".execute-api.ap-southeast-1.amazonaws.com/dev";
  static const baseURLRegistration="https://tzqehx0xp8.execute-api.ap-southeast-1.amazonaws.com/dev";
  static const baseURLEmail="https://8rysesqs67.execute-api.ap-southeast-1.amazonaws.com/dev";
  static const baseGetUser="https://kvbfc5ax81.execute-api.ap-southeast-1.amazonaws.com/dev";
  static const baseGetCommission="https://beglqube7l.execute-api.ap-southeast-1.amazonaws.com/dev";
  static const baseGetProductList="https://lixl1e1c8j.execute-api.ap-southeast-1.amazonaws.com/dev";
  static const baseUpdateProduct="https://tzqehx0xp8.execute-api.ap-southeast-1.amazonaws.com/dev";
  static const baseCreatePurchaseProduct="https://gvjruicjgg.execute-api.ap-southeast-1.amazonaws.com/dev";
  static const baseCountry="https://40wh3ispsd.execute-api.ap-southeast-1.amazonaws.com/dev";
  static const baseCurrency="https://awh12ay7o3.execute-api.ap-southeast-1.amazonaws.com/dev";
  static const baseDocument="https://2uo7oxxxuh.execute-api.ap-southeast-1.amazonaws.com/dev";
  static const walletBase="https://ntjh5lswmf.execute-api.ap-southeast-1.amazonaws.com/dev";

  // Email
  static const emailApiKey="92490f7c-7676-447a-8b56-91431512b3d8";
  static const welcomeMessage="GAT Web Portal: New Client Register";

  // Auth End Points
  static const registerUser="/clients/register";
  static const createUser="/clients/create";
  static const emailInteraction="/email/interaction";
  static const emailInteractionOpen="/email/interaction-open";
  static const emailNewPortalCreated="email/new-client-portal-created-open";

  // User End Points
  static const getUserDetails="/users/v2/get/aws-cognito";
  static const updateUserDetails="/clients/update";
  static const getUserCommission="/commission/commissions";
  static const getProductList="/products/list";
  static const createPurchase="/purchases/create/pending";
  static const updatePurchase="/purchases/update";
  static const purchaseSuccess="/ledger/create-m";
  static const updateClient="/clients/update";
  static const getCountry="/countries/list";
  static const getCurrency="/currencies/list";
  static const getEmpList="/clients/get-emp-status-list";
  static const sendReferralEmail="/email/marketing/send-referral-code";
  static const getDocumentList="/identification/categories/country";
  static const uploadVerification="/identification/create";
  static const getWallet="/wallet/list";
  static const getWalletDetails="/wallet/id";
  static const getWalletHistory="/wallet/line-items";

  // User Constants
  static const userData="UserData";
  static const jwtToken="jwtToken";
  static const userName="userName";
  static const cognitoUser="cognitoUser";

  // commission const
static const directCommissionRate=0.24;


}