import 'dart:convert';

import 'package:amazon_cognito_identity_dart_2/cognito.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:injectable/injectable.dart';
import 'package:technology/data/constants/AppConstatnt.dart';
import 'package:technology/data/models/getuser/get_user_response.dart';
import 'package:technology/others/di/injection.dart';

abstract class LocalDataSource{
  Future saveUserData(UserModel model);
  Future saveToken(String token);
  Future<String>getJwtToken();
  Future saveUserName(String userName);
  Future<String> getUserName();
  Future<UserModel> getUserData();
  Future clearData();
}

@Injectable(as: LocalDataSource)
class LocalDataSourceImpl implements LocalDataSource{
  FlutterSecureStorage _secureStorage=getIt<FlutterSecureStorage>();
  @override
  Future<UserModel> getUserData() async{
    bool hasData=await _secureStorage.readAll().then((value) => value.containsKey(AppConstants.userData));
    if(!hasData)return null;
    var userData=await _secureStorage.read(key: AppConstants.userData);
    return UserModel.fromJson(jsonDecode(userData));
  }

  @override
  Future saveUserData(UserModel model) async{
    await _secureStorage.write(key: AppConstants.userData, value: jsonEncode(model));

  }

  @override
  Future<String> getJwtToken() async => _secureStorage.read(key: AppConstants.jwtToken).then((value) => value);


  @override
  Future saveToken(String token) async{
    await _secureStorage.write(key: AppConstants.jwtToken, value: token);
  }

  @override
  Future clearData() async => await _secureStorage.deleteAll();



  @override
  Future<String> getUserName() async => await _secureStorage.read(key: AppConstants.userName);


  @override
  Future saveUserName(String userName) async{
    await _secureStorage.write(key: AppConstants.userName, value: userName);
  }



}