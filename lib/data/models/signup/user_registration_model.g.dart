// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_registration_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_RegistrationModel _$_$_RegistrationModelFromJson(Map<String, dynamic> json) {
  return _$_RegistrationModel(
    aws_cognito_id: json['aws_cognito_id'] as String,
    email: json['email'] as String,
    first_name: json['first_name'] as String,
    last_name: json['last_name'] as String,
    referring_client_id: json['referring_client_id'] as String,
    middle_names: json['middle_names'] as String,
    postal_address_country: json['postal_address_country'] as String,
    postal_address_line_1: json['postal_address_line_1'] as String,
    postal_address_line_2: json['postal_address_line_2'] as String,
    postal_address_postcode: json['postal_address_postcode'] as String,
    postal_address_state: json['postal_address_state'] as String,
    postal_address_suburb: json['postal_address_suburb'] as String,
    street_address_country: json['street_address_country'] as String,
    street_address_line_1: json['street_address_line_1'] as String,
    street_address_line_2: json['street_address_line_2'] as String,
    street_address_postcode: json['street_address_postcode'] as String,
    street_address_state: json['street_address_state'] as String,
    street_address_suburb: json['street_address_suburb'] as String,
    tel_home: json['tel_home'] as String,
    tel_mobile: json['tel_mobile'] as String,
    tel_work: json['tel_work'] as String,
    password: json['password'] as String,
  );
}

Map<String, dynamic> _$_$_RegistrationModelToJson(
        _$_RegistrationModel instance) =>
    <String, dynamic>{
      'aws_cognito_id': instance.aws_cognito_id,
      'email': instance.email,
      'first_name': instance.first_name,
      'last_name': instance.last_name,
      'referring_client_id': instance.referring_client_id,
      'middle_names': instance.middle_names,
      'postal_address_country': instance.postal_address_country,
      'postal_address_line_1': instance.postal_address_line_1,
      'postal_address_line_2': instance.postal_address_line_2,
      'postal_address_postcode': instance.postal_address_postcode,
      'postal_address_state': instance.postal_address_state,
      'postal_address_suburb': instance.postal_address_suburb,
      'street_address_country': instance.street_address_country,
      'street_address_line_1': instance.street_address_line_1,
      'street_address_line_2': instance.street_address_line_2,
      'street_address_postcode': instance.street_address_postcode,
      'street_address_state': instance.street_address_state,
      'street_address_suburb': instance.street_address_suburb,
      'tel_home': instance.tel_home,
      'tel_mobile': instance.tel_mobile,
      'tel_work': instance.tel_work,
      'password': instance.password,
    };

_$_EmailInteractionModel _$_$_EmailInteractionModelFromJson(
    Map<String, dynamic> json) {
  return _$_EmailInteractionModel(
    api_key: json['api_key'] as String,
    interaction: json['interaction'] as String,
    data: json['data'] as Map<String, dynamic>,
  );
}

Map<String, dynamic> _$_$_EmailInteractionModelToJson(
        _$_EmailInteractionModel instance) =>
    <String, dynamic>{
      'api_key': instance.api_key,
      'interaction': instance.interaction,
      'data': instance.data,
    };

_$_NewPortalCreatedModel _$_$_NewPortalCreatedModelFromJson(
    Map<String, dynamic> json) {
  return _$_NewPortalCreatedModel(
    api_key: json['api_key'] as String,
    id: json['id'] as String,
    username: json['username'] as String,
    password: json['password'] as String,
  );
}

Map<String, dynamic> _$_$_NewPortalCreatedModelToJson(
        _$_NewPortalCreatedModel instance) =>
    <String, dynamic>{
      'api_key': instance.api_key,
      'id': instance.id,
      'username': instance.username,
      'password': instance.password,
    };

_$_UpdateUserModel _$_$_UpdateUserModelFromJson(Map<String, dynamic> json) {
  return _$_UpdateUserModel(
    aws_cognito_id: json['aws_cognito_id'] as String,
    email: json['email'] as String,
    first_name: json['first_name'] as String,
    last_name: json['last_name'] as String,
    middle_names: json['middle_names'] as String,
    postal_address_country: json['postal_address_country'] as String,
    postal_address_line_1: json['postal_address_line_1'] as String,
    postal_address_line_2: json['postal_address_line_2'] as String,
    postal_address_postcode: json['postal_address_postcode'] as String,
    postal_address_state: json['postal_address_state'] as String,
    postal_address_suburb: json['postal_address_suburb'] as String,
    street_address_country: json['street_address_country'] as String,
    street_address_line_1: json['street_address_line_1'] as String,
    street_address_line_2: json['street_address_line_2'] as String,
    street_address_postcode: json['street_address_postcode'] as String,
    street_address_state: json['street_address_state'] as String,
    street_address_suburb: json['street_address_suburb'] as String,
    tel_home: json['tel_home'] as String,
    tel_mobile: json['tel_mobile'] as String,
    tel_work: json['tel_work'] as String,
  );
}

Map<String, dynamic> _$_$_UpdateUserModelToJson(_$_UpdateUserModel instance) =>
    <String, dynamic>{
      'aws_cognito_id': instance.aws_cognito_id,
      'email': instance.email,
      'first_name': instance.first_name,
      'last_name': instance.last_name,
      'middle_names': instance.middle_names,
      'postal_address_country': instance.postal_address_country,
      'postal_address_line_1': instance.postal_address_line_1,
      'postal_address_line_2': instance.postal_address_line_2,
      'postal_address_postcode': instance.postal_address_postcode,
      'postal_address_state': instance.postal_address_state,
      'postal_address_suburb': instance.postal_address_suburb,
      'street_address_country': instance.street_address_country,
      'street_address_line_1': instance.street_address_line_1,
      'street_address_line_2': instance.street_address_line_2,
      'street_address_postcode': instance.street_address_postcode,
      'street_address_state': instance.street_address_state,
      'street_address_suburb': instance.street_address_suburb,
      'tel_home': instance.tel_home,
      'tel_mobile': instance.tel_mobile,
      'tel_work': instance.tel_work,
    };
