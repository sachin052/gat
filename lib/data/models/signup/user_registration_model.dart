import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meta/meta.dart';

part 'user_registration_model.freezed.dart';

part 'user_registration_model.g.dart';

@freezed
abstract class RegistrationModel with _$RegistrationModel {
  const factory RegistrationModel({String aws_cognito_id,
    String email,
    String first_name,
    String last_name,
    String referring_client_id,
    String middle_names,
    String postal_address_country,
    String postal_address_line_1,
    String postal_address_line_2,
    String postal_address_postcode,
    String postal_address_state,
    String postal_address_suburb,
    String street_address_country,
    String street_address_line_1,
    String street_address_line_2,
    String street_address_postcode,
    String street_address_state,
    String street_address_suburb,
    String tel_home,
    String tel_mobile,
    String tel_work,
    String password}) = _RegistrationModel;

  factory RegistrationModel.fromJson(Map<String, dynamic> json) =>
      _$RegistrationModelFromJson(json);
}

@freezed
abstract class EmailInteractionModel with _$EmailInteractionModel {
  const factory EmailInteractionModel({
    String api_key,
    String interaction,
    Map<dynamic, dynamic> data
  }) = _EmailInteractionModel;

  factory EmailInteractionModel.fromJson(Map<dynamic, dynamic> json) =>
      _$EmailInteractionModelFromJson(json);
}

@freezed
abstract class NewPortalCreatedModel with _$NewPortalCreatedModel {
  const factory NewPortalCreatedModel({String api_key,
    String id,
    String username,
    String password
  }) = _NewPortalCreatedModel;

  factory NewPortalCreatedModel.fromJson(Map<dynamic, dynamic> json) =>
      _$NewPortalCreatedModelFromJson(json);
}

@freezed
abstract class UpdateUserModel with _$UpdateUserModel {
  const factory UpdateUserModel({String aws_cognito_id,
    String email,
    String first_name,
    String last_name,
    String middle_names,
    String postal_address_country,
    String postal_address_line_1,
    String postal_address_line_2,
    String postal_address_postcode,
    String postal_address_state,
    String postal_address_suburb,
    String street_address_country,
    String street_address_line_1,
    String street_address_line_2,
    String street_address_postcode,
    String street_address_state,
    String street_address_suburb,
    String tel_home,
    String tel_mobile,
    String tel_work}) = _UpdateUserModel;

  factory UpdateUserModel.fromJson(Map<String, dynamic> json) =>
      _$UpdateUserModelFromJson(json);
}