// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'user_registration_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
RegistrationModel _$RegistrationModelFromJson(Map<String, dynamic> json) {
  return _RegistrationModel.fromJson(json);
}

class _$RegistrationModelTearOff {
  const _$RegistrationModelTearOff();

  _RegistrationModel call(
      {String aws_cognito_id,
      String email,
      String first_name,
      String last_name,
      String referring_client_id,
      String middle_names,
      String postal_address_country,
      String postal_address_line_1,
      String postal_address_line_2,
      String postal_address_postcode,
      String postal_address_state,
      String postal_address_suburb,
      String street_address_country,
      String street_address_line_1,
      String street_address_line_2,
      String street_address_postcode,
      String street_address_state,
      String street_address_suburb,
      String tel_home,
      String tel_mobile,
      String tel_work,
      String password}) {
    return _RegistrationModel(
      aws_cognito_id: aws_cognito_id,
      email: email,
      first_name: first_name,
      last_name: last_name,
      referring_client_id: referring_client_id,
      middle_names: middle_names,
      postal_address_country: postal_address_country,
      postal_address_line_1: postal_address_line_1,
      postal_address_line_2: postal_address_line_2,
      postal_address_postcode: postal_address_postcode,
      postal_address_state: postal_address_state,
      postal_address_suburb: postal_address_suburb,
      street_address_country: street_address_country,
      street_address_line_1: street_address_line_1,
      street_address_line_2: street_address_line_2,
      street_address_postcode: street_address_postcode,
      street_address_state: street_address_state,
      street_address_suburb: street_address_suburb,
      tel_home: tel_home,
      tel_mobile: tel_mobile,
      tel_work: tel_work,
      password: password,
    );
  }
}

// ignore: unused_element
const $RegistrationModel = _$RegistrationModelTearOff();

mixin _$RegistrationModel {
  String get aws_cognito_id;
  String get email;
  String get first_name;
  String get last_name;
  String get referring_client_id;
  String get middle_names;
  String get postal_address_country;
  String get postal_address_line_1;
  String get postal_address_line_2;
  String get postal_address_postcode;
  String get postal_address_state;
  String get postal_address_suburb;
  String get street_address_country;
  String get street_address_line_1;
  String get street_address_line_2;
  String get street_address_postcode;
  String get street_address_state;
  String get street_address_suburb;
  String get tel_home;
  String get tel_mobile;
  String get tel_work;
  String get password;

  Map<String, dynamic> toJson();
  $RegistrationModelCopyWith<RegistrationModel> get copyWith;
}

abstract class $RegistrationModelCopyWith<$Res> {
  factory $RegistrationModelCopyWith(
          RegistrationModel value, $Res Function(RegistrationModel) then) =
      _$RegistrationModelCopyWithImpl<$Res>;
  $Res call(
      {String aws_cognito_id,
      String email,
      String first_name,
      String last_name,
      String referring_client_id,
      String middle_names,
      String postal_address_country,
      String postal_address_line_1,
      String postal_address_line_2,
      String postal_address_postcode,
      String postal_address_state,
      String postal_address_suburb,
      String street_address_country,
      String street_address_line_1,
      String street_address_line_2,
      String street_address_postcode,
      String street_address_state,
      String street_address_suburb,
      String tel_home,
      String tel_mobile,
      String tel_work,
      String password});
}

class _$RegistrationModelCopyWithImpl<$Res>
    implements $RegistrationModelCopyWith<$Res> {
  _$RegistrationModelCopyWithImpl(this._value, this._then);

  final RegistrationModel _value;
  // ignore: unused_field
  final $Res Function(RegistrationModel) _then;

  @override
  $Res call({
    Object aws_cognito_id = freezed,
    Object email = freezed,
    Object first_name = freezed,
    Object last_name = freezed,
    Object referring_client_id = freezed,
    Object middle_names = freezed,
    Object postal_address_country = freezed,
    Object postal_address_line_1 = freezed,
    Object postal_address_line_2 = freezed,
    Object postal_address_postcode = freezed,
    Object postal_address_state = freezed,
    Object postal_address_suburb = freezed,
    Object street_address_country = freezed,
    Object street_address_line_1 = freezed,
    Object street_address_line_2 = freezed,
    Object street_address_postcode = freezed,
    Object street_address_state = freezed,
    Object street_address_suburb = freezed,
    Object tel_home = freezed,
    Object tel_mobile = freezed,
    Object tel_work = freezed,
    Object password = freezed,
  }) {
    return _then(_value.copyWith(
      aws_cognito_id: aws_cognito_id == freezed
          ? _value.aws_cognito_id
          : aws_cognito_id as String,
      email: email == freezed ? _value.email : email as String,
      first_name:
          first_name == freezed ? _value.first_name : first_name as String,
      last_name: last_name == freezed ? _value.last_name : last_name as String,
      referring_client_id: referring_client_id == freezed
          ? _value.referring_client_id
          : referring_client_id as String,
      middle_names: middle_names == freezed
          ? _value.middle_names
          : middle_names as String,
      postal_address_country: postal_address_country == freezed
          ? _value.postal_address_country
          : postal_address_country as String,
      postal_address_line_1: postal_address_line_1 == freezed
          ? _value.postal_address_line_1
          : postal_address_line_1 as String,
      postal_address_line_2: postal_address_line_2 == freezed
          ? _value.postal_address_line_2
          : postal_address_line_2 as String,
      postal_address_postcode: postal_address_postcode == freezed
          ? _value.postal_address_postcode
          : postal_address_postcode as String,
      postal_address_state: postal_address_state == freezed
          ? _value.postal_address_state
          : postal_address_state as String,
      postal_address_suburb: postal_address_suburb == freezed
          ? _value.postal_address_suburb
          : postal_address_suburb as String,
      street_address_country: street_address_country == freezed
          ? _value.street_address_country
          : street_address_country as String,
      street_address_line_1: street_address_line_1 == freezed
          ? _value.street_address_line_1
          : street_address_line_1 as String,
      street_address_line_2: street_address_line_2 == freezed
          ? _value.street_address_line_2
          : street_address_line_2 as String,
      street_address_postcode: street_address_postcode == freezed
          ? _value.street_address_postcode
          : street_address_postcode as String,
      street_address_state: street_address_state == freezed
          ? _value.street_address_state
          : street_address_state as String,
      street_address_suburb: street_address_suburb == freezed
          ? _value.street_address_suburb
          : street_address_suburb as String,
      tel_home: tel_home == freezed ? _value.tel_home : tel_home as String,
      tel_mobile:
          tel_mobile == freezed ? _value.tel_mobile : tel_mobile as String,
      tel_work: tel_work == freezed ? _value.tel_work : tel_work as String,
      password: password == freezed ? _value.password : password as String,
    ));
  }
}

abstract class _$RegistrationModelCopyWith<$Res>
    implements $RegistrationModelCopyWith<$Res> {
  factory _$RegistrationModelCopyWith(
          _RegistrationModel value, $Res Function(_RegistrationModel) then) =
      __$RegistrationModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String aws_cognito_id,
      String email,
      String first_name,
      String last_name,
      String referring_client_id,
      String middle_names,
      String postal_address_country,
      String postal_address_line_1,
      String postal_address_line_2,
      String postal_address_postcode,
      String postal_address_state,
      String postal_address_suburb,
      String street_address_country,
      String street_address_line_1,
      String street_address_line_2,
      String street_address_postcode,
      String street_address_state,
      String street_address_suburb,
      String tel_home,
      String tel_mobile,
      String tel_work,
      String password});
}

class __$RegistrationModelCopyWithImpl<$Res>
    extends _$RegistrationModelCopyWithImpl<$Res>
    implements _$RegistrationModelCopyWith<$Res> {
  __$RegistrationModelCopyWithImpl(
      _RegistrationModel _value, $Res Function(_RegistrationModel) _then)
      : super(_value, (v) => _then(v as _RegistrationModel));

  @override
  _RegistrationModel get _value => super._value as _RegistrationModel;

  @override
  $Res call({
    Object aws_cognito_id = freezed,
    Object email = freezed,
    Object first_name = freezed,
    Object last_name = freezed,
    Object referring_client_id = freezed,
    Object middle_names = freezed,
    Object postal_address_country = freezed,
    Object postal_address_line_1 = freezed,
    Object postal_address_line_2 = freezed,
    Object postal_address_postcode = freezed,
    Object postal_address_state = freezed,
    Object postal_address_suburb = freezed,
    Object street_address_country = freezed,
    Object street_address_line_1 = freezed,
    Object street_address_line_2 = freezed,
    Object street_address_postcode = freezed,
    Object street_address_state = freezed,
    Object street_address_suburb = freezed,
    Object tel_home = freezed,
    Object tel_mobile = freezed,
    Object tel_work = freezed,
    Object password = freezed,
  }) {
    return _then(_RegistrationModel(
      aws_cognito_id: aws_cognito_id == freezed
          ? _value.aws_cognito_id
          : aws_cognito_id as String,
      email: email == freezed ? _value.email : email as String,
      first_name:
          first_name == freezed ? _value.first_name : first_name as String,
      last_name: last_name == freezed ? _value.last_name : last_name as String,
      referring_client_id: referring_client_id == freezed
          ? _value.referring_client_id
          : referring_client_id as String,
      middle_names: middle_names == freezed
          ? _value.middle_names
          : middle_names as String,
      postal_address_country: postal_address_country == freezed
          ? _value.postal_address_country
          : postal_address_country as String,
      postal_address_line_1: postal_address_line_1 == freezed
          ? _value.postal_address_line_1
          : postal_address_line_1 as String,
      postal_address_line_2: postal_address_line_2 == freezed
          ? _value.postal_address_line_2
          : postal_address_line_2 as String,
      postal_address_postcode: postal_address_postcode == freezed
          ? _value.postal_address_postcode
          : postal_address_postcode as String,
      postal_address_state: postal_address_state == freezed
          ? _value.postal_address_state
          : postal_address_state as String,
      postal_address_suburb: postal_address_suburb == freezed
          ? _value.postal_address_suburb
          : postal_address_suburb as String,
      street_address_country: street_address_country == freezed
          ? _value.street_address_country
          : street_address_country as String,
      street_address_line_1: street_address_line_1 == freezed
          ? _value.street_address_line_1
          : street_address_line_1 as String,
      street_address_line_2: street_address_line_2 == freezed
          ? _value.street_address_line_2
          : street_address_line_2 as String,
      street_address_postcode: street_address_postcode == freezed
          ? _value.street_address_postcode
          : street_address_postcode as String,
      street_address_state: street_address_state == freezed
          ? _value.street_address_state
          : street_address_state as String,
      street_address_suburb: street_address_suburb == freezed
          ? _value.street_address_suburb
          : street_address_suburb as String,
      tel_home: tel_home == freezed ? _value.tel_home : tel_home as String,
      tel_mobile:
          tel_mobile == freezed ? _value.tel_mobile : tel_mobile as String,
      tel_work: tel_work == freezed ? _value.tel_work : tel_work as String,
      password: password == freezed ? _value.password : password as String,
    ));
  }
}

@JsonSerializable()
class _$_RegistrationModel
    with DiagnosticableTreeMixin
    implements _RegistrationModel {
  const _$_RegistrationModel(
      {this.aws_cognito_id,
      this.email,
      this.first_name,
      this.last_name,
      this.referring_client_id,
      this.middle_names,
      this.postal_address_country,
      this.postal_address_line_1,
      this.postal_address_line_2,
      this.postal_address_postcode,
      this.postal_address_state,
      this.postal_address_suburb,
      this.street_address_country,
      this.street_address_line_1,
      this.street_address_line_2,
      this.street_address_postcode,
      this.street_address_state,
      this.street_address_suburb,
      this.tel_home,
      this.tel_mobile,
      this.tel_work,
      this.password});

  factory _$_RegistrationModel.fromJson(Map<String, dynamic> json) =>
      _$_$_RegistrationModelFromJson(json);

  @override
  final String aws_cognito_id;
  @override
  final String email;
  @override
  final String first_name;
  @override
  final String last_name;
  @override
  final String referring_client_id;
  @override
  final String middle_names;
  @override
  final String postal_address_country;
  @override
  final String postal_address_line_1;
  @override
  final String postal_address_line_2;
  @override
  final String postal_address_postcode;
  @override
  final String postal_address_state;
  @override
  final String postal_address_suburb;
  @override
  final String street_address_country;
  @override
  final String street_address_line_1;
  @override
  final String street_address_line_2;
  @override
  final String street_address_postcode;
  @override
  final String street_address_state;
  @override
  final String street_address_suburb;
  @override
  final String tel_home;
  @override
  final String tel_mobile;
  @override
  final String tel_work;
  @override
  final String password;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'RegistrationModel(aws_cognito_id: $aws_cognito_id, email: $email, first_name: $first_name, last_name: $last_name, referring_client_id: $referring_client_id, middle_names: $middle_names, postal_address_country: $postal_address_country, postal_address_line_1: $postal_address_line_1, postal_address_line_2: $postal_address_line_2, postal_address_postcode: $postal_address_postcode, postal_address_state: $postal_address_state, postal_address_suburb: $postal_address_suburb, street_address_country: $street_address_country, street_address_line_1: $street_address_line_1, street_address_line_2: $street_address_line_2, street_address_postcode: $street_address_postcode, street_address_state: $street_address_state, street_address_suburb: $street_address_suburb, tel_home: $tel_home, tel_mobile: $tel_mobile, tel_work: $tel_work, password: $password)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'RegistrationModel'))
      ..add(DiagnosticsProperty('aws_cognito_id', aws_cognito_id))
      ..add(DiagnosticsProperty('email', email))
      ..add(DiagnosticsProperty('first_name', first_name))
      ..add(DiagnosticsProperty('last_name', last_name))
      ..add(DiagnosticsProperty('referring_client_id', referring_client_id))
      ..add(DiagnosticsProperty('middle_names', middle_names))
      ..add(
          DiagnosticsProperty('postal_address_country', postal_address_country))
      ..add(DiagnosticsProperty('postal_address_line_1', postal_address_line_1))
      ..add(DiagnosticsProperty('postal_address_line_2', postal_address_line_2))
      ..add(DiagnosticsProperty(
          'postal_address_postcode', postal_address_postcode))
      ..add(DiagnosticsProperty('postal_address_state', postal_address_state))
      ..add(DiagnosticsProperty('postal_address_suburb', postal_address_suburb))
      ..add(
          DiagnosticsProperty('street_address_country', street_address_country))
      ..add(DiagnosticsProperty('street_address_line_1', street_address_line_1))
      ..add(DiagnosticsProperty('street_address_line_2', street_address_line_2))
      ..add(DiagnosticsProperty(
          'street_address_postcode', street_address_postcode))
      ..add(DiagnosticsProperty('street_address_state', street_address_state))
      ..add(DiagnosticsProperty('street_address_suburb', street_address_suburb))
      ..add(DiagnosticsProperty('tel_home', tel_home))
      ..add(DiagnosticsProperty('tel_mobile', tel_mobile))
      ..add(DiagnosticsProperty('tel_work', tel_work))
      ..add(DiagnosticsProperty('password', password));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _RegistrationModel &&
            (identical(other.aws_cognito_id, aws_cognito_id) ||
                const DeepCollectionEquality()
                    .equals(other.aws_cognito_id, aws_cognito_id)) &&
            (identical(other.email, email) ||
                const DeepCollectionEquality().equals(other.email, email)) &&
            (identical(other.first_name, first_name) ||
                const DeepCollectionEquality()
                    .equals(other.first_name, first_name)) &&
            (identical(other.last_name, last_name) ||
                const DeepCollectionEquality()
                    .equals(other.last_name, last_name)) &&
            (identical(other.referring_client_id, referring_client_id) ||
                const DeepCollectionEquality()
                    .equals(other.referring_client_id, referring_client_id)) &&
            (identical(other.middle_names, middle_names) ||
                const DeepCollectionEquality()
                    .equals(other.middle_names, middle_names)) &&
            (identical(other.postal_address_country, postal_address_country) ||
                const DeepCollectionEquality().equals(
                    other.postal_address_country, postal_address_country)) &&
            (identical(other.postal_address_line_1, postal_address_line_1) ||
                const DeepCollectionEquality().equals(
                    other.postal_address_line_1, postal_address_line_1)) &&
            (identical(other.postal_address_line_2, postal_address_line_2) ||
                const DeepCollectionEquality().equals(
                    other.postal_address_line_2, postal_address_line_2)) &&
            (identical(other.postal_address_postcode, postal_address_postcode) ||
                const DeepCollectionEquality().equals(
                    other.postal_address_postcode, postal_address_postcode)) &&
            (identical(other.postal_address_state, postal_address_state) ||
                const DeepCollectionEquality().equals(
                    other.postal_address_state, postal_address_state)) &&
            (identical(other.postal_address_suburb, postal_address_suburb) ||
                const DeepCollectionEquality().equals(
                    other.postal_address_suburb, postal_address_suburb)) &&
            (identical(other.street_address_country, street_address_country) ||
                const DeepCollectionEquality().equals(
                    other.street_address_country, street_address_country)) &&
            (identical(other.street_address_line_1, street_address_line_1) ||
                const DeepCollectionEquality().equals(
                    other.street_address_line_1, street_address_line_1)) &&
            (identical(other.street_address_line_2, street_address_line_2) ||
                const DeepCollectionEquality().equals(
                    other.street_address_line_2, street_address_line_2)) &&
            (identical(other.street_address_postcode, street_address_postcode) ||
                const DeepCollectionEquality().equals(
                    other.street_address_postcode, street_address_postcode)) &&
            (identical(other.street_address_state, street_address_state) ||
                const DeepCollectionEquality().equals(
                    other.street_address_state, street_address_state)) &&
            (identical(other.street_address_suburb, street_address_suburb) ||
                const DeepCollectionEquality().equals(other.street_address_suburb, street_address_suburb)) &&
            (identical(other.tel_home, tel_home) || const DeepCollectionEquality().equals(other.tel_home, tel_home)) &&
            (identical(other.tel_mobile, tel_mobile) || const DeepCollectionEquality().equals(other.tel_mobile, tel_mobile)) &&
            (identical(other.tel_work, tel_work) || const DeepCollectionEquality().equals(other.tel_work, tel_work)) &&
            (identical(other.password, password) || const DeepCollectionEquality().equals(other.password, password)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(aws_cognito_id) ^
      const DeepCollectionEquality().hash(email) ^
      const DeepCollectionEquality().hash(first_name) ^
      const DeepCollectionEquality().hash(last_name) ^
      const DeepCollectionEquality().hash(referring_client_id) ^
      const DeepCollectionEquality().hash(middle_names) ^
      const DeepCollectionEquality().hash(postal_address_country) ^
      const DeepCollectionEquality().hash(postal_address_line_1) ^
      const DeepCollectionEquality().hash(postal_address_line_2) ^
      const DeepCollectionEquality().hash(postal_address_postcode) ^
      const DeepCollectionEquality().hash(postal_address_state) ^
      const DeepCollectionEquality().hash(postal_address_suburb) ^
      const DeepCollectionEquality().hash(street_address_country) ^
      const DeepCollectionEquality().hash(street_address_line_1) ^
      const DeepCollectionEquality().hash(street_address_line_2) ^
      const DeepCollectionEquality().hash(street_address_postcode) ^
      const DeepCollectionEquality().hash(street_address_state) ^
      const DeepCollectionEquality().hash(street_address_suburb) ^
      const DeepCollectionEquality().hash(tel_home) ^
      const DeepCollectionEquality().hash(tel_mobile) ^
      const DeepCollectionEquality().hash(tel_work) ^
      const DeepCollectionEquality().hash(password);

  @override
  _$RegistrationModelCopyWith<_RegistrationModel> get copyWith =>
      __$RegistrationModelCopyWithImpl<_RegistrationModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_RegistrationModelToJson(this);
  }
}

abstract class _RegistrationModel implements RegistrationModel {
  const factory _RegistrationModel(
      {String aws_cognito_id,
      String email,
      String first_name,
      String last_name,
      String referring_client_id,
      String middle_names,
      String postal_address_country,
      String postal_address_line_1,
      String postal_address_line_2,
      String postal_address_postcode,
      String postal_address_state,
      String postal_address_suburb,
      String street_address_country,
      String street_address_line_1,
      String street_address_line_2,
      String street_address_postcode,
      String street_address_state,
      String street_address_suburb,
      String tel_home,
      String tel_mobile,
      String tel_work,
      String password}) = _$_RegistrationModel;

  factory _RegistrationModel.fromJson(Map<String, dynamic> json) =
      _$_RegistrationModel.fromJson;

  @override
  String get aws_cognito_id;
  @override
  String get email;
  @override
  String get first_name;
  @override
  String get last_name;
  @override
  String get referring_client_id;
  @override
  String get middle_names;
  @override
  String get postal_address_country;
  @override
  String get postal_address_line_1;
  @override
  String get postal_address_line_2;
  @override
  String get postal_address_postcode;
  @override
  String get postal_address_state;
  @override
  String get postal_address_suburb;
  @override
  String get street_address_country;
  @override
  String get street_address_line_1;
  @override
  String get street_address_line_2;
  @override
  String get street_address_postcode;
  @override
  String get street_address_state;
  @override
  String get street_address_suburb;
  @override
  String get tel_home;
  @override
  String get tel_mobile;
  @override
  String get tel_work;
  @override
  String get password;
  @override
  _$RegistrationModelCopyWith<_RegistrationModel> get copyWith;
}

EmailInteractionModel _$EmailInteractionModelFromJson(
    Map<String, dynamic> json) {
  return _EmailInteractionModel.fromJson(json);
}

class _$EmailInteractionModelTearOff {
  const _$EmailInteractionModelTearOff();

  _EmailInteractionModel call(
      {String api_key, String interaction, Map<dynamic, dynamic> data}) {
    return _EmailInteractionModel(
      api_key: api_key,
      interaction: interaction,
      data: data,
    );
  }
}

// ignore: unused_element
const $EmailInteractionModel = _$EmailInteractionModelTearOff();

mixin _$EmailInteractionModel {
  String get api_key;
  String get interaction;
  Map<dynamic, dynamic> get data;

  Map<String, dynamic> toJson();
  $EmailInteractionModelCopyWith<EmailInteractionModel> get copyWith;
}

abstract class $EmailInteractionModelCopyWith<$Res> {
  factory $EmailInteractionModelCopyWith(EmailInteractionModel value,
          $Res Function(EmailInteractionModel) then) =
      _$EmailInteractionModelCopyWithImpl<$Res>;
  $Res call({String api_key, String interaction, Map<dynamic, dynamic> data});
}

class _$EmailInteractionModelCopyWithImpl<$Res>
    implements $EmailInteractionModelCopyWith<$Res> {
  _$EmailInteractionModelCopyWithImpl(this._value, this._then);

  final EmailInteractionModel _value;
  // ignore: unused_field
  final $Res Function(EmailInteractionModel) _then;

  @override
  $Res call({
    Object api_key = freezed,
    Object interaction = freezed,
    Object data = freezed,
  }) {
    return _then(_value.copyWith(
      api_key: api_key == freezed ? _value.api_key : api_key as String,
      interaction:
          interaction == freezed ? _value.interaction : interaction as String,
      data: data == freezed ? _value.data : data as Map<dynamic, dynamic>,
    ));
  }
}

abstract class _$EmailInteractionModelCopyWith<$Res>
    implements $EmailInteractionModelCopyWith<$Res> {
  factory _$EmailInteractionModelCopyWith(_EmailInteractionModel value,
          $Res Function(_EmailInteractionModel) then) =
      __$EmailInteractionModelCopyWithImpl<$Res>;
  @override
  $Res call({String api_key, String interaction, Map<dynamic, dynamic> data});
}

class __$EmailInteractionModelCopyWithImpl<$Res>
    extends _$EmailInteractionModelCopyWithImpl<$Res>
    implements _$EmailInteractionModelCopyWith<$Res> {
  __$EmailInteractionModelCopyWithImpl(_EmailInteractionModel _value,
      $Res Function(_EmailInteractionModel) _then)
      : super(_value, (v) => _then(v as _EmailInteractionModel));

  @override
  _EmailInteractionModel get _value => super._value as _EmailInteractionModel;

  @override
  $Res call({
    Object api_key = freezed,
    Object interaction = freezed,
    Object data = freezed,
  }) {
    return _then(_EmailInteractionModel(
      api_key: api_key == freezed ? _value.api_key : api_key as String,
      interaction:
          interaction == freezed ? _value.interaction : interaction as String,
      data: data == freezed ? _value.data : data as Map<dynamic, dynamic>,
    ));
  }
}

@JsonSerializable()
class _$_EmailInteractionModel
    with DiagnosticableTreeMixin
    implements _EmailInteractionModel {
  const _$_EmailInteractionModel({this.api_key, this.interaction, this.data});

  factory _$_EmailInteractionModel.fromJson(Map<String, dynamic> json) =>
      _$_$_EmailInteractionModelFromJson(json);

  @override
  final String api_key;
  @override
  final String interaction;
  @override
  final Map<dynamic, dynamic> data;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'EmailInteractionModel(api_key: $api_key, interaction: $interaction, data: $data)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'EmailInteractionModel'))
      ..add(DiagnosticsProperty('api_key', api_key))
      ..add(DiagnosticsProperty('interaction', interaction))
      ..add(DiagnosticsProperty('data', data));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _EmailInteractionModel &&
            (identical(other.api_key, api_key) ||
                const DeepCollectionEquality()
                    .equals(other.api_key, api_key)) &&
            (identical(other.interaction, interaction) ||
                const DeepCollectionEquality()
                    .equals(other.interaction, interaction)) &&
            (identical(other.data, data) ||
                const DeepCollectionEquality().equals(other.data, data)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(api_key) ^
      const DeepCollectionEquality().hash(interaction) ^
      const DeepCollectionEquality().hash(data);

  @override
  _$EmailInteractionModelCopyWith<_EmailInteractionModel> get copyWith =>
      __$EmailInteractionModelCopyWithImpl<_EmailInteractionModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_EmailInteractionModelToJson(this);
  }
}

abstract class _EmailInteractionModel implements EmailInteractionModel {
  const factory _EmailInteractionModel(
      {String api_key,
      String interaction,
      Map<dynamic, dynamic> data}) = _$_EmailInteractionModel;

  factory _EmailInteractionModel.fromJson(Map<String, dynamic> json) =
      _$_EmailInteractionModel.fromJson;

  @override
  String get api_key;
  @override
  String get interaction;
  @override
  Map<dynamic, dynamic> get data;
  @override
  _$EmailInteractionModelCopyWith<_EmailInteractionModel> get copyWith;
}

NewPortalCreatedModel _$NewPortalCreatedModelFromJson(
    Map<String, dynamic> json) {
  return _NewPortalCreatedModel.fromJson(json);
}

class _$NewPortalCreatedModelTearOff {
  const _$NewPortalCreatedModelTearOff();

  _NewPortalCreatedModel call(
      {String api_key, String id, String username, String password}) {
    return _NewPortalCreatedModel(
      api_key: api_key,
      id: id,
      username: username,
      password: password,
    );
  }
}

// ignore: unused_element
const $NewPortalCreatedModel = _$NewPortalCreatedModelTearOff();

mixin _$NewPortalCreatedModel {
  String get api_key;
  String get id;
  String get username;
  String get password;

  Map<String, dynamic> toJson();
  $NewPortalCreatedModelCopyWith<NewPortalCreatedModel> get copyWith;
}

abstract class $NewPortalCreatedModelCopyWith<$Res> {
  factory $NewPortalCreatedModelCopyWith(NewPortalCreatedModel value,
          $Res Function(NewPortalCreatedModel) then) =
      _$NewPortalCreatedModelCopyWithImpl<$Res>;
  $Res call({String api_key, String id, String username, String password});
}

class _$NewPortalCreatedModelCopyWithImpl<$Res>
    implements $NewPortalCreatedModelCopyWith<$Res> {
  _$NewPortalCreatedModelCopyWithImpl(this._value, this._then);

  final NewPortalCreatedModel _value;
  // ignore: unused_field
  final $Res Function(NewPortalCreatedModel) _then;

  @override
  $Res call({
    Object api_key = freezed,
    Object id = freezed,
    Object username = freezed,
    Object password = freezed,
  }) {
    return _then(_value.copyWith(
      api_key: api_key == freezed ? _value.api_key : api_key as String,
      id: id == freezed ? _value.id : id as String,
      username: username == freezed ? _value.username : username as String,
      password: password == freezed ? _value.password : password as String,
    ));
  }
}

abstract class _$NewPortalCreatedModelCopyWith<$Res>
    implements $NewPortalCreatedModelCopyWith<$Res> {
  factory _$NewPortalCreatedModelCopyWith(_NewPortalCreatedModel value,
          $Res Function(_NewPortalCreatedModel) then) =
      __$NewPortalCreatedModelCopyWithImpl<$Res>;
  @override
  $Res call({String api_key, String id, String username, String password});
}

class __$NewPortalCreatedModelCopyWithImpl<$Res>
    extends _$NewPortalCreatedModelCopyWithImpl<$Res>
    implements _$NewPortalCreatedModelCopyWith<$Res> {
  __$NewPortalCreatedModelCopyWithImpl(_NewPortalCreatedModel _value,
      $Res Function(_NewPortalCreatedModel) _then)
      : super(_value, (v) => _then(v as _NewPortalCreatedModel));

  @override
  _NewPortalCreatedModel get _value => super._value as _NewPortalCreatedModel;

  @override
  $Res call({
    Object api_key = freezed,
    Object id = freezed,
    Object username = freezed,
    Object password = freezed,
  }) {
    return _then(_NewPortalCreatedModel(
      api_key: api_key == freezed ? _value.api_key : api_key as String,
      id: id == freezed ? _value.id : id as String,
      username: username == freezed ? _value.username : username as String,
      password: password == freezed ? _value.password : password as String,
    ));
  }
}

@JsonSerializable()
class _$_NewPortalCreatedModel
    with DiagnosticableTreeMixin
    implements _NewPortalCreatedModel {
  const _$_NewPortalCreatedModel(
      {this.api_key, this.id, this.username, this.password});

  factory _$_NewPortalCreatedModel.fromJson(Map<String, dynamic> json) =>
      _$_$_NewPortalCreatedModelFromJson(json);

  @override
  final String api_key;
  @override
  final String id;
  @override
  final String username;
  @override
  final String password;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NewPortalCreatedModel(api_key: $api_key, id: $id, username: $username, password: $password)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'NewPortalCreatedModel'))
      ..add(DiagnosticsProperty('api_key', api_key))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('username', username))
      ..add(DiagnosticsProperty('password', password));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _NewPortalCreatedModel &&
            (identical(other.api_key, api_key) ||
                const DeepCollectionEquality()
                    .equals(other.api_key, api_key)) &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.username, username) ||
                const DeepCollectionEquality()
                    .equals(other.username, username)) &&
            (identical(other.password, password) ||
                const DeepCollectionEquality()
                    .equals(other.password, password)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(api_key) ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(username) ^
      const DeepCollectionEquality().hash(password);

  @override
  _$NewPortalCreatedModelCopyWith<_NewPortalCreatedModel> get copyWith =>
      __$NewPortalCreatedModelCopyWithImpl<_NewPortalCreatedModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_NewPortalCreatedModelToJson(this);
  }
}

abstract class _NewPortalCreatedModel implements NewPortalCreatedModel {
  const factory _NewPortalCreatedModel(
      {String api_key,
      String id,
      String username,
      String password}) = _$_NewPortalCreatedModel;

  factory _NewPortalCreatedModel.fromJson(Map<String, dynamic> json) =
      _$_NewPortalCreatedModel.fromJson;

  @override
  String get api_key;
  @override
  String get id;
  @override
  String get username;
  @override
  String get password;
  @override
  _$NewPortalCreatedModelCopyWith<_NewPortalCreatedModel> get copyWith;
}

UpdateUserModel _$UpdateUserModelFromJson(Map<String, dynamic> json) {
  return _UpdateUserModel.fromJson(json);
}

class _$UpdateUserModelTearOff {
  const _$UpdateUserModelTearOff();

  _UpdateUserModel call(
      {String aws_cognito_id,
      String email,
      String first_name,
      String last_name,
      String middle_names,
      String postal_address_country,
      String postal_address_line_1,
      String postal_address_line_2,
      String postal_address_postcode,
      String postal_address_state,
      String postal_address_suburb,
      String street_address_country,
      String street_address_line_1,
      String street_address_line_2,
      String street_address_postcode,
      String street_address_state,
      String street_address_suburb,
      String tel_home,
      String tel_mobile,
      String tel_work}) {
    return _UpdateUserModel(
      aws_cognito_id: aws_cognito_id,
      email: email,
      first_name: first_name,
      last_name: last_name,
      middle_names: middle_names,
      postal_address_country: postal_address_country,
      postal_address_line_1: postal_address_line_1,
      postal_address_line_2: postal_address_line_2,
      postal_address_postcode: postal_address_postcode,
      postal_address_state: postal_address_state,
      postal_address_suburb: postal_address_suburb,
      street_address_country: street_address_country,
      street_address_line_1: street_address_line_1,
      street_address_line_2: street_address_line_2,
      street_address_postcode: street_address_postcode,
      street_address_state: street_address_state,
      street_address_suburb: street_address_suburb,
      tel_home: tel_home,
      tel_mobile: tel_mobile,
      tel_work: tel_work,
    );
  }
}

// ignore: unused_element
const $UpdateUserModel = _$UpdateUserModelTearOff();

mixin _$UpdateUserModel {
  String get aws_cognito_id;
  String get email;
  String get first_name;
  String get last_name;
  String get middle_names;
  String get postal_address_country;
  String get postal_address_line_1;
  String get postal_address_line_2;
  String get postal_address_postcode;
  String get postal_address_state;
  String get postal_address_suburb;
  String get street_address_country;
  String get street_address_line_1;
  String get street_address_line_2;
  String get street_address_postcode;
  String get street_address_state;
  String get street_address_suburb;
  String get tel_home;
  String get tel_mobile;
  String get tel_work;

  Map<String, dynamic> toJson();
  $UpdateUserModelCopyWith<UpdateUserModel> get copyWith;
}

abstract class $UpdateUserModelCopyWith<$Res> {
  factory $UpdateUserModelCopyWith(
          UpdateUserModel value, $Res Function(UpdateUserModel) then) =
      _$UpdateUserModelCopyWithImpl<$Res>;
  $Res call(
      {String aws_cognito_id,
      String email,
      String first_name,
      String last_name,
      String middle_names,
      String postal_address_country,
      String postal_address_line_1,
      String postal_address_line_2,
      String postal_address_postcode,
      String postal_address_state,
      String postal_address_suburb,
      String street_address_country,
      String street_address_line_1,
      String street_address_line_2,
      String street_address_postcode,
      String street_address_state,
      String street_address_suburb,
      String tel_home,
      String tel_mobile,
      String tel_work});
}

class _$UpdateUserModelCopyWithImpl<$Res>
    implements $UpdateUserModelCopyWith<$Res> {
  _$UpdateUserModelCopyWithImpl(this._value, this._then);

  final UpdateUserModel _value;
  // ignore: unused_field
  final $Res Function(UpdateUserModel) _then;

  @override
  $Res call({
    Object aws_cognito_id = freezed,
    Object email = freezed,
    Object first_name = freezed,
    Object last_name = freezed,
    Object middle_names = freezed,
    Object postal_address_country = freezed,
    Object postal_address_line_1 = freezed,
    Object postal_address_line_2 = freezed,
    Object postal_address_postcode = freezed,
    Object postal_address_state = freezed,
    Object postal_address_suburb = freezed,
    Object street_address_country = freezed,
    Object street_address_line_1 = freezed,
    Object street_address_line_2 = freezed,
    Object street_address_postcode = freezed,
    Object street_address_state = freezed,
    Object street_address_suburb = freezed,
    Object tel_home = freezed,
    Object tel_mobile = freezed,
    Object tel_work = freezed,
  }) {
    return _then(_value.copyWith(
      aws_cognito_id: aws_cognito_id == freezed
          ? _value.aws_cognito_id
          : aws_cognito_id as String,
      email: email == freezed ? _value.email : email as String,
      first_name:
          first_name == freezed ? _value.first_name : first_name as String,
      last_name: last_name == freezed ? _value.last_name : last_name as String,
      middle_names: middle_names == freezed
          ? _value.middle_names
          : middle_names as String,
      postal_address_country: postal_address_country == freezed
          ? _value.postal_address_country
          : postal_address_country as String,
      postal_address_line_1: postal_address_line_1 == freezed
          ? _value.postal_address_line_1
          : postal_address_line_1 as String,
      postal_address_line_2: postal_address_line_2 == freezed
          ? _value.postal_address_line_2
          : postal_address_line_2 as String,
      postal_address_postcode: postal_address_postcode == freezed
          ? _value.postal_address_postcode
          : postal_address_postcode as String,
      postal_address_state: postal_address_state == freezed
          ? _value.postal_address_state
          : postal_address_state as String,
      postal_address_suburb: postal_address_suburb == freezed
          ? _value.postal_address_suburb
          : postal_address_suburb as String,
      street_address_country: street_address_country == freezed
          ? _value.street_address_country
          : street_address_country as String,
      street_address_line_1: street_address_line_1 == freezed
          ? _value.street_address_line_1
          : street_address_line_1 as String,
      street_address_line_2: street_address_line_2 == freezed
          ? _value.street_address_line_2
          : street_address_line_2 as String,
      street_address_postcode: street_address_postcode == freezed
          ? _value.street_address_postcode
          : street_address_postcode as String,
      street_address_state: street_address_state == freezed
          ? _value.street_address_state
          : street_address_state as String,
      street_address_suburb: street_address_suburb == freezed
          ? _value.street_address_suburb
          : street_address_suburb as String,
      tel_home: tel_home == freezed ? _value.tel_home : tel_home as String,
      tel_mobile:
          tel_mobile == freezed ? _value.tel_mobile : tel_mobile as String,
      tel_work: tel_work == freezed ? _value.tel_work : tel_work as String,
    ));
  }
}

abstract class _$UpdateUserModelCopyWith<$Res>
    implements $UpdateUserModelCopyWith<$Res> {
  factory _$UpdateUserModelCopyWith(
          _UpdateUserModel value, $Res Function(_UpdateUserModel) then) =
      __$UpdateUserModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String aws_cognito_id,
      String email,
      String first_name,
      String last_name,
      String middle_names,
      String postal_address_country,
      String postal_address_line_1,
      String postal_address_line_2,
      String postal_address_postcode,
      String postal_address_state,
      String postal_address_suburb,
      String street_address_country,
      String street_address_line_1,
      String street_address_line_2,
      String street_address_postcode,
      String street_address_state,
      String street_address_suburb,
      String tel_home,
      String tel_mobile,
      String tel_work});
}

class __$UpdateUserModelCopyWithImpl<$Res>
    extends _$UpdateUserModelCopyWithImpl<$Res>
    implements _$UpdateUserModelCopyWith<$Res> {
  __$UpdateUserModelCopyWithImpl(
      _UpdateUserModel _value, $Res Function(_UpdateUserModel) _then)
      : super(_value, (v) => _then(v as _UpdateUserModel));

  @override
  _UpdateUserModel get _value => super._value as _UpdateUserModel;

  @override
  $Res call({
    Object aws_cognito_id = freezed,
    Object email = freezed,
    Object first_name = freezed,
    Object last_name = freezed,
    Object middle_names = freezed,
    Object postal_address_country = freezed,
    Object postal_address_line_1 = freezed,
    Object postal_address_line_2 = freezed,
    Object postal_address_postcode = freezed,
    Object postal_address_state = freezed,
    Object postal_address_suburb = freezed,
    Object street_address_country = freezed,
    Object street_address_line_1 = freezed,
    Object street_address_line_2 = freezed,
    Object street_address_postcode = freezed,
    Object street_address_state = freezed,
    Object street_address_suburb = freezed,
    Object tel_home = freezed,
    Object tel_mobile = freezed,
    Object tel_work = freezed,
  }) {
    return _then(_UpdateUserModel(
      aws_cognito_id: aws_cognito_id == freezed
          ? _value.aws_cognito_id
          : aws_cognito_id as String,
      email: email == freezed ? _value.email : email as String,
      first_name:
          first_name == freezed ? _value.first_name : first_name as String,
      last_name: last_name == freezed ? _value.last_name : last_name as String,
      middle_names: middle_names == freezed
          ? _value.middle_names
          : middle_names as String,
      postal_address_country: postal_address_country == freezed
          ? _value.postal_address_country
          : postal_address_country as String,
      postal_address_line_1: postal_address_line_1 == freezed
          ? _value.postal_address_line_1
          : postal_address_line_1 as String,
      postal_address_line_2: postal_address_line_2 == freezed
          ? _value.postal_address_line_2
          : postal_address_line_2 as String,
      postal_address_postcode: postal_address_postcode == freezed
          ? _value.postal_address_postcode
          : postal_address_postcode as String,
      postal_address_state: postal_address_state == freezed
          ? _value.postal_address_state
          : postal_address_state as String,
      postal_address_suburb: postal_address_suburb == freezed
          ? _value.postal_address_suburb
          : postal_address_suburb as String,
      street_address_country: street_address_country == freezed
          ? _value.street_address_country
          : street_address_country as String,
      street_address_line_1: street_address_line_1 == freezed
          ? _value.street_address_line_1
          : street_address_line_1 as String,
      street_address_line_2: street_address_line_2 == freezed
          ? _value.street_address_line_2
          : street_address_line_2 as String,
      street_address_postcode: street_address_postcode == freezed
          ? _value.street_address_postcode
          : street_address_postcode as String,
      street_address_state: street_address_state == freezed
          ? _value.street_address_state
          : street_address_state as String,
      street_address_suburb: street_address_suburb == freezed
          ? _value.street_address_suburb
          : street_address_suburb as String,
      tel_home: tel_home == freezed ? _value.tel_home : tel_home as String,
      tel_mobile:
          tel_mobile == freezed ? _value.tel_mobile : tel_mobile as String,
      tel_work: tel_work == freezed ? _value.tel_work : tel_work as String,
    ));
  }
}

@JsonSerializable()
class _$_UpdateUserModel
    with DiagnosticableTreeMixin
    implements _UpdateUserModel {
  const _$_UpdateUserModel(
      {this.aws_cognito_id,
      this.email,
      this.first_name,
      this.last_name,
      this.middle_names,
      this.postal_address_country,
      this.postal_address_line_1,
      this.postal_address_line_2,
      this.postal_address_postcode,
      this.postal_address_state,
      this.postal_address_suburb,
      this.street_address_country,
      this.street_address_line_1,
      this.street_address_line_2,
      this.street_address_postcode,
      this.street_address_state,
      this.street_address_suburb,
      this.tel_home,
      this.tel_mobile,
      this.tel_work});

  factory _$_UpdateUserModel.fromJson(Map<String, dynamic> json) =>
      _$_$_UpdateUserModelFromJson(json);

  @override
  final String aws_cognito_id;
  @override
  final String email;
  @override
  final String first_name;
  @override
  final String last_name;
  @override
  final String middle_names;
  @override
  final String postal_address_country;
  @override
  final String postal_address_line_1;
  @override
  final String postal_address_line_2;
  @override
  final String postal_address_postcode;
  @override
  final String postal_address_state;
  @override
  final String postal_address_suburb;
  @override
  final String street_address_country;
  @override
  final String street_address_line_1;
  @override
  final String street_address_line_2;
  @override
  final String street_address_postcode;
  @override
  final String street_address_state;
  @override
  final String street_address_suburb;
  @override
  final String tel_home;
  @override
  final String tel_mobile;
  @override
  final String tel_work;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'UpdateUserModel(aws_cognito_id: $aws_cognito_id, email: $email, first_name: $first_name, last_name: $last_name, middle_names: $middle_names, postal_address_country: $postal_address_country, postal_address_line_1: $postal_address_line_1, postal_address_line_2: $postal_address_line_2, postal_address_postcode: $postal_address_postcode, postal_address_state: $postal_address_state, postal_address_suburb: $postal_address_suburb, street_address_country: $street_address_country, street_address_line_1: $street_address_line_1, street_address_line_2: $street_address_line_2, street_address_postcode: $street_address_postcode, street_address_state: $street_address_state, street_address_suburb: $street_address_suburb, tel_home: $tel_home, tel_mobile: $tel_mobile, tel_work: $tel_work)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'UpdateUserModel'))
      ..add(DiagnosticsProperty('aws_cognito_id', aws_cognito_id))
      ..add(DiagnosticsProperty('email', email))
      ..add(DiagnosticsProperty('first_name', first_name))
      ..add(DiagnosticsProperty('last_name', last_name))
      ..add(DiagnosticsProperty('middle_names', middle_names))
      ..add(
          DiagnosticsProperty('postal_address_country', postal_address_country))
      ..add(DiagnosticsProperty('postal_address_line_1', postal_address_line_1))
      ..add(DiagnosticsProperty('postal_address_line_2', postal_address_line_2))
      ..add(DiagnosticsProperty(
          'postal_address_postcode', postal_address_postcode))
      ..add(DiagnosticsProperty('postal_address_state', postal_address_state))
      ..add(DiagnosticsProperty('postal_address_suburb', postal_address_suburb))
      ..add(
          DiagnosticsProperty('street_address_country', street_address_country))
      ..add(DiagnosticsProperty('street_address_line_1', street_address_line_1))
      ..add(DiagnosticsProperty('street_address_line_2', street_address_line_2))
      ..add(DiagnosticsProperty(
          'street_address_postcode', street_address_postcode))
      ..add(DiagnosticsProperty('street_address_state', street_address_state))
      ..add(DiagnosticsProperty('street_address_suburb', street_address_suburb))
      ..add(DiagnosticsProperty('tel_home', tel_home))
      ..add(DiagnosticsProperty('tel_mobile', tel_mobile))
      ..add(DiagnosticsProperty('tel_work', tel_work));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _UpdateUserModel &&
            (identical(other.aws_cognito_id, aws_cognito_id) ||
                const DeepCollectionEquality()
                    .equals(other.aws_cognito_id, aws_cognito_id)) &&
            (identical(other.email, email) ||
                const DeepCollectionEquality().equals(other.email, email)) &&
            (identical(other.first_name, first_name) ||
                const DeepCollectionEquality()
                    .equals(other.first_name, first_name)) &&
            (identical(other.last_name, last_name) ||
                const DeepCollectionEquality()
                    .equals(other.last_name, last_name)) &&
            (identical(other.middle_names, middle_names) ||
                const DeepCollectionEquality()
                    .equals(other.middle_names, middle_names)) &&
            (identical(other.postal_address_country, postal_address_country) ||
                const DeepCollectionEquality().equals(
                    other.postal_address_country, postal_address_country)) &&
            (identical(other.postal_address_line_1, postal_address_line_1) ||
                const DeepCollectionEquality().equals(
                    other.postal_address_line_1, postal_address_line_1)) &&
            (identical(other.postal_address_line_2, postal_address_line_2) ||
                const DeepCollectionEquality().equals(
                    other.postal_address_line_2, postal_address_line_2)) &&
            (identical(other.postal_address_postcode, postal_address_postcode) ||
                const DeepCollectionEquality().equals(
                    other.postal_address_postcode, postal_address_postcode)) &&
            (identical(other.postal_address_state, postal_address_state) ||
                const DeepCollectionEquality().equals(
                    other.postal_address_state, postal_address_state)) &&
            (identical(other.postal_address_suburb, postal_address_suburb) ||
                const DeepCollectionEquality().equals(
                    other.postal_address_suburb, postal_address_suburb)) &&
            (identical(other.street_address_country, street_address_country) ||
                const DeepCollectionEquality().equals(
                    other.street_address_country, street_address_country)) &&
            (identical(other.street_address_line_1, street_address_line_1) ||
                const DeepCollectionEquality().equals(
                    other.street_address_line_1, street_address_line_1)) &&
            (identical(other.street_address_line_2, street_address_line_2) ||
                const DeepCollectionEquality().equals(
                    other.street_address_line_2, street_address_line_2)) &&
            (identical(other.street_address_postcode, street_address_postcode) ||
                const DeepCollectionEquality().equals(
                    other.street_address_postcode, street_address_postcode)) &&
            (identical(other.street_address_state, street_address_state) ||
                const DeepCollectionEquality().equals(
                    other.street_address_state, street_address_state)) &&
            (identical(other.street_address_suburb, street_address_suburb) ||
                const DeepCollectionEquality()
                    .equals(other.street_address_suburb, street_address_suburb)) &&
            (identical(other.tel_home, tel_home) || const DeepCollectionEquality().equals(other.tel_home, tel_home)) &&
            (identical(other.tel_mobile, tel_mobile) || const DeepCollectionEquality().equals(other.tel_mobile, tel_mobile)) &&
            (identical(other.tel_work, tel_work) || const DeepCollectionEquality().equals(other.tel_work, tel_work)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(aws_cognito_id) ^
      const DeepCollectionEquality().hash(email) ^
      const DeepCollectionEquality().hash(first_name) ^
      const DeepCollectionEquality().hash(last_name) ^
      const DeepCollectionEquality().hash(middle_names) ^
      const DeepCollectionEquality().hash(postal_address_country) ^
      const DeepCollectionEquality().hash(postal_address_line_1) ^
      const DeepCollectionEquality().hash(postal_address_line_2) ^
      const DeepCollectionEquality().hash(postal_address_postcode) ^
      const DeepCollectionEquality().hash(postal_address_state) ^
      const DeepCollectionEquality().hash(postal_address_suburb) ^
      const DeepCollectionEquality().hash(street_address_country) ^
      const DeepCollectionEquality().hash(street_address_line_1) ^
      const DeepCollectionEquality().hash(street_address_line_2) ^
      const DeepCollectionEquality().hash(street_address_postcode) ^
      const DeepCollectionEquality().hash(street_address_state) ^
      const DeepCollectionEquality().hash(street_address_suburb) ^
      const DeepCollectionEquality().hash(tel_home) ^
      const DeepCollectionEquality().hash(tel_mobile) ^
      const DeepCollectionEquality().hash(tel_work);

  @override
  _$UpdateUserModelCopyWith<_UpdateUserModel> get copyWith =>
      __$UpdateUserModelCopyWithImpl<_UpdateUserModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_UpdateUserModelToJson(this);
  }
}

abstract class _UpdateUserModel implements UpdateUserModel {
  const factory _UpdateUserModel(
      {String aws_cognito_id,
      String email,
      String first_name,
      String last_name,
      String middle_names,
      String postal_address_country,
      String postal_address_line_1,
      String postal_address_line_2,
      String postal_address_postcode,
      String postal_address_state,
      String postal_address_suburb,
      String street_address_country,
      String street_address_line_1,
      String street_address_line_2,
      String street_address_postcode,
      String street_address_state,
      String street_address_suburb,
      String tel_home,
      String tel_mobile,
      String tel_work}) = _$_UpdateUserModel;

  factory _UpdateUserModel.fromJson(Map<String, dynamic> json) =
      _$_UpdateUserModel.fromJson;

  @override
  String get aws_cognito_id;
  @override
  String get email;
  @override
  String get first_name;
  @override
  String get last_name;
  @override
  String get middle_names;
  @override
  String get postal_address_country;
  @override
  String get postal_address_line_1;
  @override
  String get postal_address_line_2;
  @override
  String get postal_address_postcode;
  @override
  String get postal_address_state;
  @override
  String get postal_address_suburb;
  @override
  String get street_address_country;
  @override
  String get street_address_line_1;
  @override
  String get street_address_line_2;
  @override
  String get street_address_postcode;
  @override
  String get street_address_state;
  @override
  String get street_address_suburb;
  @override
  String get tel_home;
  @override
  String get tel_mobile;
  @override
  String get tel_work;
  @override
  _$UpdateUserModelCopyWith<_UpdateUserModel> get copyWith;
}
