import 'package:technology/domain/repo/user_repo.dart';

class CommissionRequestModel{
  final String userId;
  final String startDate;
  final String endDate;
  CommissionEnum commissionEnum;
  CommissionRequestModel({this.userId, this.startDate, this.endDate,this.commissionEnum=CommissionEnum.FOR_DASHBOARD});
}