// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

CommissionResponse welcomeFromJson(String str) => CommissionResponse.fromJson(json.decode(str));

String welcomeToJson(CommissionResponse data) => json.encode(data.toJson());

class CommissionResponse {
  CommissionResponse({
    this.payload,
  });

  Payload payload;

  factory CommissionResponse.fromJson(Map<String, dynamic> json) => CommissionResponse(
    payload: Payload.fromJson(json["payload"]),
  );

  Map<String, dynamic> toJson() => {
    "payload": payload.toJson(),
  };
}

class Payload {
  Payload({
    this.currentProduct,
    this.currentProductMaxCommission,
    this.from,
    this.to,
    this.referralsFiltered,
    this.referralsCountDirect,
    this.referralsCountIndirect,
    this.totalCountDirect,
    this.totalCountIndirect,
    this.rangeCountDirect,
    this.rangeCountIndirect,
    this.totalSalesAmount,
    this.totalSalesAmountDirect,
    this.totalSalesAmountIndirect,
    this.rangeSalesAmountDirect,
    this.rangeSalesAmountIndirect,
    this.currentSalesLevelPrice,
    this.salesLevelTotal,
    this.currentSalesLevel,
    this.commissionLedgerResults,
    this.newDirectTotalCount,
    this.newDirectTotalSale,
    this.newDirectTotalCommission,
    this.newIndirectTotalCount,
    this.newIndirectTotalSale,
    this.newIndirectTotalCommission,
    this.newTotalCount,
    this.newTotalSale,
    this.newTotalCommission,
    this.newLifetimeSales,
  });

  List<CurrentProduct> currentProduct;
  double currentProductMaxCommission;
  DateTime from;
  DateTime to;
  List<ReferralsFiltered> referralsFiltered;
  int referralsCountDirect;
  int referralsCountIndirect;
  int totalCountDirect;
  int totalCountIndirect;
  int rangeCountDirect;
  int rangeCountIndirect;
  int totalSalesAmount;
  int totalSalesAmountDirect;
  int totalSalesAmountIndirect;
  int rangeSalesAmountDirect;
  int rangeSalesAmountIndirect;
  List<CurrentSalesLevelPrice> currentSalesLevelPrice;
  List<CurrentSalesLevel> salesLevelTotal;
  List<CurrentSalesLevel> currentSalesLevel;
  List<CommissionLedgerResult> commissionLedgerResults;
  int newDirectTotalCount;
  int newDirectTotalSale;
  int newDirectTotalCommission;
  int newIndirectTotalCount;
  int newIndirectTotalSale;
  int newIndirectTotalCommission;
  int newTotalCount;
  int newTotalSale;
  int newTotalCommission;
  int newLifetimeSales;

  factory Payload.fromJson(Map<String, dynamic> json) => Payload(
    currentProduct: List<CurrentProduct>.from(json["current_product"].map((x) => CurrentProduct.fromJson(x))),
    currentProductMaxCommission: json["current_product_max_commission"].toDouble(),
    from: DateTime.parse(json["from"]),
    to: DateTime.parse(json["to"]),
    referralsFiltered: List<ReferralsFiltered>.from(json["referrals_filtered"].map((x) => ReferralsFiltered.fromJson(x)..commissionRate=json["current_product_max_commission"].toDouble())),
    referralsCountDirect: json["referrals_count_direct"],
    referralsCountIndirect: json["referrals_count_indirect"],
    totalCountDirect: json["total_count_direct"],
    totalCountIndirect: json["total_count_indirect"],
    rangeCountDirect: json["range_count_direct"],
    rangeCountIndirect: json["range_count_indirect"],
    totalSalesAmount: json["total_sales_amount"],
    totalSalesAmountDirect: json["total_sales_amount_direct"],
    totalSalesAmountIndirect: json["total_sales_amount_indirect"],
    rangeSalesAmountDirect: json["range_sales_amount_direct"],
    rangeSalesAmountIndirect: json["range_sales_amount_indirect"],
    currentSalesLevelPrice: List<CurrentSalesLevelPrice>.from(json["current_sales_level_price"].map((x) => CurrentSalesLevelPrice.fromJson(x))),
    salesLevelTotal: List<CurrentSalesLevel>.from(json["sales_level_total"].map((x) => CurrentSalesLevel.fromJson(x))),
    currentSalesLevel: List<CurrentSalesLevel>.from(json["current_sales_level"].map((x) => CurrentSalesLevel.fromJson(x))),
    commissionLedgerResults: List<CommissionLedgerResult>.from(json["commission_ledger_results"].map((x) => CommissionLedgerResult.fromJson(x))),
    newDirectTotalCount: json["new_direct_total_count"],
    newDirectTotalSale: json["new_direct_total_sale"],
    newDirectTotalCommission: json["new_direct_total_commission"],
    newIndirectTotalCount: json["new_indirect_total_count"],
    newIndirectTotalSale: json["new_indirect_total_sale"],
    newIndirectTotalCommission: json["new_indirect_total_commission"],
    newTotalCount: json["new_total_count"],
    newTotalSale: json["new_total_sale"],
    newTotalCommission: json["new_total_commission"],
    newLifetimeSales: json["new_lifetime_sales"],
  );

  Map<String, dynamic> toJson() => {
    "current_product": List<dynamic>.from(currentProduct.map((x) => x.toJson())),
    "current_product_max_commission": currentProductMaxCommission,
    "from": from.toIso8601String(),
    "to": to.toIso8601String(),
    "referrals_filtered": List<dynamic>.from(referralsFiltered.map((x) => x.toJson())),
    "referrals_count_direct": referralsCountDirect,
    "referrals_count_indirect": referralsCountIndirect,
    "total_count_direct": totalCountDirect,
    "total_count_indirect": totalCountIndirect,
    "range_count_direct": rangeCountDirect,
    "range_count_indirect": rangeCountIndirect,
    "total_sales_amount": totalSalesAmount,
    "total_sales_amount_direct": totalSalesAmountDirect,
    "total_sales_amount_indirect": totalSalesAmountIndirect,
    "range_sales_amount_direct": rangeSalesAmountDirect,
    "range_sales_amount_indirect": rangeSalesAmountIndirect,
    "current_sales_level_price": List<dynamic>.from(currentSalesLevelPrice.map((x) => x.toJson())),
    "sales_level_total": List<dynamic>.from(salesLevelTotal.map((x) => x.toJson())),
    "current_sales_level": List<dynamic>.from(currentSalesLevel.map((x) => x.toJson())),
    "commission_ledger_results": List<dynamic>.from(commissionLedgerResults.map((x) => x.toJson())),
    "new_direct_total_count": newDirectTotalCount,
    "new_direct_total_sale": newDirectTotalSale,
    "new_direct_total_commission": newDirectTotalCommission,
    "new_indirect_total_count": newIndirectTotalCount,
    "new_indirect_total_sale": newIndirectTotalSale,
    "new_indirect_total_commission": newIndirectTotalCommission,
    "new_total_count": newTotalCount,
    "new_total_sale": newTotalSale,
    "new_total_commission": newTotalCommission,
    "new_lifetime_sales": newLifetimeSales,
  };
}

class CommissionLedgerResult {
  CommissionLedgerResult({
    this.id,
    this.createdDatetime,
    this.recordDatetime,
    this.clientId,
    this.retailPrice,
    this.salePrice,
    this.degree,
    this.commissionRate,
    this.commissionDownstream,
    this.commissionAmount,
    this.freemode,
    this.purchaseId,
    this.purchaserId,
  });

  int id;
  DateTime createdDatetime;
  DateTime recordDatetime;
  int clientId;
  int retailPrice;
  int salePrice;
  int degree;
  double commissionRate;
  dynamic commissionDownstream;
  int commissionAmount;
  dynamic freemode;
  int purchaseId;
  int purchaserId;

  factory CommissionLedgerResult.fromJson(Map<String, dynamic> json) => CommissionLedgerResult(
    id: json["id"],
    createdDatetime: DateTime.parse(json["created_datetime"]),
    recordDatetime: DateTime.parse(json["record_datetime"]),
    clientId: json["client_id"],
    retailPrice: json["retail_price"],
    salePrice: json["sale_price"],
    degree: json["degree"],
    commissionRate: json["commission_rate"].toDouble(),
    commissionDownstream: json["commission_downstream"],
    commissionAmount: json["commission_amount"],
    freemode: json["freemode"],
    purchaseId: json["purchase_id"],
    purchaserId: json["purchaser_id"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "created_datetime": createdDatetime.toIso8601String(),
    "record_datetime": recordDatetime.toIso8601String(),
    "client_id": clientId,
    "retail_price": retailPrice,
    "sale_price": salePrice,
    "degree": degree,
    "commission_rate": commissionRate,
    "commission_downstream": commissionDownstream,
    "commission_amount": commissionAmount,
    "freemode": freemode,
    "purchase_id": purchaseId,
    "purchaser_id": purchaserId,
  };
}

class CurrentProduct {
  CurrentProduct({
    this.currentProductId,
    this.id,
    this.nickname,
    this.productActive,
    this.retailPrice,
    this.badgeColor,
    this.maxCommission,
  });

  int currentProductId;
  int id;
  String nickname;
  int productActive;
  int retailPrice;
  String badgeColor;
  double maxCommission;

  factory CurrentProduct.fromJson(Map<String, dynamic> json) => CurrentProduct(
    currentProductId: json["current_product_id"],
    id: json["id"],
    nickname: json["nickname"],
    productActive: json["product_active"],
    retailPrice: json["retail_price"],
    badgeColor: json["badge_color"],
    maxCommission: json["max_commission"].toDouble(),
  );

  Map<String, dynamic> toJson() => {
    "current_product_id": currentProductId,
    "id": id,
    "nickname": nickname,
    "product_active": productActive,
    "retail_price": retailPrice,
    "badge_color": badgeColor,
    "max_commission": maxCommission,
  };
}

class CurrentSalesLevel {
  CurrentSalesLevel({
    this.id,
    this.nickname,
    this.salesBenchmark,
    this.commissionRate,
  });

  int id;
  String nickname;
  int salesBenchmark;
  double commissionRate;

  factory CurrentSalesLevel.fromJson(Map<String, dynamic> json) => CurrentSalesLevel(
    id: json["id"],
    nickname: json["nickname"],
    salesBenchmark: json["sales_benchmark"],
    commissionRate: json["commission_rate"].toDouble(),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "nickname": nickname,
    "sales_benchmark": salesBenchmark,
    "commission_rate": commissionRate,
  };
}

class CurrentSalesLevelPrice {
  CurrentSalesLevelPrice({
    this.maxSalesBenchmark,
  });

  int maxSalesBenchmark;

  factory CurrentSalesLevelPrice.fromJson(Map<String, dynamic> json) => CurrentSalesLevelPrice(
    maxSalesBenchmark: json["MAX(sales_benchmark)"],
  );

  Map<String, dynamic> toJson() => {
    "MAX(sales_benchmark)": maxSalesBenchmark,
  };
}

class ReferralsFiltered {
  ReferralsFiltered({
    this.key,
    this.id,
    this.recordCreated,
    this.referrerClientId,
    this.degree,
    this.clientId,
    this.clientNickname,
    this.productNickname,
    this.productBadgeColor,
    this.productRetailPrice,
    this.commissionRate
  });

  int key;
  int id;
  DateTime recordCreated;
  int referrerClientId;
  int degree;
  int clientId;
  String clientNickname;
  String productNickname;
  String productBadgeColor;
  int productRetailPrice=0;
  double commissionRate;
  factory ReferralsFiltered.fromJson(Map<String, dynamic> json) => ReferralsFiltered(
    key: json["key"],
    id: json["id"],
    recordCreated: DateTime.parse(json["record_created"]),
    referrerClientId: json["referrer_client_id"],
    degree: json["degree"],
    clientId: json["client_id"],
    clientNickname: json["client_nickname"],
    productNickname: json["product_nickname"],
    productBadgeColor: json["product_badge_color"],
    productRetailPrice: json["product_retail_price"],
  );

  Map<String, dynamic> toJson() => {
    "key": key,
    "id": id,
    "record_created": recordCreated.toIso8601String(),
    "referrer_client_id": referrerClientId,
    "degree": degree,
    "client_id": clientId,
    "client_nickname": clientNickname,
    "product_nickname": productNickname,
    "product_badge_color": productBadgeColor,
    "product_retail_price": productRetailPrice,
  };
}
