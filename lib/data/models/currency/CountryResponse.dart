import 'package:technology/data/models/currency/Payload.dart';

class CurrencyResponse {
    List<CurrenyModel> payload;

    CurrencyResponse({this.payload});

    factory CurrencyResponse.fromJson(Map<String, dynamic> json) {
        return CurrencyResponse(
            payload: json['payload'] != null ? (json['payload'] as List).map((i) => CurrenyModel.fromJson(i)).toList() : null,
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        if (this.payload != null) {
            data['payload'] = this.payload.map((v) => v.toJson()).toList();
        }
        return data;
    }
}