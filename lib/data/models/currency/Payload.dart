class CurrenyModel {
    int aba_routing;
    int account_number;
    int active;
    int bsb;
    int deleted;
    String full_name;
    int iban;
    int id;
    String iso_alpha_3;
    String iso_numeric;
    int list_priority;
    String short_name;
    int sort_code;
    int swift_code;

    CurrenyModel({this.aba_routing, this.account_number, this.active, this.bsb, this.deleted, this.full_name, this.iban, this.id, this.iso_alpha_3, this.iso_numeric, this.list_priority, this.short_name, this.sort_code, this.swift_code});

    factory CurrenyModel.fromJson(Map<String, dynamic> json) {
        return CurrenyModel(
            aba_routing: json['aba_routing'], 
            account_number: json['account_number'], 
            active: json['active'], 
            bsb: json['bsb'], 
            deleted: json['deleted'], 
            full_name: json['full_name'], 
            iban: json['iban'], 
            id: json['id'], 
            iso_alpha_3: json['iso_alpha_3'], 
            iso_numeric: json['iso_numeric'] != null ? json['iso_numeric'] : null,
            list_priority: json['list_priority'], 
            short_name: json['short_name'], 
            sort_code: json['sort_code'], 
            swift_code: json['swift_code'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['aba_routing'] = this.aba_routing;
        data['account_number'] = this.account_number;
        data['active'] = this.active;
        data['bsb'] = this.bsb;
        data['deleted'] = this.deleted;
        data['full_name'] = this.full_name;
        data['iban'] = this.iban;
        data['id'] = this.id;
        data['iso_alpha_3'] = this.iso_alpha_3;
        data['list_priority'] = this.list_priority;
        data['short_name'] = this.short_name;
        data['sort_code'] = this.sort_code;
        data['swift_code'] = this.swift_code;
        if (this.iso_numeric != null) {
            data['iso_numeric'] = this.iso_numeric;
        }
        return data;
    }
}