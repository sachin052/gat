
import 'dart:convert';

EmpStatus welcomeFromJson(String str) => EmpStatus.fromJson(json.decode(str));

String welcomeToJson(EmpStatus data) => json.encode(data.toJson());

class EmpStatus {
  EmpStatus({
    this.payload,
  });

  List<Status> payload;

  factory EmpStatus.fromJson(Map<String, dynamic> json) => EmpStatus(
    payload: List<Status>.from(json["payload"].map((x) => Status.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "payload": List<dynamic>.from(payload.map((x) => x.toJson())),
  };
}

class Status {
  Status({
    this.id,
    this.fileBy,
  });

  int id;
  String fileBy;

  factory Status.fromJson(Map<String, dynamic> json) => Status(
    id: json["id"],
    fileBy: json["file_by"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "file_by": fileBy,
  };
}
