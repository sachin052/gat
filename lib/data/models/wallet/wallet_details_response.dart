// To parse this JSON data, do
//
//     final walletDetailResponse = walletDetailResponseFromJson(jsonString);

import 'dart:convert';

WalletDetailResponse walletDetailResponseFromJson(String str) => WalletDetailResponse.fromJson(json.decode(str));

String walletDetailResponseToJson(WalletDetailResponse data) => json.encode(data.toJson());

class WalletDetailResponse {
  WalletDetailResponse({
    this.payload,
  });

  Payload payload;

  factory WalletDetailResponse.fromJson(Map<String, dynamic> json) => WalletDetailResponse(
    payload: json["payload"] == null ? null : Payload.fromJson(json["payload"]),
  );

  Map<String, dynamic> toJson() => {
    "payload": payload == null ? null : payload.toJson(),
  };
}

class Payload {
  Payload({
    this.id,
    this.clientId,
    this.currencyId,
    this.isoAlpha3,
  });

  int id;
  int clientId;
  int currencyId;
  String isoAlpha3;

  factory Payload.fromJson(Map<String, dynamic> json) => Payload(
    id: json["id"] == null ? null : json["id"],
    clientId: json["client_id"] == null ? null : json["client_id"],
    currencyId: json["currency_id"] == null ? null : json["currency_id"],
    isoAlpha3: json["iso_alpha_3"] == null ? null : json["iso_alpha_3"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "client_id": clientId == null ? null : clientId,
    "currency_id": currencyId == null ? null : currencyId,
    "iso_alpha_3": isoAlpha3 == null ? null : isoAlpha3,
  };
}
