import 'dart:convert';

WalletListResponse welcomeFromJson(String str) => WalletListResponse.fromJson(json.decode(str));

String welcomeToJson(WalletListResponse data) => json.encode(data.toJson());

class WalletListResponse {
  WalletListResponse({
    this.payload,
  });

  List<WalletModel> payload;

  factory WalletListResponse.fromJson(Map<String, dynamic> json) => WalletListResponse(
    payload: json["payload"] == null ? null : List<WalletModel>.from(json["payload"].map((x) => WalletModel.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "payload": payload == null ? null : List<dynamic>.from(payload.map((x) => x.toJson())),
  };
}

class WalletModel {
  WalletModel({
    this.id,
    this.clientId,
    this.currencyId,
    this.isoAlpha3,
  });

  int id;
  int clientId;
  int currencyId;
  String isoAlpha3;

  factory WalletModel.fromJson(Map<String, dynamic> json) => WalletModel(
    id: json["id"] == null ? null : json["id"],
    clientId: json["client_id"] == null ? null : json["client_id"],
    currencyId: json["currency_id"] == null ? null : json["currency_id"],
    isoAlpha3: json["iso_alpha_3"] == null ? null : json["iso_alpha_3"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "client_id": clientId == null ? null : clientId,
    "currency_id": currencyId == null ? null : currencyId,
    "iso_alpha_3": isoAlpha3 == null ? null : isoAlpha3,
  };
}