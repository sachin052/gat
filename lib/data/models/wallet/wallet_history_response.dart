// To parse this JSON data, do
//
//     final walletHistoryResponse = walletHistoryResponseFromJson(jsonString);

import 'dart:convert';

WalletHistoryResponse walletHistoryResponseFromJson(String str) => WalletHistoryResponse.fromJson(json.decode(str));

String walletHistoryResponseToJson(WalletHistoryResponse data) => json.encode(data.toJson());

class WalletHistoryResponse {
  WalletHistoryResponse({
    this.payload,
  });

  List<Payload> payload;

  factory WalletHistoryResponse.fromJson(Map<String, dynamic> json) => WalletHistoryResponse(
    payload: json["payload"] == null ? null : List<Payload>.from(json["payload"].map((x) => Payload.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "payload": payload == null ? null : List<dynamic>.from(payload.map((x) => x.toJson())),
  };
}

class Payload {
  Payload({
    this.id,
    this.walletId,
    this.effectiveDatetime,
    this.createdDatetime,
    this.commissionLedgerId,
    this.memo,
    this.debit,
    this.credit,
    this.tradingDepositId,
  });

  int id;
  int walletId;
  DateTime effectiveDatetime;
  DateTime createdDatetime;
  int commissionLedgerId;
  String memo;
  double debit;
  int credit;
  int tradingDepositId;

  factory Payload.fromJson(Map<String, dynamic> json) => Payload(
    id: json["id"] == null ? null : json["id"],
    walletId: json["wallet_id"] == null ? null : json["wallet_id"],
    effectiveDatetime: json["effective_datetime"] == null ? null : DateTime.parse(json["effective_datetime"]),
    createdDatetime: json["created_datetime"] == null ? null : DateTime.parse(json["created_datetime"]),
    commissionLedgerId: json["commission_ledger_id"] == null ? null : json["commission_ledger_id"],
    memo: json["memo"] == null ? null : json["memo"],
    debit: json["debit"] == null ? null : json["debit"].toDouble(),
    credit: json["credit"] == null ? null : json["credit"],
    tradingDepositId: json["trading_deposit_id"] == null ? null : json["trading_deposit_id"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "wallet_id": walletId == null ? null : walletId,
    "effective_datetime": effectiveDatetime == null ? null : effectiveDatetime.toIso8601String(),
    "created_datetime": createdDatetime == null ? null : createdDatetime.toIso8601String(),
    "commission_ledger_id": commissionLedgerId == null ? null : commissionLedgerId,
    "memo": memo == null ? null : memo,
    "debit": debit == null ? null : debit,
    "credit": credit == null ? null : credit,
    "trading_deposit_id": tradingDepositId == null ? null : tradingDepositId,
  };
}
