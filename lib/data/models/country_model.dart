import 'dart:convert';

CustomCountry countryFromJson(String str) => CustomCountry.fromJson(json.decode(str));

String countryToJson(CustomCountry data) => json.encode(data.toJson());

class CustomCountry {
  CustomCountry({
    this.payload,
  });

  List<Payload> payload;

  factory CustomCountry.fromJson(Map<String, dynamic> json) => CustomCountry(
    payload: List<Payload>.from(json["payload"].map((x) => Payload.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "payload": List<dynamic>.from(payload.map((x) => x.toJson())),
  };
}

class Payload {
  Payload({
    this.id,
    this.fullName,
    this.isoAlpha2,
    this.isoAlpha3,
    this.unCode,
    this.deleted,
    this.allowBankAccount,
    this.useBsb,
    this.useAccountNumber,
    this.useAba,
    this.useSwift,
    this.useIban,
    this.useSortCode,
    this.useTransitCode,
    this.useBankCode,
  });

  int id;
  String fullName;
  String isoAlpha2;
  String isoAlpha3;
  String unCode;
  dynamic deleted;
  int allowBankAccount;
  int useBsb;
  int useAccountNumber;
  int useAba;
  int useSwift;
  dynamic useIban;
  dynamic useSortCode;
  dynamic useTransitCode;
  dynamic useBankCode;

  factory Payload.fromJson(Map<String, dynamic> json) => Payload(
    id: json["id"],
    fullName: json["full_name"],
    isoAlpha2: json["iso_alpha_2"],
    isoAlpha3: json["iso_alpha_3"],
    unCode: json["un_code"],
    deleted: json["deleted"],
    allowBankAccount: json["allow_bank_account"] == null ? null : json["allow_bank_account"],
    useBsb: json["use_bsb"] == null ? null : json["use_bsb"],
    useAccountNumber: json["use_account_number"] == null ? null : json["use_account_number"],
    useAba: json["use_aba"] == null ? null : json["use_aba"],
    useSwift: json["use_swift"] == null ? null : json["use_swift"],
    useIban: json["use_iban"],
    useSortCode: json["use_sort_code"],
    useTransitCode: json["use_transit_code"],
    useBankCode: json["use_bank_code"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "full_name": fullName,
    "iso_alpha_2": isoAlpha2,
    "iso_alpha_3": isoAlpha3,
    "un_code": unCode,
    "deleted": deleted,
    "allow_bank_account": allowBankAccount == null ? null : allowBankAccount,
    "use_bsb": useBsb == null ? null : useBsb,
    "use_account_number": useAccountNumber == null ? null : useAccountNumber,
    "use_aba": useAba == null ? null : useAba,
    "use_swift": useSwift == null ? null : useSwift,
    "use_iban": useIban,
    "use_sort_code": useSortCode,
    "use_transit_code": useTransitCode,
    "use_bank_code": useBankCode,
  };
}

class CountryEntity{
  final int id;
  final String countryName;
  final String fullName;
  CountryEntity({this.id, this.countryName,this.fullName});
}