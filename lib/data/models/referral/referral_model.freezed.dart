// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'referral_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
ReferralModel _$ReferralModelFromJson(Map<String, dynamic> json) {
  return _ReferralModel.fromJson(json);
}

class _$ReferralModelTearOff {
  const _$ReferralModelTearOff();

  _ReferralModel call({int client_id, String email, String language_id}) {
    return _ReferralModel(
      client_id: client_id,
      email: email,
      language_id: language_id,
    );
  }
}

// ignore: unused_element
const $ReferralModel = _$ReferralModelTearOff();

mixin _$ReferralModel {
  int get client_id;
  String get email;
  String get language_id;

  Map<String, dynamic> toJson();
  $ReferralModelCopyWith<ReferralModel> get copyWith;
}

abstract class $ReferralModelCopyWith<$Res> {
  factory $ReferralModelCopyWith(
          ReferralModel value, $Res Function(ReferralModel) then) =
      _$ReferralModelCopyWithImpl<$Res>;
  $Res call({int client_id, String email, String language_id});
}

class _$ReferralModelCopyWithImpl<$Res>
    implements $ReferralModelCopyWith<$Res> {
  _$ReferralModelCopyWithImpl(this._value, this._then);

  final ReferralModel _value;
  // ignore: unused_field
  final $Res Function(ReferralModel) _then;

  @override
  $Res call({
    Object client_id = freezed,
    Object email = freezed,
    Object language_id = freezed,
  }) {
    return _then(_value.copyWith(
      client_id: client_id == freezed ? _value.client_id : client_id as int,
      email: email == freezed ? _value.email : email as String,
      language_id:
          language_id == freezed ? _value.language_id : language_id as String,
    ));
  }
}

abstract class _$ReferralModelCopyWith<$Res>
    implements $ReferralModelCopyWith<$Res> {
  factory _$ReferralModelCopyWith(
          _ReferralModel value, $Res Function(_ReferralModel) then) =
      __$ReferralModelCopyWithImpl<$Res>;
  @override
  $Res call({int client_id, String email, String language_id});
}

class __$ReferralModelCopyWithImpl<$Res>
    extends _$ReferralModelCopyWithImpl<$Res>
    implements _$ReferralModelCopyWith<$Res> {
  __$ReferralModelCopyWithImpl(
      _ReferralModel _value, $Res Function(_ReferralModel) _then)
      : super(_value, (v) => _then(v as _ReferralModel));

  @override
  _ReferralModel get _value => super._value as _ReferralModel;

  @override
  $Res call({
    Object client_id = freezed,
    Object email = freezed,
    Object language_id = freezed,
  }) {
    return _then(_ReferralModel(
      client_id: client_id == freezed ? _value.client_id : client_id as int,
      email: email == freezed ? _value.email : email as String,
      language_id:
          language_id == freezed ? _value.language_id : language_id as String,
    ));
  }
}

@JsonSerializable()
class _$_ReferralModel implements _ReferralModel {
  const _$_ReferralModel({this.client_id, this.email, this.language_id});

  factory _$_ReferralModel.fromJson(Map<String, dynamic> json) =>
      _$_$_ReferralModelFromJson(json);

  @override
  final int client_id;
  @override
  final String email;
  @override
  final String language_id;

  @override
  String toString() {
    return 'ReferralModel(client_id: $client_id, email: $email, language_id: $language_id)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ReferralModel &&
            (identical(other.client_id, client_id) ||
                const DeepCollectionEquality()
                    .equals(other.client_id, client_id)) &&
            (identical(other.email, email) ||
                const DeepCollectionEquality().equals(other.email, email)) &&
            (identical(other.language_id, language_id) ||
                const DeepCollectionEquality()
                    .equals(other.language_id, language_id)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(client_id) ^
      const DeepCollectionEquality().hash(email) ^
      const DeepCollectionEquality().hash(language_id);

  @override
  _$ReferralModelCopyWith<_ReferralModel> get copyWith =>
      __$ReferralModelCopyWithImpl<_ReferralModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_ReferralModelToJson(this);
  }
}

abstract class _ReferralModel implements ReferralModel {
  const factory _ReferralModel(
      {int client_id, String email, String language_id}) = _$_ReferralModel;

  factory _ReferralModel.fromJson(Map<String, dynamic> json) =
      _$_ReferralModel.fromJson;

  @override
  int get client_id;
  @override
  String get email;
  @override
  String get language_id;
  @override
  _$ReferralModelCopyWith<_ReferralModel> get copyWith;
}
