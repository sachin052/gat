import 'package:freezed_annotation/freezed_annotation.dart';
part 'referral_model.freezed.dart';

part 'referral_model.g.dart';
@freezed
abstract class ReferralModel with _$ReferralModel {
  const factory ReferralModel({
    int client_id,
    String email,
    String language_id
  }) = _ReferralModel;

  factory ReferralModel.fromJson(Map<dynamic, dynamic> json) =>
      _$ReferralModelFromJson(json);
}