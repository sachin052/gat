// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'referral_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ReferralModel _$_$_ReferralModelFromJson(Map<String, dynamic> json) {
  return _$_ReferralModel(
    client_id: json['client_id'] as int,
    email: json['email'] as String,
    language_id: json['language_id'] as String,
  );
}

Map<String, dynamic> _$_$_ReferralModelToJson(_$_ReferralModel instance) =>
    <String, dynamic>{
      'client_id': instance.client_id,
      'email': instance.email,
      'language_id': instance.language_id,
    };
