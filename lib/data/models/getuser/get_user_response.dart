// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

UserModel welcomeFromJson(String str) => UserModel.fromJson(json.decode(str));

String welcomeToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
  UserModel({
    this.payload,
  });

  UserInnerModel payload;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
    payload: UserInnerModel.fromJson(json["payload"]),
  );

  Map<String, dynamic> toJson() => {
    "payload": payload.toJson(),
  };
}

class UserInnerModel {
  UserInnerModel({
    this.userClient,
    this.defaultClientId,
    this.client,
    this.clientAccess,
  });

  UserClient userClient;
  int defaultClientId;
  Client client;
  List<ClientAccess> clientAccess;

  factory UserInnerModel.fromJson(Map<String, dynamic> json) => UserInnerModel(
    userClient: UserClient.fromJson(json["user_client"]),
    defaultClientId:json[""]!=null?json["default_client_id"]:null,
    client: Client.fromJson(json["client"]),
    clientAccess: List<ClientAccess>.from(json["client_access"].map((x) => ClientAccess.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "user_client": userClient.toJson(),
    "default_client_id": defaultClientId,
    "client": client.toJson(),
    "client_access": List<dynamic>.from(clientAccess.map((x) => x.toJson())),
  };
}

class Client {
  Client({
    this.id,
    this.userId,
    this.awsCognitoId,
    this.email,
    this.nickname,
    this.clientStatus,
    this.clientCategoryId,
    this.referringClientId,
    this.currentProductId,
    this.impactId,
    this.deliosCrmId,
    this.deliosCrmStatusId,
    this.teId,
    this.teIdDemo,
    this.teLoginDemo,
    this.teIdLive,
    this.teLoginLive,
    this.firstName,
    this.middleNames,
    this.lastName,
    this.genderId,
    this.telWork,
    this.telHome,
    this.telMobile,
    this.dateOfBirth,
    this.placeOfBirthCity,
    this.placeOfBirthCountryId,
    this.emailSecondary,
    this.wechatId,
    this.wechatDepositEnabled,
    this.streetAddressLine1,
    this.streetAddressLine2,
    this.streetAddressSuburb,
    this.streetAddressState,
    this.streetAddressPostcode,
    this.streetAddressCountry,
    this.postalAddressLine1,
    this.postalAddressLine2,
    this.postalAddressSuburb,
    this.postalAddressState,
    this.postalAddressPostcode,
    this.postalAddressCountry,
    this.productMaxCommissionOverride,
    this.commissionOverride,
    this.commissionOverrideBase,
    this.commissionOverrideExtra,
    this.preferredLanguage,
    this.wechatDeposit,
    this.usdtWalletAddress,
    this.usdtWalletFileId,
    this.accountBsb,
    this.accountCountry,
    this.accountCurrency,
    this.accountNumber,
    this.accountSortCode,
    this.accountIban,
    this.accountSwift,
    this.accountAba,
    this.activeDataId,
    this.accountBeneficiaryName,
    this.accountBankCardFileId,
    this.accountBankName,
    this.accountBankAddress,
    this.streetAddressCity,
    this.postalAddressCity,
    this.streetAddressRegion,
    this.postalAddressRegion,
    this.employmentStatusId,
    this.employer,
    this.annualIncome,
    this.netWorth,
    this.intendedDeposit,
    this.taxCountryId,
    this.tfnExemption,
    this.taxFileNumber,
    this.usCitizen,
    this.usTaxResident,
    this.knowledgeTradedLeveraged,
    this.knowledgeTradedOptions,
    this.knowledgeQualification,
    this.productsNickname,
    this.productsBadgeColor,
  });

  int id;
  String userId;
  String awsCognitoId;
  String email;
  String nickname;
  int clientStatus;
  dynamic clientCategoryId;
  dynamic referringClientId;
  int currentProductId;
  dynamic impactId;
  int deliosCrmId;
  int deliosCrmStatusId;
  String teId;
  String teIdDemo;
  String teLoginDemo;
  dynamic teIdLive;
  String teLoginLive;
  String firstName;
  String middleNames;
  String lastName;
  int genderId;
  String telWork;
  String telHome;
  String telMobile;
  DateTime dateOfBirth;
  String placeOfBirthCity;
  int placeOfBirthCountryId;
  dynamic emailSecondary;
  String wechatId;
  int wechatDepositEnabled;
  String streetAddressLine1;
  dynamic streetAddressLine2;
  dynamic streetAddressSuburb;
  dynamic streetAddressState;
  dynamic streetAddressPostcode;
  int streetAddressCountry;
  dynamic postalAddressLine1;
  dynamic postalAddressLine2;
  dynamic postalAddressSuburb;
  dynamic postalAddressState;
  dynamic postalAddressPostcode;
  dynamic postalAddressCountry;
  int productMaxCommissionOverride;
  double commissionOverride;
  double commissionOverrideBase;
  double commissionOverrideExtra;
  String preferredLanguage;
  dynamic wechatDeposit;
  String usdtWalletAddress;
  String usdtWalletFileId;
  String accountBsb;
  int accountCountry;
  int accountCurrency;
  String accountNumber;
  dynamic accountSortCode;
  dynamic accountIban;
  dynamic accountSwift;
  dynamic accountAba;
  int activeDataId;
  dynamic accountBeneficiaryName;
  String accountBankCardFileId;
  dynamic accountBankName;
  dynamic accountBankAddress;
  dynamic streetAddressCity;
  dynamic postalAddressCity;
  dynamic streetAddressRegion;
  dynamic postalAddressRegion;
  int employmentStatusId;
  String employer;
  int annualIncome;
  int netWorth;
  int intendedDeposit;
  int taxCountryId;
  int tfnExemption;
  dynamic taxFileNumber;
  int usCitizen;
  int usTaxResident;
  int knowledgeTradedLeveraged;
  int knowledgeTradedOptions;
  int knowledgeQualification;
  String productsNickname;
  String productsBadgeColor;

  factory Client.fromJson(Map<String, dynamic> json) => Client(
    id:json["id"]!=null?json["id"]:null,
    userId:json["user_id"]!=null?json["user_id"]:null,
    awsCognitoId:json["aws_cognito_id"]!=null?json["aws_cognito_id"]:null,
    email:json["email"]!=null?json["email"]:null,
    nickname:json["nickname"]!=null?json["nickname"]:null,
    clientStatus:json["client_status"]!=null?json["client_status"]:null,
    clientCategoryId:json["client_category_id"]!=null?json["client_category_id"]:null,
    referringClientId:json["referring_client_id"]!=null?json["referring_client_id"]:null,
    currentProductId:json["current_product_id"]!=null?json["current_product_id"]:null,
    impactId:json["impact_id"]!=null?json["impact_id"]:null,
    deliosCrmId:json["delios_crm_id"]!=null?json["delios_crm_id"]:null,
    deliosCrmStatusId:json["delios_crm_status_id"]!=null?json["delios_crm_status_id"]:null,
    teId:json["te_id"]!=null?json["te_id"]:null,
    teIdDemo:json["te_id_demo"]!=null?json["te_id_demo"]:null,
    teLoginDemo:json["te_login_demo"]!=null?json["te_login_demo"]:null,
    teIdLive:json["te_id_ive"]!=null?json["te_id_live"]:null,
    teLoginLive:json["te_login_live"]!=null?json["te_login_live"]:null,
    firstName:json["first_name"]!=null?json["first_name"]:null,
    middleNames:json[""]!=null?json["middle_names"]:null,
    lastName:json["last_name"]!=null?json["last_name"]:null,
    genderId:json["gender_id"]!=null?json["gender_id"]:null,
    telWork:json["tel_work"]!=null?json["tel_work"]:null,
    telHome:json["tel_home"]!=null?json["tel_home"]:null,
    telMobile:json["tel_mobile"]!=null?json["tel_mobile"]:null,
    dateOfBirth:json[""]!=null?DateTime.parse(json["date_of_birth"]):null,
    placeOfBirthCity:json["place_of_birth_city"]!=null?json["place_of_birth_city"]:null,
    placeOfBirthCountryId:json["place_of_birth_country_id"]!=null?json["place_of_birth_country_id"]:null,
    emailSecondary:json["email_secondary"]!=null?json["email_secondary"]:null,
    wechatId:json["wechat_id"]!=null?json["wechat_id"]:null,
    wechatDepositEnabled:json["wechat_deposit_enabled"]!=null?json["wechat_deposit_enabled"]:null,
    streetAddressLine1:json["street_address_line_1"]!=null?json["street_address_line_1"]:null,
    streetAddressLine2:json["street_address_line_2"]!=null?json["street_address_line_2"]:null,
    streetAddressSuburb:json["street_address_suburb"]!=null?json["street_address_suburb"]:null,
    streetAddressState:json["street_address_state"]!=null?json["street_address_state"]:null,
    streetAddressPostcode:json["street_address_postcode"]!=null?json["street_address_postcode"]:null,
    streetAddressCountry:json["street_address_country"]!=null?json["street_address_country"]:null,
    postalAddressLine1:json["postal_address_line_1"]!=null?json["postal_address_line_1"]:null,
    postalAddressLine2:json["postal_address_line_2"]!=null?json["postal_address_line_2"]:null,
    postalAddressSuburb:json["postal_address_suburb"]!=null?json["postal_address_suburb"]:null,
    postalAddressState:json["postal_address_state"]!=null?json["postal_address_state"]:null,
    postalAddressPostcode:json["postal_address_postcode"]!=null?json["postal_address_postcode"]:null,
    postalAddressCountry:json["postal_address_country"]!=null?json["postal_address_country"]:null,
    productMaxCommissionOverride:json["product_max_commission_override"]!=null?json["product_max_commission_override"]:null,
    commissionOverride:json["commission_override"]!=null?json["commission_override"].toDouble():null,
    commissionOverrideBase:json["commission_override_base"]!=null?json["commission_override_base"].toDouble():null,
    commissionOverrideExtra:json["commission_override_extra"]!=null?json["commission_override_extra"].toDouble():null,
    preferredLanguage:json["preferred_language"]!=null?json["preferred_language"]:null,
    wechatDeposit:json["wechat_deposit"]!=null?json["wechat_deposit"]:null,
    usdtWalletAddress:json["usdt_wallet_address"]!=null?json["usdt_wallet_address"]:null,
    usdtWalletFileId:json["usdt_wallet_file_id"]!=null?json["usdt_wallet_file_id"]:null,
    accountBsb:json["account_bsb"]!=null?json["account_bsb"]:null,
    accountCountry:json["account_country"]!=null?json["account_country"]:null,
    accountCurrency:json["account_currency"]!=null?json["account_currency"]:null,
    accountNumber:json["account_number"]!=null?json["account_number"]:null,
    accountSortCode:json["account_sort_code"]!=null?json["account_sort_code"]:null,
    accountIban:json["account_iban"]!=null?json["account_iban"]:null,
    accountSwift:json["account_swift"]!=null?json["account_swift"]:null,
    accountAba:json["account_aba"]!=null?json["account_aba"]:null,
    activeDataId:json["active_data_id"]!=null?json["active_data_id"]:null,
    accountBeneficiaryName:json["account_beneficiary_name"]!=null?json["account_beneficiary_name"]:null,
    accountBankCardFileId:json["account_bank_card_file_id"]!=null?json["account_bank_card_file_id"]:null,
    accountBankName:json["account_bank_name"]!=null?json["account_bank_name"]:null,
    accountBankAddress:json["account_bank_address"]!=null?json["account_bank_address"]:null,
    streetAddressCity:json["street_address_city"]!=null?json["street_address_city"]:null,
    postalAddressCity:json["postal_address_city"]!=null?json["postal_address_city"]:null,
    streetAddressRegion:json["street_address_region"]!=null?json["street_address_region"]:null,
    postalAddressRegion:json["postal_address_region"]!=null?json["postal_address_region"]:null,
    employmentStatusId:json["employment_status_id"]!=null?json["employment_status_id"]:null,
    employer:json["employer"]!=null?json["employer"]:null,
    annualIncome:json["annual_income"]!=null?json["annual_income"]:null,
    netWorth:json["net_worth"]!=null?json["net_worth"]:null,
    intendedDeposit:json["intended_deposit"]!=null?json["intended_deposit"]:null,
    taxCountryId:json["tax_country_id"]!=null?json["tax_country_id"]:null,
    tfnExemption:json["tfn_exemptiontfn_exemption"]!=null?json["tfn_exemption"]:null,
    taxFileNumber:json["tax_file_number"]!=null?json["tax_file_number"]:null,
    usCitizen:json["us_citizen"]!=null?json["us_citizen"]:null,
    usTaxResident:json["us_tax_resident"]!=null?json["us_tax_resident"]:null,
    knowledgeTradedLeveraged:json["knowledge_traded_leveraged"]!=null?json["knowledge_traded_leveraged"]:null,
    knowledgeTradedOptions:json["knowledge_traded_options"]!=null?json["knowledge_traded_options"]:null,
    knowledgeQualification:json["knowledge_qualification"]!=null?json["knowledge_qualification"]:null,
    productsNickname:json["products_nickname"]!=null?json["products_nickname"]:null,
    productsBadgeColor:json["products_badge_color"]!=null?json["products_badge_color"]:null,
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "user_id": userId,
    "aws_cognito_id": awsCognitoId,
    "email": email,
    "nickname": nickname,
    "client_status": clientStatus,
    "client_category_id": clientCategoryId,
    "referring_client_id": referringClientId,
    "current_product_id": currentProductId,
    "impact_id": impactId,
    "delios_crm_id": deliosCrmId,
    "delios_crm_status_id": deliosCrmStatusId,
    "te_id": teId,
    "te_id_demo": teIdDemo,
    "te_login_demo": teLoginDemo,
    "te_id_live": teIdLive,
    "te_login_live": teLoginLive,
    "first_name": firstName,
    "middle_names": middleNames,
    "last_name": lastName,
    "gender_id": genderId,
    "tel_work": telWork,
    "tel_home": telHome,
    "tel_mobile": telMobile,
    "date_of_birth": dateOfBirth?.toIso8601String(),
    "place_of_birth_city": placeOfBirthCity,
    "place_of_birth_country_id": placeOfBirthCountryId,
    "email_secondary": emailSecondary,
    "wechat_id": wechatId,
    "wechat_deposit_enabled": wechatDepositEnabled,
    "street_address_line_1": streetAddressLine1,
    "street_address_line_2": streetAddressLine2,
    "street_address_suburb": streetAddressSuburb,
    "street_address_state": streetAddressState,
    "street_address_postcode": streetAddressPostcode,
    "street_address_country": streetAddressCountry,
    "postal_address_line_1": postalAddressLine1,
    "postal_address_line_2": postalAddressLine2,
    "postal_address_suburb": postalAddressSuburb,
    "postal_address_state": postalAddressState,
    "postal_address_postcode": postalAddressPostcode,
    "postal_address_country": postalAddressCountry,
    "product_max_commission_override": productMaxCommissionOverride,
    "commission_override": commissionOverride,
    "commission_override_base": commissionOverrideBase,
    "commission_override_extra": commissionOverrideExtra,
    "preferred_language": preferredLanguage,
    "wechat_deposit": wechatDeposit,
    "usdt_wallet_address": usdtWalletAddress,
    "usdt_wallet_file_id": usdtWalletFileId,
    "account_bsb": accountBsb,
    "account_country": accountCountry,
    "account_currency": accountCurrency,
    "account_number": accountNumber,
    "account_sort_code": accountSortCode,
    "account_iban": accountIban,
    "account_swift": accountSwift,
    "account_aba": accountAba,
    "active_data_id": activeDataId,
    "account_beneficiary_name": accountBeneficiaryName,
    "account_bank_card_file_id": accountBankCardFileId,
    "account_bank_name": accountBankName,
    "account_bank_address": accountBankAddress,
    "street_address_city": streetAddressCity,
    "postal_address_city": postalAddressCity,
    "street_address_region": streetAddressRegion,
    "postal_address_region": postalAddressRegion,
    "employment_status_id": employmentStatusId,
    "employer": employer,
    "annual_income": annualIncome,
    "net_worth": netWorth,
    "intended_deposit": intendedDeposit,
    "tax_country_id": taxCountryId,
    "tfn_exemption": tfnExemption,
    "tax_file_number": taxFileNumber,
    "us_citizen": usCitizen,
    "us_tax_resident": usTaxResident,
    "knowledge_traded_leveraged": knowledgeTradedLeveraged,
    "knowledge_traded_options": knowledgeTradedOptions,
    "knowledge_qualification": knowledgeQualification,
    "products_nickname": productsNickname,
    "products_badge_color": productsBadgeColor,
  };
}

class ClientAccess {
  ClientAccess({
    this.clientId,
  });

  int clientId;

  factory ClientAccess.fromJson(Map<String, dynamic> json) => ClientAccess(
    clientId:json[""]!=null?json["client_id"]:null,
  );

  Map<String, dynamic> toJson() => {
    "client_id": clientId,
  };
}

class UserClient {
  UserClient({
    this.id,
    this.awsCognitoId,
    this.email,
  });

  int id;
  String awsCognitoId;
  String email;

  factory UserClient.fromJson(Map<String, dynamic> json) => UserClient(
    id:json["id"]!=null?json["id"]:null,
    awsCognitoId:json["aws_cognito_id"]!=null?json["aws_cognito_id"]:null,
    email:json["email"]!=null?json["email"]:null,
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "aws_cognito_id": awsCognitoId,
    "email": email,
  };
}
