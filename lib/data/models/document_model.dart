// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

// Welcome welcomeFromJson(String str) => Welcome.fromJson(json.decode(str));
//
// String welcomeToJson(Welcome data) => json.encode(data.toJson());

class DocumentModel {
  DocumentModel({
    this.payload,
  });

  List<DocumentItem> payload;

  factory DocumentModel.fromJson(Map<String, dynamic> json) => DocumentModel(
    payload: List<DocumentItem>.from(json["payload"].map((x) => DocumentItem.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "payload": List<dynamic>.from(payload.map((x) => x.toJson())),
  };
}

class DocumentItem {
  DocumentItem({
    this.id,
    this.countryId,
    this.nickname,
    this.documentDetail,
    this.detail1Label,
    this.detail2Label,
    this.detail3Label,
    this.hasExpiry,
    this.uploadFront,
    this.uploadBack,
  });

  int id;
  int countryId;
  String nickname;
  String documentDetail;
  String detail1Label;
  String detail2Label;
  dynamic detail3Label;
  int hasExpiry;
  int uploadFront;
  int uploadBack;

  factory DocumentItem.fromJson(Map<String, dynamic> json) => DocumentItem(
    id: json["id"],
    countryId: json["country_id"],
    nickname: json["nickname"],
    documentDetail: json["document_detail"] == null ? null : json["document_detail"],
    detail1Label: json["detail1_label"] == null ? null : json["detail1_label"],
    detail2Label: json["detail2_label"] == null ? null : json["detail2_label"],
    detail3Label: json["detail3_label"],
    hasExpiry: json["has_expiry"] == null ? null : json["has_expiry"],
    uploadFront: json["upload_front"],
    uploadBack: json["upload_back"] == null ? null : json["upload_back"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "country_id": countryId,
    "nickname": nickname,
    "document_detail": documentDetail == null ? null : documentDetail,
    "detail1_label": detail1Label == null ? null : detail1Label,
    "detail2_label": detail2Label == null ? null : detail2Label,
    "detail3_label": detail3Label,
    "has_expiry": hasExpiry == null ? null : hasExpiry,
    "upload_front": uploadFront,
    "upload_back": uploadBack == null ? null : uploadBack,
  };
}
