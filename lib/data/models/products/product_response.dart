import 'dart:convert';

ProductResponse modelFromJson(String str) => ProductResponse.fromJson(json.decode(str));

String modelToJson(ProductResponse data) => json.encode(data.toJson());

class ProductResponse {
  ProductResponse({
    this.payload,
  });

  List<Product> payload;

  factory ProductResponse.fromJson(Map<String, dynamic> json) => ProductResponse(
    payload: List<Product>.from(json["payload"].map((x) => Product.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "payload": List<dynamic>.from(payload.map((x) => x.toJson())),
  };
}

class Product {
  Product({
    this.id,
    this.nickname,
    this.productActive,
    this.retailPrice,
    this.badgeColor,
    this.maxCommission,
  });

  int id;
  String nickname;
  int productActive;
  int retailPrice;
  String badgeColor;
  double maxCommission;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
    id: json["id"],
    nickname: json["nickname"],
    productActive: json["product_active"],
    retailPrice: json["retail_price"],
    badgeColor: json["badge_color"],
    maxCommission: json["max_commission"].toDouble(),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "nickname": nickname,
    "product_active": productActive,
    "retail_price": retailPrice,
    "badge_color": badgeColor,
    "max_commission": maxCommission,
  };
}
