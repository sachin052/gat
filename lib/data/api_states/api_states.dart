import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meta/meta.dart';

part 'api_states.freezed.dart';

@freezed
abstract class Failure with _$Failure {
  const factory Failure.networkError(String errorMessage) = _NetworkError;
  const factory Failure.serverError(String errorMessage) = _ServerError;
//  const factory Failure.validationError(String errorMessage) = _ValidationError;
}

